//
//  TaxiCarTypes.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 22/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class TaxiCarTypes: UICollectionViewCell {
    
    @IBOutlet weak var carTypeName: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var lowerViewUpperConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        self.backGroundView.backgroundColor = COLOR.popUpColor
        carTypeName.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        carTypeName.textColor = COLOR.ADD_NEW_CARD_HEADING_LABEL
        imageWidth.constant = SCREEN_SIZE.width * 0.137
        imageHeight.constant = SCREEN_SIZE.width * 0.137
        carImage.layer.cornerRadius = (SCREEN_SIZE.width * 0.137)/2
        carImage.clipsToBounds = true
        timeLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        timeLabel.textColor = COLOR.ADD_NEW_CARD_HEADING_LABEL
        timeLabel.text = ""
        if SCREEN_SIZE.height <= 480 {
            lowerViewUpperConstraint.constant = -1.5
        }else{
            lowerViewUpperConstraint.constant = 1
        }
        
    }
    
    
    
    func changeState(to:Bool){
        self.carImage.layer.borderWidth = 2.7
        switch to {
        case true:
            self.carImage.layer.borderColor = COLOR.THEME_FOREGROUND_COLOR.cgColor
        default:
             self.carImage.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    
}
