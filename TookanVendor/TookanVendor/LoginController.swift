//
//  LoginController.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 16/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import IQKeyboardManager

class LoginController: UIViewController, NavigationDelegate, CustomFieldDelegate, ErrorDelegate {

    let topMarginOfTextField:CGFloat = 20.0
    let buttonHeight:CGFloat = 50.0
    var navigationBar:NavigationView!
    var titleView:TitleView!
    var emailField:CustomTextField!
    var passwordField:CustomTextField!
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    var errorMessageView:ErrorView!
    
    @IBOutlet var signinButton: UIButton!
    @IBOutlet var forgotPasswordButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.setNavigationBar()
        self.setTitleView()
        self.setTextFields()
        self.setSignInButton()
        self.setForgotButton()
        /*---------- Tap Gesture ------------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        self.animateTextField()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        navigationBar = UINib(nibName: NIB_NAME.navigation, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NavigationView
        navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigationBar.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        navigationBar.delegate = self
        self.view.addSubview(navigationBar)
    }
    
    //MARK: NAVIGATION DELEGATE METHODS
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:TITLE VIEW
    func setTitleView() {
        titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.view.addSubview(titleView)
        self.titleView.setTitle(title: TEXT.SIGN_IN + " " + TEXT.TO, boldTitle: APP_NAME)
    }
    
    //MARK: SET FIELDS
    func setTextFields() {
        self.setEmailField()
        self.setPasswordField()
    }
    
    func setEmailField() {
        self.emailField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.emailField.delegate = self
        self.emailField.fieldType = FIELD_TYPE.email
        self.emailField.frame = CGRect(x: 0, y: HEIGHT.navigationHeight + HEIGHT.titleHeight + topMarginOfTextField, width: self.view.frame.width, height: HEIGHT.textFieldHeight)
        self.view.addSubview(self.emailField)
        self.emailField.setImageForDifferentStates(inactive: IMAGE.iconEmailInActive, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
        self.emailField.alpha = 0.0
        self.emailField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.emailField.textField.keyboardType = UIKeyboardType.emailAddress
        self.emailField.textField.returnKeyType = UIReturnKeyType.next
    }
    
    func setPasswordField() {
        self.passwordField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.passwordField.delegate = self
        self.passwordField.fieldType = FIELD_TYPE.password
        self.passwordField.frame = CGRect(x: 0, y: self.emailField.frame.origin.y + self.emailField.frame.height, width: self.view.frame.width, height: HEIGHT.textFieldHeight)
        self.view.addSubview(self.passwordField)
        self.passwordField.alpha = 0.0
        self.passwordField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.passwordField.setImageForDifferentStates(inactive: IMAGE.iconPasswordInactive, placeholderText: TEXT.YOUR_PASSWORD, isPasswordField: true, unlock: IMAGE.iconPasswordActiveShown)
        self.passwordField.textField.keyboardType = UIKeyboardType.default
        self.passwordField.textField.returnKeyType = UIReturnKeyType.done
    }
    
    func setSignInButton() {
        self.signinButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        self.signinButton.layer.cornerRadius = buttonHeight / 2
        self.signinButton.setShadow()
        self.signinButton.setAttributedTitle(Singleton.sharedInstance.setAttributeTitleWithOrWithoutArrow(title: TEXT.SIGN_IN, isArrow: true, yourImage: IMAGE.iconSigninSigninpage), for: .normal)
    }
    
    func setForgotButton() {
        self.forgotPasswordButton.setTitle(TEXT.FORGOT_PASSWORD, for: UIControlState.normal)
        self.forgotPasswordButton.titleLabel?.font = UIFont(name: FONT.ultraLight, size: 14.0)
        self.forgotPasswordButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
    }
    
    //MARK: CUSTOM FIELD DELEGATE METHODS
    func customFieldShouldReturn(fieldType:FIELD_TYPE) {
        print(fieldType)
        switch fieldType {
        case FIELD_TYPE.email:
            self.passwordField.textField.becomeFirstResponder()
            break
        case FIELD_TYPE.password:
            break
        default:
            break
        }
    }
    
    //MARK: SIGN IN ACTION
    @IBAction func signinAction(_ sender: AnyObject) {
        self.checkValidation()
    }
    
    //MARK: VALIDATION CHECK
    func checkValidation() {
        let email = (self.emailField.textField.text?.trimText)!
        let password = (self.passwordField.textField.text?.trimText)!
        guard Singleton.sharedInstance.validateEmail(email) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL)
            return
        }
        
        guard Singleton.sharedInstance.validPassword(password:password) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD)
            return
        }
        
        self.serverRequest(email: email, password: password)
    }
    
    
    //MARK: SERVER REQUEST
    func serverRequest(email:String, password:String) {
        self.view.endEditing(true)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.loginRequest(email, password: password) { (succeeded, response) in
            print(response)
            DispatchQueue.main.async(execute: { () -> Void in
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    if succeeded == true {
                        Singleton.sharedInstance.isSignedIn = true 
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                if let data = response["data"] as? [String:Any] {
                                    if let details = data["vendor_details"] as? [String:Any] {
                                        if let formDetails = data["formSettings"] as? [Any] {
                                        let vendorDetails = VendorDetails(json: details,formData: formDetails)
                                        Singleton.sharedInstance.vendorDetailInfo = vendorDetails
                                        UserDefaults.standard.set(vendorDetails.appAccessToken, forKey: USER_DEFAULT.accessToken)
                                        UserDefaults.standard.set((vendorDetails.firstName ?? "") + " " + (vendorDetails.lastName ?? ""), forKey: USER_DEFAULT.vendorName)
                                        UserDefaults.standard.set(vendorDetails.email, forKey: USER_DEFAULT.emailAddress)
                                        UserDefaults.standard.set(vendorDetails.company, forKey: USER_DEFAULT.companyName)
                                        UserDefaults.standard.set(vendorDetails.address, forKey: USER_DEFAULT.address)
                                        UserDefaults.standard.set(vendorDetails.phoneNo, forKey: USER_DEFAULT.phoneNumber)
                                    }
                                    }
                                    if let typeCategories = data["categories"] as? [Any]{
                                        Singleton.sharedInstance.typeCategories.removeAll()
                                        print(typeCategories)
                                        for i in typeCategories{
                                            if let data = i as? [String:Any]{
                                                Singleton.sharedInstance.typeCategories.append(CarTypeModal(json:data))
                                            }
                                        }
                                    }
                                    if let formDetails = data["formSettings"] as? [AnyObject] {
                                        if let details = formDetails[0] as? [String:Any] {
                                            Singleton.sharedInstance.formDetailsInfo = FormDetails(json: details)
                                        }
                                    }
                                    if let userOptions = data["userOptions"] as? [String:Any] {
                                        Singleton.sharedInstance.customFieldOptionsPickup = userOptions
//                                        if let items = userOptions["items"] as? [Any] {
//                                            for item in items {
//                                                let customField = CustomFieldDetails(json: item as! [String : Any])
//                                                Singleton.sharedInstance.customFieldDetails[customField.data_type!] = customField
//                                                //Singleton.sharedInstance.customFieldDetails.append(customField)
//                                            }
//                                        }
                                    }
                                    if let userOptions = data["deliveryOptions"] as? [String:Any] {
                                        Singleton.sharedInstance.customFieldOptionsDelivery = userOptions
                                    }
                                    
                                    Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
                                        ActivityIndicator.sharedInstance.showActivityIndicator()
                                    }, stopLoaderAnimation: {
                                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                                    })
                                    
//                                    if Singleton.sharedInstance.vendorDetailInfo.is_phone_verified == "0" && Singleton.sharedInstance.formDetailsInfo.isOtpRequired == "1" {
//                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.otpScreen) as? OtpScreenViewController
//                                        self.navigationController?.pushViewController(vc!, animated: true   )
//                                    }else {
//                                        if Singleton.sharedInstance.checkForPendingAmount(owener: self) == false   {
//                                            
//                                            let param = [
//                                                "access_token" : Singleton.sharedInstance.vendorDetailInfo.appAccessToken!,
//                                                "device_token" : APIManager.sharedInstance.getDeviceToken(),
//                                                "app_version"  : APP_VERSION,
//                                                "app_device_type" : APP_DEVICE_TYPE,
//                                                "app_access_token" : Singleton.sharedInstance.vendorDetailInfo.appAccessToken!,
//                                                "user_id" : Singleton.sharedInstance.vendorDetailInfo.userId!
//                                            ] as [String : Any]
//                                            print(param)
//                                            ActivityIndicator.sharedInstance.showActivityIndicator()
//                                            APIManager.sharedInstance.serverCall(apiName: "get_super_categories", params: param as [String : AnyObject]?, httpMethod: "POST", receivedResponse: { (isSuccess, Result) in
//                                                ActivityIndicator.sharedInstance.hideActivityIndicator()
//                                                print(Result)
//                                            })
//                                            
////                                            Singleton.sharedInstance.pushToHomeScreen()
//                                        }
//                                    }
                                }
                                break
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                break
                                
                            default:
                                Singleton.sharedInstance.showAlert(response["message"] as! String!)
                                break
                            }
                        }
                    } else {
                        self.showErrorMessage(error: response["message"] as! String!)
                }
                }
            })
        }
    }

    
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: true)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        self.errorMessageView.removeFromSuperview()
        self.errorMessageView = nil
    }
    
    //MARK: FORGOT ACTION
    @IBAction func forgotPasswordAction(_ sender: AnyObject) {
    }
    
    func animateTextField() {
        Singleton.sharedInstance.animateToIdentity(view: self.emailField, delayTime: 0.2)
        Singleton.sharedInstance.animateToIdentity(view: self.passwordField, delayTime: 0.3)
        self.emailField.textField.becomeFirstResponder()
    }
    
    //MARK: BACKGROUND TOUCH
    func backgroundTouch() {
        self.view.endEditing(true)
    }
    
    //MARK: Keyboard Functions
    func keyboardWillShow(_ notification : Foundation.Notification){
        //let info: NSDictionary = (notification as NSNotification).userInfo!
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue//info.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        keyboardSize = value.cgRectValue.size
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        keyboardSize = CGSize(width: 0.0, height: 0.0)
    }
}
