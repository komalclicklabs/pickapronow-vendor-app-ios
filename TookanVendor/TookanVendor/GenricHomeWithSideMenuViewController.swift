//
//  GenricHomeWithSideMenuViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 28/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class GenricHomeWithSideMenuViewController: UIViewController {
  
    @IBOutlet weak var mainViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var overLay: UIView!
    @IBOutlet weak var showOrHideMenuButton: UIButton!
    @IBOutlet weak var homeView: UIView!

    var type : HomeType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      showPopUp()
      showOrHideMenuButton.setImage(#imageLiteral(resourceName: "iconMenu").renderWithAlwaysTemplateMode(), for: UIControlState.normal)
      showOrHideMenuButton.tintColor = COLOR.SPLASH_TEXT_COLOR
      if Singleton.sharedInstance.WorkFlowFormDetailsOptions.count == 1{
         if Singleton.sharedInstance.formDetailsInfo.verticalType == .taxi {
            self.type = HomeType.Taxi
         } else {
            self.type = HomeType.deliveryOrAppointments
         }
         
         if Singleton.sharedInstance.WorkFlowFormDetailsOptions[0].isNLevel
         {
            self.type = HomeType.nLevel
         }
      }else{
         self.type = HomeType.serviceOptions
         
      }
      
      self.addChild(withType: self.type!)
  }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      UIApplication.shared.statusBarStyle = .default
   }
  
    @IBAction func showOrHideMenu(_ sender: Any) {
      
        if mainViewTrailingConstraint.constant == 0{
            mainViewTrailingConstraint.constant = -(SCREEN_SIZE.width)
            overLay.isHidden = false
        }else{
            mainViewTrailingConstraint.constant = 0
             overLay.isHidden = true
        }
        
        UIView.animate(withDuration:0.3,animations:{
            self.view.layoutIfNeeded()
        })
      
    }

    
    @IBAction func tapGestureAction(_ sender: Any) {
         showOrHideMenu("")
    }
    
    
    @IBAction func rightGestureRecogniser(_ sender: UISwipeGestureRecognizer) {
        showOrHideMenu("")
    }

    
    func addChild(withType:HomeType) {
      
        switch withType {
        case .deliveryOrAppointments:
            let vc = UIViewController.findIn(storyboard: .nLevel, withIdentifier: "checkoutScreen") as! CheckoutViewController
            vc.showBackButton = false
            vc.view.frame = self.homeView.frame
            showOrHideMenuButton.tintColor = COLOR.SPLASH_TEXT_COLOR
            showOrHideMenuButton.center.y = HEIGHT.navigationHeight/2
            self.addChildCOntroller(controller: vc)
            print("del")
            
        case .Taxi:
            let vc = UIViewController.findIn(storyboard: .taxi, withIdentifier: STORYBOARD_ID.taxiHome) as? TaxiHomeScreenViewController
            vc?.view.frame = self.homeView.frame
             self.addChildCOntroller(controller: vc!)
            print("taxi")
            
        case .serviceOptions:
            let vc = UIViewController.findIn(storyboard: .main, withIdentifier: STORYBOARD_ID.servicesOptions) as? ServicesOptionsViewController
            vc?.view.frame = self.homeView.frame
            self.addChildCOntroller(controller: vc!)
         
        case .nLevel:

         let formDetails = Singleton.sharedInstance.WorkFlowFormDetailsOptions[0]
         formDetails.nLevelController?.getFirstVC { [weak self] (success, vc) in
            if let NLevelVc = vc as? NLevelParentViewController {
               NLevelVc.isBackButtonHidden = true
               NLevelVc.view.frame = (self?.homeView.frame)!

               self?.addChildCOntroller(controller: NLevelVc)
               self?.view.bringSubview(toFront: self!.showOrHideMenuButton)
               self?.showOrHideMenuButton.tintColor = COLOR.SPLASH_TEXT_COLOR
               self?.showOrHideMenuButton.center.y = HEIGHT.navigationHeight/2
            } else {
               self?.addChild(withType: .serviceOptions)
            }
        }
      }
    }
    
    
    func addChildCOntroller(controller:UIViewController) {
        addChildViewController(controller)
        self.homeView?.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
        homeView.bringSubview(toFront: overLay)
        homeView.bringSubview(toFront: showOrHideMenuButton )
        overLay.isHidden = true
    }
    
    func hideSideMenuView(){
    showOrHideMenu("")
    }
    
    func showPopUp(){
        if isWhiteLabel == false   &&  Vendor.current?.isFirstSignup == true && Vendor.current?.welcomePopUpMessage !=  ""   {
            InfoPopUpView.loadWith(info: InfoPopUpView.InfoPopUp(heading: "Hello \(Vendor.current!.firstName!)!", subHeading: nil, description: (Vendor.current?.welcomePopUpMessage)!, image: nil, closeButtontitle: "Close", type: InfoPopUpView.PopUpType.normal))
            Vendor.current?.isFirstSignup == false
        }
    }
    
}


enum HomeType {
    case Taxi
    case deliveryOrAppointments
    case serviceOptions
   case nLevel
}
