//
//  CreateTaskModel.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 14/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class CreateTaskModel: NSObject {

    var createTaskDetail = CreateTaskDetails(json: [:])
    var pickupDeliveryFlag:Int!
    var pickupMetadata = [Any]()
    var metaData = [Any]()

    func setupCustomFieldsInArray(cellArray:[CustomFieldTypes]) -> [CustomFieldTypes] {
        var cellInfoArray = cellArray
        var customMetaData = [Any]()
        if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
            customMetaData = Singleton.sharedInstance.createTaskDetail.pickupMetadata
        } else {
            customMetaData = Singleton.sharedInstance.createTaskDetail.metaData
        }
        
        
        for i in 0..<customMetaData.count {
            let customField = customMetaData[i] as! CustomFieldDetails
            print(customField)
            switch customField.dataType {
            case .BarCode:
                cellInfoArray.append(.BarCode)
                break
            case .checkBox:
                cellInfoArray.append(.checkBox)
                break
            case .checklist:
                break
            case .date:
                cellInfoArray.append(.date)
                break
            case .dateFuture:
                cellInfoArray.append(.dateFuture)
                break
            case .datePast:
                cellInfoArray.append(.datePast)
                break
            case .dateTime:
                cellInfoArray.append(.dateTime)
                break
            case .dateTimeFuture:
                cellInfoArray.append(.dateTimeFuture)
                break
            case .dateTimePast:
                cellInfoArray.append(.dateTimePast)
                break
            case .DropDown:
                cellInfoArray.append(.DropDown)
                break
            case .email:
                cellInfoArray.append(.email)
                break
            case .image:
                cellInfoArray.append(.image)
                break
            case .Number:
                cellInfoArray.append(.Number)
                break
            case .table:
                break
            case .phonenumber:
                cellInfoArray.append(.phonenumber)
                break
            case .Text:
                cellInfoArray.append(.Text)
                break
            case .url:
                cellInfoArray.append(.url)
                break
            default:
                break
            }
        }
        return cellInfoArray
    }
    
    func saveCurrentTaskDetails() {
        self.createTaskDetail.customerAddress = Singleton.sharedInstance.createTaskDetail.customerAddress
        self.createTaskDetail.customerEmail = Singleton.sharedInstance.createTaskDetail.customerEmail
        self.createTaskDetail.customerPhone = Singleton.sharedInstance.createTaskDetail.customerPhone
        self.createTaskDetail.customerUsername = Singleton.sharedInstance.createTaskDetail.customerUsername
        self.createTaskDetail.latitude = Singleton.sharedInstance.createTaskDetail.latitude
        self.createTaskDetail.longitude = Singleton.sharedInstance.createTaskDetail.longitude
        self.createTaskDetail.jobDeliveryDatetime = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime
        self.createTaskDetail.hasDelivery = Singleton.sharedInstance.createTaskDetail.hasDelivery
        self.createTaskDetail.hasPickup = Singleton.sharedInstance.createTaskDetail.hasPickup
        self.createTaskDetail.jobDescription = Singleton.sharedInstance.createTaskDetail.jobDescription
        self.createTaskDetail.jobPickupAddress = Singleton.sharedInstance.createTaskDetail.jobPickupAddress
        self.createTaskDetail.jobPickupEmail = Singleton.sharedInstance.createTaskDetail.jobPickupEmail
        self.createTaskDetail.jobPickupPhone = Singleton.sharedInstance.createTaskDetail.jobPickupPhone
        self.createTaskDetail.jobPickupDateTime = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime
        self.createTaskDetail.jobPickupLatitude = Singleton.sharedInstance.createTaskDetail.jobPickupLatitude
        self.createTaskDetail.jobPickupLongitude = Singleton.sharedInstance.createTaskDetail.jobPickupLongitude
        self.createTaskDetail.jobPickupName = Singleton.sharedInstance.createTaskDetail.jobPickupName
        self.pickupDeliveryFlag = Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag
        
        pickupMetadata = [Any]()
        metaData = [Any]()

        for i in 0..<Singleton.sharedInstance.createTaskDetail.pickupMetadata.count {
            let localPickupCustomFields = CustomFieldDetails(json: [:])
          //  let localCustomFields = CustomFieldDetails(json: [:])
            let pickupCustomField = Singleton.sharedInstance.createTaskDetail.pickupMetadata[i] as! CustomFieldDetails
            localPickupCustomFields.data = pickupCustomField.data
            //self.pickupMetadata[i] = localPickupCustomFields
            self.pickupMetadata.append(localPickupCustomFields)
//            let customField = Singleton.sharedInstance.createTaskDetail.metaData[i] as! CustomFieldDetails
//            localCustomFields.data = customField.data
//            //self.metaData[i] = localCustomFields
//            self.metaData.append(localCustomFields)
        }
        
        for i in 0..<Singleton.sharedInstance.createTaskDetail.metaData.count {
            let localCustomFields = CustomFieldDetails(json: [:])
            let customField = Singleton.sharedInstance.createTaskDetail.metaData[i] as! CustomFieldDetails
            localCustomFields.data = customField.data
            self.metaData.append(localCustomFields)
        }
    }
    
    func setSkipData() {
        Singleton.sharedInstance.createTaskDetail.customerAddress = self.createTaskDetail.customerAddress
        Singleton.sharedInstance.createTaskDetail.customerEmail = self.createTaskDetail.customerEmail
        Singleton.sharedInstance.createTaskDetail.customerPhone = self.createTaskDetail.customerPhone
        Singleton.sharedInstance.createTaskDetail.customerUsername = self.createTaskDetail.customerUsername
        Singleton.sharedInstance.createTaskDetail.latitude = self.createTaskDetail.latitude
        Singleton.sharedInstance.createTaskDetail.longitude = self.createTaskDetail.longitude
        Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = self.createTaskDetail.jobDeliveryDatetime
        Singleton.sharedInstance.createTaskDetail.hasDelivery = self.createTaskDetail.hasDelivery
        Singleton.sharedInstance.createTaskDetail.hasPickup = self.createTaskDetail.hasPickup
        Singleton.sharedInstance.createTaskDetail.jobDescription = self.createTaskDetail.jobDescription
        Singleton.sharedInstance.createTaskDetail.jobPickupAddress = self.createTaskDetail.jobPickupAddress
        Singleton.sharedInstance.createTaskDetail.jobPickupEmail = self.createTaskDetail.jobPickupEmail
        Singleton.sharedInstance.createTaskDetail.jobPickupPhone = self.createTaskDetail.jobPickupPhone
        Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = self.createTaskDetail.jobPickupDateTime
        Singleton.sharedInstance.createTaskDetail.jobPickupLatitude = self.createTaskDetail.jobPickupLatitude
        Singleton.sharedInstance.createTaskDetail.jobPickupLongitude = self.createTaskDetail.jobPickupLongitude
        Singleton.sharedInstance.createTaskDetail.jobPickupName = self.createTaskDetail.jobPickupName
        Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = self.pickupDeliveryFlag

        for i in 0..<Singleton.sharedInstance.createTaskDetail.pickupMetadata.count {
            let localPickupCustomField = self.pickupMetadata[i] as! CustomFieldDetails
            let pickupCustomField = Singleton.sharedInstance.createTaskDetail.pickupMetadata[i] as! CustomFieldDetails
            pickupCustomField.data = localPickupCustomField.data
            //let localCustomField = self.metaData[i] as! CustomFieldDetails
            //let customField = Singleton.sharedInstance.createTaskDetail.metaData[i] as! CustomFieldDetails
            //customField.data = localCustomField.data
        }
        
        for i in 0..<Singleton.sharedInstance.createTaskDetail.metaData.count {
            let localCustomField = self.metaData[i] as! CustomFieldDetails
            let customField = Singleton.sharedInstance.createTaskDetail.metaData[i] as! CustomFieldDetails
            customField.data = localCustomField.data
        }
    }
    
}
