//
//  TitleView.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 16/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class TitleView: UIView {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var underline: UIView!
    
    override func awakeFromNib() {
        titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        underline.backgroundColor = COLOR.TITLE_UNDER_LINE_COLOR
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    }
    
    func setTitle(title:String, boldTitle:String) {
        let attributedString = NSMutableAttributedString(string: title, attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: 19.0)!])
        let boldAttributedString = NSMutableAttributedString(string: " " + boldTitle, attributes: [NSFontAttributeName:UIFont(name: FONT.regular, size: 19.0)!])
        attributedString.append(boldAttributedString)
        self.titleLabel.attributedText = attributedString
    }

}
