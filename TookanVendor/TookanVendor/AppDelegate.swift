  //
//  AppDelegate.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import FBSDKCoreKit
import SDKDemo1
import Firebase

 
  func logEvent(label: String){
    Analytics.logEvent(label, parameters: [:])
  }
  
  
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var notifiCationDataForBackgroundState:[AnyHashable: Any]?
    var notificationDataForKilledState:[AnyHashable: Any]?
    var mainApplication : UIApplication?
    var timerRuntime = 0
   
  private let keyboard = KeyBoard(heightChanged: nil)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        /*-------------- Fabric/Crashlytics -------------*/
        Fabric.with([Crashlytics.self])
        
        /*------------ Google Maps --------------*/
        GMSServices.provideAPIKey(KEYS.GOOGLE_MAPS_API_KEY)
        GMSPlacesClient.provideAPIKey(KEYS.GOOGLE_MAPS_API_KEY)
        
        FirebaseApp.configure()
        
        
        /*----------------- Setting Locale on App Launch ------------------*/
        UserDefaults.standard.setValue((UserDefaults.standard.value(forKey: "AppleLanguages") as! [Any])[0], forKey: USER_DEFAULT.selectedLocale)
        print((UserDefaults.standard.value(forKey: "AppleLanguages") as! [Any])[0])
//        if(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) == nil) {
//            UserDefaults.standard.setValue("en", forKey: USER_DEFAULT.selectedLocale)
//            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
//        } else {
//            let locale = UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String
//            UserDefaults.standard.set([locale!], forKey: "AppleLanguages")
//        }
//        UserDefaults.standard.synchronize()
        
        /*----------------- Set Server URL on App Launch ------------------*/
        if(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) == nil) {
            UserDefaults.standard.setValue(SERVER.live, forKey: USER_DEFAULT.selectedServer)
        } else {
            if(APP_STORE.value == 1) {
                UserDefaults.standard.setValue(SERVER.live, forKey: USER_DEFAULT.selectedServer)
            }
        }

        /*------------- Register remote notification / Device Token -------------------*/
        let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
        let setting = UIUserNotificationSettings(types: type, categories: nil);
        UIApplication.shared.registerUserNotificationSettings(setting);
        UIApplication.shared.registerForRemoteNotifications();
    
        /*---------------- Location Tracker -----------------*/
        LocationTracker.sharedInstance.setLocationUpdate()
        
        
        
        return  FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
   
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = NSString(format: "%@", deviceToken as CVarArg) as String
        let characterSet: CharacterSet = CharacterSet( charactersIn: "<>" )
        let finalToken: String = (deviceTokenString)
            .trimmingCharacters( in: characterSet )
            .replacingOccurrences( of: " ", with: "" ) as String
        print(finalToken)
      
      FuguConfig.shared.registerDeviceToken(deviceToken: deviceToken)
        UserDefaults.standard.set(finalToken, forKey: USER_DEFAULT.deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error){
        UserDefaults.standard.set("deviceToken", forKey: USER_DEFAULT.deviceToken)
    }

    
   func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      
      if FuguConfig.shared.isFuguNotification(userInfo: userInfo as! [String : Any])  {
          FuguConfig.shared.handleRemoteNotification(userInfo: userInfo as! [String : Any])
      } else {
         
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.updateOnPushNotification), object: nil)
         
         if let pushId = userInfo["push_id"] as? String,
            pushId == "REGISTRATION_STATUS_CHANGED" {
            handleRegistrationStatusChangedNotificationWith(userInfo: userInfo)
            return
         }
         
         
         if [UIApplicationState.active].contains(application.applicationState) == true {
            if Singleton.sharedInstance.isSignedIn == true {
               DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.2, execute: {
                  self.showNotification(userInfo: userInfo, application: application)
               })
               
            }
            notifiCationDataForBackgroundState = nil
         } else if application.applicationState == .background{
            notifiCationDataForBackgroundState = userInfo
         } else {
            notifiCationDataForBackgroundState = nil
            notificationDataForKilledState = userInfo
            mainApplication = application
            let killedTimer = Timer.init(timeInterval: 1, target: self, selector: #selector(AppDelegate.forKilledState), userInfo: nil, repeats: false)
            killedTimer.fire()
         }
      }
   }
   
   func handleRegistrationStatusChangedNotificationWith(userInfo: [AnyHashable: Any]) {
      guard let registrationStatusRawValue = userInfo["registration_status"] as? NSNumber else {
         print("Registration Status not found in push")
         return
      }
      
      if UIApplication.shared.applicationState == .active {
         let message = (userInfo["message"] as? String) ?? ""
         
         let info = InfoPopUpView.InfoPopUp(heading: "", subHeading: nil, description: message, image: nil, closeButtontitle: TEXT.OK_TEXT, type: InfoPopUpView.PopUpType.normal)
         InfoPopUpView.loadWith(info: info) {
            SignupTemplateFlowManager.shared?.registrationStatusUpdatedTo(statusRawValue: registrationStatusRawValue.intValue)
         }
         
      } else {
         SignupTemplateFlowManager.shared?.registrationStatusUpdatedTo(statusRawValue: registrationStatusRawValue.intValue)
      }
      
   }
   
   
    //MARK: FUNCTION TO HANDEL PUSH IN KILLED STATE
    func forKilledState()  {
        if notifiCationDataForBackgroundState == nil {
        if Singleton.sharedInstance.isSignedIn == true  {
            if (notificationDataForKilledState != nil) && (mainApplication != nil){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5, execute: { 
           self.showNotification(userInfo: self.notificationDataForKilledState!, application: self.mainApplication!)
                })
            }
        }else {
            timerRuntime = timerRuntime + 1
            if timerRuntime >= 10 {
            }else{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
                    let killedTimer = Timer.init(timeInterval: 1, target: self, selector: #selector(AppDelegate.forKilledState), userInfo: nil, repeats: false)
                    killedTimer.fire()
                })
            }
            }
        }
    }
    
    
    //MARK: FUNCTION TO SHOW NOTIFICATION
    
    func showNotification(userInfo: [AnyHashable : Any], application: UIApplication) {
        //Singleton.sharedInstance.showAlert("\(userInfo)")
        if let message = userInfo["message"] as? String {
            Singleton.sharedInstance.showPushNotificationView(message: message, onclickAction: {
                DispatchQueue.main.async {
                    
                
                application.applicationIconBadgeNumber = 0
                application.cancelAllLocalNotifications()
                if let jobid = userInfo["job_id"] as? Int {
                    if let navBar = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController{
                        var orderDetailVC : OrderDetailVC!
                        orderDetailVC = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.orderDetailVC) as! OrderDetailVC
                        orderDetailVC.jobId = "\(jobid)"
//                        Singleton.sharedInstance.showAlert("\(navBar.visibleViewController)")
                        if let current = navBar.visibleViewController as? OrderDetailVC {
                        
                        }else{
                            navBar.pushViewController(orderDetailVC, animated: true)
                        }
                    }
                }else  if let jobid = userInfo["job_id"] as? String{
                    if let navBar = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController{
                        var orderDetailVC : OrderDetailVC!
                        orderDetailVC = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.orderDetailVC) as! OrderDetailVC
//                        Singleton.sharedInstance.showAlert("\(navBar.visibleViewController)")
                        orderDetailVC.jobId = "\(jobid)"
                        if let current = navBar.visibleViewController as? OrderDetailVC {
                            
                        }else{
                            navBar.pushViewController(orderDetailVC, animated: true)
                        }
                    }
                }
                }
            })
            
        }

    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
      print("Saving data")
      CartPersistencyManager.shared?.saveInDocumentDirectory()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if notifiCationDataForBackgroundState != nil{
            showNotification(userInfo: notifiCationDataForBackgroundState!, application: application)
            notifiCationDataForBackgroundState = nil
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
      print("Saving data")
      CartPersistencyManager.shared?.saveInDocumentDirectory()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

