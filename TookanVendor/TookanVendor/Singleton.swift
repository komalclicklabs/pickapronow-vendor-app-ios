
//
//  Singleton.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreTelephony
import CoreLocation
import SDKDemo1

class Singleton: NSObject, CAAnimationDelegate {
    static let sharedInstance = Singleton()
    
//    var allOrderHistoryList = [OrderHistory]()
//    var vendorDetailInfo = VendorDetails(json: [:])
//  var formDetailsBeforeLogin = FormDetails(json: [:])

  
    var formDetailsInfo = FormDetails(json: [:])
    var createTaskDetail = CreateTaskDetails(json: [:])
    var typeCategories = [CarTypeModal]()
    var selectedData = String()
    
    var distanceBetweenPickUpDelivery = String()
    
    //var customFieldDetails = [String:Any]()
    var customFieldOptionsPickup = [String:Any]()
    var customFieldOptionsDelivery = [String:Any]()
    let appdelegate = UIApplication.shared.delegate  as! AppDelegate
    let appWindow = (UIApplication.shared.delegate  as! AppDelegate).window
    var allImagesCache = NSCache<NSString,UIImage>()
    var isSignedIn = false
    var selectedDestinationAddressForTaxi = ""
    var selectedDestinationLatForTaxi = Double()
    var selectedDestinationlongForTaxi = Double()
    
    var WorkFlowFormDetailsOptions = [FormDetails]()
    var servicesOptionPickUpDetails = [[String:Any]]()
    var servicesOptionsDeliveryDetails = [[String:Any]]()
    var servicesTypeCategories = [[CarTypeModal]]()
    var subTotal = Double()
    
    var couponsArray = [PromoCodeModal]()
    
    var promoAccessId = String()
    var promoId = String()
    var productLocation : CLLocationCoordinate2D = LocationTracker.sharedInstance.myLocation.coordinate
    var addressForNlevelFlow = ""
   // var SelectAgent = Agent()
    
    
    func logoutButtonAction() {
      CartPersistencyManager.shared = nil
      FuguConfig.shared.clearFuguUserData()
        self.isSignedIn = false 
        Singleton.sharedInstance.isSignedIn = false
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
        Vendor.current = nil
        self.createTaskDetail = CreateTaskDetails(json: [:])
        self.formDetailsInfo = FormDetails(json: [:])
        let deviceToken = UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken)
        let selectedServer = UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer)
        let appDomain = Bundle.main.bundleIdentifier
        let deviceType = UserDefaults.standard.value(forKey: "deviceType")
      let hasUserLoggedInBefore = Vendor.hasSignedUpBefore()
      SignupTemplateFlowManager.shared = nil
      
        UserDefaults.standard.removePersistentDomain(forName: appDomain!)
        UserDefaults.standard.set(selectedServer, forKey: USER_DEFAULT.selectedServer)
        UserDefaults.standard.set(deviceToken, forKey: USER_DEFAULT.deviceToken)
        UserDefaults.standard.set(deviceType, forKey: "deviceType")
      
      if hasUserLoggedInBefore {
         Vendor.userHasLoggedIntoAppAtLeastOnce()
      }
      
        selectedDestinationAddressForTaxi = ""
        selectedDestinationLatForTaxi = Double()
        selectedDestinationlongForTaxi = Double()
        
        let afterLogin = UIStoryboard(name: STORYBOARD_NAME.main, bundle: Bundle.main)
        let appdelegate = UIApplication.shared.delegate  as! AppDelegate
        let appWindow = appdelegate.window
        appWindow?.rootViewController = afterLogin.instantiateViewController(withIdentifier: STORYBOARD_ID.splashNavigation) as! UINavigationController
        appWindow?.makeKeyAndVisible()
    }
    
    func getTaskTypeTosetDetailsForCreateTask() -> PICKUP_DELIVERY {
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                return PICKUP_DELIVERY.pickup
            case PICKUP_DELIVERY.delivery.rawValue?:
                return PICKUP_DELIVERY.delivery
            case PICKUP_DELIVERY.both.rawValue?:
                return PICKUP_DELIVERY.both
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                return PICKUP_DELIVERY.addDeliveryDetails
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?:
            return PICKUP_DELIVERY.delivery
        case WORKFLOW.fieldWorkforce.rawValue?:
            return PICKUP_DELIVERY.delivery
        default:
            break
        }
        return PICKUP_DELIVERY.pickup
    }
    
    func getMetadataForTask(customFieldType:Int) -> CustomFieldDetails {
        
        switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
        case PICKUP_DELIVERY.pickup:
            
            guard Singleton.sharedInstance.customFieldOptionsPickup.count > 0 else {
                return CustomFieldDetails(json:[:])
            }
            
            if Singleton.sharedInstance.createTaskDetail.pickupMetadata.count-1 < customFieldType{
                return CustomFieldDetails(json:[:])
            }
            
              return Singleton.sharedInstance.createTaskDetail.pickupMetadata[customFieldType] as! CustomFieldDetails
            
        case PICKUP_DELIVERY.delivery:
//            switch Singleton.sharedInstance.formDetailsInfo.work_flow {
//            case WORKFLOW.pickupDelivery.rawValue?:
//                guard Singleton.sharedInstance.customFieldOptionsDelivery.count > 0 else {
//                    return CustomFieldDetails(json:[:])
//                }
//                return Singleton.sharedInstance.createTaskDetail.metaData[customFieldType] as! CustomFieldDetails
//            default:
                guard Singleton.sharedInstance.customFieldOptionsPickup.count > 0 else {
                    return CustomFieldDetails(json:[:])
                }
                
                if Singleton.sharedInstance.createTaskDetail.metaData.count-1 < customFieldType{
                    return CustomFieldDetails(json:[:])
                }
                
                
                return Singleton.sharedInstance.createTaskDetail.metaData[customFieldType] as! CustomFieldDetails
          //  }
            
        case PICKUP_DELIVERY.both, PICKUP_DELIVERY.addDeliveryDetails:
            if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                
                guard Singleton.sharedInstance.customFieldOptionsPickup.count > 0 else {
                    return CustomFieldDetails(json:[:])
                }
                
                if Singleton.sharedInstance.createTaskDetail.pickupMetadata.count-1 < customFieldType{
                    return CustomFieldDetails(json:[:])
                }
                
                return Singleton.sharedInstance.createTaskDetail.pickupMetadata[customFieldType] as! CustomFieldDetails
                
            } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                
                guard Singleton.sharedInstance.customFieldOptionsDelivery.count > 0 else {
                    return CustomFieldDetails(json:[:])
                }
                
                if Singleton.sharedInstance.createTaskDetail.metaData.count-1 < customFieldType{
                    return CustomFieldDetails(json:[:])
                }
                
                
                return Singleton.sharedInstance.createTaskDetail.metaData[customFieldType] as! CustomFieldDetails
            }
        }
        return CustomFieldDetails(json:[:])
    }
    
    func isForTutorial() -> Bool {
        if UserDefaults.standard.value(forKey: USER_DEFAULT.isFirstTimeSignUp) != nil{
            let value = UserDefaults.standard.value(forKey: USER_DEFAULT.isFirstTimeSignUp) as! String
            if value == "1"{
                if APP_DEVICE_TYPE == "1" || APP_DEVICE_TYPE == "5" {
                    return false
                } else {
                    return false
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func resetPickupMetaData() {
        Singleton.sharedInstance.createTaskDetail.pickupMetadata = [Any]()
        if let items = self.customFieldOptionsPickup["items"] as? [Any] {
            for item in items {
                let customField = CustomFieldDetails(json: item as! [String : Any])
                switch customField.dataType {
                case .checklist, .table:
                    break
                default:
                    Singleton.sharedInstance.createTaskDetail.pickupMetadata.append(customField)
                    break
                }
            }
        }
    }
    
    func resetMetaData() {
        switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
        case PICKUP_DELIVERY.pickup:
            self.resetPickupCustomField()
            break
        case PICKUP_DELIVERY.delivery:
//            switch Singleton.sharedInstance.formDetailsInfo.work_flow {
//            case WORKFLOW.pickupDelivery.rawValue?:
//                self.resetDeliveryCustonField()
//                break
//            default:
                self.resetPickupCustomField()
//                break
//            }
            break
        case PICKUP_DELIVERY.both, PICKUP_DELIVERY.addDeliveryDetails:
//            if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
//                self.resetPickupCustomField()
//            } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                self.resetDeliveryCustonField()
           // }
            break
        }

        
    }
    
    func resetPickupCustomField() {
        Singleton.sharedInstance.createTaskDetail.metaData = [Any]()
        if let items = self.customFieldOptionsPickup["items"] as? [Any] {
            for item in items {
                let customField = CustomFieldDetails(json: item as! [String : Any])
                switch customField.dataType {
                case .checklist, .table:
                    break
                default:
                    Singleton.sharedInstance.createTaskDetail.metaData.append(customField)
                    break
                }
            }
        }
    }
    
    func resetDeliveryCustonField() {
        Singleton.sharedInstance.createTaskDetail.metaData = [Any]()
        if let items = self.customFieldOptionsDelivery["items"] as? [Any] {
            for item in items {
                let customField = CustomFieldDetails(json: item as! [String : Any])
                switch customField.dataType {
                case .checklist, .table:
                    break
                default:
                    Singleton.sharedInstance.createTaskDetail.metaData.append(customField)
                    break
                }
            }
        }
    }
    
    func setAddDelivaryDetails() {
        self.createTaskDetail.hasDelivery = 1
        self.createTaskDetail.hasPickup = 0
        self.createTaskDetail.hidePickupDelivery = true
        self.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.addDeliveryDetails.rawValue
        self.createTaskDetail.customerAddress = ""
        self.createTaskDetail.latitude = ""
        self.createTaskDetail.longitude = ""
        self.createTaskDetail.customerUsername = ""
        self.createTaskDetail.customerEmail = ""
        self.createTaskDetail.customerPhone = ""
        self.createTaskDetail.jobDeliveryDatetime = ""
        self.resetMetaData()
    }
    
    func setAddPickupDetails() {
        self.createTaskDetail.hasDelivery = 0
        self.createTaskDetail.hasPickup = 1
        self.createTaskDetail.hidePickupDelivery = true
        self.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.addDeliveryDetails.rawValue
        self.createTaskDetail.jobPickupAddress = TEXT.ADD_PICKUP_LOCATION
        self.createTaskDetail.jobPickupLatitude = ""
        self.createTaskDetail.jobPickupLongitude = ""
        self.createTaskDetail.jobPickupName = ""
        self.createTaskDetail.jobPickupEmail = ""
        self.createTaskDetail.jobPickupPhone = ""
        self.createTaskDetail.jobPickupDateTime = ""
        self.resetPickupMetaData()
    }
    
    func resetDeliveryDetails() {
        self.createTaskDetail.customerAddress = ""
        self.createTaskDetail.latitude = ""
        self.createTaskDetail.longitude = ""
        self.createTaskDetail.customerUsername = ""
        self.createTaskDetail.customerEmail = ""
        self.createTaskDetail.customerPhone = ""
        self.createTaskDetail.jobDeliveryDatetime = ""
        self.resetMetaData()
    }
    
    func resetPickupDetails() {
        self.createTaskDetail.jobPickupAddress = TEXT.ADD_PICKUP_LOCATION
        self.createTaskDetail.jobPickupLatitude = ""
        self.createTaskDetail.jobPickupLongitude = ""
        self.createTaskDetail.jobPickupName = ""
        self.createTaskDetail.jobPickupEmail = ""
        self.createTaskDetail.jobPickupPhone = ""
        self.createTaskDetail.jobPickupDateTime = ""
        self.resetPickupMetaData()
    }
    
    func deletePickupDetails() {
        self.createTaskDetail.jobPickupAddress = self.createTaskDetail.customerAddress
        self.createTaskDetail.jobPickupLatitude = self.createTaskDetail.latitude
        self.createTaskDetail.jobPickupLongitude = self.createTaskDetail.longitude
        self.createTaskDetail.jobPickupName = self.createTaskDetail.customerUsername
        self.createTaskDetail.jobPickupEmail = self.createTaskDetail.customerEmail
        self.createTaskDetail.jobPickupPhone = self.createTaskDetail.customerPhone
        self.createTaskDetail.jobPickupDateTime = self.createTaskDetail.jobDeliveryDatetime
        //self.createTaskDetail.pickupMetadata = self.createTaskDetail.metaData
        self.resetPickupMetaData()
    }
    
    func deleteDelivaryDetails() {
        self.createTaskDetail.customerAddress = self.createTaskDetail.jobPickupAddress
        self.createTaskDetail.latitude = self.createTaskDetail.jobPickupLatitude
        self.createTaskDetail.longitude = self.createTaskDetail.jobPickupLongitude
        self.createTaskDetail.customerUsername = self.createTaskDetail.jobPickupName
        self.createTaskDetail.customerEmail = self.createTaskDetail.jobPickupEmail
        self.createTaskDetail.customerPhone = self.createTaskDetail.jobPickupPhone
        self.createTaskDetail.jobDeliveryDatetime = self.createTaskDetail.jobPickupDateTime
        //self.createTaskDetail.metaData = self.createTaskDetail.pickupMetadata
        self.resetMetaData()
    }
    
    func deleteAllDetails() {
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                self.createTaskDetail.hasDelivery = 0
                self.createTaskDetail.hasPickup = 1
                break
            case PICKUP_DELIVERY.delivery.rawValue?:
                self.createTaskDetail.hasDelivery = 1
                self.createTaskDetail.hasPickup = 0
                self.createTaskDetail.customerAddress = ""
                break
            case PICKUP_DELIVERY.both.rawValue?:
                self.createTaskDetail.hasDelivery = 0
                self.createTaskDetail.hasPickup = 1
                self.createTaskDetail.customerAddress = ""
                break
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
            self.createTaskDetail.hasDelivery = 1
            self.createTaskDetail.hasPickup = 0
            self.createTaskDetail.customerAddress = ""
            break
        default:
            break
        }

        self.createTaskDetail.hidePickupDelivery = false
        self.createTaskDetail.jobPickupAddress = ""
        self.createTaskDetail.jobPickupLatitude = ""
        self.createTaskDetail.jobPickupLongitude = ""
        self.createTaskDetail.jobPickupName = ""
        self.createTaskDetail.jobPickupEmail = ""
        self.createTaskDetail.jobPickupPhone = ""
        self.createTaskDetail.jobPickupDateTime = ""
        
        self.createTaskDetail.latitude = ""
        self.createTaskDetail.longitude = ""
        self.createTaskDetail.customerUsername = ""
        self.createTaskDetail.customerEmail = ""
        self.createTaskDetail.customerPhone = ""
        self.createTaskDetail.jobDeliveryDatetime = ""
        self.createTaskDetail.jobDescription = ""
        self.resetPickupMetaData()
        self.resetMetaData()
    }
   
   
    func enableDisableIQKeyboard(enable:Bool) {
        IQKeyboardManager.sharedManager().enable = enable
        IQKeyboardManager.sharedManager().enableAutoToolbar = enable
    }
    
   func pushToHomeScreen(completed: @escaping () -> Void) {
   
   let navVC = getAfterLoginMainNavigationController()
    
   if isSingleOfferingAndNLevel() {
   
      Catalogue.getCatalogueOfFormWith(formId: WorkFlowFormDetailsOptions[0].form_id!,location:LocationTracker.sharedInstance.myLocation.coordinate, showActivityIndicator: false, showAlert: false) {[weak self] success, catalogue, error in
         if success {
            self?.WorkFlowFormDetailsOptions[0].nLevelController = NLevelFlowManager(navigationController: navVC, catalogue: catalogue!)
            self?.setcontrollerAsRootViewController(controller: navVC)
         } else {
            self?.handleFailureOfCatalogueFetchingWith(error: error!)
         }
         completed()
      }
      return
   }
   
      completed()
   setcontrollerAsRootViewController(controller: navVC)
  }
   
   private func getAfterLoginMainNavigationController() -> UINavigationController {
      return UIViewController.findIn(storyboard: .afterLogin, withIdentifier: STORYBOARD_ID.afterLoginNavigation) as! UINavigationController
   }
   
   private func isSingleOfferingAndNLevel() -> Bool {
      return WorkFlowFormDetailsOptions.count == 1 && WorkFlowFormDetailsOptions[0].isNLevel
   }
   
   private func setcontrollerAsRootViewController(controller: UIViewController) {
      Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
      appWindow?.rootViewController = controller
      appWindow?.makeKeyAndVisible()
   }
   
   private func handleFailureOfCatalogueFetchingWith(error: Error) {
      let rootViewController = UIApplication.shared.keyWindow?.rootViewController
      showAlertWithOption(owner: rootViewController!, message: error.localizedDescription, leftButtonAction: { [weak self] in
         ActivityIndicator.sharedInstance.showActivityIndicator()
         self?.pushToHomeScreen {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
         }
         }, leftButtonTitle: TEXT.RETRY)
   }
  
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        // remove mask when animation completes
        appWindow!.rootViewController!.view.layer.mask = nil
    }

    
    func animateToIdentity(view:UIView, delayTime:Double) {
        UIView.animate(withDuration: 0.8, delay: delayTime, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            view.transform = CGAffineTransform.identity
            view.alpha = 1.0
            }, completion: nil)
    }
    
    func transferViewToParicularPoints(view:UIView, delayTime:Double, points:CGPoint) {
        UIView.animate(withDuration: 0.8, delay: delayTime, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            view.transform = CGAffineTransform(translationX: points.x, y: points.y)
            view.alpha = 0.0
            }, completion: nil)
    }
    
    func setAttributeTitleWithOrWithoutArrow(title:String,isArrow:Bool,yourImage:UIImage) -> NSMutableAttributedString {
        
        switch title {
        case TEXT.EDIT_PROFILE:
            let attributedString = NSMutableAttributedString(string: title + " ", attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: 19.0)!, NSForegroundColorAttributeName:UIColor.gray])
            let image1Attachment = NSTextAttachment()
            //  image1Attachment.image = IMAGE.iconSigninSigninpage
            image1Attachment.image = yourImage
            image1Attachment.bounds = CGRect(x: 0, y: -2, width: 13, height: 13)
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            if isArrow == true {
                attributedString.append(image1String)
                return attributedString
            }
            return attributedString

        default:
            let attributedString = NSMutableAttributedString(string: title + " ", attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: 19.0)!, NSForegroundColorAttributeName:COLOR.LOGIN_BUTTON_TITLE_COLOR])
            let image1Attachment = NSTextAttachment()
            //  image1Attachment.image = IMAGE.iconSigninSigninpage
            image1Attachment.image = yourImage
            image1Attachment.bounds = CGRect(x: 0, y: -2, width: 8, height: 13)
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            if isArrow == true {
                attributedString.append(image1String)
                return attributedString
            }
            return attributedString

        }
           }
    
    func showAlert(_ message:String) {
        DispatchQueue.main.async(execute: { () -> Void in
            let alertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: TEXT.OK_TEXT.uppercased())
            alertView.show()
        })
    }
    
    
    //MARK: FUNCTION TO SHOW PUSH NOTOFICATION VIEW
    
    func showPushNotificationView(message : String , onclickAction:@escaping ()->Void) {
        
        if PushNotificationView.notiFicationView != nil{
            if PushNotificationView.notiFicationView?.superview != nil{
                PushNotificationView.notiFicationView?.removeFromSuperview()
            }
        }
        
            PushNotificationView.notiFicationView = PushNotificationView.showPush(message: message, actionOnClick: onclickAction)
            UIApplication.shared.keyWindow?.addSubview( PushNotificationView.notiFicationView!)
            UIView.animate(withDuration: 0.5, animations: {
                 PushNotificationView.notiFicationView?.alpha = 1
                let size = message.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 60, font:  UIFont(name: FONT.light, size: FONT_SIZE.medium)!)
                let titleSize = APP_NAME.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 60, font:  UIFont(name: FONT.light, size: FONT_SIZE.medium)!)
                 PushNotificationView.notiFicationView?.frame =  CGRect(x: 20, y: 10, width: (UIApplication.shared.keyWindow?.frame.width)! - 40, height:  size.height + titleSize.height + 60)
                if  PushNotificationView.notiFicationView?.superview != nil{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: {
                     PushNotificationView.notiFicationView?.hideNotification()
                })
                }
        })
        }
    
    
    //MARK: SHOW ALERT VIEW CONTROLLER
    
    func showAlertWithOption(owner:UIViewController,title:String = "",message:String,showRight:Bool = false,leftButtonAction: (()->Void)?,rightButtonAction: (()->Void)? = nil ,leftButtonTitle:String = "No",rightButtonTitle :String = "Yes"){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let rightButtonAction = UIAlertAction(title: rightButtonTitle, style: UIAlertActionStyle.default) { (action) in
            if rightButtonAction != nil {
                rightButtonAction!()
            }
        }
        
        
        let leftButtonAction = UIAlertAction(title: leftButtonTitle, style: UIAlertActionStyle.default) { (action) in
            leftButtonAction?()
        }
        alert.addAction(leftButtonAction)
        if showRight == true{
            alert.addAction(rightButtonAction)
        }
        
        DispatchQueue.main.async {
            owner.present(alert, animated: true, completion: nil)

        }
        }
    
    
    
    //MARK: VALIDATION
    
    func validateEmail(_ candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    func validPassword(password:String) -> Bool {
        if password.length >= 6 {
            return true
        }
        return false
    }
    
    func validateDate(jobDate:String) -> Bool {
        if jobDate.length > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//"MM/dd/yyyy HH:mm"
            let startDate = dateFormatter.date(from: jobDate)
            if(Int((startDate?.timeIntervalSince(Date()))!) > 0) {
                return true
            }
        }
        return false
    }
    
    func comparePickupAndDeliveryDate(pickupDate:String, deliveryDate:String,greaterMessage:String = "Start time should be before end time" , bufferMessage : String = "There should be buffer of atleast 15 mins between start time and end time.") -> Bool {
        if pickupDate.length > 0 && deliveryDate.length > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//"MM/dd/yyyy HH:mm"
            let pickupDate = dateFormatter.date(from: pickupDate)
            let deliveryDate = dateFormatter.date(from: deliveryDate)
            let timeInterval = Int((deliveryDate?.timeIntervalSince(pickupDate!))!)
            if(timeInterval > 60*15) {
                return true
            }else if timeInterval < 0{
                ErrorView.showWith(message: greaterMessage, isErrorMessage: true, removed: nil)
            }else{
                ErrorView.showWith(message: bufferMessage, isErrorMessage: true, removed: nil)
            }
        }
        return false
    }
    
    func validatePhoneNumber(phoneNumber:String) -> Bool {
        let array = phoneNumber.components(separatedBy: " ")
        if array.count > 1 {
            let number = array[1]
            if number.length >= 5 && number.length <= 15  {
                return true
            }
        } else {
            let number = array[0]
            if number.length >= 5 && number.length <= 15  {
                return true
            }
        }
        
        return false
    }
    
    func validateAddress(address:String, latitude:String, longitude:String) -> Bool {
        guard address.length > 0 else {
            return false
        }
        
        guard latitude.length > 0 else {
            return false
        }
        
        guard Double(latitude) != 0.0 else {
            return false
        }
        
        guard longitude.length > 0 else {
            return false
        }
        
        guard Double(longitude) != 0.0 else {
            return false
        }
        
        return true
    }
    
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let _ = URL(string: urlString!){
            return true
        }
        return false
    }
    
    func convertDateToString() -> String {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone = TimeZone(abbreviation:  "UTC")
        styler.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return styler.string(from: Date())
    }
    
    func getTimeZone() -> Int {
        let timezone = -(NSTimeZone.system.secondsFromGMT() / 60)
        return timezone
    }
    
    func currentDialingCode() -> String {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        // Get carrier name
        return (carrier?.isoCountryCode) != nil ? (carrier?.isoCountryCode)! : ""
    }
    
    func saveImageToDocumentDirectory(_ imageName:String, image:UIImage) -> String {
        let filePath = self.createPath(imageName)
        let pngImageData = UIImageJPEGRepresentation(image, 0.1)
        let fileManager = FileManager.default
        if(fileManager.fileExists(atPath: filePath) == true) {
            do {
                try fileManager.removeItem(atPath: filePath)
                self.allImagesCache.removeObject(forKey: "\(imageName)" as NSString)
            } catch let error as NSError {
                print("Error in removing Path = \(error.description)")
            }
        }
        try? pngImageData!.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        return imageName
    }
    
    func deleteAllFilesFromDocumentDirectory(){
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let imagePath = URL(fileURLWithPath: dirPath).appendingPathComponent("Image")
        let fileManager = FileManager.default
        do {
            let directoryContents:[URL] = try fileManager.contentsOfDirectory(at: imagePath, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles) //fileManager.contentsOfDirectoryAtPath(tempDirPath, error: error)?
            for path in directoryContents {
                //print(path)
                let fullPath = imagePath.appendingPathComponent((path).path)//dirPath.stringByAppendingString(path as! String)
                do {
                    //print(fullPath)
                    try fileManager.removeItem(at: fullPath)
                } catch let error as NSError {
                    print("Error in removing Path = \(error.description)")
                }
            }
        } catch let error as NSError {
            print("Error in removing Path = \(error.description)")
        }
    }
    
    func deleteImageFromDocumentDirectory(_ imagePath:String) {
        // print(imagePath)
        let fileManager = FileManager.default
        let filePath = self.createPath(imagePath)
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print("Error in removing Path = \(error.description)")
        }
    }
    
    func createPath(_ imageName:String) -> String {
        let fileManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        let imageDirUrl = url.appendingPathComponent("Image")
        var isDirectory:ObjCBool = false
        if fileManager.fileExists(atPath: imageDirUrl.path, isDirectory: &isDirectory) == false {
            do {
                try FileManager.default.createDirectory(atPath: imageDirUrl.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        let filePath = imageDirUrl.appendingPathComponent(imageName).path
        return filePath
    }
    
    func isFileExistAtPath(_ path:String) -> Bool {
        //  print(path)
        let filePath = self.createPath(path)
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: filePath)
    }
    
    func parsingLocations(locationArray:[Any]) {
        if locationArray.count > 0 {
        for i in (0..<locationArray.count) {
            if let locationData = locationArray[i] as? [String:Any] {
            /*------- For Updating Path ------------*/
            var locationDictionary = [String:Any]()
            var updatingLocationArray = [Any]()
            var latitudeString:Double?
            var longitudeString:Double?
            if let lat = locationData["lat"] as? NSNumber {
                latitudeString = Double(lat)
            } else if let lat = locationData["lat"] as? String {
                latitudeString = Double(lat)
            }
            
            if let long = locationData["lng"] as? NSNumber {
                longitudeString = Double(long)
            } else if let long = locationData["lng"] as? String {
                longitudeString = Double(long)
            }
            if latitudeString != nil && longitudeString != nil  {
            let coordinate = CLLocationCoordinate2D(latitude: latitudeString!, longitude: longitudeString!)
            locationDictionary = [
               "Latitude":coordinate.latitude,
               "Longitude":coordinate.longitude
               ]
            if let array = UserDefaults.standard.value(forKey: USER_DEFAULT.updatingLocationPathArray) as? [Any] {
                updatingLocationArray = array
            }
            updatingLocationArray.append(locationDictionary)
            UserDefaults.standard.setValue(updatingLocationArray, forKey: USER_DEFAULT.updatingLocationPathArray)
            }
            /*----------------------------------------------*/
        }
       
        }
        }
         NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.updatePath), object: nil)
    }
    
   func checkToSHowCancelButton(value:String, formId: String) -> CGFloat{
      print(Singleton.sharedInstance.formDetailsInfo.cancelDict)
      var form: FormDetails?
      for formTemp in Singleton.sharedInstance.WorkFlowFormDetailsOptions {
         if formId == formTemp.form_id! {
            form = formTemp
            break
         }
      }
      if let toShow = form?.cancelDict["\(value)"] as? Int {
         if toShow == 1{
            return 60
         }else{
            return 0
         }
      }else if let toShow = form?.cancelDict["\(value)"] as? String {
         if toShow == "1"{
            return 60
         }else{
            return 0
         }
      }
      return 0
   }

    
    func clearQuantityFromCategories() {
        for i in Singleton.sharedInstance.typeCategories{
            i.quantity = 0
        }
    }
    
    
    func checkIfQauntityIsMoreThanZero() -> Bool{
        for i in Singleton.sharedInstance.typeCategories{
            if i.quantity > 0{
                return true
            }
            
        }
        return false
    }
    
    
   func checkForLogin(owener: UIViewController, startLoaderAnimation: @escaping (() -> Void),stopLoaderAnimation: @escaping (()->Void)) {
      
      guard Vendor.current != nil else {
         print("Vendor nil in check for login")
         return
      }
      
      if Vendor.current!.is_phone_verified == "0" && AppConfiguration.current.isOtpRequired == "1" {
         OtpScreenViewController.pushIn(navVC: owener.navigationController!)
         return
      }
      
      if AppConfiguration.current.isSignupTemplateEnabled && Vendor.current!.registrationStatus.shouldStartSignupTemplateFlow() {
         SignupTemplateFlowManager.startSignupTemplateFlowIn(navVC: owener.navigationController!, withSignupTemplate: Vendor.current!.signupTemplate, andWithRegistrationStatus: Vendor.current!.registrationStatus)
         return
      }
      

         DispatchQueue.main.async {
            startLoaderAnimation()
         }
         FormDetails.fetchOfferingsFromServer(completed: { (success, error) in
            guard success else {
               stopLoaderAnimation()
               Singleton.sharedInstance.showAlertWithOption(owner: owener, message: error!.localizedDescription ,leftButtonAction: {
                  Singleton.sharedInstance.checkForLogin(owener: owener,startLoaderAnimation: startLoaderAnimation,stopLoaderAnimation:stopLoaderAnimation)
               }, leftButtonTitle: TEXT.RETRY)
               return
            }
            
            
            Singleton.sharedInstance.pushToHomeScreen() {
               SignupTemplateFlowManager.shared = nil
               stopLoaderAnimation()
            }
         })
         return
      
   }
   
   
    func getDefaultLatLong() -> (lat:Double,long:Double){
        var lat  = Double()
        var long = Double()
        for i in Singleton.sharedInstance.createTaskDetail.pickupMetadata{
            if let data = i as? CustomFieldDetails{
              if data.label == "default_lat"{
                    if Double(data.input!) != nil{
                        lat = Double(data.input!)!
                    }
                }else if data.label == "default_lng"{
                if Double(data.input!) != nil{
                    long = Double(data.input!)!
                }
                }
            }
        }
        return (lat,long)
    }
    
    
    func getPromoCodeArray(data:[String:Any]){
        print(data)
        couponsArray.removeAll()
        if let value = data["coupons"] as? [Any]{
            for i in value{
                couponsArray.append(PromoCodeModal(json: i as! [String : Any]))
            }
            
        }
        
        if let  promos = data["promos"] as? [Any]{
            for i in promos{
                couponsArray.append(PromoCodeModal(json: i as! [String : Any]))
            }
        }
   }
    
    func getFormDetails(forId:String){
        for i in Singleton.sharedInstance.WorkFlowFormDetailsOptions{
            if i.form_id == forId{
                Singleton.sharedInstance.formDetailsInfo = i
            }
        }
    }
    
    func getDeviceType() -> String{
        if UserDefaults.standard.value(forKey: "deviceType") != nil{
            return UserDefaults.standard.value(forKey: "deviceType") as! String
        }else{
            return APP_DEVICE_TYPE
        }
    }
    
}
