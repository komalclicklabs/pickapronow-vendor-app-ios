//
//  OtherExtension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation

extension CLLocationCoordinate2D {
    func lowAccuracy() -> CLLocationCoordinate2D {
        let lat = (self.latitude*100000000)/100000000
        let long = (self.longitude*100000000)/100000000
        return CLLocationCoordinate2D(latitude: lat, longitude: long)
    }
}

extension IndexPath {
   func generateTag() -> Int {
      let tag = (1000 * (self.section + 1)) + (self.row + 1)
      return tag
   }
   
}

extension Int {
   func getRowAndSection() -> (row: Int, section: Int) {
      return ((self % 1000)-1, (self / 1000) - 1)
   }
}

extension UITableView {
   func registerCellWith(nibName: String, reuseIdentifier: String) {
      let cellNib = UINib(nibName: nibName, bundle: nil)
      self.register(cellNib, forCellReuseIdentifier: reuseIdentifier)
   }
}

extension UIDevice {
   var modelName: String {
      var systemInfo = utsname()
      uname(&systemInfo)
      let machineMirror = Mirror(reflecting: systemInfo.machine)
      let identifier = machineMirror.children.reduce("") { identifier, element in
         guard let value = element.value as? Int8, value != 0 else { return identifier }
         return identifier + String(UnicodeScalar(UInt8(value)))
      }
      return identifier
   }
}


