//
//  UITextField+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension MKTextField {
  
  func configureWith(text: String, placeholder: String) {
    self.placeholder = placeholder
    self.text = text
    tintColor = COLOR.TITLE_UNDER_LINE_COLOR
    bottomBorderColor = COLOR.PLACEHOLDER_COLOR
    textColor = COLOR.SPLASH_TEXT_COLOR
    placeHolderColor = COLOR.PLACEHOLDER_COLOR
    font = UIFont(name: FONT.light, size: FONT_SIZE.textfieldSize)
  }
}
