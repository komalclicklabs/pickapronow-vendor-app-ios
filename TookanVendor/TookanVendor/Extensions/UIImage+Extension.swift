//
//  UIImage+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 22/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension UIImage {
  func renderWithAlwaysTemplateMode() -> UIImage {
    return self.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
  }
}
