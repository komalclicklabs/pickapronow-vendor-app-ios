//
//  UIView+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 10/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit


extension UIView {
   
   var safeAreaInsetsForAllOS: UIEdgeInsets {
      var insets: UIEdgeInsets
      if #available(iOS 11.0, *) {
         insets = safeAreaInsets
      } else {
         insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
      }
      return insets
   }
   
   func setShadow() {
      setShadowBounds()
      self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
   }
   
   func setShadowBounds() {
      self.layer.shadowOffset = CGSize(width: 2, height: 3)
      self.layer.shadowOpacity = 0.3
      self.layer.shadowRadius = 3
    self.clipsToBounds = false  
   }
  
  func showSmoothly(duration: TimeInterval = 1,completion: Completion) {
    isHidden = false
    UIView.animate(withDuration: 1.0, animations: {
      self.alpha = 1.0
    }, completion: { completed in
      completion?(completed)
    })
  }
  
  func setAlphaToZero() {
    self.alpha = 0
    isHidden = true
  }
  
  func hideSmoothly(completion: Completion) {
    UIView.animate(withDuration: 0.4, animations: {
      self.alpha = 0
    }, completion: { [weak self] completed in
      completion?(completed)
      self?.isHidden = true
    })
  }
  
  func setCornerRadius(radius:CGFloat) {
    self.layer.cornerRadius = radius
    self.layer.masksToBounds = true
  }
  
  func setBorder(borderColor: UIColor) {
    self.layer.borderColor = borderColor.cgColor
    self.layer.borderWidth = 1.0
    self.layer.masksToBounds = true
  }
   
   func setSameColorShadow() {
      setShadowBounds()
      self.layer.shadowColor = self.backgroundColor?.cgColor
   }
  
  func roundCorners(cornersToRound: UIRectCorner) {
    let path = UIBezierPath(roundedRect: self.bounds,
                            byRoundingCorners: cornersToRound,
                            cornerRadii: CGSize(width: 15, height:  15))
    
    let maskLayer = CAShapeLayer()
    
    maskLayer.path = path.cgPath
    self.layer.mask = maskLayer
  }
  
  @IBInspectable var _cornerRadius: CGFloat {
    set {
      self.layer.cornerRadius = newValue
      self.layer.masksToBounds = true
    } get {
      return self.layer.cornerRadius
    }
  }
  
}
