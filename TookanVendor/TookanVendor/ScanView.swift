//
//  ScanView.swift
//  RKScanner
//
//  Created by Rakesh Kumar on 20/07/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import AVFoundation
public protocol ScannerDelegate {
    func decodedOutput(_ value:String, index:Int)
    func dismissScannerView()
}

open class ScanView: UIView, AVCaptureMetadataOutputObjectsDelegate {
    open var delegate:ScannerDelegate!
    let session = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var ratio:CGFloat = 0.208
    var ratioHeight:CGFloat!
    var scanningLine = UIImageView()
    var indexTag:Int!
    
    open func setScanView(index:Int) {
        self.indexTag = index
        ratioHeight = self.frame.height * ratio
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let inputDevice:AVCaptureDeviceInput!
        do {
            inputDevice =  try AVCaptureDeviceInput(device: captureDevice)  //AVCaptureDeviceInput(device: captureDevice, error: &error)
            if let inp = inputDevice {
                session.addInput(inp)
            } else {
                print("error")
            }
        } catch {
            print("error")
        }
        
        addPreviewLayer()
        
        /* Check for metadata */
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        output.metadataObjectTypes = output.availableMetadataObjectTypes
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        session.startRunning()
        
        /*------------- Camera Layer -------------*/
        let scannerLayer = UIImageView(frame: self.frame)
        scannerLayer.image = UIImage(named: "scanner")
        self.addSubview(scannerLayer)
        
        /*------------- Back Button ---------------*/
        let dismissButton = UIButton()
        dismissButton.frame = CGRect(x: self.frame.width - 50, y: 20, width: 50, height: 50)
        dismissButton.setImage(UIImage(named: "close"), for: UIControlState())
        dismissButton.addTarget(self, action: #selector(ScanView.dismissScanView), for: UIControlEvents.touchUpInside)
        self.addSubview(dismissButton)
        
        /*----------- Scanning line -----------*/
        self.scanningLine.frame = CGRect(x: 50, y: 0, width: self.frame.width - 100, height: 2)
        self.scanningLine.center = CGPoint(x: self.center.x, y: self.center.y - self.ratioHeight)
        self.scanningLine.image = UIImage(named: "scanner_line")
        self.addSubview(self.scanningLine)
        
        UIView.animate(withDuration: 1.5, delay:0, options: [.repeat, .autoreverse], animations: {
            self.scanningLine.frame = CGRect(x: self.scanningLine.frame.origin.x, y: self.scanningLine.frame.origin.y + self.ratioHeight * 2, width: self.scanningLine.frame.width, height: self.scanningLine.frame.height)
            }, completion: nil)
    }
    
    /* Add the preview layer here */
    func addPreviewLayer() {
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewLayer?.bounds = self.bounds
        previewLayer?.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        self.layer.addSublayer(previewLayer!)
    }
    
    open func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        for data in metadataObjects {
            let metaData = data as! AVMetadataObject
            let transformed = previewLayer?.transformedMetadataObject(for: metaData) as? AVMetadataMachineReadableCodeObject
            if let unwraped = transformed {
                if let decodeValue = unwraped.stringValue {
                    delegate.decodedOutput(decodeValue, index: self.indexTag)
                    self.scanningLine.layer.removeAllAnimations()
                    self.session.stopRunning()
                    let systemSoundID: SystemSoundID = 1114
                    AudioServicesPlaySystemSound (systemSoundID)
                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                    Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ScanView.dismissScanView), userInfo: nil, repeats: false)
                    return
                }
            }
        }
    }
    
    func dismissScanView() {
        session.stopRunning()
        self.removeFromSuperview()
        delegate.dismissScannerView()
    }
}
