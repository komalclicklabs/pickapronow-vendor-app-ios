//
//  PickUpOptionCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/24/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
protocol PickupDeliveryDelegate {
    func pickupDeliveryLayoutType()
}

class PickUpOptionCell: UITableViewCell {

    @IBOutlet var orLabel: UILabel!
    @IBOutlet var forLabel: UILabel!
    @IBOutlet weak var pickUpButton: UIButton!
    @IBOutlet weak var deliveryButton: UIButton!
    @IBOutlet var underLine: UIView!
    @IBOutlet var bottomLIne: UIView!
    @IBOutlet var orLine: UIView!
    
    let orHeight:CGFloat = 27.0
    var delegate:PickupDeliveryDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        /*----------- For Label -------------*/
        forLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        forLabel.text = TEXT.FOR
        forLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        
        /*------------ Pickup button ------------------*/
        self.pickUpButton.setTitleColor(COLOR.PLACEHOLDER_COLOR, for: .normal)
        self.pickUpButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: .selected)
        self.pickUpButton.setTitle(TEXT.PICKUP, for: UIControlState.normal)
        self.pickUpButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        
        /*----------- Delivery Button ----------------*/
        self.deliveryButton.setTitleColor(COLOR.PLACEHOLDER_COLOR, for: .normal)
        self.deliveryButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: .selected)
        self.deliveryButton.setTitle(TEXT.DELIVERY, for: UIControlState.normal)
        self.deliveryButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        

        orLabel.textColor = COLOR.SPLASH_LINE_COLOR
        orLabel.text = TEXT.OR
        orLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        orLabel.layer.cornerRadius = orHeight / 2
        orLabel.layer.borderColor = COLOR.SPLASH_LINE_COLOR.cgColor
        orLabel.layer.borderWidth = 0.9

        /*----------- Or line -------------*/
        orLine.backgroundColor = COLOR.SPLASH_LINE_COLOR

        /*----------- Bottom Line -----------*/
        bottomLIne.backgroundColor = COLOR.SPLASH_LINE_COLOR
        
        /*----------- underline -----------*/
        underLine.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
    }
    
    func setOptionsAsPerConfig() {
        self.contentView.isHidden = false
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                self.setOnlyPickup()
                Singleton.sharedInstance.createTaskDetail.hidePickupDelivery = true
                break
            case PICKUP_DELIVERY.delivery.rawValue?:
                self.setOnlyDelivery()
                Singleton.sharedInstance.createTaskDetail.hidePickupDelivery = true
                break
            case PICKUP_DELIVERY.both.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hidePickupDelivery = false
                self.setForBothPickupDelivery()
                break
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hidePickupDelivery = true
                self.hidePickupDelivery()
                break
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?:
            self.hidePickupDelivery()
            break
        case WORKFLOW.fieldWorkforce.rawValue?:
            self.hidePickupDelivery()
            break
        default:
            break
        }
    }
    
    func setOnlyPickup() {
        self.pickUpButton.isSelected = true
        self.deliveryButton.isSelected = false
        Singleton.sharedInstance.createTaskDetail.hasPickup = 1
        Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
        self.deliveryButton.isHidden = true
        self.orLabel.isHidden = true
        self.orLine.isHidden = true
        self.underLine.isHidden = true
        self.pickUpButton.transform = CGAffineTransform(translationX: 77.0, y: 0)
    }

    func setOnlyDelivery() {
        self.pickUpButton.isSelected = false
        self.deliveryButton.isSelected = true
        Singleton.sharedInstance.createTaskDetail.hasPickup = 0
        Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
        self.pickUpButton.isHidden = true
        self.orLabel.isHidden = true
        self.orLine.isHidden = true
        self.underLine.isHidden = true
        self.deliveryButton.transform = CGAffineTransform(translationX: -82.0, y: 0)
    }
    
    func setForBothPickupDelivery() {
        if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
            self.setPickupData()
        } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
            self.setDeliveryData()
        }
    }
    
    func setPickupData() {
        self.pickUpButton.isSelected = true
        self.deliveryButton.isSelected = false
        Singleton.sharedInstance.createTaskDetail.hasPickup = 1
        Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
        self.resetUnderLine()

    }
    
    func setDeliveryData() {
        self.pickUpButton.isSelected = false
        self.deliveryButton.isSelected = true
        Singleton.sharedInstance.createTaskDetail.hasPickup = 0
        Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
        self.animateUnderline()
    }
    
    func hidePickupDelivery() {
        self.contentView.isHidden = true
    }
    
    @IBAction func pickupAction(_ sender: AnyObject) {
        if pickUpButton.isSelected == false {
            self.setPickupData()
            delegate.pickupDeliveryLayoutType()
        }
    }
    
    @IBAction func deliveryAction(_ sender: AnyObject) {
        if deliveryButton.isSelected == false {
            self.setDeliveryData()
            delegate.pickupDeliveryLayoutType()
        }
    }
    
    
    func animateUnderline() {
        UIView.animate(withDuration: 0.2) {
            self.underLine.transform = CGAffineTransform(translationX: self.underLine.frame.width, y: 0.0)
        }
    }
    
    func resetUnderLine() {
        UIView.animate(withDuration: 0.2) { 
            self.underLine.transform = CGAffineTransform.identity
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
