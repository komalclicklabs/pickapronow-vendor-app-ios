//
//  ActivityIndicator.swift
//  Butlers
//
//  Created by Rakesh Kumar on 5/20/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import UIKit
import DGActivityIndicatorView

class ActivityIndicator:UIView {
    
    var titleView : PleaseWaitView?
    
    var pleaseWait:Bool{
        set{
            switch  newValue {
            case true:
                titleView = UINib(nibName: NIB_NAME.PleaseWaitView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! PleaseWaitView
                titleView?.frame = CGRect(x: 0, y: self.frame.maxY - 103, width: SCREEN_SIZE.width, height: 103)
                self.addSubview(titleView!)
                titleView?.alpha = 0
                UIView.animate(withDuration: 0.5, animations: { 
                    self.titleView?.alpha = 1
                })
            default:
                if titleView != nil{
                    titleView?.removeFromSuperview()
                }
            }
        }
        
        get{
            return false
        }
        
    }
    
    struct Static {
        
        static var instance: ActivityIndicator?
        static var token: Int = 0
    }
    
    private static var __once: () = {
            Static.instance = ActivityIndicator(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        }()
    
    class var sharedInstance: ActivityIndicator {
        _ = ActivityIndicator.__once
        return Static.instance!
    }
    
    var activityIndicator:DGActivityIndicatorView!
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.backgroundColor = COLOR.TRANS_COLOR
        self.clipsToBounds = true
    }

    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showActivityIndicator(_ controller:UIViewController){
        self.setActivityIndicator()
        controller.view.addSubview(self)
    }
    
    func showActivityIndicator(){
        self.setActivityIndicator()
        //if pleaseWeight != nil && pleaseWeight == true{
        
       // }
        (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(self)
    }
    
    func setActivityIndicator() {
        DispatchQueue.main.async(execute: { () -> Void in
            if(self.activityIndicator == nil)
            {
                self.activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballTrianglePath, tintColor: .white, size: 40.0)
            }
            
         
            self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
            self.activityIndicator.center = self.center
            self.activityIndicator.clipsToBounds = true
            self.activityIndicator.startAnimating()
            self.addSubview(self.activityIndicator)
        })
    }
    
    func hideActivityIndicator(){
        pleaseWait = false
        DispatchQueue.main.async(execute: { () -> Void in
            if(self.activityIndicator != nil)
            {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.removeFromSuperview()
                self.activityIndicator = nil
                self.removeFromSuperview()
            }
        })
    }
    
    
}
