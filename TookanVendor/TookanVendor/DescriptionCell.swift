//
//  DescriptionCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/24/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//


import UIKit

protocol DescriptionDelegate {
    func updateDescriptionCellHeight(height:CGFloat, descriptionText:String)
    func textviewDidBegin()
    func showBarcodePopup(cell:DescriptionCell)
}

class DescriptionCell: UITableViewCell, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var upperLeftCheckboxHeight: NSLayoutConstraint!
    @IBOutlet weak var upperLeftCheckBox: UIButton!
    @IBOutlet var rightButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet var rightButton: UIButton!
    @IBOutlet weak var bottomUnderLine: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var placeholder: UILabel!
    
    var delegate:DescriptionDelegate!
    let rightButtonWidth:CGFloat = 50.0
    var fieldType: CustomFieldTypes!
    var scannerFlag = true
    var checkboxLabel:String!
    var pickerView = UIPickerView()
    var dropdownArray = [String]()
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionTextView.delegate = self
        self.selectionStyle = .none
        /*--------- Header Label -------------*/
        headerLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        headerLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        
        /*------------- Placeholder label -----------*/
        placeholder.textColor = COLOR.PLACEHOLDER_COLOR
        placeholder.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.large)
        
        /*---------- Text View -------------*/
        descriptionTextView.textColor = COLOR.SPLASH_TEXT_COLOR
        descriptionTextView.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.large)
        descriptionTextView.delegate = self
        descriptionTextView.tintColor = COLOR.THEME_FOREGROUND_COLOR
        
        /*--------- UnderLine ------------*/
        bottomUnderLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
    }
    
    func setDescriptionView() {
        self.rightButton.isHidden = true
        self.rightButtonWidthConstraint.constant = 0.0
        self.upperLeftCheckboxHeight.constant = 0.0
        self.descriptionTextView.keyboardType = .default
        self.descriptionTextView.autocapitalizationType =  .sentences
        self.descriptionTextView.inputView = nil
        self.descriptionTextView.text = Singleton.sharedInstance.createTaskDetail.jobDescription
        self.headerLabel.text = TEXT.DESCRIPTION
        self.placeholder.text = TEXT.ENTER_DESCRIPTION
        self.setPlaceholder()
    }
    
    func setTextCustomField(label:String, text:String) {
        self.descriptionTextView.inputView = nil
        self.rightButton.isHidden = false
        self.rightButton.isSelected = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = 0
        self.descriptionTextView.keyboardType = .default
        self.descriptionTextView.autocapitalizationType =  .sentences
        self.descriptionTextView.text = text
        self.headerLabel.text = label
        self.placeholder.text = TEXT.ENTER + " " + label
        self.rightButton.setImage(#imageLiteral(resourceName: "iconTextInactive").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.setPlaceholder()
    }
    
    func setNumberCustomField(label:String, text:String) {
        self.descriptionTextView.inputView = nil
        self.rightButton.isHidden = false
        self.rightButton.isSelected = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = 0
        self.descriptionTextView.keyboardType = .numberPad
        self.descriptionTextView.text = text
        self.headerLabel.text = label
        self.placeholder.text = TEXT.ENTER + " " + label
        self.rightButton.setImage(#imageLiteral(resourceName: "iconNumberInactive").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.setPlaceholder()
    }
    
    func setEmailCustomField(label:String, text:String) {
        self.descriptionTextView.inputView = nil
        self.rightButton.isHidden = false
        self.rightButton.isSelected = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = 0
        self.descriptionTextView.keyboardType = .emailAddress
        self.descriptionTextView.text = text
        self.headerLabel.text = label
        self.placeholder.text = TEXT.ENTER + " " + label
        self.rightButton.setImage(#imageLiteral(resourceName: "iconEmailInactive").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.setPlaceholder()
    }
    
    func setUrlCustomField(label:String, text:String) {
        self.descriptionTextView.inputView = nil
        self.rightButton.isHidden = false
        self.rightButton.isSelected = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = 0
        self.descriptionTextView.keyboardType = .URL
        self.descriptionTextView.text = text
        self.headerLabel.text = label
        self.placeholder.text = TEXT.ENTER + " " + label
        self.rightButton.setImage(#imageLiteral(resourceName: "iconUrlInactive").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.setPlaceholder()
    }
    
    func setTelephoneCustomField(label:String, text:String) {
        self.descriptionTextView.inputView = nil
        self.rightButton.isHidden = false
        self.rightButton.isSelected = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = 0
        self.descriptionTextView.keyboardType = .phonePad
        self.descriptionTextView.text = text
        self.headerLabel.text = label
        self.placeholder.text = TEXT.ENTER + " " + label
        self.rightButton.setImage(#imageLiteral(resourceName: "iconContactUnfilled").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.setPlaceholder()
    }
    
    func setBarcodeCustomField(label:String, text:String) {
        self.descriptionTextView.inputView = nil
        self.rightButton.isHidden = false
        self.rightButton.isSelected = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = 0
        self.descriptionTextView.keyboardType = .default
        self.descriptionTextView.text = text
        self.headerLabel.text = label
        self.placeholder.text = TEXT.ENTER + " " + label
        self.rightButton.setImage(#imageLiteral(resourceName: "iconScanInactive").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.setPlaceholder()
    }
    
    func setCheckboxCustomField(label:String, text:String) {
        self.descriptionTextView.inputView = nil
        self.checkboxLabel = label
        self.rightButton.isHidden = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = rightButtonWidth
        self.descriptionTextView.keyboardType = .default
        if text == "true" {
            self.rightButton.isSelected = true
            self.upperLeftCheckBox.isSelected = true
            self.descriptionTextView.text = ""//label + " " + TEXT.OPTION_CHECKED
        } else {
            self.rightButton.isSelected = false
            self.upperLeftCheckBox.isSelected = false
            self.descriptionTextView.text = ""
        }

        self.headerLabel.text = label
        self.placeholder.text = ""//TEXT.CHECK_THIS_OPTION + " " + label
        self.rightButtonWidthConstraint.constant = 0.0
        self.rightButton.setImage(#imageLiteral(resourceName: "iconCheckboxUnticked"), for: UIControlState.normal)
        self.rightButton.setImage(#imageLiteral(resourceName: "iconCheckboxTicked"), for: UIControlState.selected)
        self.upperLeftCheckBox.setImage(#imageLiteral(resourceName: "iconCheckboxUnticked"), for: UIControlState.normal)
        self.upperLeftCheckBox.setImage(#imageLiteral(resourceName: "iconCheckboxTicked"), for: UIControlState.selected)
        self.setPlaceholder()
    }
    
    func setDropdownCustomField(label:String, text:String, dropDownValues:String) {
        self.dropdownArray = dropDownValues.components(separatedBy: ",")
        self.rightButton.isSelected = false
        self.rightButtonWidthConstraint.constant = rightButtonWidth
        self.upperLeftCheckboxHeight.constant = 0
        self.descriptionTextView.inputView = pickerView
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.descriptionTextView.text = text
        self.headerLabel.text = label
        self.placeholder.text = TEXT.SELECT_OPTION
        self.rightButton.setImage(#imageLiteral(resourceName: "iconDropdownClosed"), for: UIControlState.normal)
        self.rightButton.setImage(#imageLiteral(resourceName: "iconDropdownOpen"), for: UIControlState.selected)

        self.setPlaceholder()
    }
    
    func setPlaceholder() {
        if self.descriptionTextView.text == "" {
            //self.heightConstraint.constant = 40.0
            placeholder.isHidden = false
            self.setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.inActive)
        } else {
            placeholder.isHidden = true
            self.setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.filled)
        }
    }

    //MARK: UITEXTVIEW DELEGATE
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty == true {
          //  self.heightConstraint.constant = 40.0
            placeholder.isHidden = false
        } else {
           // let size = self.descriptionTextView.text.heightWithConstrainedWidth(width: self.descriptionTextView.frame.width - 20, font: UIFont(name: FONT.ultraLight, size: 19.0)!)
           // self.heightConstraint.constant = size.height + 20
            placeholder.isHidden = true
        }
        switch fieldType! {
        case .description:
            Singleton.sharedInstance.createTaskDetail.jobDescription = self.descriptionTextView.text
            break
        case .Text:
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:textView.tag)
            customField.data = self.descriptionTextView.text.trimText
            break
        case .Number:
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:textView.tag)
            customField.data = self.descriptionTextView.text.trimText
            break
        case .email:
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:textView.tag)
            customField.data = self.descriptionTextView.text.trimText
            break
        case .url:
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:textView.tag)
            customField.data = self.descriptionTextView.text.trimText
            break
        case .phonenumber:
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:textView.tag)
            customField.data = self.descriptionTextView.text.trimText
            break
        case .BarCode:
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:textView.tag)
            customField.data = self.descriptionTextView.text.trimText
            break
        default:
            break
        }
        delegate.updateDescriptionCellHeight(height:0, descriptionText: self.descriptionTextView.text)
    }
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        switch fieldType! {
        case .checkBox:
            //self.setCheckbox()
            return false
        default:
            return true
        }
    }
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
       // delegate.textviewDidBegin()
        switch fieldType! {
        case .BarCode:
            if scannerFlag == true{
                self.endEditing(true)
                delegate.showBarcodePopup(cell: self)
            } else {
                self.shrinkUnderLine()
            }
            break
        case .checkBox:
            self.setCheckbox()
            break
        case .DropDown:
            self.rightButton.isSelected = true
            break
        default:
            self.shrinkUnderLine()
            break
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.showInactiveUnderline()
        switch fieldType! {
        case .BarCode:
            self.scannerFlag = true
            break
        case .DropDown:
            self.setDropdownValue()
            self.rightButton.isSelected = false
            break
        default:
            break
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func shrinkUnderLine() {
        UIView.animate(withDuration: 0.2, animations: {
            self.bottomUnderLine.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }, completion: { finished in
                self.showActiveUnderLine()
        })
    }
    
    func showActiveUnderLine() {
        UIView.animate(withDuration: 0.2, animations: {
            self.bottomUnderLine.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
            self.bottomUnderLine.transform = CGAffineTransform.identity
            self.setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.active)
            }, completion: { finished in
        })
    }
    
    func showInactiveUnderline() {
        self.bottomUnderLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
        self.bottomUnderLine.transform = CGAffineTransform.identity
        if self.descriptionTextView.text?.length == 0 {
            self.setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.inActive )
        } else {
            self.setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.filled )
        }
    }
    
    func setActiveInActiveStatus(yourState:String) {
        print(yourState)
        switch yourState {
        case ACTIVE_INACTIVE_STATES.active:
            self.rightButton.tintColor = COLOR.ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.inActive:
            self.rightButton.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.filled:
            self.rightButton.tintColor = COLOR.FILLED_IMAGE_COLOR
        default :
            self.rightButton.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        }
    }
    
    @IBAction func rightButtonAction(_ sender: AnyObject) {
        switch fieldType! {
        case .BarCode:
            if scannerFlag == true{
                self.endEditing(true)
                delegate.showBarcodePopup(cell: self)
            } else {
                self.shrinkUnderLine()
            }
            break
        case .checkBox:
            self.setCheckbox()
            break
        case .DropDown:
            if rightButton.isSelected == false {
                self.descriptionTextView.becomeFirstResponder()
            } else {
                self.descriptionTextView.resignFirstResponder()
            }
        default:
            self.descriptionTextView.becomeFirstResponder()
            break
        }
    }
    
    func setCheckbox() {
        self.rightButton.isSelected = !self.rightButton.isSelected
        self.upperLeftCheckBox.isSelected = !self.upperLeftCheckBox.isSelected
        let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.descriptionTextView.tag)
        if self.rightButton.isSelected == true {
            customField.data = "true"
            self.descriptionTextView.text = ""
        } else {
            customField.data = "false"
            self.descriptionTextView.text = ""
        }
        self.setPlaceholder()
        self.endEditing(true)
    }
    
    func setDropdownValue() {
        let selectedValue = dropdownArray[pickerView.selectedRow(inComponent: 0)]
        let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.descriptionTextView.tag)
        customField.data = selectedValue
        self.descriptionTextView.text = selectedValue
        self.setPlaceholder()
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    
    //MARK: UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.setDropdownValue()
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let attributedString = NSMutableAttributedString(string: dropdownArray[row])
        return attributedString
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return dropdownArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}
