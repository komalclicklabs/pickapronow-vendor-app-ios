//
//  OrderIDCallInfoCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/16/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class OrderIDCallInfoCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet var taskIdLabel: UILabel!
    @IBOutlet var taskId: UILabel!
    @IBOutlet var driverLabel: UILabel!
    @IBOutlet var driver: UILabel!
    @IBOutlet var callButton: UIButton!
    @IBOutlet var messageButton: UIButton!
    @IBOutlet var seperatorline: UIView!
    @IBOutlet var jobStatus: UILabel!
    @IBOutlet weak var fareLabel: UILabel!
    @IBOutlet weak var fare: UILabel!
    @IBOutlet weak var fareUpperConstraint: NSLayoutConstraint!
    @IBOutlet weak var fareLowerConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var driverDetails: UILabel!
    
    var number = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        /*-------------- BackView ------------------*/
        self.backView.backgroundColor = UIColor.white
        self.backView.layer.cornerRadius = 4.0
        self.backView.layer.shadowOffset = CGSize(width: 2, height: 3)
        self.backView.layer.shadowOpacity = 0.7
        self.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        
        /*----------------- Header Label ---------------*/
        self.taskIdLabel.textColor = COLOR.PLACEHOLDER_COLOR
        self.taskIdLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.carTimeLabelSize)
        
        let endIndex = Singleton.sharedInstance.formDetailsInfo.call_tasks_as.index(Singleton.sharedInstance.formDetailsInfo.call_tasks_as.endIndex, offsetBy: -1)
        
         let calTaskAs = Singleton.sharedInstance.formDetailsInfo.call_tasks_as[Singleton.sharedInstance.formDetailsInfo.call_tasks_as.index(before: Singleton.sharedInstance.formDetailsInfo.call_tasks_as.endIndex)] == "s" ? Singleton.sharedInstance.formDetailsInfo.call_tasks_as.substring(to: endIndex).lowercased() :  Singleton.sharedInstance.formDetailsInfo.call_tasks_as
        
        
        self.taskIdLabel.text = calTaskAs.capitalized + " " + "ID"

        self.driverLabel.textColor = COLOR.PLACEHOLDER_COLOR
        self.driverLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.carTimeLabelSize)
        self.driverLabel.text = TEXT.DRIVER
        
        /*----------------- Text Label -------------*/
        self.taskId.textColor = COLOR.SPLASH_TEXT_COLOR
        self.taskId.font = UIFont(name: FONT.bold, size: FONT_SIZE.medium)
        self.taskId.text = "######"
        
        self.driver.textColor = COLOR.SPLASH_TEXT_COLOR
        self.driver.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.driver.text = TEXT.NO_NAME

        /*------------------ Status ----------------*/
        self.jobStatus.textColor = UIColor.white
        self.jobStatus.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        self.jobStatus.text = TEXT.FAILED_STATUS
        self.jobStatus.backgroundColor = COLOR.PLACEHOLDER_COLOR
        self.jobStatus.layer.cornerRadius = 4.0
        self.jobStatus.layer.masksToBounds = true
        self.jobStatus.layer.cornerRadius = 4.0
        self.jobStatus.layer.shadowOffset = CGSize(width: 2, height: 3)
        self.jobStatus.layer.shadowOpacity = 0.7
        self.jobStatus.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        
        ///// FARE
        self.fareLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.carTimeLabelSize)
        self.fareLabel.text = TEXT.Fare
        self.fare.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.fare.textColor = COLOR.SPLASH_TEXT_COLOR
        self.fareLabel.textColor = COLOR.PLACEHOLDER_COLOR
        self.fareLabel.textColor = COLOR.PLACEHOLDER_COLOR
        
        self.driverDetails.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.driverDetails.textColor = COLOR.SPLASH_TEXT_COLOR
        
        self.backView.backgroundColor = COLOR.popUpColor
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        
        
        
        // Initialization code
    }
    
    @IBAction func callButtonAction(_ sender: UIButton) {
        let phoneNumberUrlString = "tel://\(number.replacingOccurrences(of: " ", with: ""))"
      guard let phoneUrl = URL(string: phoneNumberUrlString), UIApplication.shared.canOpenURL(phoneUrl) else {
         ErrorView.showWith(message: TEXT.unableToCall, removed: nil)
         return
      }
      UIApplication.shared.openURL(phoneUrl)
      
    }
   
    func hidePayment()  {
        fareLabel.text = ""
        fare.text = ""
        fareLowerConstraint.constant = 0
        fareUpperConstraint.constant = 8
    }
    
    func showPayment(value:String)  {
        self.fareLabel.text = TEXT.Fare
        fare.text = value
        fareLowerConstraint.constant = 10
        fareUpperConstraint.constant = 10
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
