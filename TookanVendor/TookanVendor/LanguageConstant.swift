//
//  LanguageConstant.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class LanguageConstant: NSObject {

}


struct TEXT {
    static let enterEmailToContinue = "Enter email to continue".localized
   static let welcomeBack = "Welcome back,".localized
   static let welcome = "Welcome".localized
    static let SIGN_UP = "Sign Up".localized
    static let NEW_ACCOUNT = "for a new account".localized
    static let NEW_USER = "New User?".localized
    static let SIGN_IN = "Sign In".localized
    static let WITH_EMAIL = "with your email".localized
    static let OR = "or".localized
    static let OR_CONTINUE = "or continue with".localized
    static let CONTINUE = "Continue".localized
    static let CONTINUE_WITH = "Continue with".localized
    static let TO = "to".localized
    static let YOUR_EMAIL = "Your email".localized
    static let YOUR_PASSWORD = "Your Password".localized
    static let PASSWORD = "Password".localized
    static let VIEW_PASSWORD = "View Password".localized
    static let HIDE_PASSWORD = "Hide Password".localized
    static let FORGOT_PASSWORD = "Forgot Password?".localized
    static let RETRY = "Retry".localized
    static let SIGN_IN_TITLE = "Sign in to your\naccount!".localized
    static let SIGN_UP_TITLE = "Sign up for a new\naccount".localized
    static let HAVE_AN_ACCOUNT = "Have an account?".localized
    static let I_AGREE_TO = "I agree to".localized
    static let TERMS_CONDITIONS = "Terms & Conditions".localized
    static let REFERRAL_CODE = "Referral code".localized
    static let VERIFY_MOBILE = "Verify OTP".localized
    static let VERIFY = "Verify".localized
    static let OTP_DESCRIPTION = "Enter the OTP that you've received on your registered phone number".localized
    static let RESEND_OTP = "Resend OTP".localized
    static let CHANGE_NUMBER = "Change Number".localized
    static let WITH = "with".localized
    static let NEXT = "Next".localized
    static let SKIP = "Skip".localized
    static let YOUR_NAME = "Your Name".localized
    static let YOUR_CONTACT = "Your Contact".localized
    static let YOUR_PHONE = "Your phone".localized
    static let FORGOT_PASS_TITLE_ = "Forgot your ".localized
    static let FORGOT_PASS_BOLD = "password?".localized
    static let FORGOT_ENTER_EMAIL_FOR_LINK_LABEL = "Enter your email address and we’ll send you a link to reset the password".localized
    static let SEND_LINK_BUTTON = "Send Link".localized
    static let EMAIL = "Email".localized
    static let PHONE = "Phone number".localized
    static let DESCRIPTION = "Description".localized
    static let ENTER_DESCRIPTION = "Enter description".localized
    static let NAME = "Name".localized
    static let DATE = "Date".localized
    static let PICK_BEFORE = "Pick Before".localized
    static let DELIVER_BEFORE = "Deliver Before".localized
    static let CONTACT = "Contact".localized
    static let IMAGE = "Image".localized
    static let UPLOAD = "Upload".localized
    static let CHANGE_PASSWORD_PROFILE = "Change Password".localized
    static let LOGOUT_BUTTON = "Logout".localized
    static let PAYMENT_OPTION = "Payment Methods".localized
    static let COMPANY_PLACEHOLDER = "Company".localized
    static let ADDRESS_PLACEHODLER = "Address".localized
    static let EDIT_PROFILE = "Edit Profile".localized
    static let CONTACT_NUMBER = "Contact Number".localized
    static let ENTER_DATE = "Enter Date".localized
    static let PICKUP = "Pick-up".localized
    static let DELIVERY = "Delivery".localized
    static let FOR = "For".localized
    static let GOTO = "Go to".localized
    static let ENTER_LOCATION = "Enter Location".localized
    static let ADD_LOCATION = "Add Location".localized
    static let ADD_DELIVERY_DETAILS = "Add Delivery".localized + "\n" + "Details".localized
    static let SKIP_DELIVERY_DETAILS = "Skip Delivery".localized + "\n" + "Details".localized
    static let SAVE_REVIEW = "Save and".localized + "\n" + "Review".localized
    static let RESET = "Reset".localized
    static let ADD_DELIVERY_LOCATION = "Add Delivery Location".localized
    static let ADD_PICKUP_LOCATION = "Add Pickup Location".localized
    static let START_TIME = "Start Time".localized
    static let END_TIME = "End Time".localized
    static let YOU_HAVE = "You have".localized
    static let SET = "set".localized
    static let DELETE = "Delete".localized
    static let EDIT = "Edit".localized
    static let SAVE = "Save".localized
    static let DATE_TIME = "Date & Time".localized
    static let NO_NAME = "No name entered".localized
    static let NO_ADDRESS = "No address entered".localized
    static let NO_DATETIME = "No datetime selected".localized
    static let ENTER = "Enter".localized
    static let BARCODE = "Barcode".localized
    static let SCAN = "Scan".localized
    static let MANUAL = "Manual".localized
    static let CHECK_THIS_OPTION = "Check".localized
    static let OPTION_CHECKED = "Checked".localized
    static let SELECT_OPTION = "Select option".localized
    static let SKIP_TO_REVIEW = "Back to Review".localized
    static let LOADING = "Loading...".localized
    static let GETTING_ADDRESS = "Getting address...".localized
    static let TYPE = "Type".localized
    static let PULL_TO_REFRESH = "Pull to refresh".localized
    static let DRIVER = "Driver".localized
    static let NOT = "Not".localized
    static let ACTIVITY_TIMELINE = "Activity Timeline".localized
    static let ADD_NEW = "Add new".localized
    static let CARD_TEXT = "card".localized
    static let CARD_DETAILS = "card details".localized
    static let CARD_NUMBER = "Card Number".localized
    static let VALID_THRU = "Valid Thru".localized
    static let MAKE_A = "Make a".localized//.localized was missing
    static let VIEW = "View".localized
    static let Delete_Card = "Delete Card".localized
    static let ASSIGNED_STATUS = "Assigned".localized
    static let STARTED_STATUS = "Started".localized
    static let SUCCESSFUL_STATUS = "Successful".localized
    static let FAILED_STATUS = "Failed".localized
    static let ARRIVED_STATUS = "Arrived".localized
    static let PARTIAL_STATUS = "Partial".localized
    static let UNASSIGNED_STATUS = "Unassigned".localized
    static let ACCEPTED_STATUS = "Accepted".localized
    static let DECLINED_STATUS = "Declined".localized
    static let CANCELED_STATUS = "Canceled".localized
    static let IGNORED_STATUS = "Ignored".localized
    static let ADD_IMAGE = "Add Image".localized
    static let NO = "No".localized
    static let FOUND = "found".localized
    static let Tap_to_complete_the_booking = "Click here.".localized
    static let CHANGE_PASS_TITLE_REGULAR = "Change ".localized
    static let CHANGE_PASS_INFO_LABEL = "Please enter your new password. Include a special character in it.".localized
    static let CHANGE_OLD_PASS_PLACEHOLDER = "Old Password".localized
    static let CHANGE_PASS_FILED_PLACEHOLDER = "New Password".localized
    static let ARE_YOU_WANT_LOGOUT_ALERT = "Are you sure want to logout?".localized
    static let YES_ACTION = "Yes".localized
    static let CHOOSE_IMAGE = "Choose image".localized
    static let GALLERY = "Gallery".localized
    static let CAMERA = "Camera".localized
    static let CAMERA_NOT_ACCESSIBLE = "Access to Camera is not possible. Please check settings.".localized
    static let GALLERY_NOT_ACCESSIBLE = "Access to Gallery is not possible. Please check settings.".localized
    static let ALL = "All".localized
    static let DETAILS = "Details".localized
    static let TRACK = "Track".localized
    static let HISTORY = "History".localized
    static let CURRENT = "Current".localized
    static let TUTORIALHEADING = "You’ve successfully created your booking!".localized
    static let TUTORIALCONTENTDEL = "Thank you for trying out the Tookan deliveries app.".localized
    static let TUTORIALCONTENTAPOINTMENT = "Thank you for trying out the Tookan appointments app".localized
    static let TOCHECKOUT = "To check out a live version of this app linked to".localized
    static let YOURTOOKANBOLD = "Your Tookan Account".localized
    static let CREATEDSPECIALY = "& created especially for".localized
    static let YOURBRANDBOLD = "Your Brand,".localized
    static let GET_IN_TOUCH = "Get in Touch".localized
    static let SUBMIT = "Submit".localized
    static let ADD_COMMENT = "Add a comment…".localized
    static let DONE = "Done".localized
    static let ADD_NEW_PAYMENT = "Add a new payment".localized
    static let METHOD = "method".localized
    static let YOUR_PAYMENT = "Your payment".localized
    static let PAYMENT = "payment".localized
    static let TOTAL_AMMOUNT = "Total amount to be paid".localized
    static let CHOOSE_FROM_TEXT = "Choose from one of the methods below".localized
    static let DELETE_CARD_POPUP_TEXT = "Are you sure you want to delete this card ?".localized
    static let FORCE_PICKUP_DELIVERY_MESSAGE = "Please enter both delivery and pick up details before proceeding.".localized
    static let OK_TEXT = "Ok".localized
    static let NO_CARDS_FOUND = "No cards found".localized
    static let ENTER_OTP = "Enter one time password".localized
    static let otpErrorMessage = "Invalid Otp".localized
    static let RideNow = "Ride Now".localized
    static let DropLocation = "Drop Location".localized
    static let PickUpLocation = "Pick Up Location".localized
    static let noCarsFound = "No vehicle".localized
    static let getFareEstimate = "Get Fare Estimate".localized
    static let PLEASE = "please".localized
    static let WAIT = "wait".localized
    static let WHILE_WE_FETCH = "While we are fetching a vehicle for you…".localized
    static let pleaseEnterTheDropLocation = "Please enter the drop off location before proceeding".localized
    static let CancelRide = "Cancel Task".localized
    static let NoCarsFoundMessage = "No vehicle found of this type".localized
    static let Distance_Travelled = "Distance".localized
    static let PayByCash = "Pay by Cash".localized
    static let RateYour = "Rate your".localized
    static let Fare = "Fare".localized
    static let EnterNewNuberLabel = "Enter New Number".localized
    static let CANCEL = "Cancel".localized
    static let APPLY = "Apply".localized
    static let CONFIRM = "Confirm".localized
    static let pleaseEnterTheRating = "Please enter the rating for the driver.".localized
    static let ScheduleText = "Schedule".localized
    static let cancelPopUpMessage = "Are you sure you want to cancel this task ?".localized
    static let bookrequestMessage = "Are you sure you want to book this task ?".localized
    static let estimatedCost = "Total estimated cost is".localized
    static let pendingAmount = "Pending amount to be paid.".localized
    static let scheduleErrorMessage = "Selected time should be 30 minutes prior to current time.".localized
    static let goToPinText = "Go to Pin".localized
    static let tapOnPlus = "Tap on “+” to add another method".localized
    static let scheduled = "Scheduled".localized
    static let onTheWay = "On The Way".localized
    static let completed = "Completed".localized
    static let GettingAddress = "Getting Address..".localized
    static let selectA = "Select a".localized
    static let paymentMethod = " Payment Method" //used as payment methods
    static let Service = "Service".localized
    static let PleaseSelectOneItem = "Please select atleast one product before proceeding".localized
    static let addClothesToBas = "Add clothes to the".localized
    static let basket = "basket".localized
    static let addString  = "Add".localized
    static let ReviewThe = "Review the".localized
    static let SetAddress = "Set Address".localized
    static let areYouSureYouWantToDelete = "Are you sure you want to delete this task ?".localized
    static let unavailableText = "unavailable".localized
    static let specialCharecterString = "The new password must contain a special character.".localized
    static let refferAndEarn = "Refer and Earn".localized
    static let howItWorks =  "How it works?".localized
    static let tapTheBox = "tap the box to copy the referral code".localized
    static let shareOn = "Share on".localized
    static let AddAnotherMethod = "Add another method".localized
    static let AddDropLocation = "Add Drop Location".localized
    static let ApplyPromocode = "Promos".localized
    static let BookText = "Book".localized
    static let subtotal = "Subtotal".localized
    static let serviceTax = "Service Tax".localized
    static let VAT = "VAT".localized
    static let accountCredits = "Account Credits".localized
    static let total = "Total".localized
    static let Discount = "Discount".localized
    static let addACouponcode = "Add a promo code".localized
    static let Credits = "Credits".localized
    static let tip = "Tip".localized
    static let Taxi = "Taxi".localized
    static let Laundry = "Laundry".localized
    static let APP_UPDATE_NORMAL = "New Version of".localized + " \(APP_NAME) " + "is available on App Store do you want to download?".localized
    static let APP_UPDATE_FORCED = "New Version of".localized + " \(APP_NAME) " + "is available on App Store. Please download to continue.".localized
    static let DOWNLOAD = "Download".localized
    static let EMERGENCY_SOS = "Emergency SOS".localized
    static let EMERGENCY_NOTIFY_ADMIN = "Are you sure you want to\nnotify the administrator at\n".localized
    static let EMERGENCY_REQUEST_SENT = "We have received your request. Our team will be in touch shortly.".localized
    static let DISABLE_EMERGENECY = "DISABLE EMERGENCY MODE".localized
    static let searchYourAddress = "Search your Address".localized
    static let FavoritesText = "FAVORITES".localized
    static let HomeAddress = "Home Address".localized
    static let WorkAddress = "Work Address".localized
    static let addAnotherAddress = "Add Another Address".localized
    static let SearchYourWork = "Search your work address".localized
    static let SearchYourHome = "Search your home address".localized
    static let wouldYouLikeToTip = "Would you like to tip".localized
    static let checkout = "Checkout".localized
    static let clearCartAlert = "Are you sure, you want to remove the added products?".localized
    static let deleteImage = "Are you sure, you want to delete this image?".localized
    static let deleteLocation = "Are you sure you want to delete this address?".localized
    static let saveAddress = "Save Address".localized
    static let label = "Label".localized
    static let landmark = "Landmark".localized
    static let flatNo = "Flat No. / House No.".localized
    static let selectFrommap = "Select from Map".localized
    static let AppointmentDetails = "Appointment Details".localized
    static let DeliveryDetails = "Delivery Details".localized
    static let pickUpDetails = "Pick-Up Details".localized
    static let Notes = "Notes".localized
    static let Cart = "Cart".localized
    static let error = "error".localized
    static let CustomerName = "Customer Name".localized
    static let DeliveryAddress = "Delivery Address".localized
    static let AppointmentAddress = "Appointment Address".localized
    static let DeliveryTime = "Latest deliver by time".localized
    static let PhoneNumber = "Phone Number".localized
    static let PickUpTime  = "Ready for pickup".localized
    static let PickUpAddress = "Pick up Address".localized
   
   static let toContinue = "to continue".localized
   static let select = "Select".localized
   
   static let cartDeletionWarning = "If you go back items in your cart will be lost. Do you still want to continue?".localized
   static let rateTheService = "Rate the Service".localized
   static let writeAReview = "Write a review".localized
   
   static let proceedToPay = "Proceed to pay".localized
   static let pleaseAddAPromoCode = "Please add a Promocode before proceeding.".localized
   
   static let unableToCall = "Call feature not available in current device".localized
   static let additionalInfo = "ADDITIONAL INFORMATION".localized
   static let pickAOffering = "Pick a Offering".localized
   
   static let home = "Home".localized
   static let payments = "Payments".localized
   static let referrals = "Referrals".localized
   static let profile = "Profile".localized
   static let chatWith = "Chat with".localized
   
   static let signupTemplateTitle = "Enter Additional\nInformation".localized
   static let signupTemplateSubTitle = "You will need the following to complete the sign up process".localized
   static let refresh = "Refresh".localized
   static let accountBeingReviewed = "Your account is being reviewed".localized
   static let accountRejected = "Your account has been rejected by admin".localized
   
   static let resubmitTemplate = "You have to submit your additional information again".localized
    
    //New Strings 01/12/17
    static let RETRYING = "Retrying".localized
    static let cameraNotFoundInDevice = "Camera not found in this device.".localized
    static let photoLibraryNotAvailable = "Photo library is not available".localized
    static let NoInternetConnection = "No Internet Connection".localized
    static let cannotBeEmpty = "cannot be empty".localized
    static let isNotValid = "is not valid".localized
  
    static let GETTING_LOCATION = "Getting location" + "..."
}
