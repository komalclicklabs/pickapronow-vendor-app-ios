//
//  NoInternetConnectionView.swift
//  Tookan
//
//  Created by CL-macmini45 on 6/2/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

protocol NoInternetConnectionDelegate {
    func dismissInternetToast()
}
class NoInternetConnectionView: UIView {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dismissButton: UIButton!
    var delegate:NoInternetConnectionDelegate!
    
    override func awakeFromNib() {
       //self.transform = CGAffineTransformMakeTranslation(0, -self.frame.height)
    }
    @IBAction func dismissAction(_ sender: AnyObject) {
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.frame = CGRect(x: self.frame.origin.x,y: self.frame.origin.y, width: self.frame.width, height: 0)
            }, completion:{ finished in
                self.delegate.dismissInternetToast()
        })
        
    }
    
    func showToast() {
        self.messageLabel.text = TEXT.NoInternetConnection
        UIView.animate(withDuration: 0.3, delay: 0.1, options: UIViewAnimationOptions.curveLinear, animations: {
            self.frame = CGRect(x: self.frame.origin.x,y: self.frame.origin.y, width: self.frame.width, height: 40)
            }, completion:{ finished in
                self.showFirstText()
            })
    }
    
    
    func showFirstText() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.messageLabel.text = TEXT.NoInternetConnection
            }, completion: { finished in
                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.dismissAction(_:)), userInfo: nil, repeats: false)
                
            })
    }
    
//    func showSecondText() {
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
//            self.messageLabel.text = "You can still update the task in offline mode".localized
//            }, completion:{ finished in
//                Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.dismissAction(_:)), userInfo: nil, repeats: false)
//        })
//    }
    
    
}
