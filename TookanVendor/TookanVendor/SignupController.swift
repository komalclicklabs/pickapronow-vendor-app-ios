//
//  SignupController.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 17/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class SignupController: UIViewController, NavigationDelegate, PickerDelegate {
  
  // MARK: - Properties
  let marginTitleAndTextFields: CGFloat =  42
  let marginTextFieldsAndSignUpStackView: CGFloat = 35
  
  var navigationBar: NavigationView!
  
  var pickerForDropdown: CustomDropdown!
  var countryCodeAndCountryName = [String: String]()
  var selectedLocale: String!
  
  var isFacebook = false
  var faceBookData = FacebookDetail()
  
  var email: String?
  
  var shouldHideReferralField: Bool {
    return !AppConfiguration.current.isReferralRequired
  }
  
  //MARK: - IBOutlets
  @IBOutlet weak var viewPasswordButton: UIButton!
  @IBOutlet weak var checkBoxButton: UIButton!
  @IBOutlet weak var termsAndConditionButton: UIButton!
  @IBOutlet weak var iAgreeToLabel: UILabel!
  @IBOutlet weak var checkboxView: UIView!
  
  @IBOutlet var signupButton: UIButton!
  
  @IBOutlet weak var nameField: MKTextField!
  @IBOutlet weak var emailField: MKTextField!
  @IBOutlet weak var phoneField: MKTextField!
  @IBOutlet weak var passwordField: MKTextField!
  @IBOutlet weak var referalField: MKTextField!
  @IBOutlet weak var countryCodeField: MKTextField!


  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var haveAnAccountLabel: UILabel!
  @IBOutlet weak var signInButton: UIButton!
  
  @IBOutlet weak var distanceBetweenTopAndTitleLabel: NSLayoutConstraint!
  @IBOutlet weak var distanceBetweenTitleAndTextFields: NSLayoutConstraint!
  @IBOutlet weak var distanceBtwSignupStackViewAndTextFields: NSLayoutConstraint!
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    termsAndConditionButton.addTarget(self, action: #selector(SignupController.termsAndConditionAction), for: UIControlEvents.touchUpInside)
    self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    configureViews()
    checkBoxButton.isHidden = true
    termsAndConditionButton.isHidden = true
    iAgreeToLabel.isHidden = true
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if isFacebook == true {
      setUpFacebookData()
    }
    
    passwordField.text = ""
  }
  
  //MARK: - Configure Views
  private func configureViews() {
    self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    self.setNavigationBar()
    self.setTextFields()
    self.setSignupButton()
    setSignInButtonView()
    titleLabel.configureSignInTitleTypeWith(text: TEXT.SIGN_UP_TITLE)
  }
  
  private func setTextFields() {
    distanceBetweenTitleAndTextFields.constant = marginTitleAndTextFields * heightMultiplierForDifferentDevices
    setEmailField()
    setPasswordField()
    setNameField()
    setPhoneNumberField()
    setRefferalField()
    setCheckBoxView()
    setCountryCodeField()
  }
  
  private func setEmailField() {
    emailField.delegate = self
    emailField.configureWith(text: email ?? "", placeholder: TEXT.YOUR_EMAIL)
  }

  private func setPasswordField() {
    passwordField.isHidden = isFacebook
    passwordField.delegate = self
    passwordField.configureWith(text: "", placeholder: TEXT.YOUR_PASSWORD)
    viewPasswordButton.tintColor = passwordField.tintColor
    viewPasswordButton.setImage(#imageLiteral(resourceName: "eyeActive").renderWithAlwaysTemplateMode(), for: .selected)
  }
  
  private func setNameField() {
    self.nameField.delegate = self
    nameField.configureWith(text: "", placeholder: TEXT.YOUR_NAME)
  }
  
  private func setCountryCodeField() {
    countryCodeField.delegate = self
    
    let countryDialingCode = "+" + NSLocale.locale.getCurrentDialingCode()
    countryCodeField.configureWith(text: countryDialingCode, placeholder: "")
    
    countryCodeField.rightView = getViewWithDownArrowImage()
    countryCodeField.rightViewMode = .always
    
  }
  
  private func getViewWithDownArrowImage() -> UIView {
    let view = UIView()
    view.backgroundColor = UIColor.clear
   
    let imageView = UIImageView.init(image: #imageLiteral(resourceName: "downArrow").renderWithAlwaysTemplateMode())
   imageView.tintColor = COLOR.SPLASH_TEXT_COLOR
    imageView.contentMode = .center
    view.frame = imageView.frame
    view.frame.size.width += 10
    view.addSubview(imageView)
    return view
  }
  
  private func setPhoneNumberField() {
    phoneField.delegate = self
    phoneField.configureWith(text: "", placeholder: TEXT.YOUR_CONTACT)
  }
  
  private func setRefferalField() {
    referalField.delegate = self
    referalField.configureWith(text: "", placeholder: TEXT.REFERRAL_CODE)
    referalField.isHidden = shouldHideReferralField
  }
  
  private func setSignupButton() {
    signupButton.configureNormalButtonWith(title: TEXT.SIGN_UP)
    distanceBtwSignupStackViewAndTextFields.constant = marginTextFieldsAndSignUpStackView * heightMultiplierForDifferentDevices
  }
  
  private func setCheckBoxView() {
    iAgreeToLabel.configureNewUserTypeLabelWith(text: TEXT.I_AGREE_TO)
    termsAndConditionButton.configureSmallButtonWith(title: TEXT.TERMS_CONDITIONS)
    termsAndConditionButton.backgroundColor = UIColor.clear
    termsAndConditionButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
    checkboxView.isHidden = true
  }
  
  private func setSignInButtonView() {
    haveAnAccountLabel.configureNewUserTypeLabelWith(text: TEXT.HAVE_AN_ACCOUNT)
    signInButton.configureSmallButtonWith(title: TEXT.SIGN_IN)
  }
  
  //MARK: NAVIGATION BAR
  func setNavigationBar() {
   navigationBar = NavigationView.getNibFile(leftButtonAction: { [weak self] in
      self?.backAction()
      }, rightButtonAction: nil)
   navigationBar.setBackgroundColor(color: UIColor.clear, andTintColor: COLOR.SPLASH_TEXT_COLOR)
   navigationBar.bottomLine.isHidden = true
    self.view.addSubview(navigationBar)
  }
  
  //MARK: - IBAction
  func backAction() {
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func signupAction(_ sender: AnyObject) {
      self.checkValidation()
  }
  
  @IBAction func viewPasswordButtonPressed(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
    passwordField.isSecureTextEntry = !sender.isSelected
  }
  
  @IBAction func countryCodeButtonPressed() {
    showCountryCode()
  }
  
  @IBAction func signInButtonPressed(_ sender: UIButton) {
    if previousControllerIsLogInController() {
      _ = self.navigationController?.popViewController(animated: true)
    } else {
      LoginController.pushIn(navVC: self.navigationController!, withEmail: nil)
    }
  }
  
  private func previousControllerIsLogInController() -> Bool {
    let previousController = self.navigationController?.getSecondLastController()
    return previousController is LoginController
  }
  
  //MARK: VALIDATION CHECK
  func checkFirstViewValidations()  {
    
//            if checkBoxButton.isSelected == false    {
//                self.showErrorMessage(error: "Please select the terms and conditions.")
//                self.view.endEditing(true)
//                return
//            }
  }
  
  func checkValidation() {
    let email = (self.emailField.text?.trimText)!
    var password = String()
    
    let name = (self.nameField.text?.trimText)!

    guard name.length > 0 else {
      self.showErrorMessage(error: ERROR_MESSAGE.INVALID_NAME)
      return
    }
    
    guard Singleton.sharedInstance.validateEmail(email) == true else {
      self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL)
      return
    }
    
    let contact = (self.countryCodeField.text)! + " " + (self.phoneField.text?.trimText)!
    
    guard Singleton.sharedInstance.validatePhoneNumber(phoneNumber: contact) == true,
      phoneNumberContainsCharacterOtherThanNumbers() == false else {
        self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER)
        return
    }
    
    if isFacebook == false{
      password = (self.passwordField.text?.trimText)!
      guard Singleton.sharedInstance.validPassword(password:password) == true else {
        self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD)
        return
      }
    }
    
    if isFacebook == false  {
      password = (self.passwordField.text?.trimText)!
    }else{
      password = "123456"
    }

    
//            if checkBoxButton.isSelected == false    {
//                self.showErrorMessage(error: "Please select the terms and conditions.")
//                self.view.endEditing(true)
//                return
//            }
    
    if isFacebook == true {
      self.serverRequest(email: email, password: password, name: name, contact: contact,isFacebook: true,id: faceBookData.id)
    }else{
      self.serverRequest(email: email, password: password, name: name, contact: contact)
    }
  }
  
  func phoneNumberContainsCharacterOtherThanNumbers() -> Bool {
    let phoneNo = phoneField.text!
    
    return phoneNo.characters.count != phoneNo.stripOutUnwantedCharacters(charactersYouWant: "0123456789+-").characters.count
  }
  
  
  //MARK: SERVER REQUEST
  func serverRequest(email:String, password:String, name:String, contact:String,isFacebook : Bool = false,id:String = "") {
    
    self.view.endEditing(true)
   
   ActivityIndicator.sharedInstance.showActivityIndicator()

   getCountryCodeAndContinentCode { [weak self] (success, countryCode, continentCode) in
      
      guard success, self != nil else {
         print("Contry Code, Continent Code Not Found")
         ActivityIndicator.sharedInstance.hideActivityIndicator()
         return
      }
    
    if isFacebook == true{
    logEvent(label: "sign_up_facebook")
        
    }else{
        
        logEvent(label: "sign_up_email")
        
    }
      logEvent(label: "sign_up_taxi_app")
      
      APIManager.sharedInstance.signupRequest(name, contact: contact, email: email, password: password,isFacebook: isFacebook,id: id, countryCode: countryCode!, continentCode: continentCode!) { (succeeded, response) in
         print(response)
         
         DispatchQueue.main.async(execute: { () -> Void in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if succeeded == true {
                logEvent(label: "user_sign_up_success")
               Singleton.sharedInstance.isSignedIn = true
               self?.signupButton.isEnabled = true
               if let status = response["status"] as? Int {
                  switch(status) {
                    
                  case STATUS_CODES.SHOW_DATA:
                     if let data = response["data"] as? [String: Any] {
                        Vendor.logInWith(data: data)
                        
                        self?.onSuccessFullSignUp()
                        
                        Vendor.current?.isFirstSignup = true

                     }
                  case STATUS_CODES.INVALID_ACCESS_TOKEN:
                     break
                     
                  default:
                     self?.showErrorMessage(error: response["message"] as! String!)
                     break
                  }
               }
            } else {
               self?.signupButton.isEnabled = true
               self?.showErrorMessage(error: response["message"] as! String!)
            }
         })
         
      }
   }
    
   
  }
    
    func onSuccessFullSignUp(){
        Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
            ActivityIndicator.sharedInstance.showActivityIndicator()
        }, stopLoaderAnimation: {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
        })
        self.sendReferralCodeToServerIfPresent()
    }
    
    
  private func sendReferralCodeToServerIfPresent() {
    guard let referralCode = referalField.text, referralCode.isEmpty == false else {
      print("No referral code present")
      return
    }
    logEvent(label: "referral_applied")
    Vendor.current!.sendReferralCodeToServer(referralCode: self.referalField.text!, completion: nil)
  }
   
   func getCountryCodeAndContinentCode(completion: @escaping (Bool, String?, String?) -> Void) {
      let serverUrl = "https://ip.tookanapp.com:8000/requestCountryCodeGeoIP2"
      
      HTTPClient.shared.makeSingletonConnectionWith(method: .GET, showActivityIndicator: false, baseUrl: serverUrl, extendedUrl: "") { (responseObject, _, _, statusCode) in
         guard let response = responseObject as? [String: Any],
         let data = response["data"] as? [String: Any],
            let countryCode = data["country_code"] as? String,
         let continentCode = data["continent_code"] as? String
            else {
            completion(false, nil, nil)
            return
         }
         
         completion(true, countryCode, continentCode)
      }
   }
  
  //MARK: COUNTRY CODE
  func showCountryCode() {
    self.view.endEditing(true)
    let countryArray = NSMutableArray()
    for locale in NSLocale.locales() {
      countryArray.add(locale.countryName)
      countryCodeAndCountryName[locale.countryName] = locale.countryCode
    }
    
    pickerForDropdown = UINib(nibName: NIB_NAME.customDropdown, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomDropdown
    pickerForDropdown.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
    pickerForDropdown.valueArray =  countryArray
    pickerForDropdown.delegate = self
    self.view.addSubview(pickerForDropdown)
    pickerForDropdown.setPickerView(customFieldRow: 0)
  }
  
  //MARK: - Dropdown Picker Delegate
  func dismissPicker() {
    pickerForDropdown = nil
  }
  
  func selectedValueFromPicker(_ value: String, customFieldRowIndex:Int) {
    var code = ""
    if countryCodeAndCountryName[value] != nil {
      let locale = countryCodeAndCountryName[value]!
      self.selectedLocale = locale
      if dialingCode[selectedLocale] != nil {
        code = "+" + dialingCode[self.selectedLocale]!
      } else {
        code = "+1"
      }
      
    } else {
      code = "+"
    }
    pickerForDropdown = nil
    self.countryCodeField.text = code
  }
    
  //MARK: ERROR MESSAGE
  func showErrorMessage(error:String) {
    ErrorView.showWith(message: error, removed: nil)
  }
  
  @IBAction func checkBoxButtonAction(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
  }
  
  func setUpFacebookData()  {
      emailField.text = faceBookData.email
      emailField.isEnabled = faceBookData.email.isEmpty
    
      nameField.text = faceBookData.fullName
    viewPasswordButton.isHidden = true
  }
  
  //MARK: - Navigation
  class func getWith(email: String?) -> SignupController {
    let signUpController = findIn(storyboard: .main, withIdentifier: STORYBOARD_ID.sigupController) as! SignupController
    signUpController.email = email
    return signUpController
  }
  
  class func pushIn(navVC: UINavigationController, withEmail email: String?) {
    let signUpController = getWith(email: email)
    navVC.pushViewController(signUpController, animated: true)
  }
    
    func termsAndConditionAction(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "terms") as? TermsAndConsitionViewController
        self.present(vc!, animated: true, completion: nil)
    }
    
  
}

extension SignupController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    switch textField {
    case nameField:
      emailField.becomeFirstResponder()
    case emailField:
      showCountryCode()
    case phoneField:
      if isFacebook {
        passwordField.becomeFirstResponder()
      } else if !shouldHideReferralField {
        referalField.resignFirstResponder()
      } else {
        textField.resignFirstResponder()
      }
    case passwordField:
      if !shouldHideReferralField {
        referalField.becomeFirstResponder()
      } else {
        textField.resignFirstResponder()
      }
    case referalField:
      referalField.resignFirstResponder()
    default:
      break
    }
    return true
  }
}
