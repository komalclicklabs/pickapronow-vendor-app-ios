//
//  TipTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 22/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class TipTableViewCell: UITableViewCell {
   
   // MARK: - IBOutlets
   @IBOutlet weak var firstTipButton: UIButton!
   @IBOutlet weak var secondTipButton: UIButton!
   @IBOutlet weak var thirdTipButton: UIButton!
   @IBOutlet weak var othersTipButton: UIButton!
   
   // MARK: - Properties
   var tipDict = [Int: String]()
   var selectedTag = 2232
   
   var onButtonClick: ((_ value:Double,_ Tag:Int) -> Void)?
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      self.selectionStyle = .none
   }
   
   // mRK: - IBOutlets
   @IBAction func addTipAction(_ sender: UIButton) {
      if sender.tag != selectedTag {
         resetAllButtonsAccept(tag:sender.tag)
         self.onButtonClick!(Double(tipDict[sender.tag]!)!,sender.tag)
      } else {
         resetAllButtonsAccept(tag:12469)
         self.onButtonClick!(0.0,2232)
      }
      
   }
   
   // MARK: - Methods
   func setUpButtons() {
      let buttonArray = [firstTipButton,secondTipButton,thirdTipButton,othersTipButton]
      
      for i in 0..<buttonArray.count{
         buttonArray[i]?.setBorder(borderColor: COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.3))
         if Singleton.sharedInstance.formDetailsInfo.tipDetails.type == .amount {
            buttonArray[i]?.setTitle("\(Singleton.sharedInstance.formDetailsInfo.currencyid) \(self.tipDict[(buttonArray[i]?.tag)!]!)", for: UIControlState.normal)
         } else {
            buttonArray[i]?.setTitle("\(self.tipDict[(buttonArray[i]?.tag)!]!) %", for: UIControlState.normal)
         }
         
         buttonArray[i]?.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
         buttonArray[i]?.setTitleColor(COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.7), for: UIControlState.normal)
         buttonArray[i]?.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
         buttonArray[i]?._cornerRadius = 2
      }
   }
   

   
   func resetAllButtonsAccept(tag:Int)  {
      for i in [1,2,3,4]{
         let button = self.viewWithTag(i) as! UIButton
         if tag != i {
            button.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
            button.setTitleColor(COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.7), for: UIControlState.normal)
            button.setBorder(borderColor: COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.3))
         }else{
            button.backgroundColor = COLOR.paymentGreenColor
            button.setTitleColor(COLOR.SPLASH_BACKGROUND_COLOR, for: UIControlState.normal)
            button.setBorder(borderColor: COLOR.paymentGreenColor)

         }
         
         
      }
   }
   
   
}
