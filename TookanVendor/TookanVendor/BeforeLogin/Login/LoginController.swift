//
//  LoginController.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 16/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class LoginController: UIViewController, NavigationDelegate {
  
  let topMarginOfTextField:CGFloat = 20.0
  let buttonHeight:CGFloat = 50.0
  weak var navigationBar:NavigationView!
  var titleView:TitleView!
  
  @IBOutlet weak var viewPasswordButton: UIButton!
  @IBOutlet weak var titleLabel: UILabel!
  var email: String?
  
  // MARK: - IBOutlets
  @IBOutlet weak var distanceBetweenMailFieldAndTitle: NSLayoutConstraint!
  @IBOutlet var signinButton: UIButton!
  @IBOutlet var forgotPasswordButton: UIButton!
  @IBOutlet weak var emailField: MKTextField!
  @IBOutlet weak var passwordField: MKTextField!
  @IBOutlet weak var newUserLabel: UILabel!
  @IBOutlet weak var signUpButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureViews()
    /*---------- Tap Gesture ------------*/
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTouch))
    tap.numberOfTapsRequired = 1
    self.view.addGestureRecognizer(tap)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    passwordField.text = ""
  }
  
  //MARK: NAVIGATION BAR
  func setNavigationBar() {
   navigationBar = NavigationView.getNibFile(leftButtonAction: { [weak self] in
      self?.backAction()
      }, rightButtonAction: nil)
   navigationBar.setBackgroundColor(color: UIColor.clear, andTintColor: COLOR.SPLASH_TEXT_COLOR)
    navigationBar.backgroundColor = UIColor.clear
   navigationBar.bottomLine.isHidden = true
    self.view.addSubview(navigationBar)
    
  }
  
  //MARK: NAVIGATION DELEGATE METHODS
  func backAction() {
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  //MARK:TITLE VIEW
  func configureViews() {
    self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    self.setNavigationBar()
    self.setTitleLabel()
    self.setTextFields()
    self.setSignInButton()
    self.setForgotButton()
    signUpButton.configureSmallButtonWith(title: TEXT.SIGN_UP)
    newUserLabel.configureNewUserTypeLabelWith(text: TEXT.NEW_USER)
  }
  
  func setTitleLabel() {
    distanceBetweenMailFieldAndTitle.constant = heightMultiplierForDifferentDevices * 65
    titleLabel.configureSignInTitleTypeWith(text: TEXT.SIGN_IN_TITLE)

  }
  
  //MARK: SET FIELDS
  func setTextFields() {
    self.setEmailField()
    self.setPasswordField()
  }
  
  func setEmailField() {
    emailField.configureWith(text: email ?? "", placeholder: TEXT.YOUR_EMAIL)
    emailField.delegate = self
  }
  
  func setPasswordField() {
    passwordField.configureWith(text: "", placeholder: TEXT.PASSWORD)
    self.passwordField.delegate = self
    
    viewPasswordButton.tintColor = passwordField.tintColor
    viewPasswordButton.setImage(#imageLiteral(resourceName: "eyeActive").renderWithAlwaysTemplateMode(), for: .selected)
  }
  
  func setSignInButton() {
    signinButton.configureNormalButtonWith(title: TEXT.SIGN_IN)
  }
  
  func setForgotButton() {
    forgotPasswordButton.configureSmallButtonWith(title: TEXT.FORGOT_PASSWORD)
  }
  
  //MARK: - IBACTION
  @IBAction func signinAction(_ sender: AnyObject) {
   

    self.checkValidation()
  }
  
  @IBAction func viewPasswordButtonPressed(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
    passwordField.isSecureTextEntry = !sender.isSelected
  }
  
  @IBAction func signUpButtonPressed() {
    if previousControllerIsSignUpController() {
      _ = self.navigationController?.popViewController(animated: true)
    } else {
      SignupController.pushIn(navVC: self.navigationController!, withEmail: nil)
    }
    
  }
  
  private func previousControllerIsSignUpController() -> Bool {
    let previousController = self.navigationController?.getSecondLastController()
    return previousController is SignupController
  }
  
  //MARK: VALIDATION CHECK
  func checkValidation() {
    let email = (self.emailField.text?.trimText)!
    let password = (self.passwordField.text?.trimText)!
    guard Singleton.sharedInstance.validateEmail(email) == true else {
      self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL)
      return
    }
    
    guard Singleton.sharedInstance.validPassword(password:password) == true else {
      self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD)
      return
    }
    
    self.serverRequest(email: email, password: password)
  }
  
  
  //MARK: SERVER REQUEST
  func serverRequest(email:String, password:String) {
    self.view.endEditing(true)
    ActivityIndicator.sharedInstance.showActivityIndicator()
    APIManager.sharedInstance.loginRequest(email, password: password) { (succeeded, response) in
      print(response)
      DispatchQueue.main.async(execute: { () -> Void in
        DispatchQueue.main.async {
          ActivityIndicator.sharedInstance.hideActivityIndicator()
          if succeeded == true {
            logEvent(label: "user_sign_in_via_email")
            if let status = response["status"] as? Int {
              switch(status) {
              case STATUS_CODES.SHOW_DATA:
                if let data = response["data"] as? [String:Any] {
                  Vendor.logInWith(data: data)
                  Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: { 
                     ActivityIndicator.sharedInstance.showActivityIndicator()
                  }, stopLoaderAnimation: { 
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                  })
                }
                break
              case STATUS_CODES.INVALID_ACCESS_TOKEN:
                break
                
              default:
                Singleton.sharedInstance.showAlert(response["message"] as! String!)
                break
              }
            }
          } else {
            self.showErrorMessage(error: (response["message"] as? String) ?? "")
          }
        }
      })
    }
  }
  
  
  
  //MARK: ERROR MESSAGE
  func showErrorMessage(error:String) {
    ErrorView.showWith(message: error, removed: nil)
  }
  
  
  //MARK: FORGOT ACTION
  @IBAction func forgotPasswordAction(_ sender: AnyObject) {
    self.view.endEditing(true)
  }
  
  //MARK: BACKGROUND TOUCH
  func backgroundTouch() {
    self.view.endEditing(true)
  }
  
  // MARK: - Navigation
  class func getWith(email: String?) -> LoginController {
    let loginVC = findIn(storyboard: .main, withIdentifier: STORYBOARD_ID.loginController) as! LoginController
    loginVC.email = email
    return loginVC
  }
  
  class func pushIn(navVC: UINavigationController, withEmail email: String?) {
    let loginVC = getWith(email: email)
    navVC.pushViewController(loginVC, animated: true)
  }
   
   deinit {
      print("Login Controller deintialized")
   }
  
}

// MARK: - TextFieldDelegate
extension LoginController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    return false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
  }
}

