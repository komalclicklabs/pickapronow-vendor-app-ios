//
//  ForgotPassVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/23/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController, NavigationDelegate {
  
  //MARK: - Properties
    var navigationBar:NavigationView!
    var titleView:TitleView!
    let topMarginOfTextField:CGFloat =  118 * heightMultiplierForDifferentDevices
  let marginForgotPasswordAndDescLabel: CGFloat = 10 * heightMultiplierForDifferentDevices
  let marginDescLabelAndEmailField: CGFloat = 93 * heightMultiplierForDifferentDevices
  let marginMailFieldAndSendLinkButton = 44 * heightMultiplierForDifferentDevices
  
  // MARK: - IBOutlets
    @IBOutlet weak var enterEmailAddressLabel: UILabel!
    @IBOutlet weak var sendLinkButton: UIButton!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var emailField: MKTextField!
  
  @IBOutlet weak var distanceTopForgotPassword: NSLayoutConstraint!
  
  
  // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      configureViews()
    }

    //MARK: - ConfigureViews
  private func configureViews() {
    self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    self.setNavigationBar()
    setEmailField()
    setDescLabel()
    seTitleLabel()
    setSendLinkButton()
  }
  
  private func setEmailField() {
    emailField.delegate = self
    emailField.configureWith(text: "", placeholder: TEXT.YOUR_EMAIL)
  }
  
  private func setDescLabel() {
    enterEmailAddressLabel.configureNewUserTypeLabelWith(text: TEXT.FORGOT_ENTER_EMAIL_FOR_LINK_LABEL)
  }
  
  private func seTitleLabel() {
    titleLabel.configureSignInTitleTypeWith(text: TEXT.FORGOT_PASSWORD)
  }
  
  private func setSendLinkButton() {
    sendLinkButton.configureNormalButtonWith(title: TEXT.SEND_LINK_BUTTON)
  }
  
    func setSendLinkButtonButton() {
        self.sendLinkButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        self.sendLinkButton.setShadow()
        self.sendLinkButton.setAttributedTitle(Singleton.sharedInstance.setAttributeTitleWithOrWithoutArrow(title: TEXT.SEND_LINK_BUTTON, isArrow: false, yourImage: IMAGE.iconSigninSigninpage), for: .normal)
    }

    
    //MARK: - CHECK VALIDATIONS
    func checkValidation() {
        let email = (self.emailField.text?.trimText)!
        guard Singleton.sharedInstance.validateEmail(email) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL,isError: true)
            return
        }
        let forgotParams = [ "email": email, "login_type":"1", "app_device_type":APP_DEVICE_TYPE ]
        self.serverRequest(yourParams: forgotParams as [String : AnyObject])
    }
    
    //MARK: - SEND LINK ACTION
    @IBAction func sendLinkAction(_ sender: UIButton) {
      emailField.resignFirstResponder()
      checkValidation()
    }
  
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
      let navigationBar = NavigationView.getNibFile(leftButtonAction: {
         _ = self.navigationController?.popViewController(animated: true)
   }, rightButtonAction: nil)
      
      navigationBar.backgroundColor = UIColor.clear
      navigationBar.bottomLine.isHidden = true
      navigationBar.backButton.tintColor = COLOR.SPLASH_TEXT_COLOR
        navigationBar.titleLabel.text = TEXT.FORGOT_PASSWORD
        self.view.addSubview(navigationBar)
    }
  
    //MARK: NAVIGATION DELEGATE METHODS
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }

    
    //MARK: SERVER REQUEST
    func serverRequest(yourParams:[String:AnyObject]) {
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.forgotPass, params: yourParams, httpMethod: HTTP_METHOD.POST) { (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    self.emailField.text = ""
                   // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }else{
                self.showErrorMessage(error: response["message"] as? String ?? "",isError: true)
                }
            }
        }
    }
    
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String,isError:Bool) {
        ErrorView.showWith(message: error, isErrorMessage: isError, removed: nil)
    }
    
}

extension ForgotPassVC: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
