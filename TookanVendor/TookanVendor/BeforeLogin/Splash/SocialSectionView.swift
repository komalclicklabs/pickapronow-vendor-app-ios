//
//  SocialSectionView.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

protocol SocialActionDelegate {
    func facebookEventAction()
    func googlePlusEventAction()
}

class SocialSectionView: UIView {

    @IBOutlet var orLabel: UILabel!
    @IBOutlet var facebookButton: UIButton!
    @IBOutlet var googlePlusButton: UIButton!
    @IBOutlet var facebookTopConstraint: NSLayoutConstraint!
    @IBOutlet var googlePlusTopConstraint: NSLayoutConstraint!
    @IBOutlet var facebookWidthConstraint: NSLayoutConstraint!
    @IBOutlet var googlePlusWidthConstraint: NSLayoutConstraint!

    let buttonHeight:CGFloat = 50.0
    var delegate:SocialActionDelegate!
    
    override func awakeFromNib() {
        self.setViewAsPerConfig()
    }
    
    func setViewAsPerConfig() {
      self.setFacebookButton(text: "Facebook")
      self.setGoogleButton(text: "Google+")
      self.setOrText(text: TEXT.OR_CONTINUE)

        if AppConfiguration.current.isLoginViaEmailRequired == "0" {
            self.orLabel.isHidden = true
            self.facebookTopConstraint.constant = 0.0
            self.googlePlusTopConstraint.constant = 0.0
        }
        
        if AppConfiguration.current.isFacebookRequired == "1" && AppConfiguration.current.isGoogleRequired == "0" {//ONLY FACEBOOK
            self.facebookWidthConstraint.constant = SCREEN_SIZE.width - 30
            self.googlePlusWidthConstraint.constant = -self.googlePlusButton.frame.width
        } else if AppConfiguration.current.isFacebookRequired == "0" && AppConfiguration.current.isGoogleRequired == "1" {//ONLY GOOGLE PLUS
            self.facebookWidthConstraint.constant = -self.facebookButton.frame.width
            self.googlePlusWidthConstraint.constant = (self.frame.width / 2) - 60
        }
    }
    
    func setOrText(text:String) {
        self.orLabel.text = "- " + text + " -"
        self.orLabel.font = UIFont(name: FONT.ultraLight, size: 15)
        self.orLabel.textColor = COLOR.SPLASH_TEXT_COLOR
    }
    
    func setFacebookButton(text:String) {
        self.facebookButton.layer.cornerRadius = 2
        self.facebookButton.setTitle(text, for: UIControlState.normal)
        self.facebookButton.titleLabel?.font = UIFont(name: FONT.regular, size: 14.0)
        self.facebookButton.setSameColorShadow()
    }
    
    func setGoogleButton(text:String) {
        self.googlePlusButton.layer.cornerRadius = 2
        self.googlePlusButton.setTitle(text, for: UIControlState.normal)
        self.googlePlusButton.titleLabel?.font = UIFont(name: FONT.regular, size: 14.0)
        self.googlePlusButton.setSameColorShadow()
    }
    
    //MARK: ACTIONS
    @IBAction func googlePlusAction(_ sender: AnyObject) {
        delegate.googlePlusEventAction()
    }

    @IBAction func facebookAction(_ sender: AnyObject) {
        delegate.facebookEventAction()
    }
  
  // MARK: - Loading
  static func loadWith(frame: CGRect) -> SocialSectionView {
    
    let nib = Bundle.main.loadNibNamed(NIB_NAME.socialSectionView, owner: nil, options: nil)?.first as! SocialSectionView
    nib.setAlphaToZero()
    nib.frame = frame
    return nib
  }
}
