//
//  ChangePhoneNumberParams.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 22/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

extension ChangePhoneNumberView {
  struct PopupParams {
    var heading: String = ""
    var textFieldPlaceholder: String = TEXT.YOUR_CONTACT
    var textFieldtext: String = ""
    var removeAlertButtonTitle: String = TEXT.CANCEL
    var saveButtonAlertTitle: String = TEXT.SAVE
        
    init() {}

    init(heading: String, textFieldPlaceholder: String = "", textFieldtext: String = "",removeAlertButtonTitle: String, saveButtonAlertTitle: String) {
      self.heading = heading
      self.textFieldPlaceholder = textFieldPlaceholder
      self.textFieldtext = textFieldtext
      self.removeAlertButtonTitle = removeAlertButtonTitle
      self.saveButtonAlertTitle = saveButtonAlertTitle
    }
    
  }
  
}
