//
//  ChangePhoneNumberView.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 01/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class ChangePhoneNumberView: UIView {
  
  // MARK: - Properties
  private var numberChangedWithCountryCodeAndPhoneNumber: ((String, String) -> Void)?
  private var newTextInField: ((String) -> Void)?
  private var cancelled: (() -> Void)?
  
  private var intialTexts = PopupParams()
  private var isChangeNumberView = true
  
  var keyboard: KeyBoard?
  
  // MARK: - IBOutlets
  @IBOutlet weak var countryCodeFieldButton: UIButton!
  @IBOutlet weak var cornerRadiusImageView: UIImageView!
  @IBOutlet weak var overLay: UIView!
  @IBOutlet weak var popUpView: UIView!
  
  @IBOutlet weak var heading: UILabel!
  @IBOutlet weak var countryCodeField: MKTextField!
  @IBOutlet weak var phoneNumberField: MKTextField!
  @IBOutlet weak var applyButton: UIButton!
  @IBOutlet weak var cancelButton: UIButton!
  
  @IBOutlet weak var bottomDistanceChangeNumber: NSLayoutConstraint!
  
  // MARK: - View Life Cycle
  override func awakeFromNib() {
    super.awakeFromNib()
    
    configureViews()
  }
  
  private func configureViews() {
    setHeadingLabel()
    setTextFields()
    setButtons()
    
    overLay.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.75)
    popUpView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
  }
  
  private func setHeadingLabel() {
    heading.configureSignInTitleTypeWith(text: intialTexts.heading)
    heading.isHidden = intialTexts.heading.isEmpty
    heading.font = heading.font.withSize(17)
  }
  
  private func setTextFields() {
    phoneNumberField.configureWith(text: intialTexts.textFieldtext, placeholder: intialTexts.textFieldPlaceholder)
    phoneNumberField.delegate = self
    
    setCountryCodeTextField()
  }

  private func setCountryCodeTextField() {
    
    guard isChangeNumberView else {
      countryCodeField.isHidden = true
      return
    }
    
    let localCountryCode = "+" + NSLocale.locale.getCurrentDialingCode()
    countryCodeField.configureWith(text: localCountryCode, placeholder: "")
    
    setCountryCodeFieldInputViewToCountryPicker()
    showRightViewOfCountryTextFieldAsDownArrow()
  }
  
  private func setCountryCodeFieldInputViewToCountryPicker() {
    let countryPicker = CountryPicker()
    countryPicker.countryPhoneCodeDelegate = self
    countryCodeField.inputView = countryPicker
  }
  
  private func showRightViewOfCountryTextFieldAsDownArrow() {
    countryCodeField.rightView = UIImageView(image: #imageLiteral(resourceName: "downArrow").renderWithAlwaysTemplateMode())
    countryCodeField.rightViewMode = .always
  }
  
  private func setButtons() {
    applyButton.configureNormalButtonWith(title: intialTexts.saveButtonAlertTitle)
    cancelButton.configureNormalButtonWithOppositeColorThemeWith(title: intialTexts.removeAlertButtonTitle)
  }
  
  //MARK: - IBAction
    @IBAction func countryCodeFieldButtonPressed() {
    countryCodeField.becomeFirstResponder()
  }
  
  @IBAction func cancelButtonAction(_ sender: UIButton) {
    cancelled?()
    removeByAnimating()
  }
  
  @IBAction func applyButtonAction(_ sender: UIButton) {
    let countryCode = countryCodeField.text!
    let phoneNumber = phoneNumberField.text!
    
    numberChangedWithCountryCodeAndPhoneNumber?(countryCode, phoneNumber)
    newTextInField?(phoneNumber)
  }
  
  // MARK: - Loading and Removing
  class func loadForChangingNumber(numberChangedWithCountryCodeAndPhoneNumber: @escaping (String, String) -> Void) -> ChangePhoneNumberView {
    
    let changeNumberView = getNibFileWithFrameOfFullScreen()
    
    changeNumberView.numberChangedWithCountryCodeAndPhoneNumber = numberChangedWithCountryCodeAndPhoneNumber

    changeNumberView.setIntialPositionBeforeDisplaying()
    changeNumberView.addViewToWindowWithAnimation()
    
    return changeNumberView
  }
  
  private class func getNibFileWithFrameOfFullScreen() -> ChangePhoneNumberView {
    
    let changeNumberView = Bundle.main.loadNibNamed(NIB_NAME.ChangePhoneNumberView, owner: nil, options: nil)?.first as! ChangePhoneNumberView
    changeNumberView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height)
    return changeNumberView
    
  }
  
   class func getWithSingleTextField(texts: PopupParams, keyboardType: UIKeyboardType = .phonePad, capitalization: UITextAutocapitalizationType, newTextSaved: ((String) -> Void)?, cancelled: (() -> Void)? = nil) -> ChangePhoneNumberView {
    let fieldAlert = getNibFileWithFrameOfFullScreen()
    
    fieldAlert.newTextInField = newTextSaved
    fieldAlert.setIntialPositionBeforeDisplaying()
    fieldAlert.isChangeNumberView = false
    fieldAlert.phoneNumberField.keyboardType = keyboardType
      fieldAlert.phoneNumberField.autocapitalizationType = capitalization
    fieldAlert.cancelled = cancelled
    
    fieldAlert.setViewWithNew(texts: texts)
    
    return fieldAlert
  }
  
  class func loadWithSingleTextField(texts: PopupParams, keyboardType: UIKeyboardType = .phonePad, capitalization: UITextAutocapitalizationType = .sentences, newTextSaved: ((String) -> Void)?, cancelled: (() -> Void)? = nil) -> ChangePhoneNumberView {
    let changeNumberView = getWithSingleTextField(texts: texts, keyboardType: keyboardType, capitalization: capitalization, newTextSaved: newTextSaved, cancelled: cancelled)
    
    changeNumberView.addViewToWindowWithAnimation()
    
    return changeNumberView
  }
  
   class func getWithHeading(texts: PopupParams,capitalization: UITextAutocapitalizationType = .sentences, successAction: (() -> Void)?, cancelAction: (() -> Void)?) -> ChangePhoneNumberView {
    let view = getWithSingleTextField(texts: texts, capitalization: capitalization, newTextSaved: {_ in successAction?()}, cancelled: cancelAction)
    view.heading.textAlignment = .center
    view.phoneNumberField.isHidden = true
    return view
  }
  
  private func setIntialPositionBeforeDisplaying() {
    self.layoutIfNeeded()
    overLay.setAlphaToZero()
    bottomDistanceChangeNumber.constant = -popUpView.frame.height
    Singleton.sharedInstance.enableDisableIQKeyboard(enable: false)
  }
  
  private func beginAnimation() {
    overLay.showSmoothly(duration: 0.33,completion: nil)
    
    if shouldAnimateViewWithKeyBoard() {
      animateWithKeyBoard()
    } else {
      setDistanceOfPopUpViewFromBottomWithAnimation(newDistance: 0)
    }
  }
  
  private func shouldAnimateViewWithKeyBoard() -> Bool {
    return !phoneNumberField.isHidden
  }
  
  private func animateWithKeyBoard() {
    keyboard = KeyBoard(heightChanged: { [weak self] newHeight in
      self?.setDistanceOfPopUpViewFromBottomWithAnimation(newDistance: newHeight)
    })
    
    phoneNumberField.becomeFirstResponder()
  }

  private func setDistanceOfPopUpViewFromBottomWithAnimation(newDistance: CGFloat) {
    self.bottomDistanceChangeNumber?.constant = newDistance
    UIView.animate(withDuration: 0.27) { [weak self] in
      self?.layoutIfNeeded()
    }
  }
  
  func addViewToWindowWithAnimation() {
    setNeedsLayout()
    layoutIfNeeded()
    UIApplication.shared.keyWindow?.addSubview(self)
    beginAnimation()
  }
  
  func removeByAnimating() {
    self.endEditing(true)
    Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)

    self.hideSmoothly { [weak self] completed in
      if completed {
        self?.removeFromSuperview()
      }
    }
  }
  
  // MARK: - Customizing
  func setViewWithNew(texts: PopupParams) {
    intialTexts = texts
    configureViews()
  }
  
}

// MARK: - UITextFieldDelegate
extension ChangePhoneNumberView: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

// MARK: - CountryPhoneCodePickerDelegate
extension ChangePhoneNumberView: CountryPhoneCodePickerDelegate {
  func countryPhoneCodePicker(picker: CountryPicker, didSelectCountryCountryWithName name: String, countryCode: String, phoneCode: String) {
    countryCodeField.text = phoneCode
  }
}
