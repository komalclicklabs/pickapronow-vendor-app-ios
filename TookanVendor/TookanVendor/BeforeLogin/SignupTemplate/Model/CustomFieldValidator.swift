//
//  CustomFieldValidator.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

struct CustomFieldError: LocalizedError {
   let message: String
   
   var errorDescription: String? {
      return message
   }
}

class CustomFieldValidator {
   
   private var customFields: [CustomFieldDetails]
   
   struct Result {
      let isSuccessful: Bool
      let error: Error?
      let parameters: [[String: Any]]?
   }
   
   init(customFields: [CustomFieldDetails]) {
      self.customFields = customFields
   }
   
   func validate() -> Result {
      
      var parameters = [[String: Any]]()
      for field in customFields {
         
         guard field.dataType.isDataTypeHandled() else {
            parameters.append(field.getParamsToSendToServer())
            continue
         }
         
         guard fieldPassesEmptyValidation(customField: field) else {
            let error = CustomFieldError(message: field.getErrorMessageInCaseFieldIsEmpty())
            return Result(isSuccessful: false, error: error, parameters: nil)
         }
         
         guard fieldPassesContentValidation(customField: field) else {
            let error = CustomFieldError(message: field.getErrorMessageInCaseFieldIsNotValid())
            return Result(isSuccessful: false, error: error, parameters: nil)
         }
         
         parameters.append(field.getParamsToSendToServer())
      }
      let result = Result(isSuccessful: true, error: nil, parameters: parameters)
      return result
   }
   
   func fieldPassesEmptyValidation(customField: CustomFieldDetails) -> Bool {
      let data = customField.data  ?? ""
      
      guard customField.isRequiredFieldAndEditable() else {
         return true
      }
      
      switch customField.dataType {
      case .phonenumber:
         return data.isPhoneNumberEmpty()
      case .DropDown:
         if customField.data == customField.input {
            return false
         }
      default:
         break
      }
      
      return !data.isEmpty
   }
   
   func fieldPassesContentValidation(customField: CustomFieldDetails) -> Bool {
      
      guard let data = customField.data else {
         return customField.isRequiredFieldAndEditable() ? false : true
      }
      
      if !customField.isRequiredFieldAndEditable() && data.isEmpty {
         return true
      }
      
      switch customField.dataType {
      case .email:
         return data.isValidEmail()
      case .phonenumber:
         
         let (_, phoneNumber) = data.seprateCountryCodeAndPhoneNumber()
         if customField.isRequiredFieldAndEditable() || !phoneNumber.isEmpty {
            return data.isValidPhoneNumberAndCountryCode()
         } else {
            return true
         }
         
      case .url:
         return data.isValidUrl()
      case .image:
         return !customField.checkIfImageIsBeingUploaded()
      default:
         return true
      }
   }
   
}

fileprivate extension String {
   func isValidEmail() -> Bool {
      let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
      return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
   }
   
   func isPhoneNumberEmpty() -> Bool {
      let (countryCode, phoneNumber) = seprateCountryCodeAndPhoneNumber()
      return countryCode.isEmpty || phoneNumber.isEmpty
   }
   
   func isValidPhoneNumberAndCountryCode() -> Bool {
      
         let (countryCode, phoneNumber) = seprateCountryCodeAndPhoneNumber()
      
         if !countryCode.isValidCountryCode() {
            return false
         }
         
         return phoneNumber.isValidPhoneNumber()
   }
   
   func isValidPhoneNumber() -> Bool {
      if characters.count < 5 || characters.count > 15  {
         return false
      }
      
      return true
   }
   
   func isValidCountryCode() -> Bool {
      return characters.count >= 2
   }
   
   func isValidUrl() -> Bool {
      return URL(string: self) != nil
   }
   
   func seprateCountryCodeAndPhoneNumber() -> (countryCode: String, phoneNumber: String) {
      let componentsOfPhoneNumber = components(separatedBy: " ")
      
      if componentsOfPhoneNumber.count >= 2 {
         return (componentsOfPhoneNumber[0], componentsOfPhoneNumber[1])
      }
      
      if componentsOfPhoneNumber.count == 1 {
         if componentsOfPhoneNumber[0].contains("+") {
            return (componentsOfPhoneNumber[0], "")
         } else {
            return ("", componentsOfPhoneNumber[0])
         }
      }
      
      return ("", "")
   }
   
}
