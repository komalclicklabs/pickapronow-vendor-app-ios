//
//  SignupTemplate.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 18/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

struct SignupTemplate {
   var name: String
   var customFields: [CustomFieldDetails]
   
   init?(data: [String: Any]) {
      guard let rawCustomFields = data["signup_template_data"] as? [[String: Any]],
          let name = data["signup_template_name"] as? String,
         rawCustomFields.count > 0 else {
         return nil
      }
            
      self.customFields = CustomFieldDetails.getArrayOfCustomFieldsFrom(json: rawCustomFields)
      self.name = name
   }
   
   // MARK: - Type Methods
   func submitToServer(completion: @escaping (_ success: Bool, _ error: Error?, _ registrationStatus: Int?) -> Void) {
      
      let validationResult = validateFieldsForSendingToServer()
      
      guard validationResult.isSuccessful else {
         ErrorView.showWith(message: validationResult.error!.localizedDescription, removed: nil)
         completion(false, validationResult.error, nil)
         return
      }
      
      let params = getParametersToSendToServerWith(templateFields: validationResult.parameters!)
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: API_NAME.submitTemplate) { (responseObject, error, _, statusCode) in
         
         guard statusCode == STATUS_CODES.SHOW_DATA,
            let response = responseObject as? [String: Any],
            let data = response["data"] as? [String: Any],
            let registrationStatus = data["registration_status"] as? Int else {   //key changed for OTP Screen  issue
               completion(false, error, nil)
               return
         }
         
         completion(true, nil, registrationStatus)
      }
      
   }
   
   func validateFieldsForSendingToServer() -> CustomFieldValidator.Result {
      let customFieldValidator = CustomFieldValidator(customFields: customFields)
      return customFieldValidator.validate()
   }
   
   func getParametersToSendToServerWith(templateFields: [[String: Any]]) -> [String: Any] {
      var params = [String: Any]()
      
      params["app_access_token"] = Vendor.current!.appAccessToken
      params["app_device_type"] = APP_DEVICE_TYPE
      params["template_name"] = name
      params["fields"] = templateFields
      
      return params
   }
}
