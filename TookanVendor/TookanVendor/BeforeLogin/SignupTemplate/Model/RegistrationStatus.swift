//
//  RegistrationStatus.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

enum RegistrationStatus: Int {
   case acknowledgementPending = 7
   case otpPending = 2
   case otpVerified = 3
   case verificationPending = 4
   case resubmitVerification = 6
   case verified = 1
   case rejected = 5
   
   func shouldStartSignupTemplateFlow() -> Bool {
      switch self {
      case .verified, .acknowledgementPending:
         return false
      default:
         return true
      }
   }
   
   func isValidWhenSignupTemplateIsNotSelected() -> Bool {
      switch self {
      case .rejected, .verified, .verificationPending, .acknowledgementPending, .otpVerified:
         return true
      default:
         return false
      }
   }
}
