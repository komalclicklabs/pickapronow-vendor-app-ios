//
//  SignupTemplateFlowManager.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 18/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class SignupTemplateFlowManager {
   static var shared: SignupTemplateFlowManager?
   
   // MARK: - Properties
   private var navVC: UINavigationController
   private var signupTemplate: SignupTemplate?
   private var registrationStatus: RegistrationStatus
   
   // MARK: - Intializer
   private init(navVC: UINavigationController, signupTemplate: SignupTemplate?, registrationStatus: RegistrationStatus) {
      self.navVC = navVC
      self.signupTemplate = signupTemplate
      self.registrationStatus = registrationStatus
   }
   
   // MARK: - Methods
   private func startFlow() {
      
      guard canStartFlow() else {
         print("Signup Template is Not selected and status is not correct")
         ErrorView.showWith(message: ERROR_MESSAGE.SERVER_NOT_RESPONDING, removed: nil)
         return
      }
      
      proceedAccordingToRegistrationStatus()
   }
   
   private func canStartFlow() -> Bool {
      return signupTemplate != nil || registrationStatus.isValidWhenSignupTemplateIsNotSelected()
   }
   
   func continueToFillTheTemplate() {
      SignupTemplateViewController.pushViewControllerIn(navVC: navVC, withflowManager: self, signupTemplate: signupTemplate!)
   }
   
   func goBackToViewTemplateList() {
      navVC.popViewController(animated: true)
   }
   
   func logoutFromApp() {
      _ = UIAlertController.showActionSheetWith(title: nil, message: TEXT.ARE_YOU_WANT_LOGOUT_ALERT, options: [TEXT.LOGOUT_BUTTON], actions: [ { _ in
         Singleton.sharedInstance.logoutButtonAction()
         }])
   }
   
   func submitTemplate() {
      signupTemplate?.submitToServer { [weak self] (success, error, registrationStatus) in
         guard success else {
            print("Submission of template to server failed")
            return
         }
         
         let newRegistrationStatus = RegistrationStatus(rawValue: registrationStatus!)!
         self?.registrationStatus = newRegistrationStatus
         Vendor.current!.registrationStatus = newRegistrationStatus

         self?.proceedAccordingToRegistrationStatus()
      }
   }
   
   private func proceedAccordingToRegistrationStatus() {
      
      switch registrationStatus {
      case .acknowledgementPending, .verified:
         Singleton.sharedInstance.checkForLogin(owener: UIApplication.shared.keyWindow!.rootViewController!, startLoaderAnimation: {
            ActivityIndicator.sharedInstance.showActivityIndicator()
         }, stopLoaderAnimation: {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
         })
      case .otpVerified where signupTemplate == nil:
         navigateToReviewScreenWithVerificationPendingStatus()
      case .otpVerified:
         pushListSignupViewController()
      case .verificationPending:
         navigateToReviewScreenWithVerificationPendingStatus()
      case .rejected:
         navigateToReviewScreenWithRejectedStatus()
      case .resubmitVerification:
         navigateToSignupVCWhenTemplateHasToBeResubmitted()
      case .otpPending:
         OtpScreenViewController.pushIn(navVC: navVC)
      }
   }
   
   
   private func pushListSignupViewController() {
      ListOfSignupTemplateViewController.pushControllerIn(navVC: navVC, withFlowManager: self, signupTemplate: signupTemplate!)
   }
   
   private func navigateToReviewScreenWithVerificationPendingStatus() {
      let params = getParamsToShowReviewScreen()
      
      guard let reviewVC = navVC.topViewController as? ReviewViewController else {
         ReviewViewController.pushIn(navVC: navVC, params: params, flowManager: self)
         return
      }
      
      reviewVC.updateParams(params: params)
   }
   
   private func getParamsToShowReviewScreen() -> ReviewViewController.Parameters {
      return ReviewViewController.Parameters(title: TEXT.accountBeingReviewed, configurableMessage: Vendor.current!.reviewMessage, refreshButtonTitle: TEXT.refresh, isRefreshButtonHidden: false)
   }
   
   private func navigateToReviewScreenWithRejectedStatus() {
      let params = getParamsToShowRejectedStateOfReview()
      
      guard let reviewVC = navVC.topViewController as? ReviewViewController else {
         ReviewViewController.pushIn(navVC: navVC, params: params, flowManager: self)
         return
      }
      
      reviewVC.updateParams(params: params)
   }
   
   private func getParamsToShowRejectedStateOfReview() -> ReviewViewController.Parameters {
      return ReviewViewController.Parameters(title: TEXT.accountRejected, configurableMessage: "", refreshButtonTitle: "", isRefreshButtonHidden: true)
   }
   
   private func navigateToSignupVCWhenTemplateHasToBeResubmitted() {
      guard let signupTemplateVC = getSignupTemplateVCFromNavVCIfItExists() else {
         pushListSignupViewController()
         return
      }
      
      signupTemplateVC.signupTemplate = self.signupTemplate
      navVC.popToViewController(signupTemplateVC, animated: true)
   }
   
   private func getSignupTemplateVCFromNavVCIfItExists() -> SignupTemplateViewController? {
      for viewController in navVC.viewControllers.reversed() {
         if let signupTemplateVC = viewController as? SignupTemplateViewController {
            return signupTemplateVC
         }
      }
      
      return nil
   }
   
   func refreshToCheckReviewStatus() {
      Vendor.getNewVendorDetail { [weak self] (vendor) in
         guard vendor != nil else {
            return
         }
         
         self?.registrationStatus = vendor!.registrationStatus
         self?.signupTemplate = vendor?.signupTemplate
         
         if self?.registrationStatus == .resubmitVerification {
            ErrorView.showWith(message: TEXT.resubmitTemplate, removed: nil)
         }
         
         Vendor.current = vendor
         self?.proceedAccordingToRegistrationStatus()
      }
      
   }
   
   func registrationStatusUpdatedTo(statusRawValue: Int) {
      guard let registrationStatus = RegistrationStatus(rawValue: statusRawValue) else {
         fatalError("Raw value of registration status is not correct in Push Notification")
      }
      self.registrationStatus = registrationStatus
      Vendor.current?.registrationStatus = registrationStatus
      
      proceedAccordingToRegistrationStatus()
   }
   
   // MARK: -Type Methods
   static func startSignupTemplateFlowIn(navVC: UINavigationController, withSignupTemplate signupTemplate: SignupTemplate?, andWithRegistrationStatus registrationStatus: RegistrationStatus) {
      let flowManager = SignupTemplateFlowManager(navVC: navVC, signupTemplate: signupTemplate, registrationStatus: registrationStatus)
      shared = flowManager
      flowManager.startFlow()
   }
   
   // MARK: - Deinit
   deinit {
      print("Flow manager deinitialized")
   }
}
