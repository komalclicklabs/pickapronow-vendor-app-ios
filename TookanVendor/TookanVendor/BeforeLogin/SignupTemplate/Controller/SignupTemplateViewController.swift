//
//  SignupTemplateViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 15/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class SignupTemplateViewController: UIViewController {
   
   // MARK: - Properties
   var flowManager: SignupTemplateFlowManager!
   var signupTemplate: SignupTemplate!
   private var tableViewDataSource: SignupTemplateDataSource!
   
   // MARK: - IBOutlets
   @IBOutlet weak var headerLabel: UILabel!
   @IBOutlet weak var continueButton: UIButton!
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var tableViewDistanceFromTop: NSLayoutConstraint!

   // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
      setupTableView()
      configureViews()
      tableViewDistanceFromTop.constant = HEIGHT.navigationHeight
      setnavigationBar()
    }
   
   private func setupTableView() {
      registerTableViewCellNibs()
      tableViewDataSource = SignupTemplateDataSource(customFields: signupTemplate.customFields)
      tableView.dataSource = tableViewDataSource
      tableView.delegate = self
      tableView.estimatedRowHeight = 57
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.separatorStyle = .none
   }
   
   private func configureViews() {
      configureTableView()
      configureTitleLabel()
      configureContinueButton()
   }
   
   private func configureTableView() {
      tableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
   }
   
   private func configureTitleLabel() {
      headerLabel.configureSignInTitleTypeWith(text: TEXT.signupTemplateTitle)
   }
   
   private func configureContinueButton() {
      continueButton.configureNormalButtonWith(title: TEXT.SUBMIT)
   }
   
   private func setnavigationBar() {
      let navBarParams = NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"))
     let navBar = NavigationView.getNibFile(params: navBarParams, leftButtonAction: { [weak self] in
         self?.flowManager.goBackToViewTemplateList()
      }, rightButtonAction: nil)
      navBar.isBottomLineVisible = false
      navBar.isShadowVisible = false
      view.addSubview(navBar)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      setupTableView()
      tableView.reloadData()
      UIApplication.shared.statusBarStyle = .default
   }
   
   // MARK: - IBAction
   @IBAction func continueButtonPressed(_ sender: UIButton) {
      flowManager.submitTemplate()
   }
   
   // MARK: - Methods
   func registerTableViewCellNibs(){
      tableView.register(UINib(nibName: NIB_NAME.TaskDescriptionCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.taskDescription)
      tableView.register(UINib(nibName: NIB_NAME.CustomFTextField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.customTextField)
      tableView.register(UINib(nibName: NIB_NAME.AddressCustomeField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.AddressCustomeField)
      tableView.register(UINib(nibName: NIB_NAME.phoneNumberCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.phoneCode)
      tableView.register(UINib(nibName: NIB_NAME.DateCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.dateCustomField)
      tableView.register(UINib(nibName: NIB_NAME.CheckBoxCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.custCheckBox)
      tableView.register(UINib(nibName: NIB_NAME.DropDownCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.DropDownCell)
      tableView.register(UINib(nibName: NIB_NAME.TextFieldForUnEditing, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.UnEdit)
      tableView.registerCellWith(nibName: NIB_NAME.checkoutCartCell, reuseIdentifier: CELL_IDENTIFIER.checkoutCartCell)
      tableView.registerCellWith(nibName: NIB_NAME.checkoutSubtotalCell, reuseIdentifier: CELL_IDENTIFIER.checkoutSubtotalCell)
      self.tableView.register(UINib.init(nibName: NIB_NAME.ImgesCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.ImageCustom)
   }
   
   // MARK: - Type Methods
   class func pushViewControllerIn(navVC: UINavigationController, withflowManager flowManager: SignupTemplateFlowManager, signupTemplate: SignupTemplate) {
      let vc = findIn(storyboard: .signUpTemplate, withIdentifier: "SignupTemplateViewController") as! SignupTemplateViewController
      vc.flowManager = flowManager
      vc.signupTemplate = signupTemplate
      navVC.pushViewController(vc, animated: true)
   }

}

extension SignupTemplateViewController: UITableViewDelegate {
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let cell = tableView.cellForRow(at: indexPath)
      
      if let _ = cell as? CheckBoxTableViewCell{
         (cell as! CheckBoxTableViewCell).updateStatus()
      }
      if let uneditCell = cell as? UnEditingTableViewCell,
         uneditCell.selfTypes == .url {
         
         let urlString = uneditCell.dataValue.text!
         guard isValidUrlString(urlString),
            let url = URL(string: urlString),
            UIApplication.shared.canOpenURL(url) else {
            ErrorView.showWith(message: ERROR_MESSAGE.invalidUrl, removed: nil)
            return
         }
         
         UIApplication.shared.openURL(url)
      }
   }
   
   fileprivate func isValidUrlString(_ string: String) -> Bool {
      return URL(string: string) != nil
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      let field = signupTemplate.customFields[indexPath.row]
      
      guard field.dataType.isDataTypeHandled() else {
         return 0.001
      }
      
      switch field.app_side! {
      case .hidden:
         return 0.001
      default:
         return UITableViewAutomaticDimension
      }
   }
}
