//
//  ReviewViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
   
   // MARK: - Properties
   private var params: Parameters!
   var flowManager: SignupTemplateFlowManager!
   
   // MARK: - IBOutlets
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var configurableMessage: UILabel!
   @IBOutlet weak var refreshButton: UIButton!
   @IBOutlet weak var logoutButton: UIButton!
   @IBOutlet weak var bottomDistanceFromLogoutButton: NSLayoutConstraint!
   
   // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
      configureViews()
    }
   
   private func configureViews() {
      titleLabel.configureSignInTitleTypeWith(text: params.title)
      configurableMessage.configureDescriptionTypeLabelWith(text: params.configurableMessage)
      configureLogoutButton()
      configureRefreshButton()
   }
   
   private func configureRefreshButton() {
      refreshButton.configureNormalButtonWith(title: TEXT.refresh)
      refreshButton.isHidden = params.isRefreshButtonHidden
   }
   
   private func configureLogoutButton() {
      if params.isRefreshButtonHidden {
         logoutButton.configureNormalButtonWith(title: TEXT.LOGOUT_BUTTON)
      } else {
         logoutButton.configureSmallButtonWith(title: TEXT.LOGOUT_BUTTON)
      }
      
      bottomDistanceFromLogoutButton.constant = params.isRefreshButtonHidden ? 30 : 10
   }
   

   // MARK: - IBAction
   @IBAction func refreshButtonPressed(_ sender: UIButton) {
      flowManager.refreshToCheckReviewStatus()
   }
   
   @IBAction func logoutButtonPressed(_ sender: UIButton) {
      flowManager.logoutFromApp()
   }
   
   // MARK: - Methods
   func updateParams(params: Parameters) {
      self.params = params
      configureViews()
   }
   
   // MARK: - Type Methods
   class func pushIn(navVC: UINavigationController, params: Parameters, flowManager: SignupTemplateFlowManager) {
      let vc = findIn(storyboard: .signUpTemplate, withIdentifier: "ReviewViewController") as! ReviewViewController
      vc.params = params
      vc.flowManager = flowManager
      navVC.pushViewController(vc, animated: true)
   }


}

extension ReviewViewController {
   struct Parameters {
      var title: String
      var configurableMessage: String
      var refreshButtonTitle: String
      var isRefreshButtonHidden: Bool
   }
}
