//
//  ListOfSignupTemplateViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class ListOfSignupTemplateViewController: UIViewController {

   // MARK: - Properties
   var signupTemplate: SignupTemplate?
   var flowManager: SignupTemplateFlowManager?
   
   // MARK: - IBOutlets
   @IBOutlet weak var headerLabel: UILabel!
   @IBOutlet weak var subtitleLabel: UILabel!
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var continueButton: UIButton!
   @IBOutlet weak var logoutButton: UIButton!
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      setUpTableView()
      configureViews()
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      tableView.reloadData()
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      
      let heightDifferenceBetweenTableViewAndScreen = (SCREEN_SIZE.height - 80) - tableView.contentSize.height
      if heightDifferenceBetweenTableViewAndScreen > 0 {
         tableView.tableFooterView?.frame.size.height = heightDifferenceBetweenTableViewAndScreen + 174
      }
   }
   
   private func setUpTableView() {
      tableView.dataSource = self
      tableView.delegate = self
      tableView.estimatedRowHeight = 35
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.separatorStyle = .none
   }
   
   private func configureViews() {
      headerLabel.configureSignInTitleTypeWith(text: TEXT.signupTemplateTitle)
      subtitleLabel.configureDescriptionTypeLabelWith(text: TEXT.signupTemplateSubTitle)
      continueButton.configureNormalButtonWith(title: TEXT.CONTINUE)
      logoutButton.configureSmallButtonWith(title: TEXT.LOGOUT_BUTTON)
   }
   
   // MARK: - IBAction
   @IBAction func continueButtonPressed(_ sender: UIButton) {
      flowManager?.continueToFillTheTemplate()
   }
   
   @IBAction func logoutButtonPressed(_ sender: UIButton) {
      self.flowManager?.logoutFromApp()
   }
   
   // MARK: - Type Methods
   class func pushControllerIn(navVC: UINavigationController, withFlowManager flowManager: SignupTemplateFlowManager, signupTemplate: SignupTemplate) {
      let vc = findIn(storyboard: .signUpTemplate, withIdentifier: "ListOfSignupTemplateViewController") as! ListOfSignupTemplateViewController
      vc.signupTemplate = signupTemplate
      vc.flowManager = flowManager
      navVC.pushViewController(vc, animated: true)
   }
  

}

// MARK: - tableView Datasource
extension ListOfSignupTemplateViewController: UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return signupTemplate?.customFields.count ?? 0
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let field = signupTemplate!.customFields[indexPath.row]
      return getListCellFor(indexPath: indexPath, forField: field)
   }
   
   private func getListCellFor(indexPath: IndexPath, forField customField: CustomFieldDetails) -> ListOfSignupTemplateTableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: ListOfSignupTemplateTableViewCell.identifier, for: indexPath) as! ListOfSignupTemplateTableViewCell
      cell.setTitle(customField.display_name)
      return cell
   }
}

// MARK: - TableView Delegate
extension ListOfSignupTemplateViewController: UITableViewDelegate {
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      let field = signupTemplate?.customFields[indexPath.row]
      
      guard field?.dataType.isDataTypeHandled() == true else {
         return 0.001
      }
      
      if field?.app_side == .hidden {
         return 0.001
      }
      
      return UITableViewAutomaticDimension
   }
}
