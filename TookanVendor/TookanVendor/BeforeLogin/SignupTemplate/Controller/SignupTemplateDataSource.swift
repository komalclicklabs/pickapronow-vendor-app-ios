//
//  SignupTemplateDataSource.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 18/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit


class SignupTemplateDataSource: NSObject, UITableViewDataSource {
   
   private var customFields: [CustomFieldDetails]
   var imageSelectManager: MediaSelector?
   
   init(customFields: [CustomFieldDetails]) {
      self.customFields = customFields
   }
   
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return customFields.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let field = customFields[indexPath.row]
      
      switch field.dataType {
      case .name, .description, .Text, .email, .Number, .url:
         if field.app_side.isEditable {
            return getCellWithTextField(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
         } else {
            return getUnEditableCell(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
         }
      case .phonenumber:
         return getPhoneNumberCell(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
         
      case .startDateTime, .endDateTime, .date, .dateTime, .dateTimePast, .dateTimeFuture, .dateFuture, .datePast:
         return getDateTypeCell(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
      case .checkBox:
         return getCheckBoxCell(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
         
      case .DropDown:
         return getDropDownCell(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
         
      case .image:
         if !field.app_side.isEditable && (field.data == "-" || field.data == "") {
            return getUnEditableCell(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
         }
         return getImageCell(forIndexPath: indexPath, inTableView: tableView, withCustomField: field)
      default:
         return UITableViewCell()
      }

   }
   
   
   private func getCellWithTextField(forIndexPath indexPath: IndexPath, inTableView tableView: UITableView, withCustomField customField: CustomFieldDetails) -> CustomFTextFieldCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.customTextField, for: indexPath) as? CustomFTextFieldCell
      cell?.setUpForCustomField(type: customField.dataType, dataForNonStatic: customField, header: customField.display_name)
      return cell!
   }
   
   private func getPhoneNumberCell(forIndexPath indexPath: IndexPath, inTableView tableView: UITableView, withCustomField customField: CustomFieldDetails) -> PhoneNumberTableViewCell {
      let phoneCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.phoneCode, for: indexPath) as? PhoneNumberTableViewCell
      
      phoneCell?.setUpPhoneNumberCell(fortype: customField.dataType, headerName: customField.display_name, isStatic: false, dataForNonStatic: customField, data: customField.data ?? "")
      return phoneCell!
   }
   
   private func getDateTypeCell(forIndexPath indexPath: IndexPath, inTableView tableView: UITableView, withCustomField customField: CustomFieldDetails) -> DateCustomTableViewCell {
      let dateCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.dateCustomField, for: indexPath) as? DateCustomTableViewCell
      
      dateCell?.setUpForCustomField(type: customField.dataType, dataForNonStatic: customField, header: customField.display_name)
      
      return dateCell!
   }
   
   private func getCheckBoxCell(forIndexPath indexPath: IndexPath, inTableView tableView: UITableView, withCustomField customField: CustomFieldDetails) -> CheckBoxTableViewCell {
      let checkBoxCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.custCheckBox, for: indexPath) as? CheckBoxTableViewCell
      checkBoxCell?.setUpCheckBoxCell(data: customField, header: customField.display_name)
      return checkBoxCell!
   }
   
   private func getDropDownCell(forIndexPath indexPath: IndexPath, inTableView tableView: UITableView, withCustomField customField: CustomFieldDetails) -> DropDownTableViewCell {
      let dropDownCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.DropDownCell, for: indexPath) as? DropDownTableViewCell
      dropDownCell?.setUpDropDown(data: customField)
      return dropDownCell!
   }
   
   private func getUnEditableCell(forIndexPath indexPath: IndexPath, inTableView tableView: UITableView, withCustomField customField: CustomFieldDetails) -> UnEditingTableViewCell {
      let unEditCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.UnEdit, for: indexPath) as? UnEditingTableViewCell
      
      unEditCell?.setUpCellWithData(label: customField.display_name, data: customField.data!, type:customField.dataType)
      
      return unEditCell!
   }
   
   private func getImageCell(forIndexPath indexPath: IndexPath, inTableView tableView: UITableView, withCustomField customField: CustomFieldDetails) -> ImagesCustomTableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.ImageCustom) as! ImagesCustomTableViewCell
      cell.imageCollectionView.tag = indexPath.row
      cell.selectionStyle = .none
      
      let images = customField.data?.jsonObjectArray
      cell.setImageCollectionView(label: customField.display_name, imageArray: images!)
      
      cell.onAddbuttonTap = { [weak self] in
         self?.addButtonTappedIn(tableView: tableView, forCustomField: customField)
      }
      
      cell.imageTappedAtIndex = { [weak self] (index) in
         self?.imageTappedIn(tableView: tableView, atIndex: index, forCustomField: customField)
      }
      
      if customField.app_side == .readOnly  || customField.app_side == .hidden  {
         cell.isForEditing = false
      }else{
         cell.isForEditing = true
      }
      return cell
   }
   
   private func addButtonTappedIn(tableView: UITableView, forCustomField customField: CustomFieldDetails) {
      if customField.checkIfImageIsBeingUploaded() == false{
         selectImageFor(customField: customField, tableView: tableView)
      }else{
         ErrorView.showWith(message: ERROR_MESSAGE.IMAGE_UPLOADING, isErrorMessage: true, removed: nil)
      }
   }
   
   private func selectImageFor(customField: CustomFieldDetails, tableView: UITableView) {
      imageSelectManager = MediaSelector { result in
         guard result.isSuccessful else {
            ErrorView.showWith(message: result.error!.localizedDescription, removed: nil)
            return
         }
         
         customField.startImageUploadingProcessForImagePath(path: result.filePath!)
         tableView.reloadData()
         let indexAtWhichImageIsBeingUploaded = (customField.getImageArrayCount() - 1)
         CustomFieldDetails.uploadImageWith(path: result.filePath!) { (success, referencePath) in
            guard success else {
               customField.deleteImageAt(index: indexAtWhichImageIsBeingUploaded)
               tableView.reloadData()
               return
            }
            customField.imageUpadationSuccessfulAt(index: indexAtWhichImageIsBeingUploaded, withPath: referencePath!)
            tableView.reloadData()
         }
      }
      
      openImagePickerAccordingToAttribute(attribute: customField.attribute)
   
   }
   
   private func openImagePickerAccordingToAttribute(attribute: ImageSource) {
      let imageFileName = Singleton.sharedInstance.convertDateToString()
      let vc = UIApplication.shared.keyWindow!.rootViewController!
      
      switch attribute {
      case .camera:
         imageSelectManager?.openCameraFor(fileName: imageFileName, fileType: .image, inViewController: vc)
      case .photoLibrary:
         imageSelectManager?.openPhotoLibraryFor(fileName: imageFileName, fileType: .image, inViewController: vc)
      case .cameraAndPhotoLibrary:
         imageSelectManager?.selectImage(viewController: vc, fileName: imageFileName, fileType: .image)
      }
   }
   
   private func imageTappedIn(tableView: UITableView, atIndex index: Int, forCustomField customField: CustomFieldDetails) {
      
      guard !customField.checkIfImageIsBeingUploaded() else {
         ErrorView.showWith(message: ERROR_MESSAGE.IMAGE_UPLOADING, removed: nil)
         return
      }
      let arrayOfImagePath = (customField.data?.jsonObjectArray as? [String]) ?? [""]
      let imagePreview = ImagesPreview.loadForImagePaths(arrayOfPath: arrayOfImagePath, whilePreviewingImageAtIndex: index)
      imagePreview.deleteButton.isHidden = customField.app_side == .readOnly
      imagePreview.deleteAtIndex = { (index) in
         customField.deleteImageAt(index: index)
         imagePreview.confirmationDoneForDeletion()
         tableView.reloadData()
      }
      imagePreview.viewWillBeRemoved = {}
   }
   
}




