//
//  ListOfSignupTemplateTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class ListOfSignupTemplateTableViewCell: UITableViewCell {
   
   static let identifier = "ListOfSignupTemplateTableViewCell"
   
   // MARK: - IBOutlets
   @IBOutlet weak var tickImage: UIImageView!
   @IBOutlet weak var titleName: UILabel!

   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      
      selectionStyle = .none
      configureViews()
   }
   
   
   func configureViews() {
      titleName.configureDescriptionTypeLabelWith(text: "")
   }
   
   func setTitle(_ title: String) {
      titleName.text = title
   }


}
