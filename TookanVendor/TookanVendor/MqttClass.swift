//
//  MqttClass.swift
//  Tookan
//
//  Created by cl-macmini-45 on 11/07/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit
import CoreLocation
//import CocoaMQTT

open class MqttClass: NSObject {
    
    static let sharedInstance = MqttClass()
    var mqtt: CocoaMQTT?
    var didConnectAck = false
    var hostAddress = "tracking.tookan.io"
    var portNumber:UInt16 = 1883
    var topic = ""
    
    override init() {
        mqtt = CocoaMQTT(clientId: "", host: hostAddress, port:portNumber)
    }
    
    func connectToServer() {
        _ = mqtt!.connect()
    }
    
    func mqttSetting() {
        if let mqtt = mqtt {
            mqtt.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
            mqtt.keepAlive = 90
            mqtt.delegate = self
        }
    }
    
    func disconnect() {
        mqtt!.disconnect()
    }
    
    func subscribeLocation() {
        print(self.topic)
        if IJReachability.isConnectedToNetwork() == true {
            if(didConnectAck == true) {
                UserDefaults.standard.set(true, forKey: "isHitInProgress")
                _ = mqtt?.subscribe(topic: self.topic, qos: CocoaMQTTQOS.QOS1)
            } else {
                if(mqtt?.connState == CocoaMQTTConnState.DISCONNECTED) {
                    self.mqttSetting()
                    self.connectToServer()
                }
            }
        }
    }
    
    func isConnectionEstablished() -> Bool {
        if(mqtt?.connState == CocoaMQTTConnState.DISCONNECTED || mqtt?.connState == CocoaMQTTConnState.CONNECTING) {
            return false
        }
        return true
    }
    
    func unsubscribeLocation() {
       _ = mqtt?.unsubscribe(topic: self.topic)
    }
}

extension MqttClass: CocoaMQTTDelegate {
    
    public func mqtt(_ mqtt: CocoaMQTT, didConnect host: String, port: Int) {
        print("didConnect \(host):\(port)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        if ack == .ACCEPT {
           // _ = mqtt.subscribe(topic: self.accessToken, qos: CocoaMQTTQOS.QOS1)
            mqtt.ping()
            didConnectAck = true
            if UserDefaults.standard.bool(forKey:"subscribeLocation") == true {
                _ = mqtt.subscribe(topic: self.topic, qos: CocoaMQTTQOS.QOS1)
            }
           // _ = mqtt.publish(topic: "UpdateLocation", withString:"Hello" , qos: .QOS1)
        }
        
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
    
    }
    
    
    
    
    public func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("didPublishAck with id: \(id)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let locationDictionaryArray = message.string?.jsonObjectArray
        if locationDictionaryArray!.count > 0 {
        let locationDictionary = message.string?.jsonObjectArray[0] as! [String:Any]
        print("\(locationDictionary)" + "the location Dictionary")
        if let locationArray = locationDictionary["location"] as? [Any] {
            Singleton.sharedInstance.parsingLocations(locationArray: locationArray)
        }
        }else {
            if message.string == "NaN"{
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.stopTracking), object: nil)
            }
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
        print("didSubscribeTopic to \(topic)")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        print("didUnsubscribeTopic to \(topic)")
    }
    
    public func mqttDidPing(_ mqtt: CocoaMQTT) {
        print("didPing")
    }
    
    public func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        _console("didReceivePong")
    }
    
    public func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: NSError?) {
        didConnectAck = false
        _console("mqttDidDisconnect")
        print(err?.localizedDescription ?? "error")
        if UserDefaults.standard.bool(forKey: "subscribeLocation") == true {
            if(mqtt.connState == CocoaMQTTConnState.DISCONNECTED) {
                self.mqttSetting()
                self.connectToServer()
            }
        }
    }
    
    func _console(_ info: String) {
        print("Delegate: \(info)")
    }
}
