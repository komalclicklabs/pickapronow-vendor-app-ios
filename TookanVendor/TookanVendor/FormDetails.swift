//
//  FormDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 03/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

enum Vertical: String {
   case pdaf = "0" // Pickup, Delivery, Appointment, FOS
   case taxi = "1"
   case multipleCategory = "2"
}


class FormDetails: NSObject {

   // MARK: - Properties
    var auto_assign:Int?
    var color:String?
    var domain_name:String?
    var font:String?
    var form_id:String?
    var logo:String?
    var pickup_delivery_flag:Int?
    var status:Int?
    var user_id:Int?
    var work_flow:Int?
   
    var call_tasks_as:String = "Task"
   var singularCallTaskAs: String {
      let indexOfLastCharacter = call_tasks_as.index(call_tasks_as.endIndex, offsetBy: -1)
      let callTaskAs = call_tasks_as[indexOfLastCharacter] == "s" ? call_tasks_as.substring(to: indexOfLastCharacter).lowercased() : call_tasks_as
      return callTaskAs
   }
   var callServiceProvidersAs = "Agent"
   
    var payementMethod = Int()
    var toShowPayment = false
    var forcePickupDelivery = 0
    var verticalType = Vertical.pdaf
    var waiting_screen_time = Int()
    var isDestinationRequired = String()
    var cancelDict = [String: Any]()
    var paymentMethodArray = [Int]()
    var isRatingRequired  = "0"
    var isSchedulingEnabled = "0"
    var scheduleOffsetTime = Int()
    var currencyid = String()
    var formName = String()
    var showPrefilledDataType = "0"
    
   var isNLevel = false
   var nLevelController: NLevelFlowManager?
   
   var showServiceProvider = false
  
   var tipDetails: Tip
   
   // MARK: - Intializers

    init(json: [String: Any]) {
      
        if let value = json["show_prefilled_data"] as? String{
            self.showPrefilledDataType  = value
        }else if let value = json["show_prefilled_data"] as? Int{
            self.showPrefilledDataType = "\(value)"
        }
        
        if let value = json["form_name"] as? String{
            self.formName = value
        }
      
        if let value = json["logo"] as? String{
            self.logo = value
        }
      
        if let value = json["auto_assign"] as? String {
            self.auto_assign = Int(value)
        } else if let value = json["auto_assign"] as? Int {
            self.auto_assign = value
        } else {
            self.auto_assign = 0
        }
   
        if let value = json["domain_name"] as? String {
            self.domain_name = value
        } else {
            self.domain_name = ""
        }
        
        if let value = json["pickup_delivery_flag"] as? String {
            self.pickup_delivery_flag = Int(value)
        } else if let value = json["pickup_delivery_flag"] as? Int {
            self.pickup_delivery_flag = value
        } else {
            self.pickup_delivery_flag = 0
        }
      
      tipDetails = Tip(json: json)
      isNLevel =  (json["is_nlevel"] as? Bool) ?? false
        
        if let value = json["work_flow"] as? String {
            self.work_flow = Int(value)!
        } else if let value = json["work_flow"] as? Int {
            self.work_flow = value
        } else {
            self.work_flow = 0
        }
        
        if let value = json["call_tasks_as"] as? String {
            self.call_tasks_as = value
        }
      
      showServiceProvider = (json["show_service_providers"] as? Bool) ?? false
        
        if let value = json["payment_methods"] as? [Any]{
            self.paymentMethodArray = []
            for i in value{
                print(i)
                if let paymentData = i as? [String:Any]{
                    if let isEnabled = paymentData["enabled"] as? Int{
                        if isEnabled == 1{
                            toShowPayment = true
                            if let value = paymentData["value"] as? Int{
                                self.payementMethod = value
                                self.paymentMethodArray.append(value)
                            }
                        }else{
                            toShowPayment = false
                        }
                    }
                }
                if paymentMethodArray.count > 0{
                    self.payementMethod = paymentMethodArray[0]
                }
            }
            
            if self.paymentMethodArray.count > 0{
                self.toShowPayment   = true
            }else{
                self.toShowPayment = false
            }
        }
      
        if let value = json["force_pickup_delivery"] as? Int{
            forcePickupDelivery = value
        }else if let value = json["force_pickup_delivery"] as? String{
            if let _ = Int(value){
                forcePickupDelivery = Int(value)!
            }
        }
        
        if let value = json["user_id"] as? Int{
            self.user_id = value
        }else if let value = json["user_id"] as? String{
            self.user_id = Int(value)
        }
        
        if let rawVertical = json["vertical"] {
            self.verticalType = Vertical(rawValue: "\(rawVertical)")!
        }
        
        if let value = json["waiting_screen_time"] as? String{
            self.waiting_screen_time = Int(value)!
        }else if let value = json["waiting_screen_time"] as? Int{
            self.waiting_screen_time = value
        }
      
        if let value = json["is_destination_required"] as? String{
            self.isDestinationRequired = value
        }else if let value = json["is_destination_required"] as? Int{
            self.isDestinationRequired = "\(value)"
        }
        
        if let value = json["cancel_config"] as? [Any]{
            self.cancelDict = [String:String]()
            for i in value {
                if let data = i as? [String:Any]{
                    cancelDict[returnData(key: "job_status", fromObject: data)] = returnData(key: "is_cancel", fromObject: data)
                }
            }
            
        }
      
        if let value = json["is_rating_required"] as? String{
            self.isRatingRequired = value
        }else if let value = json["is_rating_required"] as? Int {
            self.isRatingRequired = "\(value)"
        }
        
        if let value = json["form_id"] as? String{
            self.form_id = value
        }else if let value = json["form_id"] as? Int{
            self.form_id  = "\(value)"
        }
      
        if let value = json["is_scheduling_enabled"] as? String{
            self.isSchedulingEnabled = value
        }else if let value = json["is_scheduling_enabled"] as? Int{
            self.isSchedulingEnabled = "\(value)"
        }
      
        if let value = json["schedule_offset_time"] as? String{
            self.scheduleOffsetTime = Int(value)!
        } else if let value = json["schedule_offset_time"] as? Int{
            self.scheduleOffsetTime = value 
        }
//        
        if let value = json["payment_settings"] as? [Any]{
            print(value)
            
            for i in value{
                if let data = i as? [String:Any]{
                    if let symbol = data["symbol"] as? String{
                        self.currencyid = symbol
                    }
                }
            }
        }
    }
   
   // MARK: - Methods
    func getPrefiledData() -> ConstantFieldHandlingType {
        switch self.showPrefilledDataType {
        case "0":
            return .empty
        case "1":
            return .showPrefiled
        case "2":
            return .hidePrefiled
        default:
            return .empty
        }
    }
   
   func hasPaymentMethodOtherThanCash() -> Bool {
      if paymentMethodArray.count == 1 &&  hasCashAsAPaymentMethod() {
         return false
      }
      return true
   }
   
   func hasCashAsAPaymentMethod() -> Bool {
      return paymentMethodArray.contains(8)
   }
   
   // MARK: - Type Methods
   
   class func fetchOfferingsFromServer(completed: ((_ success: Bool, _ error: Error?) -> Void)?) {
      let param = [
         "access_token" : Vendor.current!.appAccessToken!,
         "device_token" : APIManager.sharedInstance.getDeviceToken(),
         "app_version"  : APP_VERSION,
         "app_device_type" : APP_DEVICE_TYPE,
         "app_access_token" : Vendor.current!.appAccessToken!,
         "user_id" : "\(Vendor.current!.userId!)"
         ] as [String: Any]
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: param, extendedUrl: API_NAME.getSuperCategories) { (responseObject, error, _, statusCode) in
         guard statusCode == STATUS_CODES.SHOW_DATA,
         let response = responseObject as? [String: Any] else {
            completed?(false, error)
            return
         }
         
            Singleton.sharedInstance.WorkFlowFormDetailsOptions.removeAll()
            Singleton.sharedInstance.servicesOptionPickUpDetails.removeAll()
            Singleton.sharedInstance.servicesOptionsDeliveryDetails.removeAll()
            Singleton.sharedInstance.servicesTypeCategories.removeAll()
            
            if let arrayOfOfferings = response["data"] as? [[String: Any]] {
               for offering in arrayOfOfferings {
                  
                  Singleton.sharedInstance.WorkFlowFormDetailsOptions.append(FormDetails(json: offering))
                  
                  if let userOptions = offering["userOptions"] as? [String: Any] {
                     Singleton.sharedInstance.servicesOptionPickUpDetails.append(userOptions)
                  } else {
                     Singleton.sharedInstance.servicesOptionPickUpDetails.append([String:Any]())
                  }
                  
                  if let userOptions = offering["deliveryOptions"] as? [String:Any] {
                     Singleton.sharedInstance.servicesOptionsDeliveryDetails.append(userOptions)
                  } else {
                     Singleton.sharedInstance.servicesOptionsDeliveryDetails.append([String:Any]())
                  }
                  
                  
                  if let typeCategories = offering["categories"] as? [Any] {
                     print(typeCategories)
                     var categories = [CarTypeModal]()
                     for i in typeCategories{
                        if let data = i as? [String:Any]{
                           categories.append(CarTypeModal(json:data))
                        }
                     }
                     Singleton.sharedInstance.servicesTypeCategories.append(categories)
                  }else{
                     Singleton.sharedInstance.servicesTypeCategories.append([CarTypeModal]())
                  }
               }
               
               if Singleton.sharedInstance.WorkFlowFormDetailsOptions.count == 1{
                  Singleton.sharedInstance.formDetailsInfo = Singleton.sharedInstance.WorkFlowFormDetailsOptions[0]
                  Singleton.sharedInstance.customFieldOptionsPickup = Singleton.sharedInstance.servicesOptionPickUpDetails[0]
                  Singleton.sharedInstance.customFieldOptionsDelivery = Singleton.sharedInstance.servicesOptionsDeliveryDetails[0]
                  Singleton.sharedInstance.typeCategories = Singleton.sharedInstance.servicesTypeCategories[0]
               }
               completed?(true, nil)
               
            }
         
      }
   }
}



func returnData(key: String, fromObject: [String:Any]) -> String {
    if let value = fromObject[key] as? String{
        return value
    }else if let value = fromObject[key] as? Int{
        return "\(value)"
    }else{
        return ""
    }
}
