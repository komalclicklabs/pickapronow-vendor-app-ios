//
//  OrderDetailModel.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 27/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class OrderDetailModel: NSObject {

    func getUserName(orderDetails:OrderDetails) -> String {
        switch orderDetails.job_type {
        case JOB_TYPE.pickup: 
            return orderDetails.job_pickup_name
        default:
            return orderDetails.customer_username
        }
    }
    
    func getAddress(orderDetails:OrderDetails) -> String {
        switch orderDetails.job_type {
        case JOB_TYPE.pickup:
            return orderDetails.job_pickup_address
        default:
            return orderDetails.job_address
        }
    }
    
    func getDatetime(orderDetails:OrderDetails) -> String {
        switch orderDetails.job_type {
        case JOB_TYPE.pickup:
            print(orderDetails.job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: "MM/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a"))
            
            return orderDetails.job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: "MM/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a")
        case JOB_TYPE.delivery:
            
            print(orderDetails.job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: "MM/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a"))
            
            
            return orderDetails.job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: "MM/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a")
        default:
            
            print(orderDetails.job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: "MM/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a") + " - " + orderDetails.job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: "mm/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a"))
            
            return orderDetails.job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: "MM/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a") + " - " + orderDetails.job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: "MM/dd/yyyy hh:mm a", output: "dd, MMM yyyy, hh:mm a")
        }
    }
    
    func getPhoneNumber(orderDetails:OrderDetails) -> String {
        switch orderDetails.job_type {
        case JOB_TYPE.pickup:
            return orderDetails.job_pickup_phone
        default:
            return orderDetails.customer_phone
        }
    }
    
    func getUserEmail(orderDetails:OrderDetails) -> String {
        switch orderDetails.job_type {
        case JOB_TYPE.pickup:
            return orderDetails.job_pickup_email
        default:
            return orderDetails.customer_email
        }
    }
    
    func setupCustomFieldsInArray(cellArray:[CustomFieldTypes], customFieldDetails:[CustomFieldDetails]) -> [CustomFieldTypes] {
        var cellInfoArray = cellArray
        for i in 0..<customFieldDetails.count {
            let customField = customFieldDetails[i]
            print(customField)
            switch customField.dataType {
            case .BarCode:
                cellInfoArray.append(.BarCode)
                break
            case .checkBox:
                cellInfoArray.append(.checkBox)
                break
            case .checklist:
                break
            case .date:
                cellInfoArray.append(.date)
                break
            case .dateFuture:
                cellInfoArray.append(.dateFuture)
                break
            case .datePast:
                cellInfoArray.append(.datePast)
                break
            case .dateTime:
                cellInfoArray.append(.dateTime)
                break
            case .dateTimeFuture:
                cellInfoArray.append(.dateTimeFuture)
                break
            case .dateTimePast:
                cellInfoArray.append(.dateTimePast)
                break
            case .DropDown:
                cellInfoArray.append(.DropDown)
                break
            case .email:
                cellInfoArray.append(.email)
                break
            case .image:
                cellInfoArray.append(.image)
                break
            case .Number:
                cellInfoArray.append(.Number)
                break
            case .table:
                break
            case .phonenumber:
                cellInfoArray.append(.phonenumber)
                break
            case .Text:
                cellInfoArray.append(.Text)
                break
            case .url:
                cellInfoArray.append(.url)
                break
            default:
                break
            }
        }
        return cellInfoArray
    }
    
    
    
  
    
    
}

class TaskHistory:NSObject {
    
    var creationDatetime:String!
    var stateDescription:String!
    var type:String!
    
    init(json:[String:Any]) {
        
        if let value = json["creation_datetime"] as? String {
            self.creationDatetime = value
        } else {
            self.creationDatetime = ""
        }
        
      if let value = json["description"] as? String,
         let typeValue = json["type"] as? String {
         if typeValue == "state_changed" {
            self.stateDescription = value
         } else {
            self.stateDescription = typeValue
         }
      } else {
         self.stateDescription = "-"
      }
      
      
      
        if let typeValue = json["type"] as? String {
            self.type = typeValue
        } else {
            self.type = "-"
        }
        
    }
}
