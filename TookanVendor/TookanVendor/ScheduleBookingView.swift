//
//  ScheduleBookingView.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 05/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ScheduleBookingView: UIView,UITextFieldDelegate {
    @IBOutlet weak var scheduleBookingTextView: UITextField!

    @IBOutlet weak var textFieldBackgroundView: UIView!
    @IBOutlet weak var textfieldUpperConstraint: NSLayoutConstraint!
    @IBOutlet weak var overlay: UIView!
    var selectedDate = Date()
    var onDoneAction:((_ date:String,_ dateInDateFOrmat:Date)->Void)?
    var cancelAction:(()->Void)?
    
    override func awakeFromNib() {
       IQKeyboardManager.sharedManager().enable = false
       IQKeyboardManager.sharedManager().enableAutoToolbar = false
       textFieldBackgroundView.layer.cornerRadius = 10
       scheduleBookingTextView.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        addToolBar(textField: scheduleBookingTextView)
        self.scheduleBookingTextView.text = getMiniMumdate()
        scheduleBookingTextView.tintColor = UIColor.clear
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        //dateFormatter.data
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        selectedDate = sender.date
        print(sender.date)
        scheduleBookingTextView.text = dateFormatter.string(from: sender.date)
       
        
        
    }
    
    @IBAction func valuChange(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.dateAndTime
        sender.inputView = datePickerView
        datePickerView.minimumDate = Date().addingTimeInterval((TimeInterval(Singleton.sharedInstance.formDetailsInfo.scheduleOffsetTime) + 5) * 60)
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func didEndEditing(_ sender: UITextField) {
        
        
    }
    
    deinit {
         IQKeyboardManager.sharedManager().enableAutoToolbar = true
         IQKeyboardManager.sharedManager().enable = true
    }
    
    
    func addToolBar(textField: UITextField) {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = COLOR.popUpColor
        toolBar.backgroundColor = COLOR.popUpColor
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(donePressed))
        doneButton.tintColor = COLOR.THEME_FOREGROUND_COLOR
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
         cancelButton.tintColor = COLOR.THEME_FOREGROUND_COLOR
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    
    func donePressed() {
        self.endEditing(true)
      
      let dateFormatter = DateFormatter()
      //dateFormatter.data
      dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
      dateFormatter.locale = Locale(identifier: "en_US_POSIX")
      let dateInString = dateFormatter.string(from: selectedDate)
      
      self.onDoneAction!(dateInString,selectedDate)
    }
    
    func cancelPressed() {
        self.endEditing(true)
        self.cancelAction!()
        UIView.animate(withDuration: 0.4, animations: {
            self.textfieldUpperConstraint.constant = -60
        }, completion: { (true) in
            UIView.animate(withDuration: 0.5, animations: {
                self.overlay.alpha = 0
            }, completion: { (true) in
                self.removeFromSuperview()
                IQKeyboardManager.sharedManager().enable = true
                IQKeyboardManager.sharedManager().enableAutoToolbar = true
            })
        })
        
        // or do something
    }
    
    func getMiniMumdate() -> String{
        let dateFormatter = DateFormatter()
        //dateFormatter.data
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
        selectedDate = Date().addingTimeInterval((TimeInterval(Singleton.sharedInstance.formDetailsInfo.scheduleOffsetTime) + 5)*60)
        return dateFormatter.string(from: Date().addingTimeInterval((TimeInterval(Singleton.sharedInstance.formDetailsInfo.scheduleOffsetTime) + 5)*60))
    }
    
    
}
