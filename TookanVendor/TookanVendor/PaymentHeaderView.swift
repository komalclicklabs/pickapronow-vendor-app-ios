//
//  PaymentHeaderView.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class PaymentHeaderView: UIView {

    @IBOutlet weak var paymentTypeName: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var rightbutton: UIButton!
   @IBOutlet weak var didTapHeaderButton: UIButton!

    override func awakeFromNib() {
      super.awakeFromNib()
      
        self.loader.color  = COLOR.SPLASH_TEXT_COLOR
        loader.isHidden  = true
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.backgroundView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.paymentTypeName.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle.customizeSize())
        self.paymentTypeName.textColor = COLOR.SPLASH_TEXT_COLOR
        
        self.backgroundView.layer.shadowColor = UIColor.black.cgColor
        self.backgroundView.layer.shadowOpacity = 0.2
        self.backgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.backgroundView.layer.shadowRadius = 2
    }
   
   func startLoaderAnimation() {
      loader.isHidden = false
      loader.startAnimating()
   }
   
   func stopLoaderAnimation() {
      loader.isHidden = true
      loader.stopAnimating()
   }
   
   func setRightButtonWithBordersWith(color: UIColor) {
      rightbutton.setBorder(borderColor: color)
      rightbutton.backgroundColor = color
      rightbutton._cornerRadius = 2
      rightbutton.setTitleColor(COLOR.SPLASH_BACKGROUND_COLOR, for: UIControlState.normal)
   }
   
   func setRightButtonWithThemeTextColor(title: String) {
      rightbutton.configureSmallButtonWith(title: title)
      rightbutton.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
   }


}
