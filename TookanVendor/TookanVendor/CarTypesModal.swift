//
//  CarTypesModal.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 22/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

class CarTypeModal {
    var carTypeName = String()
    var carImageString = String()
    var baseFare = String()
    var categoryId = String()
    var maxSize = String()
    var minimumFare = String()
    var perKilometerCharges = String()
    var perMinuteCharges = String()
    var eta = Double()
    var carArray = [carModal]()
    var selectedCategoryImage = String()
    var quantity = Int()
    var cancellation_charges = String()
    
    
    init(json:[String:Any]) {
        if let value = json["base_fare"] as? String{
            self.baseFare = value
        }else if let value = json["base_fare"] as? Double{
            self.baseFare = "\(value)"
        }
        
        if let value = json["category_id"] as? String{
            self.categoryId = value
        }else if let value = json["category_id"] as? Int{
            self.categoryId = "\(value)"
        }
        
        if let value = json["category_image"] as? String{
            self.carImageString = value
        }
        
        if let value = json["category_name"] as? String{
            self.carTypeName = value
        }
        
        if let value = json["max_size"] as? String{
            self.maxSize = value
        }else if let value = json["max_size"] as? Int{
            self.maxSize = "\(value)"
        }
        
        if let value = json["minimum_fare"] as? String{
            self.minimumFare = value
        }else if let value = json["minimum_fare"] as? Int{
            self.minimumFare = "\(value)"
        }
        
        if let value = json["per_kilometer_charges"] as? String{
            self.perKilometerCharges = value
        }else if let value = json["per_kilometer_charges"] as? Int{
            self.perKilometerCharges = "\(value)"
        }
        
        if let value = json["per_minute_charges"] as? String{
            self.perMinuteCharges = value
        }else if let value = json["per_minute_charges"] as? Int{
            self.perMinuteCharges = "\(value)"
        }
        
        if let value = json["taxis"] as? [Any]{
            for i in value {
                if let data = i as? [String:Any]{
                    self.carArray.append(carModal(json: data))
                }
            }
        }
        
        if let value = json["selected_category_image"] as? String{
            self.selectedCategoryImage = value
        }
        
        if let value = json["cancellation_charges"] as? String{
            self.cancellation_charges = value
        }else if let value = json["cancellation_charges"] as? Int{
            self.cancellation_charges = "\(value)"
        }
        findTheSmallestEta() 
        
    }
    
   private func findTheSmallestEta() {
      var lowest:Double = 100000.0
      for i in self.carArray{
         if i.eta < lowest{
            
            lowest = i.eta
         }
      }
      if lowest != 100000.0 && lowest != 0.0{
         self.eta = lowest
         
      }else{
         self.eta = 1
      }
   }
}
