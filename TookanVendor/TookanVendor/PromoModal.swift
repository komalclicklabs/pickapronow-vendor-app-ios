//
//  PromoModal.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation
//vendorPromos
class PromoCodeModal{
    
    var accessId = String()
    var benefitType = String()
    var description = String()
    var details = String()
    var promoCode = String()
    var promoId = String()
    var promoState = String()
    var promoType = String()
    var promoValue = String()
    var userId = String()
    var vendorId = String()
    var expiryDatetimeUtc = String()
    
    init(json:[String:Any]) {
        print(json)
        if let value = json["benefit_type"] as? String{
            self.benefitType = value
        }else if let value = json["benefit_type"] as? Int{
            self.benefitType = "\(value)"
        }
        
        if let value = json["access_id"] as? String{
            self.accessId = value
        }else if let value = json["access_id"] as? Int{
            self.accessId = "\(value)"
        }
        
        if let value = json["description"] as? String{
            self.description = value
        }
        
        if let value = json["details"] as? String{
            self.details = value
        }
        
        if let value = json["promo_code"] as? String{
            self.promoCode = value
        }else if let value = json["promo_code"] as? Int{
            self.promoCode = "\(value)"
        }
        
        if let value = json["promo_id"] as? String{
            self.promoId = value
        }else if let value = json["promo_id"] as? Int{
            self.promoId = "\(value)"
        }
        
        if let value = json["promo_state"] as? String{
            self.promoState = value
        }else if let value = json["promo_state"] as? Int{
            self.promoState = "\(value)"
        }
        
        if let value = json["promo_type"] as? String{
            self.promoType = value
        }else if let value = json["promo_type"] as? Int{
            self.promoType = "\(value)"
        }
        
        if let value = json["promo_value"] as? String{
            self.promoValue = value
        }else if let value = json["promo_value"] as? Double{
            self.promoValue = "\(value)"
        }
        
        if let value = json["expiry_datetime_utc"] as? String{
         self.expiryDatetimeUtc = value.convertDateFromUTCtoLocal(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: " dd MMM yyyy, hh:mm a")
            print(self.expiryDatetimeUtc)
        }
    }
    
    
}
