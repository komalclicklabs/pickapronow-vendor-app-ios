//
//  LocationTracker.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 01/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation

class LocationTracker: NSObject, CLLocationManagerDelegate {
    static let sharedInstance = LocationTracker()
    var myLocation = CLLocation()
    var locationManager = CLLocationManager()
    var firstTime = true

    func setLocationUpdate() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation //kCLLocationAccuracyThreeKilometers//
        locationManager.activityType = CLActivityType.automotiveNavigation
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.distanceFilter = 20 //kCLDistanceFilterNone//
//        if #available(iOS 9.0, *) {
//            locationManager.allowsBackgroundLocationUpdates = true
//        } else {
//            // Fallback on earlier versions
//        }
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        self.firstTime = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.last != nil {
            if firstTime == true {
                self.myLocation = locations.last! as CLLocation
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.currentLocationFetched), object: self)
                firstTime = false
            }
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }

    func checkAuthorizationForLocation() {
        if CLLocationManager.locationServicesEnabled() == false {
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.locationServicesDisabled), object: self)
        }
        else {
            let authorizationStatus = CLLocationManager.authorizationStatus()
            if authorizationStatus == CLAuthorizationStatus.denied || authorizationStatus == CLAuthorizationStatus.restricted {
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.locationServicesDisabled), object: self)
            }
            else {
                setLocationUpdate()
            }
        }

    }

    
}
