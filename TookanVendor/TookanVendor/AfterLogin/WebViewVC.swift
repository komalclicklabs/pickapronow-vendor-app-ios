//
//  WebViewVC.swift
//
//
//  Created by cl-macmini145 on 6/14/16.
//  Copyright © 2016 cl-macmini145. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController, UIWebViewDelegate {
   
   // MARK: - Properties
   private var webUrl = String()
   private var headerTitle = String()
   
   // MARK: - IBOutlets
    @IBOutlet var webView: UIWebView!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var backButton: UIButton!
    
   // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        titleLabel.text = headerTitle
        
        guard let url = URL(string: self.webUrl) else {
            print("Error - > Not valid URL")
            return
        }
        
        let requestObj = URLRequest(url: url)
        webView.loadRequest(requestObj)
      
      roundTopCorners()
    }
   
   private func configureViews() {
      view.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
   }
   
   private func configureTitleLabel() {
      
   }
   
   private func configureBackButton() {
      
   }
   
   private func roundTopCorners() {
      let shapeLayer = CAShapeLayer()
      let layerFrame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height)
      let roundCornersBezierPath = UIBezierPath(roundedRect: layerFrame, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 15, height: 15))
      shapeLayer.path = roundCornersBezierPath.cgPath
      
      
      webView.layer.mask = shapeLayer
   }
   
    // MARK: Web View Delegates
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
         UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    // MARK: Button Actions
    @IBAction func backButtonPressed(_ sender: AnyObject) {
      _ = self.navigationController?.popViewController(animated: true)
    }
   
    // MARK: - Navigation
    class func openWebViewVC(webUrl: String, title: String, vc: UIViewController) {
        
        guard let viewController = findIn(storyboard: .afterLogin, withIdentifier: "WebViewVC") as? WebViewVC else {
            fatalError("WebViewVC not found")
        }
        viewController.webUrl = webUrl
        viewController.headerTitle = title
        vc.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
}
