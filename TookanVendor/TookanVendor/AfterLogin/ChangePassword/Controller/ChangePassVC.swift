//
//  ChangePassVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/7/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class ChangePassVC: UIViewController,NavigationDelegate,CustomFieldDelegate,ErrorDelegate {
    
    
    @IBOutlet weak var backgroundView: UIView!
    let buttonHeight:CGFloat = 50.0
    var navigationBar:NavigationView!
    var titleView:TitleView!
    var changePassField:CustomTextField!
    var oldPassField:CustomTextField!

    var errorMessageView:ErrorView!
    let topMarginOfTextField:CGFloat = 20.0
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    @IBOutlet weak var enterNewPassLabel: UILabel!
    
    @IBOutlet weak var submitButton: UIButton!
    
    //MARK: - VIEW APPEAR FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.backgroundView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.setNavigationBar()
//        self.setTitleView()
        enterNewPassLabel.setLabelWithFontColorText(yourtext: TEXT.CHANGE_PASS_INFO_LABEL, yourColor:COLOR.SPLASH_TEXT_COLOR, fontSize: 14.5, fontName: FONT.light)
        self.setSendLinkButton()
        setPasswordField()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        self.oldPassField.textField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Singleton.sharedInstance.enableDisableIQKeyboard(enable: false)
        self.animateTextField()
    }
    
    
    func setSendLinkButton() {
        self.submitButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        self.submitButton.setCornerRadius(radius: buttonHeight / 2)
        self.submitButton.setShadow()
        self.submitButton.setAttributedTitle(Singleton.sharedInstance.setAttributeTitleWithOrWithoutArrow(title: TEXT.CHANGE_PASS_SUBMIT_BUTTON, isArrow: false, yourImage: IMAGE.iconSigninSigninpage), for: .normal)
    }
    
    
    //MARK: - ANIMATE TEXTFIELDS
    func animateTextField() {
        
        Singleton.sharedInstance.animateToIdentity(view: self.oldPassField, delayTime: 0.4)
        Singleton.sharedInstance.animateToIdentity(view: self.changePassField, delayTime: 0.65)
    }
    
    //MARK: - CHECK VALIDATIONS
    func checkValidation() {
      
        let oldPasswordText = (self.oldPassField.textField.text?.trimText)!

        guard Singleton.sharedInstance.validPassword(password:oldPasswordText) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
            return
        }
        
        let newPasswordText = (self.changePassField.textField.text?.trimText)!
        guard Singleton.sharedInstance.validPassword(password:newPasswordText) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
            return
        }
        
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if newPasswordText.rangeOfCharacter(from: characterset.inverted) == nil {
            
            self.showErrorMessage(error: TEXT.specialCharecterString, isError: true)
            return
        }
        
        if oldPasswordText == newPasswordText  {
            self.showErrorMessage(error: ERROR_MESSAGE.OLD_NEW_PASSWORD,isError: true)
            return
        }

        let changePass = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,"new_password":newPasswordText,"current_password":oldPasswordText,"app_device_type":APP_DEVICE_TYPE]
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.changePassword, params: changePass as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    logEvent(label: "password_change_success")
                    //  self.emailField.textField.text = ""
                    if let data = response["data"] as? [String:AnyObject] {
                        if let accessToken = data["access_token"] as? String {
                            UserDefaults.standard.setValue(accessToken, forKey: USER_DEFAULT.accessToken)
                            Vendor.current?.accessToken = accessToken
                         //   Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                            self.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: { 
                                 _ = self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }
                }else{
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: true )
                }
            }
        }
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        checkValidation()
    }
 
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        
        navigationBar = NavigationView.getNibFile(withHeight: 80,params:NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: TEXT.CHANGE_PASS_TITLE_REGULAR + " " + TEXT.CHANGE_PASS_TITLE_BOLD, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil) , leftButtonAction: {
          _ = self.navigationController?.popViewController(animated: true)
        }, rightButtonAction: nil)
        navigationBar.setBackgroundColor(color: COLOR.THEME_FOREGROUND_COLOR, andTintColor: COLOR.LOGIN_BUTTON_TITLE_COLOR)
        self.view.addSubview(navigationBar)
    }
    
    
    //MARK: NAVIGATION DELEGATE METHODS
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:TITLE VIEW
    func setTitleView() {
        titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.view.addSubview(titleView)
        self.titleView.setTitle(title: TEXT.CHANGE_PASS_TITLE_REGULAR, boldTitle: TEXT.CHANGE_PASS_TITLE_BOLD)
    }
    
    //MARK: - SETTINGS PASSWORDS FIELDS
    func setPasswordField() {
        
        self.oldPassField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.oldPassField.delegate = self
        self.oldPassField.fieldType = FIELD_TYPE.password
        self.oldPassField.frame = CGRect(x: 0, y: 185, width: self.view.frame.width, height: HEIGHT.textFieldHeight)
        self.view.addSubview(self.oldPassField)
        self.oldPassField.setImageForDifferentStates(inactive: IMAGE.iconPasswordInactive, placeholderText: TEXT.CHANGE_OLD_PASS_PLACEHOLDER, isPasswordField: true, unlock: IMAGE.iconPasswordActiveShown)
        self.oldPassField.alpha = 0.0
        self.oldPassField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.oldPassField.textField.keyboardType = UIKeyboardType.emailAddress
        self.oldPassField.textField.returnKeyType = UIReturnKeyType.next

        
        self.changePassField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.changePassField.delegate = self
        self.changePassField.fieldType = FIELD_TYPE.newPassword
        self.changePassField.frame = CGRect(x: 0, y: oldPassField.frame.origin.y+oldPassField.frame.size.height + 8, width: self.view.frame.width, height: HEIGHT.textFieldHeight)
        self.view.addSubview(self.changePassField)
        self.changePassField.setImageForDifferentStates(inactive: IMAGE.iconPasswordInactive, placeholderText: TEXT.CHANGE_PASS_FILED_PLACEHOLDER, isPasswordField: true, unlock: IMAGE.iconPasswordActiveShown)
        self.changePassField.alpha = 0.0
        self.changePassField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.changePassField.textField.keyboardType = UIKeyboardType.emailAddress
        self.changePassField.textField.returnKeyType = UIReturnKeyType.done
    }
   
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String,isError:Bool) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: isError)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        self.errorMessageView = nil
    }
    
   //MARK: - TOUCHES BEGAN
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - CSUTOM FIELD RETURN
    func customFieldShouldReturn(fieldType: FIELD_TYPE) {
        if fieldType == .password {
              self.changePassField.textField.becomeFirstResponder()
        }else
        {
            self.view.endEditing(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
