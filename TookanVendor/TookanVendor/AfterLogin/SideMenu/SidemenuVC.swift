//
//  SidemenuVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/14/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import SDKDemo1
import IQKeyboardManagerSwift


class SidemenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
   // MARK: - Properties
   var navigationBar: NavigationView!
   let optionManager = SideMenuOptionManager()
   
   // MARK: IBOutlets
   @IBOutlet weak var sideMenuTable: UITableView!
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      configureViews()
      setNavigationBar()
      setUpTableView()
   }
   
   func configureViews() {
      self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      sideMenuTable.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
   }
   
   func setNavigationBar() {
      navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "", leftButtonImage: #imageLiteral(resourceName: "close"), rightButtonImage: nil), leftButtonAction: { [weak self] in
         self?.backAction()
         }, rightButtonAction: nil)
      navigationBar.isShadowVisible = false
      navigationBar.isBottomLineVisible = false
      navigationBar.backgroundColor = UIColor.clear
      self.view.addSubview(navigationBar)
   }
   
   func setUpTableView() {
      self.sideMenuTable.register(UINib(nibName: NIB_NAME.SideMenuProfileCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.sideMenuProfile)
      self.sideMenuTable.rowHeight = UITableViewAutomaticDimension
      self.sideMenuTable.estimatedRowHeight = 100
      self.sideMenuTable.separatorStyle  =  .none
      self.sideMenuTable.tableFooterView = UIView(frame: CGRect.zero)
      self.sideMenuTable.register(UINib.init(nibName: NIB_NAME.sideMenuCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.sideMenuCellIdentifier)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      navigationBar.setFrame()
   }
   
   //MARK: IBAction
   func backAction() {
      if let parentVC = self.parent  as? GenricHomeWithSideMenuViewController {
         parentVC.hideSideMenuView()
      }
   }
   
   
   // MARK: - TableView DataSource
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return optionManager.sideMenuOptions.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let option = optionManager.sideMenuOptions[indexPath.row]
      
      let sidemenuCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.sideMenuCellIdentifier) as! SideMenuCell
      sidemenuCell.setCellWith(title: option.title, icon: option.image)
      return sidemenuCell
   }
   
   
   // MARK: - TableView Delegate
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return indexPath.row == 0 ? 70 : 80
   }
   
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let sideMenuOptions = optionManager.sideMenuOptions
      let option = sideMenuOptions[indexPath.row].type
      
      if option == .logout {
         logEvent(label: "side_menu_logout")

         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: { [weak self] in
            let alertController = UIAlertController(title: "", message: TEXT.ARE_YOU_WANT_LOGOUT_ALERT, preferredStyle: .actionSheet)
            let yesAction = UIAlertAction(title: TEXT.LOGOUT_BUTTON, style: .destructive, handler: { (action) -> Void in
               Singleton.sharedInstance.logoutButtonAction()
               logEvent(label: "user_log_out")
            })
            let noAction = UIAlertAction(title: TEXT.CANCEL, style: .cancel, handler: { (action) -> Void in
               self?.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self?.present(alertController, animated: true, completion: nil)
         })
      } else {
         if let parentVC = self.parent  as? GenricHomeWithSideMenuViewController {
            parentVC.hideSideMenuView()
            
            switch option {
               
            case .home:
               break
               
            case .orders:
               var changeVC : OrderHistoryVC!
               logEvent(label: "side_menu_all_tasks")
               changeVC = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.orderHistoryVC) as! OrderHistoryVC
               parent?.navigationController?.pushViewController(changeVC, animated: true)
               
            case .profile:
               logEvent(label: "side_menu_profile")
               var changeVC : VendorProfileVC!
               changeVC = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.vendorProfileVC) as! VendorProfileVC
               parent?.navigationController?.pushViewController(changeVC, animated: true)
               
            case .payments:
               logEvent(label: "side_menu_payments")
               var changeVC : NewPaymentScreenViewController!
               changeVC = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as! NewPaymentScreenViewController
               changeVC.isOpenedFromSideMenu = true
               parent?.navigationController?.pushViewController(changeVC, animated: true)
               
               
            case .referrals:
               logEvent(label: "side_menu_refer_and_earn")
               var changeVC : ReferralViewController!
               changeVC = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.refer) as! ReferralViewController
               parent?.navigationController?.pushViewController(changeVC, animated: true)
            case .chatWithSupport:
               customizeFugu()
               FuguConfig.shared.presentChatsViewController()
               Singleton.sharedInstance.enableDisableIQKeyboard(enable: false)
            case .logout:
               break
            }
         }
      }
   }
   
   
   func customizeFugu(){
      let fuguTheme = FuguTheme()
      fuguTheme.leftBarButtonImage = #imageLiteral(resourceName: "iconBackTitleBar")
      fuguTheme.headerTextColor = COLOR.SPLASH_TEXT_COLOR
      fuguTheme.headerBackgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      FuguConfig.shared.setCustomisedFuguTheme(theme: fuguTheme)
   }
   
}
