//
//  FavLocationCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class FavLocationCell: UITableViewCell {
   
   // MARK: - IBOutlets
   @IBOutlet weak var addressLocation: UILabel!
   @IBOutlet weak var favLocationTypeImage: UIImageView!
   @IBOutlet weak var addorEditButton: UIButton!
   @IBOutlet weak var horizontalLine: UILabel!
   @IBOutlet weak var headingLabel: UILabel!
   
   // MARK: - Properties
   var isAddressSaved = false
   
   var addButonAction:(() -> Void)?
   var deleteAction : (() -> Void)?
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      
      favLocationTypeImage.tintColor = COLOR.SPLASH_TEXT_COLOR
      headingLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
      headingLabel.textColor = COLOR.THEME_FOREGROUND_COLOR
      addressLocation.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
   }
   
   
   // MARK: - IBAction
   @IBAction func addOrEditButton(_ sender: UIButton) {
      
      if isAddressSaved {
         deleteAction?()
      } else {
         addButonAction?()
      }
   }
   
   
   // MARK: - Methods
   func setUpCellWith(icon: UIImage, address: String, label: String, isAddressSaved: Bool) {
      
      self.isAddressSaved = isAddressSaved
      favLocationTypeImage.image = icon
      addorEditButton.setImage(isAddressSaved ? #imageLiteral(resourceName: "deleteAddress") : #imageLiteral(resourceName: "addAddress"), for: .normal)
      addressLocation.text = isAddressSaved ? address : label
      headingLabel.text = isAddressSaved ? label : ""
   }
   
}


