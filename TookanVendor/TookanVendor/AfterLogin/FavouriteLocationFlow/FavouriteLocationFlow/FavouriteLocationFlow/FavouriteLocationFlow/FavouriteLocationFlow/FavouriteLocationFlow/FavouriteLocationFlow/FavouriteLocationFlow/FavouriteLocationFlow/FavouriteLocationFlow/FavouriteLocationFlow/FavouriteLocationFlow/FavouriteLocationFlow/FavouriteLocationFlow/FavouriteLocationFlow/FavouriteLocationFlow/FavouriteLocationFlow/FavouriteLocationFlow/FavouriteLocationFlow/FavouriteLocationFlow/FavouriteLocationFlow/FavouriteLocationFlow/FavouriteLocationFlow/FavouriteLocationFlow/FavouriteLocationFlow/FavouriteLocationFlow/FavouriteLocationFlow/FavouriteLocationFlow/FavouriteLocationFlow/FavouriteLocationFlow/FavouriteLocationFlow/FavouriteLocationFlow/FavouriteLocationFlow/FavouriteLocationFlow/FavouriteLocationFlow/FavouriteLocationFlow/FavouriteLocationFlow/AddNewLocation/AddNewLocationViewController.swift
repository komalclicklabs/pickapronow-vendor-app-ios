//
//  AddNewLocationViewController.swift
//  TookanVendor
//
//  Created by No One on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class AddNewLocationViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
   
   enum Section: Int {
      case searchResult = 0, home, work, other
      
      func getSectionHeaderHeight() -> CGFloat {
         switch  self {
         case .home:
            return 32
         default:
            return 0
         }
      }
      
      func getIcon() -> UIImage {
         switch self {
         case .home:
            return #imageLiteral(resourceName: "homeAddress").renderWithAlwaysTemplateMode()
         case .work:
            return #imageLiteral(resourceName: "workAddress").renderWithAlwaysTemplateMode()
         default:
            return #imageLiteral(resourceName: "otherAddress").renderWithAlwaysTemplateMode()
         }
      }
      
      func labelIfAddressNotAdded() -> String {
         switch self {
         case .home:
            return TEXT.HomeAddress
         case .other:
            return TEXT.addAnotherAddress
         case .work:
            return TEXT.WorkAddress
         case .searchResult:
            return ""
         }
      }
   }
   
   // MARK: - Properties
   var searchResults = [GMSAutocompleteModel]()
   
   var homeAddresses = [FavouriteAddress]()
   var workAdressess = [FavouriteAddress]()
   var otherAddressess = [FavouriteAddress]()
   
   //Completion handler for returning coordinates and address ot previous screen.
   var completionHandler:((_ coordinate:CLLocationCoordinate2D,_ address:String) -> Void)?
   
   //MARK: - IBOUTLETS
   @IBOutlet weak var upperView: UIView!
   @IBOutlet weak var searchTextField: UITextField!
   @IBOutlet weak var backButton: UIButton!
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var selectFromMapButton: UIButton!

   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      getAllSavedAddress()
      addShadowToView()
      
      searchTextField.placeholder = TEXT.searchYourAddress
      searchTextField.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
      selectFromMapButton.configureNormalButtonWith(title: TEXT.selectFrommap)
      
      tableView.separatorStyle = .none
      tableView.delegate = self
      tableView.dataSource = self
      searchTextField.delegate = self
      
      tableView.registerCellWith(nibName: NIB_NAME.addressListCell, reuseIdentifier: CELL_IDENTIFIER.addressCellIdentifier)
      tableView.registerCellWith(nibName: NIB_NAME.FavLocationCell, reuseIdentifier: CELL_IDENTIFIER.FavLocationCell)
      
      self.tableView.rowHeight = UITableViewAutomaticDimension
      self.tableView.estimatedRowHeight = 55
      
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      searchTextField.becomeFirstResponder()
      
      UIApplication.shared.statusBarStyle = .default
   }
   
     //MARK: - UITABLE VIEW DATASOURCE
   func numberOfSections(in tableView: UITableView) -> Int {
      return 4
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch Section(rawValue: section)! {
      case .searchResult:
         return searchResults.count
      case .home:
         return homeAddresses.count > 0 ? homeAddresses.count : 1
      case .work:
         return workAdressess.count > 0 ? workAdressess.count : 1
      case .other:
         return otherAddressess.count + 1
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let section = Section(rawValue: indexPath.section)!
      switch section {
      case .home, .work, .other:
         return loadFavLocationCellFor(indexPath: indexPath)
      case .searchResult:
         let addressCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.addressCellIdentifier) as! AddressListCell
         addressCell.selectionStyle = .none
         if indexPath.row < searchResults.count {
            addressCell.addressLabel.text = searchResults[indexPath.row].address
         }
         
         return addressCell
      }
   }
   
   private func loadFavLocationCellFor(indexPath: IndexPath) -> FavLocationCell {
      let favLocationCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.FavLocationCell) as! FavLocationCell
      favLocationCell.selectionStyle = .none
      let section = Section(rawValue: indexPath.section)!
      
      favLocationCell.tag = indexPath.generateTag()
      let favAddress = getAddressAt(section: indexPath.section, row: indexPath.row)
      
      favLocationCell.addButonAction = { [weak self] (type) in
         self?.addButtonPressedInCellWith(tag: favLocationCell.tag)
      }
      favLocationCell.deleteAction = { [weak self] (type) in
         self?.deleteButtonPressedInCellWith(tag: favLocationCell.tag)
      }
      
      let icon = section.getIcon()
      let address = favAddress?.address ?? ""
      let label = favAddress?.name ?? section.labelIfAddressNotAdded()
      favLocationCell.setUpCellWith(icon: icon, address: address, label: label, isAddressSaved: favAddress != nil)
      
      return favLocationCell
   }
   
   private func getAddressAt(section: Int, row: Int) -> FavouriteAddress? {
      let sectionName = Section(rawValue: section)!
      
      switch sectionName {
      case .home:
         return isHomeAddressSavedFor(row: row) ? homeAddresses[row] : nil
      case .work:
         return isWorkAdressSavedFor(row: row) ? workAdressess[row] : nil
      case .other:
         return isOtherAddressSavedFor(row: row) ? otherAddressess[row] : nil
      case .searchResult:
         return searchResults[row]
      }
   }
   
   private func isHomeAddressSavedFor(row: Int) -> Bool {
      return homeAddresses.count > row
   }
   
   private func isWorkAdressSavedFor(row: Int) -> Bool {
      return workAdressess.count > row
   }
   
   private func isOtherAddressSavedFor(row: Int) -> Bool {
      return otherAddressess.count > row
   }
   
   
   // MARK: - Table View Delegate
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let section = Section(rawValue: indexPath.section)!
      
      switch section {
      case .searchResult:
         guard let placeid = searchResults[indexPath.row].placeId else {
            print("Place Id Not found")
            return
         }
         getPlaceDetailFrom(placeId: placeid, atRow: indexPath.row)

      default:
         break
      }
      
   }
   
   private func getPlaceDetailFrom(placeId: String, atRow row: Int) {
      guard IJReachability.isConnectedToNetwork() else {
         ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isErrorMessage: true, removed: nil)
         return
      }
      
      tableView.isUserInteractionEnabled = false
      GMSPlacesClient.shared().lookUpPlaceID(placeId) { [weak self] (place, error) in
         self?.tableView.isUserInteractionEnabled = true
         
         guard let placeUnwraaped = place, error == nil else {
            return
         }
         
         let address = self?.searchResults[row].address ?? ""
         
         self?.completionHandler?(placeUnwraaped.coordinate, address)
         self?.backButtonAction(UIButton())
      }

   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
      guard let sectionName = Section(rawValue: section), sectionName == .home else {
         return UIView()
      }
      
      let view = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 32))
      
      let labelFrame = CGRect(x: 20, y: 0, width: view.bounds.width, height: view.bounds.height)
      let label = UILabel(frame: labelFrame)
      
      label.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
      label.text = TEXT.FavoritesText
      label.letterSpacing = 0.4
      label.backgroundColor = UIColor.clear
      view.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.05)
      view.addSubview(label)
      
      return view
      
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      let sectionName = Section(rawValue: section)!
      
      return sectionName.getSectionHeaderHeight()
   }
   
   func scrollViewDidScroll(_ scrollView: UIScrollView) {
      self.searchTextField.resignFirstResponder()
   }
   
   //MARK: - UITEXTFIELD DELEGATE
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      var text = String()
      
      if string != ""{
         text = textField.text! + string
         
      }else{
         text = textField.text!
         text.remove(at: text.index(before:  text.endIndex))
      }
      print(text)
      
      APIManager.sharedInstance.autoCompleteSearch(text) { [weak self] (succeeded, response) in
         
         guard succeeded else {
            return
         }
         self?.searchResults.removeAll()
         if let arrayOfPredictions = response["predictions"] as? [[String: Any]] {
            for prediction in arrayOfPredictions {
               let response = [
                  "address": prediction["description"] as? String ?? "",
                  "placeId": prediction["place_id"] as? String ?? ""
               ]
               let gmsResult = GMSAutocompleteModel(json: response)
               self?.searchResults.append(gmsResult)
            }
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
         }
      }
      
      return true
   }
   
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
   }
   

   
   //MARK: - IBAction
   @IBAction func backButtonAction(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
   }
   
   @IBAction func selectFromMapButtonPressed(_ sender: UIButton) {
   }
   
   // MARK: - Methods
   
   //On pressing add button on the fav cell
   func addButtonPressedInCellWith(tag: Int) {
      // TODO: Open Map
   }
   
   func deleteButtonPressedInCellWith(tag: Int) {
      let (row, section) = tag.getRowAndSection()
      let addressToBeDeleted = getSavedAddressIn(section: section, atRow: row)
      
      Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: TEXT.deleteLocation, showRight: true, leftButtonAction:  { [weak self] _ in
         addressToBeDeleted.deleteFromServer { success in
            guard success else {
               return
            }
            self?.deleteElementIn(section: section, atRow: row)
         }
      }, rightButtonAction: nil, leftButtonTitle: TEXT.YES_ACTION, rightButtonTitle: TEXT.NO)
   }
   
   
   //AddingShadow
   func addShadowToView(){
      upperView.layer.shadowColor = UIColor.black.cgColor
      upperView.layer.shadowOpacity = 0.3
      upperView.layer.shadowOffset = CGSize(width: 0, height: 7)
      upperView.layer.shadowRadius = 6
   }
   
   private func getSavedAddressIn(section: Int, atRow row: Int) -> FavouriteAddress {
      return FavouriteAddress()
   }
   
   private func deleteElementIn(section: Int, atRow row: Int) {
      let sectionName = Section(rawValue: section)!
      
      switch sectionName {
      case .home:
         break
      case .work:
         break
      case .other:
         break
      default:
         break
      }
   }

   
   // MARK: - Server Hits
   func getAllSavedAddress() {
      FavouriteAddress.fetchAllFromServer { [weak self] (success, home, work, others) in
         guard success else {
            return
         }
         
         self?.homeAddresses = home
         self?.otherAddressess = others
         self?.workAdressess = work
         
         self?.tableView.reloadData()
      }
   }
   
   
}

