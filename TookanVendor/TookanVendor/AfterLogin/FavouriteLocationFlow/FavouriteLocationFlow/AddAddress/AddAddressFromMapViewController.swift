//
//  AddAddressFromMapViewController.swift
//  TookanVendor
//
//  Created by Aditi on 7/25/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

import GoogleMaps
import GooglePlaces
import CoreLocation


class AddAddressFromMapViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate {
    
    //MARK :- IBOutlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var outletAddressButton: UIButton!
    @IBOutlet weak var textAddress: UILabel!
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var marker: UIImageView!
    
    //MARK :- Stored Properties
    var centerMapCoordinate:CLLocationCoordinate2D!
    var didFindMyLocation = false
    var locationManager = CLLocationManager()
    var geocoder = CLGeocoder()
    var googleMapsView:GMSMapView!
    var lat : Double!
    var long : Double!
    
    //MARK :- IBActions
    @IBAction func nextButton(_ sender: Any) {
    }
    
    @IBAction func getAddress(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        let    camera = GMSCameraPosition.camera(withLatitude: -33.868,longitude: 151.2086, zoom: 5)
        googleMapsView = GMSMapView.map(withFrame: self.mainView.bounds, camera: camera)
        googleMapsView.delegate = self
        
        self.view.addSubview(mainView)
        self.mainView.addSubview(googleMapsView)
        self.mainView.addSubview(marker)
        
        googleMapsView.addSubview(addressView)
        addressView.addSubview(iconImage)
        addressView.addSubview(outletAddressButton)
        outletAddressButton.addSubview(textAddress)
        googleMapsView.addSubview(nextButtonOutlet)
        
        nextButtonOutlet.configureNormalButtonWith(title: TEXT.Next)
        nextButtonOutlet.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: .normal)
        
              locationManager.desiredAccuracy = kCLLocationAccuracyBest
        googleMapsView.isMyLocationEnabled = true
        googleMapsView.settings.myLocationButton = true
        
         self.locationManager.startUpdatingLocation()
     
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let newLocation = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        
        geocoder.reverseGeocodeLocation(newLocation) {
            (placemarks, error) -> Void in
            
            guard error == nil else {
                return
            }
            
            guard let placemarks = placemarks ,placemarks.count > 0  else
            {
                return
            }
            let placemark = placemarks.last
        
            self.textAddress.text = "\((placemark?.name!)!) \((placemark?.administrativeArea!)!) \((placemark?.country)!) \((placemark?.postalCode!)!)"
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        googleMapsView.animate(to: camera)
        
        self.locationManager.stopUpdatingLocation()
        
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }


}

extension AddAddressFromMapViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress!)")
        print("Place attributions: \(place.attributions)")
        geocoder.geocodeAddressString("\(place.formattedAddress!)", completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print( error!)
            }
            
            if let placemark = placemarks?.first {
                self.lat = placemark.location?.coordinate.latitude
                self.long = (placemark.location?.coordinate.longitude)!
               
                let camera = GMSCameraPosition.camera(withLatitude: (self.lat)!, longitude: (self.long)!, zoom: 17.0)
                self.googleMapsView.animate(to: camera)
            }
        })
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

