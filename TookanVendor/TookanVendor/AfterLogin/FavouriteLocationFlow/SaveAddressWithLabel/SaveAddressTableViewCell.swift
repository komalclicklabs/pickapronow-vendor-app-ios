//
//  SaveAddressTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 24/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class SaveAddressTableViewCell: UITableViewCell {
   
   // MARK: - Properties
   static var identifier = "SaveAddressTableViewCell"
   
   var textChanged: ((_ newText: String) -> Void)?
   var textViewChanged: ((UITextView) -> Void)?
   
   // MARK: - IBOutlets
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var descriptiontextView: UITextView!
   @IBOutlet weak var bottomConstraintFromStackView: NSLayoutConstraint!
   
   // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      configureTitleLabel()
      configureTextField()
      
      descriptiontextView.delegate = self
    }
   
   private func configureTitleLabel() {
      titleLabel.font = UIFont(name: FONT.light, size: 13)
      titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
   }
   
   private func configureTextField() {
      descriptiontextView.textColor = COLOR.SPLASH_TEXT_COLOR
      descriptiontextView.font = UIFont(name: FONT.light, size: 17)
   }

   
   func setCellWith(title: String, description: String, isDescriptionEditable: Bool) {
      titleLabel.text = title
      descriptiontextView.text = description
      descriptiontextView.isEditable = isDescriptionEditable
      
      bottomConstraintFromStackView.constant = 0
      descriptiontextView.textColor = isDescriptionEditable ? COLOR.SPLASH_TEXT_COLOR : COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
   }
}

// MARK: - UITextViewDelegate
extension SaveAddressTableViewCell: UITextViewDelegate {
   func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
      let oldString = textView.text! as NSString
      let newString = oldString.replacingCharacters(in: range, with: text)
      
      textChanged?(newString)
      
      return true
   }
   
   func textViewDidChange(_ textView: UITextView) {
      textViewChanged?(textView)
   }

}
