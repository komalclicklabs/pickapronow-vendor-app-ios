//
//  InfoPopUpView.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 30/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class InfoPopUpView: UIView {
  
  // MARK: - Properties
  private var info = InfoPopUp()
   private var viewWillBeRemoved: (() -> Void)?
  
  // MARK: - IBOutlets
   @IBOutlet weak var blurView: UIVisualEffectView!
  @IBOutlet weak var overlay: UIImageView!
  @IBOutlet weak var popUpView: UIView!

  @IBOutlet weak var headerImage: UIImageView!
  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var subHeadingLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var closeButton: UIButton!
  @IBOutlet weak var dividerView: UIView!
  
  // MARK: - Configure Views
  private func configureView() {
    
    overlay.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0)
    popUpView.backgroundColor = COLOR.popUpColor
        
    configureHeaderLabel()
    configureHeaderImage()
    configureSubHeading()
    configureDescriptionLabel()
    configureDividerView()
    configureCloseButton()
  }
  
  private func configureHeaderImage() {
    guard let image = info.image else {
      headerImage.isHidden = true
      return
    }
    headerImage.image = image
  }
  
  private func configureHeaderLabel() {
    headerLabel.configureHeadingTypeLabelWith(text: info.heading)
    headerLabel.textColor = info.type.getColor()
    headerLabel.isHidden = info.heading.isEmpty
  }
  
  private func configureSubHeading() {
    subHeadingLabel.attributedText = info.subHeading
    let subHeadingString = info.subHeading?.string
    subHeadingLabel.isHidden = subHeadingString == nil
  }
  
  private func configureDescriptionLabel() {
    descriptionLabel.configureDescriptionTypeLabelWith(text: info.description)
    descriptionLabel.isHidden = info.description.isEmpty
  }
  
  private func configureDividerView() {
    dividerView.backgroundColor = COLOR.seperatorColor
  }
  
  private func configureCloseButton() {
    closeButton.setTitle(info.closeButtontitle, for: .normal)
    closeButton.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: .normal)
  }
  
  // MARK: - IBAction
  @IBAction func closeButtonPressed() {
    removeWithAnimation()
  }
  
  //MARK: - Load
  
   class func loadWith(info: InfoPopUp, viewWillBeRemoved: (() -> Void)? = nil) {
      let view = getWith(info: info, viewWillBeRemoved: viewWillBeRemoved)
    view.addToWindowWithAnimation()
  }
  
  class func getWith(info: InfoPopUp, viewWillBeRemoved: (() -> Void)? = nil) -> InfoPopUpView {
    let view = Bundle.main.loadNibNamed(NIB_NAME.infoPopUpView, owner: nil, options: nil)?.first as! InfoPopUpView
    view.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height)
   view.viewWillBeRemoved = viewWillBeRemoved
    view.info = info
    view.configureView()
    return view
  }
  
  // MARK: - Animation
  func addToWindowWithAnimation() {
    setIntialPosition()
    UIApplication.shared.keyWindow?.addSubview(self)
    startAnimating()
  }
  
  private func setIntialPosition() {
    popUpView.transform = getSizeTransformation()
    overlay.setAlphaToZero()
    blurView.setAlphaToZero()
//   blurView.effect = UIBlurEffect(style: UIBlurEffectStyle(rawValue: 10)!)
  }
  
  private func getSizeTransformation() -> CGAffineTransform {
    return CGAffineTransform(scaleX: 0.2, y: 0.2)
  }
  
  private func startAnimating() {
    overlay.showSmoothly(completion: nil)
    addBlurViewWithAnimation()
    UIView.animate(withDuration: 0.3) { [weak self] in
      self?.popUpView.transform = CGAffineTransform.identity
    }
  }
  
  private func addBlurViewWithAnimation() {
    blurView.isHidden = false
    UIView.animate(withDuration: 0.3) { [weak self] in
      self?.blurView.alpha = 0.8
    }
  }
  
  private func removeWithAnimation() {
    
    UIView.animate(withDuration: 0.8) { [weak self] in
      guard let weakSelf = self else {
        return
      }
      weakSelf.popUpView.transform = weakSelf.getSizeTransformation()
    }
    blurView.hideSmoothly(completion: nil)
//   blurView.effect = UIBlurEffect(style: UIBlurEffectStyle(rawValue: 10)!)
    overlay.hideSmoothly(completion: { [weak self] completed in
      if completed {
         self?.viewWillBeRemoved?()
        self?.removeFromSuperview()
      }
    })
  }
  
}

extension InfoPopUpView {
  struct InfoPopUp {
    var heading = ""
    var subHeading: NSAttributedString?
    var description = ""
    var image: UIImage?
    var closeButtontitle = "Close"
    var type = PopUpType.normal
  }
  
  enum PopUpType {
    case normal
    case success
    case failure
    
    func getColor() -> UIColor {
      switch self {
      case .normal:
        return COLOR.SPLASH_TEXT_COLOR
      case .failure:
        return COLOR.ERROR_COLOR
      case .success:
        return COLOR.SUCCESS_COLOR
      }
    }
  }
}
