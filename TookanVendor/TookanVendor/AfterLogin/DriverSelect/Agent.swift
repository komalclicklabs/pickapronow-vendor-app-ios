//
//  Agent.swift
//  TookanVendor
//
//  Created by Aditi on 7/20/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


struct Agent {
   
   private(set) var formID: String = ""
   private(set) var fleet_id: String
   private var fleet_thumb_image = ""
   var fleet_image = ""
   private(set)  var username = ""
   private var transport_type = ""
   private var transport_desc = ""
   private var license = ""
   private var email = ""
   private var phone = ""
   private let registration_status : Bool
   private var latitude = ""
   private let is_available : Bool
   private var longitude = ""
   private var last_updated_location_time = ""
   private var distance = ""
   private let status : Bool
   private let team_id : Int?
   private var team_name = ""
   private(set) var tags = ""
   private let last_updated_timings : Int?
   
   private var fleet_status_color = ""
   private let view_status : Bool
   private let edit_status : Bool
   private var eta = ""
   
   
   init?(json : [String: Any] ) {
      
      guard  let fleet_id = json["fleet_id"] else {
         return nil
      }
      self.fleet_id = "\(fleet_id)"
      self.fleet_thumb_image = (json["fleet_thumb_image"] as? String) ?? ""
      self.fleet_image = (json["fleet_image"] as? String) ?? ""
      self.username = (json["username"] as? String) ?? ""
      self.transport_type = (json["transport_type"] as? String) ?? ""
      self.transport_desc = (json["transport_desc"] as? String) ?? ""
      self.license = (json["license"] as? String) ?? ""
      self.email = (json["email"] as? String) ?? ""
      self.phone = (json["phone"] as? String) ?? ""
      self.registration_status = (json["registration_status"] as? Bool) ?? false
      self.latitude = (json["latitude"] as? String) ?? ""
      self.is_available = (json["is_available"] as? Bool) ?? false
      self.longitude = (json["longitude"] as? String) ?? ""
      self.last_updated_location_time = (json["last_updated_location_time"] as? String) ?? ""
      self.distance = (json["distance"] as? String) ?? ""
      self.status = (json["status"] as? Bool) ?? false
      self.team_id = (json["team_id"] as? Int)
      self.last_updated_timings = (json["last_updated_timings"] as? Int)
      self.fleet_status_color = (json["fleet_status_color"] as? String) ?? ""
      self.eta = (json["eta"] as? String) ?? ""
      
      if let tempTags = json["tags"] as? String, !tempTags.isEmpty {
         self.tags = tempTags.replacingOccurrences(of: ",", with: ", ")
      }
      
      self.team_name = (json["team_name"] as? String) ?? ""
      self.view_status = (json["view_status"] as? Bool) ?? false
      self.edit_status = (json["edit_status"] as? Bool) ?? false
   }
   
   static func fetchAgentsFromServer(formID: String,latitude : String,longitude : String,tags : String, completion: @escaping (_ success: Bool, _ agents: [Agent]?) -> Void) {
      
      let params = getParamsForFetchingAgents(formID: formID, latitude:latitude,longitude: longitude,tags: tags)
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlertInDefaultCase: false, para: params, extendedUrl: API_NAME.getAgents) { (responseObject, error, _, statusCode) in
         guard statusCode == .some(STATUS_CODES.SHOW_DATA),
            let response = responseObject as? [String: Any],
            let data = response["data"] as? [[String: Any]] else {
               completion(false, nil)
               return
         }
         
         var tempArrayOfAgents = [Agent]()
         
         for rawTags in data {
            
            guard let outerArray = rawTags["array"] as? [[String :Any]] else{
               continue
            }
            
            for rawAgent in outerArray {
               guard let tempAgent = Agent(json: rawAgent) else {
                  continue
               }
               tempArrayOfAgents.append(tempAgent)
            }
            
            completion(true, tempArrayOfAgents)
            
         }
      }
   }
   
   private static func getParamsForFetchingAgents(formID: String, latitude :String,longitude : String,tags : String) -> [String: Any] {
      return [
         "access_token" : Vendor.current!.appAccessToken!,
         "form_id" : formID,
         "latitude" : latitude,
         "longitude" : longitude,
         "tags": tags,
         "user_id" : Vendor.current!.userId!,
         "device_token" : APIManager.sharedInstance.getDeviceToken(),
         "app_version" : APP_VERSION,
         "app_access_token" : Vendor.current!.appAccessToken!,
         "app_device_type" : APP_DEVICE_TYPE
      ]
   }
   
}
