
//
//  RateAndReviewViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 30/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class RateAndReviewViewController: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
   
   // MARK: - Properties
   var agentImageString = String()
   var agentNameString = String()
   var jobId = String()
   var jobId2 = String()
   var formId: String?
   var errorMessageView:ErrorView!
   var completionHandler:((_ message:String)->Void)?
   var tipDictionary = [1:"1",2:"2",3:"3",4:"4",2232:"0"]
   var selectedTag = 2232
   
   // MARK: - IBOutlets
   @IBOutlet weak var mainView: UIView!
   @IBOutlet weak var profileImage: UIImageView!
   @IBOutlet weak var agentName: UILabel!
   @IBOutlet weak var commentView: UITextView!
   @IBOutlet weak var submitButton: UIButton!
   @IBOutlet weak var rateView: FloatRatingView!
   @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
   @IBOutlet weak var tableView: UITableView!
   
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      giveUiColor()
      setUpTip()
      setupTableView()
      setNavigationBar()

      self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      assignUiAttributes()
      commentView.delegate = self
      commentView.text = TEXT.writeAReview
      commentView.font = UIFont(name: FONT.light, size: 17)
      commentView.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
    
    let urlAgentImage = URL(string: agentImageString)
      
      profileImage.kf.setImage(with: urlAgentImage, placeholder: #imageLiteral(resourceName: "placeHolder"))
   }

   
   private func setupTableView() {
      tableView.delegate = self
      tableView.dataSource = self
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.separatorStyle = .none
      tableView.estimatedRowHeight = 60
      tableView.register(UINib(nibName: NIB_NAME.TipCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.tip)
   }
   
   private func setNavigationBar() {
      let navVeiwParams = NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: TEXT.rateTheService, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil)
      let navBar = NavigationView.getNibFile(params: navVeiwParams, leftButtonAction: { [weak self] in
         self?.backButtonPressed()
      }, rightButtonAction: nil)
      navBar.setFrame()
      view.addSubview(navBar)
   }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        profileImage._cornerRadius = profileImage.frame.width/2
        profileImage.setShadow()
        profileImage.setBorder(borderColor: COLOR.SPLASH_BACKGROUND_COLOR)
        profileImage.layer.borderWidth = 3
    }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      UIApplication.shared.statusBarStyle = .lightContent
   }
   
   // MARK: - IBActions
   func backButtonPressed() {
      logEvent(label: "rate_skip")
      _ = self.navigationController?.popViewController(animated: true)
   }
   
   @IBAction func submitButtonAction(_ sender: UIButton) {
      self.view.endEditing(true)
      
      if tipDictionary[selectedTag]! != "0"{
         logEvent(label: "tip_applied_rating")
      }
      
      rateLog(rating: rateView.rating)

      if rateView.rating != 0{
         let param = [
            "job_id":self.jobId,
            "access_token":Vendor.current!.appAccessToken!,
            "device_token":APIManager.sharedInstance.getDeviceToken(),
            "app_version":APP_VERSION,
            "app_device_type":APP_DEVICE_TYPE,
            "app_access_token":Vendor.current!.appAccessToken!,
            "user_id":Singleton.sharedInstance.formDetailsInfo.user_id!,
            "rating":rateView.rating,
            "customer_comment":commentView.text!,
            "tip_amount" : "\(tipDictionary[selectedTag]!)",
            "form_id" : "\(formId!)",
            "job_id_2" : "\(jobId2)"
            ] as [String : Any]
         print(param)
         ActivityIndicator.sharedInstance.showActivityIndicator()
         APIManager.sharedInstance.serverCall(apiName: "rate_task", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, Response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            print(Response)
            if isSuccess == true{
               self.completionHandler!(Response["message"] as! String)
               _ = self.navigationController?.popViewController(animated: true)
            }else{
               ErrorView.showWith(message: Response["message"] as! String, isErrorMessage: true, removed: nil)
            }
         }
      }else{
         ErrorView.showWith(message: TEXT.pleaseEnterTheRating, removed: nil)
      }
      
   }
   
   
   // MARK: - UITable View Datasource
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.tip, for: indexPath) as? TipTableViewCell
      cell?.tipDict = self.tipDictionary
      cell?.setUpButtons()
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) { [weak self] in
         self?.tableViewHeightConstraint.constant = tableView.contentSize.height
      }
      cell?.selectionStyle = .none
      cell?.onButtonClick = {(value,tag) in
         print(value)
         print(tag)
         self.selectedTag  = tag
         self.tableView.reloadData()
         //self.updateBilBreakDownOrAppEstimate()
      }//  cell?.othersTipButton.addTarget(self, action: #selector(NewPaymentScreenViewController.showCustomTipPoUp), for: UIControlEvents.touchUpInside)
      cell?.resetAllButtonsAccept(tag: self.selectedTag)
      return cell!
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if Singleton.sharedInstance.formDetailsInfo.tipDetails.isEnabled && Singleton.sharedInstance.formDetailsInfo.tipDetails.location == .rateAndReviewScreen {
         return 1
      }else{
         return 0
      }
   }
   
   func numberOfSections(in tableView: UITableView) -> Int {
      if Singleton.sharedInstance.formDetailsInfo.tipDetails.isEnabled && Singleton.sharedInstance.formDetailsInfo.tipDetails.location == .rateAndReviewScreen {
         return 1
      }else{
         return 0
      }
   }
   
   
   // MARK: - UITable View Delegate
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableViewAutomaticDimension
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      
      return 30
   }
   
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      let view =  UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 25))
      let label = UILabel(frame: view.frame)
      let text = TEXT.wouldYouLikeToTip + " \(self.agentNameString.capitalized)?"
      label.configureDescriptionTypeLabelWith(text: text)
      label.font = UIFont(name: FONT.light, size: 14)
      view.addSubview(label)
      
      return view
   }
   
   func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
      let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 44))
      let button = UIButton(frame: CGRect(x: 0, y: 0, width:SCREEN_SIZE.width , height: 44))
      
//      if Singleton.sharedInstance.formDetailsInfo.tipDetails.type == .amount {
         button.setTitle("Enter tip manually", for: UIControlState.normal)
//      }else{
//         button.setTitle("Enter Percentage", for: UIControlState.normal)
//      }
      button.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
      button.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle.customizeSize())
      button.titleLabel?.letterSpacing = 0.4
      button.addTarget(self, action: #selector(RateAndReviewViewController.showCustomTipPoUp), for: UIControlEvents.touchUpInside)
      button.contentHorizontalAlignment = .left
      button.clipsToBounds = true
      footerView.addSubview(button)
      return footerView
   }
   
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      return 50
   }
   
   // MARK: - Methods
   
   func assignUiAttributes(){
      //  self.backgroundView.backgroundColor = COLOR.spla
      agentName.text = self.agentNameString.capitalized
      agentName.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
      submitButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.large)
      submitButton.setTitle(TEXT.SUBMIT.capitalized, for: UIControlState.normal)
      submitButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
      
   }
   
   func textViewDidBeginEditing(_ textView: UITextView) {
      if textView.text == TEXT.writeAReview {
         textView.text = ""
         textView.textColor = COLOR.SPLASH_TEXT_COLOR
      }
   }
   
   func textViewDidEndEditing(_ textView: UITextView) {
      if textView.text ==  "" {
         textView.text = TEXT.writeAReview
         textView.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
      }
   }
   
   func giveUiColor(){
      agentName.textColor = COLOR.SPLASH_TEXT_COLOR
      rateView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      profileImage.layer.cornerRadius = 91/2
      profileImage.clipsToBounds = true
      profileImage.layer.borderWidth = 3
      profileImage.layer.borderColor = COLOR.THEME_FOREGROUND_COLOR.cgColor
      commentView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      commentView.textColor = COLOR.SPLASH_TEXT_COLOR
      submitButton.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
   }
   
   func showCustomTipPoUp(){
      var customTipView : ChangePhoneNumberView?
      let placeholder = Singleton.sharedInstance.formDetailsInfo.tipDetails.type == .amount ? "Tip Amount" : "Tip Percentage"
      customTipView  =  ChangePhoneNumberView.loadWithSingleTextField(texts: ChangePhoneNumberView.PopupParams.init(heading: "", textFieldPlaceholder: placeholder, textFieldtext: "", removeAlertButtonTitle: "Cancel", saveButtonAlertTitle: "Add"), keyboardType: .decimalPad, newTextSaved:  { [weak self] (newTip) in
         if Double(newTip) != nil{
            if Double(newTip) == 0.0{
               ErrorView.showWith(message: "Please add a valid amount.", isErrorMessage: true, removed: nil)
               return
            }
         }else{
            ErrorView.showWith(message: "Please add a valid amount.", isErrorMessage: true, removed: nil)
            return
         }
         self?.tipDictionary[4] = newTip
         self?.selectedTag = 4
         self?.tableView.reloadData()
         customTipView?.removeByAnimating()
      })
   }
   
   
   func setUpTip(){
      for i in 0..<Singleton.sharedInstance.formDetailsInfo.tipDetails.defaultValues.count {
         let number = i
         self.tipDictionary[number+1] = "\(Singleton.sharedInstance.formDetailsInfo.tipDetails.defaultValues[i])"
      }
      
      print(tipDictionary)
      self.tableView.reloadData()
   }
   
   func rateLog(rating: Float) {
      switch Int(rating) {
      case 1:
         logEvent(label: "rate_one_star")
      case 2:
         logEvent(label: "rate_two_stars")
      case 3:
         logEvent(label: "rate_three_stars")
      case 4:
         logEvent(label: "rate_four_stars")
      case 5 :
         logEvent(label: "rate_five_stars")
      default:
         print("")
      }
   }
   
   
}
