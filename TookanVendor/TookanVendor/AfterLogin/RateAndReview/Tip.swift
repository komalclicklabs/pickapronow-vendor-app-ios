//
//  Tip.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 03/08/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

enum TipLocation {
   case paymentScreen
   case rateAndReviewScreen
   
   init(paymentStep: String) {
      self = paymentStep == "2" ? TipLocation.rateAndReviewScreen : .paymentScreen
   }
}

enum TipType: String {
   case amount = "0"
   case percentage = "1"
}

struct Tip {
   var isEnabled = false
   var location = TipLocation.paymentScreen
   var defaultValues = [String]()
   var type: TipType = .amount
   
   init(json: [String: Any]) {
      if let value = json["tip_default_values"] as? String {
         self.defaultValues = value.replacingOccurrences(of: " ", with: "").components(separatedBy: ",")
      }
      
      if let tipType = json["tip_type"] {
         type = TipType(rawValue: "\(tipType)") ?? .amount
      }
      
      self.isEnabled = (json["is_tip_enabled"] as? Bool) ?? false

      if let paymentStep = json["payment_step"] {
         self.location = TipLocation(paymentStep: "\(paymentStep)")
      }
   }
}
