//
//  ProfileTableViewDataSource.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 03/10/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class ProfileTableViewDataSource: SignupTemplateDataSource {
   
   // MARK: - Properties
   var profileData: ProfileDetails
   var cellTypeArray: [FIELD_TYPE]
   var isProfileEditing = false
   fileprivate var imageSelector: MediaSelector?
   
   var didMakeAnyChanges = false
   var wasPasswordChanged = false
   var isNeedToUpdateNewImage = false
   
   // MARK: - Initializer
   init(customFields: [CustomFieldDetails], profileData: ProfileDetails, cellTypeArray: [FIELD_TYPE]) {
      self.profileData = profileData
      self.cellTypeArray = cellTypeArray
      
//      customiseCustomFieldsForViewOnly(customFields: customFields)
      for customField in customFields {
         if customField.app_side != .hidden {
            customField.app_side = CustomFieldAccessRights.readOnly
         }
         customField.data = customField.data?.isEmpty == true ? "-" : customField.data
      }
      super.init(customFields: customFields)
   }
   
//   private func customiseCustomFieldsForViewOnly(customFields: [CustomFieldDetails]) {
//
//   }
   
   // MARK: - Methods
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let section = Section(rawValue: indexPath.section)!
      
      switch section {
      case .customFields:
         return super.tableView(tableView, cellForRowAt: indexPath)
      case .staticFields:
         let fieldType = cellTypeArray[indexPath.row]
         
         switch fieldType {
         case .image:
            return getNamePicCell(tableView: tableView, indexPath: indexPath)
            
         case .contact:
            return getPhoneNumberCell(tableView: tableView, indexPath: indexPath)
            
         default:
            return getDefaultProfileCell(tableView: tableView,indexPath: indexPath)
         }
      }
   }
   
   func getNamePicCell(tableView: UITableView, indexPath: IndexPath) -> VendorNamePicCell {
      let profileCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.vendorNamePicCellIdentifier) as! VendorNamePicCell
      profileCell.selectionStyle =  .none
      
      profileCell.delegate = self
      profileCell.setCellInfoWith(detailModel: profileData, isEditing: isProfileEditing)
      profileCell.editButtonTapped = { [weak self] in
         self?.openImagePickerForCellIn(tableView: tableView, atIndexPath: indexPath)
      }
      return profileCell
   }
   
   func getPhoneNumberCell(tableView: UITableView, indexPath: IndexPath) -> PhoneNumberTableViewCell {
      let phoneCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.phoneCode, for: indexPath) as? PhoneNumberTableViewCell
      phoneCell?.isForProfile = true
      phoneCell?.delegate = self
      phoneCell?.setUpForProfile(isEditable: isProfileEditing)
      phoneCell?.addTextToFields(withData: self.profileData.phoneNumber)
      return phoneCell!
   }
   
   func getDefaultProfileCell(tableView: UITableView, indexPath: IndexPath) -> VendorProfileCellEdit {
      let profileCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.VendorProfileCell, for: indexPath) as! VendorProfileCellEdit
      profileCell.selectionStyle = .none
      profileCell.delegate = self
      profileCell.isEditting = isProfileEditing
      profileCell.setCellInfo(fieldType: cellTypeArray[indexPath.row], detailModel: profileData)
      return profileCell
   }
   
   override func numberOfSections(in tableView: UITableView) -> Int {
      return 2
   }
   
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch Section(rawValue: section)! {
      case .staticFields:
         return cellTypeArray.count
      case .customFields:
         return super.tableView(tableView, numberOfRowsInSection: section)
      }
   }
   
   func openImagePickerForCellIn(tableView: UITableView, atIndexPath indexPath: IndexPath) {
      
      guard isProfileEditing else {
         return
      }
      
      imageSelector = MediaSelector { [weak self] (result) in
         guard result.isSuccessful else {
            ErrorView.showWith(message: result.error!.localizedDescription, removed: nil)
            return
         }
         
         self?.isNeedToUpdateNewImage = true
         let pickedImage = UIImage.init(contentsOfFile: result.filePath!)
         self?.profileData.image = pickedImage
         tableView.reloadRows(at: [indexPath], with: .none)
      }
      let rootViewController = UIApplication.shared.keyWindow!.rootViewController!
      imageSelector?.selectImage(viewController: rootViewController, fileName: "profileVC", fileType: .image)
   }
}

extension ProfileTableViewDataSource: ProfileFieldChanged, VendorNameImageChanged {
   func profileValueChanged(fieldType: Int,yourText:String) {
      switch fieldType {
      case FIELD_TYPE.email.hashValue:
         guard yourText != profileData.email else {
            return
         }
         didMakeAnyChanges = true
         profileData.email = yourText
      case FIELD_TYPE.contact.hashValue:
         guard yourText != profileData.phoneNumber else {
            return
         }
         didMakeAnyChanges = true
         profileData.phoneNumber = yourText
      case FIELD_TYPE.companyName.hashValue:
         guard yourText != profileData.company else {
            return
         }
         didMakeAnyChanges = true
         profileData.company = yourText
      case FIELD_TYPE.address.hashValue:
         guard yourText != profileData.address else {
            return
         }
         didMakeAnyChanges = true
         profileData.address = yourText
      case FIELD_TYPE.name.hashValue :
         guard yourText != profileData.fullName else {
            return
         }
         didMakeAnyChanges = true
         profileData.fullName = yourText
         
      case FIELD_TYPE.newPassword.hashValue:
         guard yourText != profileData.newPassWord else {
            return
         }
         self.wasPasswordChanged = true
         profileData.newPassWord = yourText
      case FIELD_TYPE.changepassword.hashValue :
         guard yourText != profileData.oldPassWord else {
            return
         }
         self.wasPasswordChanged = true
         profileData.oldPassWord = yourText
      default:
         break
      }
      
   }
   
   func nameChanged(yourText: String) {
      guard yourText != profileData.fullName else {
         return
      }
      didMakeAnyChanges = true
      profileData.fullName = yourText
      
      let nameComponents = yourText.components(separatedBy: " ")
      
      guard nameComponents.count > 1  else {
         profileData.firstName = yourText
         profileData.lastName = ""
         return
      }
      
      profileData.firstName = nameComponents[0]
      
      for index in 1..<nameComponents.count {
         if index != 1 {
            profileData.lastName += " "
         }
         profileData.lastName += nameComponents[index]
      }
   }
   
}

extension ProfileTableViewDataSource {
   enum Section: Int {
      case staticFields = 0
      case customFields
   }
}
