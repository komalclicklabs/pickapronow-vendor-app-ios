//
//  VendorNamePicCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/29/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

//MARK: - PROTOCOL TO UPDATE DATA IN CONTROLLER
protocol VendorNameImageChanged: class {
   func nameChanged(yourText:String)
//   func editButtonTapped()
}

class VendorNamePicCell: UITableViewCell,UITextFieldDelegate {
   
   // MARK: - IBOutlets
   @IBOutlet weak var imagePickerButton: UIButton!
   @IBOutlet weak var cameraButton: UIButton!
   @IBOutlet var vendorImageView: UIImageView!
   @IBOutlet weak var editProfileButton: UIButton!
   
   // MARK: - Properties
   var isProfileEditing = false
   weak var delegate: VendorNameImageChanged!
   var editButtonTapped: (() -> Void)?
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      
      cameraButton.backgroundColor = COLOR.ACTIVE_IMAGE_COLOR
      cameraButton.isHidden = !isProfileEditing
      cameraButton.setCornerRadius(radius: 18)
      
      vendorImageView.setCornerRadius(radius: 75)
      
      editProfileButton.isHidden = true
   }
   
   //MARK: - TEXTFIELD DELEGATE
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      return true
   }
   
   func textFieldDidEndEditing(_ textField: UITextField) {
      self.delegate.nameChanged(yourText: textField.text!)
   }
   
   //MARK: - SETTING CELL WITH FIELD TYPE
   func setCellInfoWith(detailModel: ProfileDetails, isEditing: Bool) {
      self.isProfileEditing = isEditing
      cameraButton.isHidden = !isEditing
      if detailModel.image != nil {
         self.vendorImageView.image = detailModel.image
      } else if detailModel.imageString.length > 0 {
         let imageUrl = URL(string: detailModel.imageString)
         self.vendorImageView.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "iconDefaultAvatar"))
      }
   }
   
   // MARK: - IBAction
   @IBAction func chooseImageButtonPressed(_ sender: UIButton) {
      editButtonTapped?()
   }
   
}
