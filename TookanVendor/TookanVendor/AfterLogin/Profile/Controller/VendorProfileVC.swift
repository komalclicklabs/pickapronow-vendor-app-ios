//
//  VendorProfileVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/29/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//LavishLabs123

import UIKit

class VendorProfileVC: UIViewController, UITableViewDelegate {
   
   // MARK: - Proeprties
   var cellTypeArray = [FIELD_TYPE.image,FIELD_TYPE.name,FIELD_TYPE.email,.contact,.password]
    var selectedLocale:String!
    var profileDetailsinfo = ProfileDetails()
    var navigationBar : NavigationView?
   
   var tableViewDataSource: ProfileTableViewDataSource!
   
   // MARK: - IBOutlets
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var bottomButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileTableView: UITableView!
   
    //MARK: - VIEW APPEAR & LOAD FUNCTIONS
   override func viewDidLoad() {
      super.viewDidLoad()
      
      
      
      bottomButtonConstraint.constant = 50
      configureViews()
      setNavigationBar()
      registerCells()
      
      setUserInfo()
      
      let customFields = getCustomFields()
      tableViewDataSource = ProfileTableViewDataSource(customFields: customFields, profileData: profileDetailsinfo, cellTypeArray: cellTypeArray)
      setupTableView()
      setViewForEditing(false)
   }
   
   func getCustomFields() -> [CustomFieldDetails] {
      let signupTemplate = Vendor.current!.signupTemplate
      let customFields = signupTemplate?.customFields ?? []
      return customFields
   }
   
   private func configureViews() {
      profileTableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      self.view.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
      self.view.backgroundColor = UIColor.white
      configureEditProfileButton()
   }
   
   func configureEditProfileButton() {
      editProfileButton.setTitle(TEXT.EDIT , for: UIControlState.normal)
      editProfileButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
      editProfileButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.large)
      editProfileButton.setTitleColor(COLOR.SPLASH_BACKGROUND_COLOR, for: UIControlState.normal)
      editProfileButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
   }
   
   private func registerCells() {
      self.profileTableView.register(UINib.init(nibName: NIB_NAME.vendroProfileCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.vendorProfileIdentifier)
      self.profileTableView.register(UINib.init(nibName: NIB_NAME.vendorNamePicCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.vendorNamePicCellIdentifier)
      self.profileTableView.register((UINib.init(nibName: NIB_NAME.VendorProfileCellEdit, bundle: Bundle.main)), forCellReuseIdentifier: CELL_IDENTIFIER.VendorProfileCell)
      profileTableView.register(UINib(nibName: NIB_NAME.phoneNumberCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.phoneCode)

         profileTableView.register(UINib(nibName: NIB_NAME.TaskDescriptionCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.taskDescription)
         profileTableView.register(UINib(nibName: NIB_NAME.CustomFTextField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.customTextField)
         profileTableView.register(UINib(nibName: NIB_NAME.AddressCustomeField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.AddressCustomeField)
         profileTableView.register(UINib(nibName: NIB_NAME.phoneNumberCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.phoneCode)
         profileTableView.register(UINib(nibName: NIB_NAME.DateCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.dateCustomField)
         profileTableView.register(UINib(nibName: NIB_NAME.CheckBoxCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.custCheckBox)
         profileTableView.register(UINib(nibName: NIB_NAME.DropDownCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.DropDownCell)
         profileTableView.register(UINib(nibName: NIB_NAME.TextFieldForUnEditing, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.UnEdit)
         profileTableView.registerCellWith(nibName: NIB_NAME.checkoutCartCell, reuseIdentifier: CELL_IDENTIFIER.checkoutCartCell)
         profileTableView.registerCellWith(nibName: NIB_NAME.checkoutSubtotalCell, reuseIdentifier: CELL_IDENTIFIER.checkoutSubtotalCell)
         profileTableView.register(UINib.init(nibName: NIB_NAME.ImgesCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.ImageCustom)
   }
   
   func setNavigationBar() {
      
      navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: TEXT.EDIT, title: "My Profile", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil)
         , leftButtonAction: {[weak self] in
            self?.backAction()
         }, rightButtonAction: nil)
      
      navigationBar?.rightButton.isHidden = true
      self.view.addSubview(navigationBar!)
   }
   
   private func setupTableView() {
      self.profileTableView.separatorStyle  =  .none
      self.profileTableView.tableFooterView = UIView(frame: CGRect.zero)
      profileTableView.dataSource = tableViewDataSource
      profileTableView.delegate = self
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      UIApplication.shared.statusBarStyle = .default
   }
   
   
    //MARK: - CHECK VALIDATIONS FOR EDIT PROFILE API
   func profileFieldValidationsPassed() -> Bool {
      if profileDetailsinfo.fullName.trimText == "" {
         self.showErrorMessage(error: ERROR_MESSAGE.INVALID_NAME, isError: true)
         return false
      }
      
      if profileDetailsinfo.phoneNumber.trimText == ""  || profileDetailsinfo.phoneNumber.trimText.characters.count < 7 {
         self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER, isError: true)
         return false
      }
      
      if tableViewDataSource.wasPasswordChanged == true {
         return passwordValidationPassed()
      }
      
      return true
   }
   
    func hitApiToUpdateProfile() {
        self.view.endEditing(true)
      
        guard tableViewDataSource.didMakeAnyChanges == true || tableViewDataSource.isNeedToUpdateNewImage == true else {
            self.resetEditProfileView()
            return
        }
        
        APIManager.sharedInstance.editProfile(apiName: API_NAME.editProfile, params: profileDetailsinfo.returnJson(), httpMethod: HTTP_METHOD.POST, isImage: tableViewDataSource.isNeedToUpdateNewImage, vendorImage: profileDetailsinfo.image, imageKey: "vendor_image", isNeedToShowActivityIndictor: true) {  (isSuccess, response) in
            DispatchQueue.main.async {
                print(response)
                if isSuccess {
                    if let status = response["status"] as? Int {
                        switch(status) {
                            
                        case STATUS_CODES.SHOW_DATA:
                           logEvent(label: "profile_edit_success")

                            DispatchQueue.main.async(execute: {[weak self] () -> Void in
                                if let data = response["data"] as? [String:Any] {
                                 let signupTemplate = Vendor.current?.signupTemplate
                                 let vendorDetails = Vendor(json: data, signupTemplate: signupTemplate, reviewMessage: "")
                                    Vendor.current = vendorDetails
                                    UserDefaults.standard.set(vendorDetails.appAccessToken, forKey: USER_DEFAULT.accessToken)
                                    
                                 if self?.tableViewDataSource.wasPasswordChanged == true {
                                    if self?.passwordValidationPassed() == true {
                                       self?.hitPasswordChangeAPI()
                                    }
                                 } else {
                                    self?.tableViewDataSource.isNeedToUpdateNewImage = false
                                    self?.tableViewDataSource.didMakeAnyChanges = false
                                    self?.tableViewDataSource.wasPasswordChanged = false
                                    self?.resetEditProfileView()
                                    self?.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                                 }
                              }
                            })
                            break
                        default:
                            break
                        }
                    }
                }else{
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: true)
                }
            }
        }
    }
   
    func passwordValidationPassed() -> Bool {
        
        let oldPasswordText = (profileDetailsinfo.oldPassWord.trimText)
        
        guard Singleton.sharedInstance.validPassword(password:oldPasswordText) else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
            return false
        }
        
        let newPasswordText = (profileDetailsinfo.newPassWord.trimText)
        guard Singleton.sharedInstance.validPassword(password:newPasswordText) else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
            return false
        }
      
        if oldPasswordText == newPasswordText {
            self.showErrorMessage(error: ERROR_MESSAGE.OLD_NEW_PASSWORD,isError: true)
            return false
        }
        
        return true
   }
   
   func hitPasswordChangeAPI() {
      
      let oldPasswordText = profileDetailsinfo.oldPassWord.trimText
      let newPasswordText = profileDetailsinfo.newPassWord.trimText
      
        let changePass = [
         "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
         "new_password": newPasswordText,
         "current_password": oldPasswordText,
         "app_device_type":APP_DEVICE_TYPE
      ]
      
        ActivityIndicator.sharedInstance.showActivityIndicator()
      
        APIManager.sharedInstance.serverCall(apiName: API_NAME.changePassword, params: changePass as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {[weak self] (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    logEvent(label: "password_change_success")
                    //  self.emailField.textField.text = ""
                    if let data = response["data"] as? [String:AnyObject] {
                        if let accessToken = data["access_token"] as? String {
                            UserDefaults.standard.setValue(accessToken, forKey: USER_DEFAULT.accessToken)
                            Vendor.current?.appAccessToken = accessToken

                           self?.tableViewDataSource.isNeedToUpdateNewImage = false
                            self?.tableViewDataSource.didMakeAnyChanges = false
                            self?.tableViewDataSource.wasPasswordChanged = false
                            self?.resetEditProfileView()

                           self?.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                            
                        }
                    }
                }else{
                    self?.showErrorMessage(error: response["message"] as? String ?? "", isError: true )
                }
            }
        }
    }
    
    //MARK: - RESET EDIT PROFILE VIEW AFTER EDIT DONE OR CANCELLED
    func resetEditProfileView(){
        navigationBar?.rightButton.setTitle(TEXT.EDIT, for: .normal)
        navigationBar?.rightButton.isSelected = false
        setUserInfo()
        tableViewDataSource?.isProfileEditing = true
        self.view.endEditing(true)
        profileTableView.reloadData()
        bottomButtonConstraint.constant = 50
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        setViewForEditing(false)
    }
   
   // MARK: - IBAction
   @IBAction func editAction(_ sender: UIButton) {
      guard tableViewDataSource.isProfileEditing else {
         setViewForEditing(true)
         return
      }
      
      if profileDetailsinfo.newPassWord == "" && profileDetailsinfo.oldPassWord == "" {
         tableViewDataSource.wasPasswordChanged = false
      }
      
      if tableViewDataSource.isNeedToUpdateNewImage == false && tableViewDataSource.didMakeAnyChanges == false && tableViewDataSource.wasPasswordChanged == true {
         if passwordValidationPassed() {
            hitPasswordChangeAPI()
         }
      } else if profileFieldValidationsPassed() {
         hitApiToUpdateProfile()
      }
      
   }
   
   func setViewForEditing(_ editing: Bool) {
      
      if editing == false {
         cellTypeArray = self.cellTypeArray.filter{$0 != FIELD_TYPE.newPassword && $0 != FIELD_TYPE.changepassword }
         if self.cellTypeArray.contains(FIELD_TYPE.password) == false    {
            self.cellTypeArray.append(FIELD_TYPE.password)
         }
         self.view.endEditing(true)
         tableViewDataSource?.isProfileEditing = false
         tableViewDataSource?.didMakeAnyChanges = false
         tableViewDataSource?.wasPasswordChanged = false
         tableViewDataSource?.isNeedToUpdateNewImage = false
         editProfileButton.setTitle(TEXT.EDIT , for: UIControlState.normal)
      } else {
         tableViewDataSource?.isProfileEditing = true
         cellTypeArray = self.cellTypeArray.filter{$0 != FIELD_TYPE.password}
         self.cellTypeArray.append(FIELD_TYPE.changepassword)
         self.cellTypeArray.append(FIELD_TYPE.newPassword)
         editProfileButton?.setTitle(TEXT.SAVE, for: UIControlState.normal)
      }
      
      tableViewDataSource?.cellTypeArray = cellTypeArray
      self.profileTableView.reloadData()
   }
   
   func backAction() {
      if tableViewDataSource.isProfileEditing == false {
         self.view.endEditing(true)
         _ = self.navigationController?.popViewController(animated: true)
      }else {
         self.view.endEditing(true)
         resetEditProfileView()
      }
   }
   
   // MARK: - Methods
   func setUserInfo() {
      profileDetailsinfo = ProfileDetails()
      profileDetailsinfo.email = Vendor.current?.email ?? ""
      profileDetailsinfo.fullName = (Vendor.current?.firstName ?? "") + " " +  (Vendor.current?.lastName ?? "")
      profileDetailsinfo.firstName = (Vendor.current?.firstName ?? "")
      profileDetailsinfo.lastName = (Vendor.current?.lastName ?? "")
      profileDetailsinfo.company = Vendor.current?.company ?? ""
      profileDetailsinfo.address  = Vendor.current?.address ?? ""
      profileDetailsinfo.imageString = Vendor.current?.vendorImage ?? ""
      profileDetailsinfo.phoneNumber = Vendor.current?.phoneNo ?? ""
      tableViewDataSource?.profileData = profileDetailsinfo
   }
   
   func showErrorMessage(error:String, isError:Bool) {
      ErrorView.showWith(message: error, isErrorMessage: isError, removed: nil)
   }
    
    //MARK: - TABLE VIEW DELEGATES
   func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      return 57
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      let section = ProfileTableViewDataSource.Section(rawValue: section)!
      
      switch section {
      case .customFields:
         if getCustomFields().count == 0 {
            return nil
         }
         return getViewForCustomFieldsHeader()
      case .staticFields:
         return nil
      }
   }
   
   func getViewForCustomFieldsHeader() -> UIView {
      
      let view = UIView()
      view.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
      
      let label = UILabel()
      label.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
      label.text = TEXT.additionalInfo
      let labelFont = UIFont(name: FONT.regular, size: 13)!
      label.font = labelFont

      let sizeOfLabel = label.text!.size(attributes: [NSFontAttributeName: labelFont])
      label.frame.size = sizeOfLabel
      label.frame.origin = CGPoint(x: 30, y: 9)
      
      view.addSubview(label)
      return view
   }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      let sectionType = ProfileTableViewDataSource.Section(rawValue: indexPath.section)!

      switch sectionType {
      case .staticFields:
         return  indexPath.row == 0 ? 210 : 63
      case .customFields:
         let customFields = getCustomFields()
         let currentCustomField = customFields[indexPath.row]
         
         if currentCustomField.app_side == .hidden || !currentCustomField.dataType.isDataTypeHandled() {
            return 0.001
         }
         return UITableViewAutomaticDimension
         
      }
    }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      let sectionType = ProfileTableViewDataSource.Section(rawValue: section)!
      switch sectionType {
      case .staticFields:
         return 0.01
      case .customFields:
         let signupTemplate = Vendor.current?.signupTemplate
         let customFieldCount = signupTemplate?.customFields.count ?? 0
         return customFieldCount > 0 ? 32 : 0.01
      }
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let customFields = getCustomFields()
      let section = ProfileTableViewDataSource.Section(rawValue: indexPath.section)!
      
      guard section == .customFields else {
         return
      }
      
      let customField = customFields[indexPath.row]
      
      guard customField.dataType == .url,
         customField.app_side == .readOnly,
      customField.data != "-" && customField.data != "" else {
            return
      }
      
      guard let urlString = customField.data,
         let url = URL(string: urlString),
      UIApplication.shared.canOpenURL(url) else {
         ErrorView.showWith(message: ERROR_MESSAGE.invalidUrl, removed: nil)
            return
      }
      
      UIApplication.shared.openURL(url)
      
   }
   

   
}
