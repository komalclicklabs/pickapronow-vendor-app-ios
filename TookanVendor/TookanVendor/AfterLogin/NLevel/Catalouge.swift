//
//  Catalogue.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation
import CoreLocation
enum LevelLayout: Int {
   case list = 1
   case banner = 2
   case map = 3
   case menu = 4
    
    func shouldSetFrame() -> Bool {
        switch self {
        case .map:
            return false
        default:
            return true
        }
    }
}

enum CatalaogueError: LocalizedError {
   case levelIsEmpty
   var errorDescription: String? {
      return ERROR_MESSAGE.catalougeFetchingFailure
   }
}

class Catalogue {
   
   // MARK: - Properties
   private(set) var formId = ""
   private var levels = [Level]()
   
   private var numberOfLevels: Int {
      return levels.count
   }
   
   /// 1. [Int: [Int]] in this dictionary, 'key' denotes index of parent in parent level and [Int] denotes it's children in lower level, [Int] value of 'Int' in this will be index of children in their level. '[Int]' is also referred as child array in code.
   
   /// 2. At first level(first index of array) will be empty
   
   /// 3. At last level(last index of array) every value corrensponding to key will contain empty array as they will not have any children.
   private var levelsParentChildRelationTree = [[Int: [Int]]]()
   
   /// 'Key' of dictionary is 'Id' of products parent
   /// 'value' of dictionary is 'index' of parent in level array
   private var productParentsHashMap = [String: Int]()
   
   
   // MARK: - Intializer
   init(json: [[[String: Any]]]) throws {
      
      for (index, rawLevel) in json.enumerated() {
//         guard rawLevel.count > 0 else {
//            throw CatalaogueError.levelIsEmpty
//         }
         let (level, hashMap) = LevelElement.getLevelFrom(json: rawLevel, levelIndex: index)
         levels.append(level)
         levelsParentChildRelationTree.append(hashMap)
      }
   }
   
   // MARK: - Methods
   func getChildrenOfElementAt(level: Int, withIndex parentIndex: Int, showActivityIndicator: Bool = true, completion: @escaping (Bool, [LevelElement]?) -> Void) {
      
      let levelOfChildren = level + 1
      
      guard !isAskingForProductLevel(level: levelOfChildren) else {
        self.fetchingProducts(level: level, parentIndex: parentIndex, levelOfChildren: levelOfChildren, completion: { (success, levelElement) in
            completion(success, levelElement)
        })
         return
      }
      
      guard doChildrenExistOf(parentAtIndex: parentIndex, atLevel: level) else {
         completion(false, nil)
        ErrorView.showWith(message: ERROR_MESSAGE.unableToFetchProducts, isErrorMessage: true, removed: nil)
//        if parentIndex > 0 {
//            self.fetchingProducts(level: level, parentIndex: parentIndex - 1, levelOfChildren: levelOfChildren, completion: { (success, levelElement) in
//                completion(success, levelElement)
//            })
//        }
        
         print("Children do not exist")
         return
      }
      
      guard !isZerothLevel(level: levelOfChildren) else {
         completion(true, elementsAtZerothLevel())
         return
      }
      
      let indicesOfChildren = getIndicesOfChildrenOfParent(atIndex: parentIndex, inLevel: level)
      let elements = getElementsAt(level: levelOfChildren, withIndices: indicesOfChildren)
      completion(true, elements)
   }
   
    func fetchingProducts(level:Int, parentIndex: Int, levelOfChildren: Int, completion: @escaping (Bool, [LevelElement]?) -> Void) {
        if levels.count > 0 {
            
            let parent = levels[level][parentIndex]
            
            fetchProductWith(parentWithId: parent.id, atLevel: levelOfChildren,location: Singleton.sharedInstance.productLocation, showActivityIndicator: true, withParentIndex: parentIndex) { (success, products) in
                completion(success, products)
                return
            }
            return
        }
        fetchProductWith(parentWithId: nil, atLevel: level,location: Singleton.sharedInstance.productLocation, showActivityIndicator: true, withParentIndex: parentIndex) { (success, products) in
            completion(success, products)
            return
        }
    }
    
   private func isAskingForProductLevel(level: Int) -> Bool {
      return numberOfLevels <= level
   }
   
    func getParentLevelWith(parentId: String?, productLevel: Int) -> (Int?, LevelElement?) {
        if productLevel == -1 {
            return (nil,nil)
        }
        guard let unwrappedId = parentId else {
            return (nil,nil)
        }
        
        let arrayOfLevels = self.levels[productLevel]
        for (index, product) in arrayOfLevels.enumerated() {
            if product.id == unwrappedId {
                return (index, product)
            }
        }
        
        return (nil,nil)

    }
    
   private func doChildrenExistOf(parentAtIndex parentIndex: Int, atLevel levelIndex: Int) -> Bool {
      let levelOfChildren = levelIndex + 1
      
      if isZerothLevel(level: levelOfChildren) && haveElementsAtZerothLevel() {
         return true
      }
      
      let hashMapOfChildLevel = levelsParentChildRelationTree[levelOfChildren]
    
      guard let indicesOfChildren = hashMapOfChildLevel[parentIndex], indicesOfChildren.count > 0 else {
         print("Error -> Children not found of element at index \(parentIndex) of level \(levelIndex)")
         return false
      }
      
      return true
   }
   
   private func haveElementsAtZerothLevel() -> Bool {
      return levels.count > 0 && levels[0].count > 0
   }
   
   private func isZerothLevel(level: Int) -> Bool {
      return level <= 0
   }
   
   private func elementsAtZerothLevel() -> Level? {
      return levels.first
   }
   
   private func getIndicesOfChildrenOfParent(atIndex: Int, inLevel levelIndex: Int) -> [Int] {
      let levelOfChildren = levelIndex + 1
      let hashMapOfChildLevel = levelsParentChildRelationTree[levelOfChildren]
      let indicesOfChildren = hashMapOfChildLevel[atIndex]
      return indicesOfChildren!
   }
   
   private func getElementsAt(level: Int, withIndices: [Int]) -> [LevelElement] {
      let levelOfWhichElementsAreRequired = levels[level]
      
      var elements = [LevelElement]()
      for index in withIndices {
         elements.append(levelOfWhichElementsAreRequired[index])
      }
      return elements
   }
   
    private func fetchProductWith(parentWithId parentID: String?, atLevel level: Int,location: CLLocationCoordinate2D, showActivityIndicator: Bool, withParentIndex parentIndex: Int, completion: @escaping (Bool, [LevelElement]?) -> Void) {
      
      if parentID != nil {
         productParentsHashMap[parentID!] = parentIndex
      }

      Product.fetchProductsFromServerOf(parentWithid: parentID, andFormId: formId, atLevel: level,location: location, showActivityIndicator: showActivityIndicator) { (success, products) in
         guard success else {
            completion(false, nil)
            return
         }
         let productsAsLevelElements = products!.map {$0 as LevelElement}
         completion(success, productsAsLevelElements)
      }
   }
   
   func getTitleOf(level: Int, withParentIndex parentIndex: Int, parentId: String?) -> String {
      let parentLevel = level - 1
      
      let formName = Singleton.sharedInstance.formDetailsInfo.formName
      if isZerothLevel(level: level) {
         return formName
      }
      

      if isAskingForProductLevel(level: level) && productParentsHashMap.count > 0 {
         let element = getParentOfProductWith(parentId: parentId, productLevel: level)!
         return element.name
      }
      
      let categoryName = levels[parentLevel][parentIndex].name
      
      if categoryName.isEmpty {
         return formName
      }
      return categoryName
   }
   

    
    
   func getIndexOfElementInLevelWhosePositionInChildArrayIs(position: Int, level: Int, parentIndex: Int) -> Int {
      
      return levelsParentChildRelationTree[level][parentIndex]![position]
   }
   
   func getParentOfProductWith(parentId: String?, productLevel: Int) -> LevelElement? {
      guard let unwrappedId = parentId else {
         return nil
      }
      
      let indexOfparent = productParentsHashMap[unwrappedId]!
      let parentLevel = productLevel - 1
      
      return levels[parentLevel][indexOfparent]
   }
   
   
   // MARK: - Server Hit
   static func getCatalogueOfFormWith(formId: String,location:CLLocationCoordinate2D, showActivityIndicator: Bool = true, showAlert: Bool = true,completion: ((Bool, Catalogue?, Error?) -> Void)?) {
      
    let params = getParamsToGetCatalogeLevels(formID: formId, location: location)
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlertInDefaultCase: showAlert, showActivityIndicator: showActivityIndicator , para: params, extendedUrl: API_NAME.getAppCatalogue) { (responseObject, error, _, statusCode) in
         if statusCode == .some(STATUS_CODES.SHOW_DATA),
         let response = responseObject as? [String: Any],
            let rawLevels = response["data"] as? [[[String: Any]]] {
            do {
               let catalogue = try Catalogue.init(json: rawLevels)
               catalogue.formId = formId
               completion?(true, catalogue, error)
            }
            catch let error {
               completion?(false, nil, error)
            }
            
         } else {
            guard statusCode != STATUS_CODES.INVALID_ACCESS_TOKEN else {
               //error automatiacally handled
               return
            }
            completion?(false, nil, error)
         }
         
      }
   }
   
   private static func getParamsToGetCatalogeLevels(formID: String,location:CLLocationCoordinate2D) -> [String: Any] {
      return [
         "access_token" : Vendor.current!.appAccessToken!,
         "form_id" : formID,
         "user_id" : Vendor.current!.userId!,
         "device_token" : APIManager.sharedInstance.getDeviceToken(),
         "app_version" : APP_VERSION,
         "app_access_token" : Vendor.current!.appAccessToken!,
         "app_device_type" : APP_DEVICE_TYPE,
        "delivery_latitude" : "\(location.latitude)",
        "delivery_longitude" : "\(location.longitude)"
      ]
   }

}
