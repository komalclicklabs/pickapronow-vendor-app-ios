//
//  NLevelFlowManager.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation

protocol NLevelNavigationHandler: class {
   func pushNextControllerWithChildrenOf(element: LevelElement, atChildIndex: Int)
   func fallBackToPreviousLevelWith(currentLevel: Int)
    func getNonDummyChildrenOf(element: LevelElement, atChildIndex: Int, indexInLevel: Int?, showActivityIndicator: Bool, completion: ((Bool, [LevelElement]?) -> Void)?)
}

protocol NLevelCart {
   func getCartCount() -> Int
   func getQuantityOfProductWith(id: String) -> Int
   func quantityAddedOf(element: LevelElement)
   func quantitySubtractedOf(element: LevelElement)
   func checkoutButtonPressed()
   func getSelectedProducts() -> [Product]
   func clearCart()
   func getCommaSeperatedTagsOfSelectedProducts() -> String
}

class NLevelFlowManager {
      
   // MARK: - Properties
   fileprivate var navigationController: UINavigationController
   
   var catalogue: Catalogue
   fileprivate var cartCount = 0
   
   /// 'key' is id of the product
   fileprivate var selectedProducts = [String: Product]()
   
   // MARK: - Intializer
   init(navigationController: UINavigationController, catalogue: Catalogue) {
      self.navigationController = navigationController
      self.catalogue = catalogue
      self.selectedProducts = CartPersistencyManager.shared.getSelectedProductsFor(formId: catalogue.formId)
      calculateQuantityOfSelectedProducts()
      setPricing()
   }
   
   func calculateQuantityOfSelectedProducts() {
      var tempCartCount = 0
      for (_, products) in selectedProducts {
         tempCartCount += products.quantity
      }
      self.cartCount = tempCartCount
   }
   
   func startFlow() {
      getViewControllerWithChildrentOfElement(atIndex: 0, inLevel: -1, withParentIndex: 0) { [weak self] (success, vc) in
         guard success else {
            return
         }
         
         self?.pushController(vc!)
      }
   }
   
   func getFirstVC(completion: @escaping (Bool, UIViewController?) -> Void) {
      getViewControllerWithChildrentOfElement(atIndex: 0, inLevel: -1, withParentIndex: 0, completion: completion)
   }
      
   //MARK: - NLevel View Controller Setup
   fileprivate func getViewControllerWithChildrentOfElement(atIndex index: Int, inLevel level: Int, withParentIndex parentIndex: Int, completion: @escaping (Bool, UIViewController?) -> Void) {
      getChildrenOfElementAt(index: index, inLevel: level) { [weak self] (success, levelElements) in
         
         guard let weakSelf = self, success else {
            completion(false, nil)
            return
         }
         
         if weakSelf.isLevelDummy(level: levelElements!) {
            let element = levelElements!.first!
            let indexOfDummyElementInLevel = weakSelf.getIndexInLevelArrayOf(element: element, withindexInChildArray: 0)
            weakSelf.getViewControllerWithChildrentOfElement(atIndex: indexOfDummyElementInLevel, inLevel: element.level, withParentIndex: element.parentIndexInPreviousLevel, completion: completion)
            return
         }
         
         let parentVC = weakSelf.setUpViewControllerWith(levelElements: levelElements!)
         completion(true, parentVC)
      }
   }
   
   fileprivate func isLevelDummy(level: Level) -> Bool {
      return level.first?.isDummy ?? false
   }
   
   private func setUpViewControllerWith(levelElements: [LevelElement]) -> NLevelParentViewController {
      
      let layout = getCurrentLayoutOf(level: levelElements)
      
      let title = getTitleOf(level: levelElements)
      
      let nLevelParent = NLevelParentViewController.getWith(layoutType: layout, level: levelElements, title: title)
      nLevelParent.flowManager = self
      nLevelParent.cart = self
      return nLevelParent
   }
   
   private func getTitleOf(level: Level) -> String {
      let element = level.first!
      return catalogue.getTitleOf(level: element.level, withParentIndex: element.parentIndexInPreviousLevel, parentId: element.parentId)
   }
   
   private func getCurrentLayoutOf(level: Level) -> LevelLayout {
      let element = level.first!
      return element.layoutType//catalogue.getLayoutOf(level: element.level, withParentIndex: element.parentIndexInPreviousLevel)
   }
   
   //MARK: - Element Fetching
    func getChildrenOfElementAt(index: Int, inLevel level: Int, showActivityIndicator: Bool = true, completion: ((Bool, [LevelElement]?) -> Void)?) {
      
      catalogue.getChildrenOfElementAt(level: level, withIndex: index, showActivityIndicator: showActivityIndicator) { (success, levelElements) in
            completion?(success, levelElements)
      }
   }
   //MARK: - ------

   func getCartCount() -> Int {
      return cartCount
   }
   
   func getIndexInLevelArrayOf(element: LevelElement, withindexInChildArray index: Int) -> Int {
      return catalogue.getIndexOfElementInLevelWhosePositionInChildArrayIs(position: index, level: element.level, parentIndex: element.parentIndexInPreviousLevel)
   }

   //MARK: - ----
   func deleteTask() {
      cartCount = 0
      setSelectedProductsQuantityToZero()
      
      selectedProducts.removeAll()
      elementInCartChanged()

      Singleton.sharedInstance.selectedData = ""
      Singleton.sharedInstance.subTotal = 0
   }
   
   private func setSelectedProductsQuantityToZero() {
      for product in selectedProducts.values {
         product.quantity = 0
      }
   }
    
    func updateCatalogueWith(location: CLLocationCoordinate2D, completion: @escaping () -> Void) {
        let formId = Singleton.sharedInstance.formDetailsInfo.form_id!
        Catalogue.getCatalogueOfFormWith(formId: formId, location: location) { (success, catalogue, error) in
            if success {
                self.catalogue = catalogue!
            }
            completion()
            
        }
    }
   
   deinit {
      print("NLevel Flow manager deintialized")
   }
}

// MARK: - Navigation Handler
extension NLevelFlowManager: NLevelNavigationHandler {

   func fallBackToPreviousLevelWith(currentLevel: Int) {

      _ = navigationController.popViewController(animated: true)
   }
   
   fileprivate func willGoingBackFromLevelResultInCartDeletion(_ level: Int) -> Bool {
      return level == 0 && selectedProducts.count != 0
   }
   
   func pushNextControllerWithChildrenOf(element: LevelElement, atChildIndex index: Int) {
      let indexOfElementInLevel = getIndexInLevelArrayOf(element: element, withindexInChildArray: index)
      
      getViewControllerWithChildrentOfElement(atIndex: indexOfElementInLevel, inLevel: element.level, withParentIndex: element.parentIndexInPreviousLevel) { [weak self](success, vc) in
         guard success else {
            return
         }
         self?.pushController(vc!)
      }
   }
   
    func getNonDummyChildrenOf(element: LevelElement, atChildIndex: Int, indexInLevel: Int? = nil, showActivityIndicator: Bool, completion: ((Bool, [LevelElement]?) -> Void)?) {
        
        if element is Product {
         completion?(false, nil)
         return
      }
      var index = indexInLevel
        if indexInLevel == nil {
            index = getIndexInLevelArrayOf(element: element, withindexInChildArray: atChildIndex)
        }
        getChildrenOfElementAt(index: index!, inLevel: element.level, showActivityIndicator: showActivityIndicator) { [weak self] (success, levelElements) in
         guard let weakSelf = self, success else {
            completion?(false, nil)
            return
         }
         
         if weakSelf.isLevelDummy(level: levelElements!) {
            weakSelf.getNonDummyChildrenOf(element: levelElements!.first!, atChildIndex: 0, showActivityIndicator: showActivityIndicator, completion: completion)
            return
         }
         
         completion?(success, levelElements)
      }
   }
   
}

// MARK: - Cart Manager
extension NLevelFlowManager: NLevelCart {
   func getCommaSeperatedTagsOfSelectedProducts() -> String {
      let arrayOfTags = getTagsInArray()
      var commaSeperatedTags = ""
      
      for var tag in arrayOfTags {
         tag = tag.replacingOccurrences(of: " ", with: "-")
         if !commaSeperatedTags.isEmpty {
            commaSeperatedTags += ", "
         }
         commaSeperatedTags += tag
      }
      
      return commaSeperatedTags
   }
   
   private func getTagsInArray() -> [String] {
      var tags: Set<String> = []
      let products = getSelectedProducts()
      
      for product in products {
         if product.parentId != nil {
            let tag = catalogue.getTitleOf(level: product.level, withParentIndex: 0, parentId: product.parentId)
            tags.insert(tag)
         } else {
            let tag = product.name
            tags.insert(tag)
         }
      }
      
      return Array(tags)
   }

   func getSelectedProducts() -> [Product] {
      return Array(selectedProducts.values)
   }

   func getQuantityOfProductWith(id: String) -> Int {
      return selectedProducts[id]?.quantity ?? 0
   }
   
   internal func quantitySubtractedOf(element: LevelElement) {
      guard let product = element as? Product else {
         return
      }
      
      let previousQuantity = getQuantityOfProductWith(id: element.id)
      product.quantity = previousQuantity
      
      guard previousQuantity > 0 else {
         return
      }
      product.quantity -= 1
      cartCount -= 1
      

      if product.quantity == 0 {
         selectedProducts.removeValue(forKey: product.id)
         elementInCartChanged()
         return
      }
      
      selectedProducts[product.id] = product
      elementInCartChanged()

   }
   
   internal func quantityAddedOf(element: LevelElement) {
      guard let product = element as? Product else {
         return
      }
      
      product.quantity = getQuantityOfProductWith(id: product.id)
      
      product.quantity += 1
      cartCount += 1

      selectedProducts[product.id] = product
      
      elementInCartChanged()
   }
   
   func checkoutButtonPressed() {
      guard selectedProducts.count > 0 else {
         Singleton.sharedInstance.showAlert("Please add something to cart to continue ")
         return
      }
      
      setPricing()
      
      let vc = UIViewController.findIn(storyboard: .nLevel, withIdentifier: "checkoutScreen") as! CheckoutViewController
      vc.creatTaskManager.cart = self
      pushController(vc)

   }
   
   fileprivate func elementInCartChanged() {
      setPricing()
      CartPersistencyManager.shared.update(selectedProducts: selectedProducts, forFormId: catalogue.formId)
   }
   
   fileprivate func setPricing() {
      var pricingDesc = ""
      var subtotal: Double = 0
      
      for product in selectedProducts.values {
         
         if !pricingDesc.isEmpty {
            pricingDesc += ",\n"
         }
       
         pricingDesc += product.getPricingDescription()
         subtotal += product.getPrice()
      }
      
      Singleton.sharedInstance.selectedData = pricingDesc
      Singleton.sharedInstance.subTotal = subtotal
   }
   
   func clearCart() {
      deleteTask()
   }
   
   fileprivate func pushController(_ controller: UIViewController) {
      navigationController.pushViewController(controller, animated: true)
   }
}
