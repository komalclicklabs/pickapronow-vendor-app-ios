//
//  Lines.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation
import UIKit

private enum LineFont: Int {
   case light = 1
   case regular = 2
   case bold = 3
   case semiBold = 4
   
   func getFontName() -> String {
      switch self {
      case .light:
         return FONT.light
      case .regular:
         return FONT.regular
      case .bold:
         return FONT.bold
      case .semiBold:
         return FONT.semiBold
      }
   }
   
   func getFontSize() -> CGFloat {
      switch self {
      case .light:
         return 13
      case .semiBold:
         return 15
      case .regular:
         return 16
      case .bold:
         return 14
      }
   }
}

struct Lines {
   // MARK: - Properties
   private var arrayOfStrings = [String]()
   private var arrayOfLineFont = [LineFont]()
   private var arrayOfLineColor = [UIColor]()
   
   // MARK: - Initializer
   init(rawLayout: [String: Any]) {
      guard let rawLines = rawLayout["lines"] as? [[String: Any]] else {
         return
      }
      
      for rawLine in rawLines {
         guard let line = rawLine["data"] as? String else {
            continue
         }
         arrayOfStrings.append(line)
         
         let font = parseLineStyleFrom(rawLine: rawLine)
         arrayOfLineFont.append(font)
         arrayOfLineColor.append(COLOR.SPLASH_TEXT_COLOR)
      }
      overideFontsComingFromBackend()
      setTempColorStyle()
   }
   
   private func parseLineStyleFrom(rawLine: [String: Any]) -> LineFont {
      if let font = rawLine["style"] as? Int, let lineStyle = LineFont.init(rawValue: font) {
         return lineStyle
      }
      return LineFont.regular
   }
   
   private mutating func overideFontsComingFromBackend() {
      
      if arrayOfStrings.count == 3 {
         arrayOfLineFont.removeAll()
         arrayOfLineFont = [.regular, .light, .semiBold]
      }
   }
   
   private mutating func setTempColorStyle() {
      if arrayOfStrings.count == 3 {
         arrayOfLineColor = [COLOR.SPLASH_TEXT_COLOR, COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6), COLOR.THEME_FOREGROUND_COLOR]
      }
   }
   
   // MARK: - Methods
   func getAttributedStringDescription() -> NSAttributedString {
      let attributedDescription = NSMutableAttributedString()
      
      guard arrayOfLineFont.count == arrayOfStrings.count else {
         print("ERROR -> Count of line font and string not equal")
         return NSAttributedString()
      }
      
      for index in 0..<arrayOfStrings.count {
         let attributedLine = getAttributedLineFor(index: index)
         attributedDescription.append(attributedLine)
      }
      
      setLineSpacingIn(string: attributedDescription, spacing: 5)
      return attributedDescription
   }
   
   private func getAttributedLineFor(index: Int) -> NSAttributedString {
      let fontStyle = arrayOfLineFont[index]
      let color = arrayOfLineColor[index]
      let attributes = getStringAttributesWith(font: fontStyle, andColor: color)
      
      
      var line = ""
      let newText = arrayOfStrings[index]
      
      if index != 0 && !newText.isEmpty {
         line = "\n"
      }
      
      line += arrayOfStrings[index]
      
      let attributedString = NSAttributedString(string: line, attributes: attributes)
      return attributedString
   }
   
   private func getStringAttributesWith(font: LineFont, andColor color: UIColor) -> [String: Any]{
      let uiFont = UIFont(name: font.getFontName(), size: font.getFontSize())!
      return [NSFontAttributeName: uiFont, NSForegroundColorAttributeName: color]
   }
   
   private func setLineSpacingIn(string: NSMutableAttributedString, spacing: CGFloat) {
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.lineSpacing = spacing
      let range = NSMakeRange(0, string.length)
      string.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: range)
   }
   
   
}
