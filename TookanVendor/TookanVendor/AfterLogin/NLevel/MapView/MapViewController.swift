//
//  MapViewController.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 04/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher

class MapViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,locationManagerCustomDelegate,GMSMapViewDelegate {

    
    @IBOutlet weak var destinationAddress: NSLayoutConstraint!
    @IBOutlet weak var singleSelectionToggle: UISwitch!
    @IBOutlet weak var pickUpAddressBackView: UIView!
    @IBOutlet weak var dropOffView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var currentLocationIcon: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pickUpAddress: MarqueeLabel!

    private weak var delegate: NLevelFlowDelegate?
    var levelArray = [LevelElement]()
    var selectedIndex = [Int]()
    var singleSelection = false
    var locationManager = CustomLocationManager.shared
    var toUpdate = true
    var showDestination = false
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUiattributes()
        getQuantity()
        destinationAddress.constant = 0
        collectionView.delegate = self
        collectionView.dataSource = self
        giveUiAttributes()
        mapView.delegate = self
        self.collectionView.register(UINib(nibName: NIB_NAME.TaxiCarTypes, bundle: Bundle.main), forCellWithReuseIdentifier: CELL_IDENTIFIER.carTypeItem)
        locationManager.delegate = self
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6) {[weak self] in
            self?.locationManager.startUpdatingLocation()
        }
        
    }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      updateQauntity()

      collectionView.reloadData()
   }
    
    @IBAction func addPickUpAddress(_ sender: UIButton) {
        let vc: AddNewLocationViewController?
        vc = UIViewController.findIn(storyboard: .favLocation, withIdentifier: STORYBOARD_ID.addLocation) as? AddNewLocationViewController
        vc?.completionHandler = {[weak self](coordinate,address) in
            self?.toUpdate = false
            self?.updatemap(withCoordinate: coordinate,address:address)
        }
        if IJReachability.isConnectedToNetwork() == true{
            self.navigationController?.pushViewController(vc!, animated: true)
        }else{
            ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isErrorMessage: true, removed: nil)
        }
    }
    
    @IBAction func currentLocationButton(_ sender: UIButton) {
    self.locationManager.startUpdatingLocation()
    }
    
    @IBAction func checkoutButtonAction(_ sender: UIButton) {
        delegate?.checkoutButtonPressed()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        delegate?.popViewController()
    }

    class func getWith(level: [LevelElement],delegate: NLevelFlowDelegate) -> MapViewController {
        let mapTVC = MapViewController.findIn(storyboard: .nLevel, withIdentifier: "mapViewNlevel") as! MapViewController
        mapTVC.delegate = delegate
        mapTVC.levelArray = level
        return mapTVC
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER.carTypeItem, for: indexPath) as? TaxiCarTypes
      let element = levelArray[indexPath.row]
      
        cell?.carTypeName.text = element.name
        cell?.carImage.kf.setImage(with: element.imageUrl, placeholder: #imageLiteral(resourceName: "placeHolder"))
        cell?.changeState(to: self.selectedIndex.contains(indexPath.item))
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.levelArray.count
    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch singleSelection {
        case true:
            self.collectionView.reloadItems(at: self.updateForSingleSelection(forIndexPath: indexPath))
        default:
            self.collectionView.reloadItems(at: updateForMultiSelection(forIndexPath: indexPath))
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if levelArray.count <= 3{
            return CGSize(width: collectionView.frame.width/CGFloat(levelArray.count), height: collectionView.frame.height)
        }else{
            return CGSize(width: collectionView.frame.width/CGFloat(3.5), height: collectionView.frame.height)
        }
    }
    
    func giveUiAttributes(){
        checkoutButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        checkoutButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
        checkoutButton.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
        checkoutButton.setTitle("Checkout", for: UIControlState.normal)
    }
    
    func updateForSingleSelection(forIndexPath:IndexPath) -> [IndexPath]{
        if selectedIndex.count >= 1 && selectedIndex.contains(forIndexPath.item) == true{
            selectedIndex.removeAll()
            self.updateCart(forAdition: nil, forSubtraction: levelArray[forIndexPath.item])
            return [forIndexPath]
        }else if selectedIndex.count >= 1 && selectedIndex.contains(forIndexPath.item) == false{
            let previousIndex = selectedIndex[0]
            selectedIndex.removeAll()
            selectedIndex.append(forIndexPath.item)
            self.updateCart(forAdition: levelArray[forIndexPath.item], forSubtraction: levelArray[previousIndex])
            return [IndexPath(row: previousIndex, section: 0),forIndexPath]
        }else if selectedIndex.count == 0{
            self.updateCart(forAdition: levelArray[forIndexPath.item], forSubtraction: nil)
            selectedIndex.append(forIndexPath.item)
            return [forIndexPath]
        }
        return [forIndexPath]
    }
    
    func updateForMultiSelection(forIndexPath:IndexPath) -> [IndexPath]{
        if selectedIndex.contains(forIndexPath.item) == true{
            selectedIndex = selectedIndex.filter{ $0 != forIndexPath.item }
            print(selectedIndex)
            self.updateCart(forAdition: nil, forSubtraction: levelArray[forIndexPath.item])
        }else{
            selectedIndex.append(forIndexPath.item)
            self.updateCart(forAdition: levelArray[forIndexPath.item], forSubtraction: nil)
        }
        return [forIndexPath]
    }
    
    func updateCart(forAdition:LevelElement?,forSubtraction:LevelElement?){
        if let _ = forSubtraction{
            self.delegate?.subtractQuantityButtonPressed(forElement: forSubtraction!)
        }
        if let _ = forAdition{
            self.delegate?.addQuantityButtonPressed(forElement: forAdition!)
        }
    }
    
    func didUpdateLocation(withCoordinates: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition(target: withCoordinates, zoom: 16, bearing: 0, viewingAngle: 0)
        self.mapView.isMyLocationEnabled = true
        self.mapView.animate(to: camera)
        locationManager.reverseGeocodeCoordinate(coordinate: withCoordinates) {[weak self] (address) in
            self?.pickUpAddress.text = address
        }
        self.locationManager.stopUpdatingLocation()
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if toUpdate == true{
        locationManager.reverseGeocodeCoordinate(coordinate: position.target) {[weak self] (address) in
            self?.pickUpAddress.text = address
            }
        }else{
            toUpdate = false
        }
    }
    
    func updatemap(withCoordinate:CLLocationCoordinate2D,address:String){
        self.pickUpAddress.text = address
        
        let camera = GMSCameraPosition(target: withCoordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.5) { [weak self] in
             self?.mapView.animate(to: camera)
        }
    }

    func updateQauntity(){
      selectedIndex.removeAll()
        for i in 0..<levelArray.count{
            if (delegate?.getQuantityOfProductWith(id: levelArray[i].id))! > 0{
                self.selectedIndex.append(i)
            }
        }
        self.collectionView.reloadData()
    }
    
    @IBAction func singleSelectionToggleAction(_ sender: UISwitch) {
    
    singleSelection = !sender.isOn
        selectedIndex.removeAll()
        collectionView.reloadData()
    }
    
    func getQuantity(){
        for i in 0..<levelArray.count{
            if (delegate?.getQuantityOfProductWith(id: levelArray[i].id))! > 0{
                selectedIndex.append(i)
            }
        }
        collectionView.reloadData()
    }
   
    
    
    func setUpUiattributes(){
        self.pickUpAddress.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        self.pickUpAddress.text = "Getting Address..."
        self.pickUpAddress.textColor = UIColor(colorLiteralRed: 51/255, green: 51/255, blue: 51/255, alpha: 1)
      //  pickUpAddressBackView._cornerRadius = 10
        pickUpAddressBackView.layer.shadowColor = UIColor.black.cgColor
        pickUpAddressBackView.layer.shadowOpacity = 0.25
        pickUpAddressBackView.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        pickUpAddressBackView.layer.shadowRadius = 6
        pickUpAddressBackView.layer.cornerRadius = 3.5
    }
    
    
}
