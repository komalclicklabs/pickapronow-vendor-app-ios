//
//  BannerTableViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class BannerTableViewController: UITableViewController, ActionButtonDelegate {
   
   private var level: [LevelElement]!
   private weak var delegate: NLevelFlowDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
      
      self.tableView.register(UINib(nibName: "BannerTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: BannerTableViewCell.identifier)
      
      tableView.tableFooterView = UIView()
      tableView.estimatedRowHeight = 355
      tableView.rowHeight = UITableViewAutomaticDimension
      
      
    }

   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      tableView.reloadData()
   }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return level.count
    }

   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: BannerTableViewCell.identifier, for: indexPath) as! BannerTableViewCell
      let element = level[indexPath.row]
      var quantity = 0
      
      if let product = element as? Product {
         quantity = delegate!.getQuantityOfProductWith(id: product.id)
         product.quantity = quantity
      }
      
      if element is Product {
         cell.selectionStyle = .none
      }
      
      cell.setCellWith(imageUrl: element.imageUrl, description: element.lines.getAttributedStringDescription(), actionType: element.actionType, quantity: quantity)
      cell.delegate = self
      cell.tag = indexPath.generateTag()
      
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let levelElement = level[indexPath.row]
      
      tableView.deselectRow(at: indexPath, animated: true)
      
      guard levelElement.actionType == .next || levelElement.actionType == .nextButButtonHidden else {
         return
      }
      
      delegate?.nextButtonPressed(forElement: levelElement, atIndexInChildArray: indexPath.row)
   }
   
   // MARK: - Navigation
   class func getWith(level: [LevelElement], delegate: NLevelFlowDelegate) -> BannerTableViewController {
      let bannerTVC = BannerTableViewController.findIn(storyboard: .nLevel, withIdentifier: "BannerTableViewController") as! BannerTableViewController
      bannerTVC.level = level
      bannerTVC.delegate = delegate
      return bannerTVC
   }
    
    
//    class func setCurrentLevel(level: [LevelElement]) {
//        let bannerTVC = BannerTableViewController.findIn(storyboard: .nLevel, withIdentifier: "BannerTableViewController") as! BannerTableViewController
//        bannerTVC.level = level
//    }
   
   // MARK: - ActionButtonDelegate
   func actionButtonPressed(type:ButtonPressedType, tag: Int) {
      
      let indexRow = tag.getRowAndSection().row
      let element = level[indexRow]
      switch type {
      case .Add:
         delegate?.addQuantityButtonPressed(forElement: element)
         reloadRowAt(index: indexRow)
      case .Subtract:
         delegate?.subtractQuantityButtonPressed(forElement: element)
         reloadRowAt(index: indexRow)
      case .Next:
         delegate?.nextButtonPressed(forElement: element, atIndexInChildArray: indexRow)
      }
   }
   
   // MARK: - Methods
   private func reloadRowAt(index: Int) {
      let indexPath = IndexPath(row: index, section: 0)
      tableView.reloadRows(at: [indexPath], with: .none)
   }


}
