//
//  CartPersistancyManager.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 07/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


class CartPersistencyManager {
   
   // MARK: - Type Properties
   
   /// Setting nil this property will erase carts from file system
   static var shared: CartPersistencyManager!
 
   /// Key: Product ID
   /// Value: Product
   typealias HashMappedProducts = [String: Product]
   
   private static let documentDirectoryUrl = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
   private static let archiveURL = documentDirectoryUrl.appendingPathComponent("NLevelCarts")
   
   
   // MARK: - Properties
   
   /// Key: Form ID
   /// Value: Selected Products
   private var nLevelCarts = [String: HashMappedProducts]()
   
   
   // MARK: - Intializer
   init() {
      nLevelCarts = loadFromDocumentDirectory()
   }
   
   private func loadFromDocumentDirectory() -> [String: HashMappedProducts] {
      guard let unArchivedCart = NSKeyedUnarchiver.unarchiveObject(withFile: CartPersistencyManager.archiveURL.path) as? [String: HashMappedProducts] else {
         return [String: HashMappedProducts]()
      }
      return unArchivedCart
   }
   
   // MARK: - Deintializer
   deinit {
      print("Cart Persistency Manager Deintialized")
      deleteCartsFromFileSystem()
   }
   
   private func deleteCartsFromFileSystem() {
      let fileManeger = FileManager()
      do {
         try fileManeger.removeItem(atPath: CartPersistencyManager.archiveURL.path)
      } catch {
         print("Cannot delete cart file -> \(error.localizedDescription)")
      }
   }
   
   // MARK: - Methods
   func getSelectedProductsFor(formId: String) -> HashMappedProducts {
      return nLevelCarts[formId] ?? [:]
   }
   
   func saveInDocumentDirectory() {
      let isSavedSuccessfully = NSKeyedArchiver.archiveRootObject(nLevelCarts, toFile: CartPersistencyManager.archiveURL.path)
      
      if isSavedSuccessfully {
         NSLog("Cart Saved Successfully")
      } else {
         NSLog("Cart saving action failed")
      }
   }
   
   func update(selectedProducts: HashMappedProducts, forFormId formId: String) {
      nLevelCarts[formId] = selectedProducts
   }
   
}
