//
//  Product.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 16/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation
import CoreLocation

class Product: LevelElement {
   
   // MARK: - Types
   struct PropertyKey {
      static let price = "price"
      static let quantity = "quantity"
   }
   
   // MARK: - Properties
   var quantity = 0
   private(set) var price: Double = 0
   private(set) var unitDescription = ""
   
   // MARK: - Intializer
   override init?(json: [String : Any], level: Int) {
      super.init(json: json, level: level)
      
      self.price = (json["price"] as? NSNumber)?.doubleValue ?? 0
      self.unitDescription = (json["description"] as? String) ?? ""
      
      if actionType == .next || actionType == .nextButButtonHidden {
         actionType = .singleAddition
      }
      
   }
   
   // MARK: - NSCoding
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      price = aDecoder.decodeDouble(forKey: PropertyKey.price)
      quantity = aDecoder.decodeInteger(forKey: PropertyKey.quantity)
   }
   
   override func encode(with aCoder: NSCoder) {
      super.encode(with: aCoder)
      
      aCoder.encode(price, forKey: PropertyKey.price)
      aCoder.encode(quantity, forKey: PropertyKey.quantity)
   }
   
   // MARK: - Methods
   func getPricingDescription() -> String {
    
      return "\(name) * \(quantity) = \(Singleton.sharedInstance.formDetailsInfo.currencyid)\(price * Double(quantity)) \t ProductID-\(self.id)"
   }
   
   func getPrice() -> Double {
      return price * Double(quantity)
   }
   
   // MARK: - Type Methods
   class func fetchProductsFromServerOf(parentWithid id: String?, andFormId formId: String, atLevel level: Int,location: CLLocationCoordinate2D, showActivityIndicator: Bool, completion: @escaping (Bool, [Product]?) -> Void) {
      
      let coordinates = CustomLocationManager.latestCoordinate
         
         let params = getParamsToFetchChildren(ofParentWithId: id,location: location, latitude: coordinates.latitude, longitude: coordinates.longitude, formId: formId)
         
         HTTPClient.makeConcurrentConnectionWith(method: .POST,showActivityIndicator: showActivityIndicator, para: params, extendedUrl: API_NAME.getProductsForCategory) { (responseObject, error, _, statusCode) in
            if statusCode == .some(STATUS_CODES.SHOW_DATA),
               let response = responseObject as? [String: Any],
               let rawLevels = response["data"] as? [[String: Any]] {
               
               guard rawLevels.count > 0 else {
                  let message = error?.localizedDescription ?? ERROR_MESSAGE.unableToFetchProducts
                  ErrorView.showWith(message: message, removed: nil)
                  completion(false, nil)
                  return
               }
               
               let products = getArrayOfProductsFrom(json: rawLevels, level: level)
               
               completion(true, products)
            } else {
               completion(false, nil)
            }
         }
      
   }
   
   private class func getParamsToFetchChildren(ofParentWithId id: String?,location: CLLocationCoordinate2D, latitude: Double, longitude: Double, formId: String) -> [String: Any] {
      var params: [String: Any] = [
         "access_token": Vendor.current!.appAccessToken!,
         "device_token": APIManager.sharedInstance.getDeviceToken(),
         "app_version": APP_VERSION,
         "app_device_type": APP_DEVICE_TYPE,
         "app_access_token": Vendor.current!.appAccessToken!,
         "user_id": Vendor.current!.userId!,
         "form_id": formId,
         "parent_category_id": id ?? "",
         "point": "\(latitude) \(longitude)",
        "delivery_latitude" : "\(location.latitude)",
        "delivery_longitude" : "\(location.longitude)"
      ]
      
      if id == nil {
         params.removeValue(forKey: "parent_category_id")
      }
      
      
      return params
   }
   
   private class func getArrayOfProductsFrom(json: [[String: Any]], level: Int) -> [Product] {
      var products = [Product]()
      for rawProduct in json {
         if let product = Product(json: rawProduct, level: level) {
            products.append(product)
         }
      }
      return products
   }
}
