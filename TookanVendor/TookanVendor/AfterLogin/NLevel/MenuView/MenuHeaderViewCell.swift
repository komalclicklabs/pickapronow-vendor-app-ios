//
//  MenuHeaderViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 19/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class MenuHeaderViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
   @IBOutlet weak var underLineView: UIView!
   
   override func awakeFromNib() {
      super.awakeFromNib()
      
      underLineView.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
   }
   
   func setCellWith(text: String, isSelected: Bool) {
      headerLabel.text = text
      let fontName = isSelected ? FONT.regular : FONT.light
      headerLabel.font = UIFont(name: fontName, size: 17)
      headerLabel.textColor = isSelected ? COLOR.SPLASH_TEXT_COLOR : COLOR.PLACEHOLDER_COLOR
      setUnderLineView(hidden: !isSelected)
   }
   
   private func setUnderLineView(hidden: Bool) {
      underLineView.isHidden = hidden
   }
    
}
