//
//  CheckoutViewController.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 06/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation

class CheckoutViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
   
   @IBOutlet weak var saveAndReviewbutton: UIButton!
   @IBOutlet weak var tableView: UITableView!
   
   var navigationBar: NavigationView!
   var imagePickerManager: MediaSelector?
   var creatTaskManager = CreateTaskModal()
   var cartSectionHeader: CheckoutHeaderView?
   var imagesPreview:ImagesPreview!
   var locationManager = CustomLocationManager.shared
   var showBackButton:Bool?
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.automaticallyAdjustsScrollViewInsets = false
      Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
      registerNib()
      creatTaskManager.setUpPickUpDeliveryWorkFlow()
    if Singleton.sharedInstance.addressForNlevelFlow == "" {
        self.locationManagerDelegate()
    } else {
        self.creatTaskManager.addressFromPreviousField()
        if (self.creatTaskManager.headerArray.contains(.DeliveryDetails) == true && self.creatTaskManager.headerArray.contains(.PickUpDetails) == true){
            self.locationManagerDelegate()
        }
    }
      tableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      tableView.separatorStyle = .none
      self.tableView.rowHeight = UITableViewAutomaticDimension
      self.tableView.estimatedRowHeight = 88
      setNavigationBar()
      self.creatTaskManager.onjobCreation = { [weak self] (Void) in
         self?.tableView.reloadData()
      }
      
   }
   
   override func viewWillAppear(_ animated: Bool) {
      UIApplication.shared.statusBarStyle = .default
      self.tableView.reloadData()
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      
      navigationBar?.setFrame()
   }
   
    func locationManagerDelegate() {
        DispatchQueue.main.async {[weak self] in
            self?.locationManager.delegate = self
            self?.locationManager.startUpdatingLocation()
        }
    }
   func numberOfSections(in tableView: UITableView) -> Int {
      return creatTaskManager.headerArray.count
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return creatTaskManager.checkForColapsable(header: creatTaskManager.headerArray[section])
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if creatTaskManager.headerArray[indexPath.section] == .Description {
         
         let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.taskDescription, for: indexPath) as? TaskDescriptionTableViewCell
         
         cell?.setUpWith(stringToBeReturnes: &Singleton.sharedInstance.createTaskDetail.jobDescription)
         return cell!
         
         
      }else if [headerType.PickUpDetails,.AppointmentDetails,.DeliveryDetails].contains(creatTaskManager.headerArray[indexPath.section]) {
         
         let creatTaskStaticData = creatTaskManager.getStaticFieldData(headerType: creatTaskManager.headerArray[indexPath.section], index: indexPath.row)
         
         switch creatTaskStaticData.type {
         case .name:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.customTextField, for: indexPath) as? CustomFTextFieldCell
            
            
            cell?.setUpForCustomField(type: creatTaskStaticData.type, textValueInCaseOfStatic: creatTaskStaticData.initialData, isForPickUp: creatTaskStaticData.isPickUp, isStatic: true, dataForNonStatic: nil, header: creatTaskStaticData.LabelName)
            
            return cell!
         case .email:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.customTextField, for: indexPath) as? CustomFTextFieldCell
            
            cell?.setUpForCustomField(type: creatTaskStaticData.type, textValueInCaseOfStatic: creatTaskStaticData.initialData, isForPickUp: creatTaskStaticData.isPickUp, isStatic: true, dataForNonStatic: nil, header: creatTaskStaticData.LabelName)
            return cell!
            
            
         case .pickUpAddress ,.dropAddress:
            
            let addressCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.AddressCustomeField, for: indexPath) as? AddressCustomFieldTableViewCell
            
            addressCell?.setUpCell(type:creatTaskStaticData.type, textValueInCaseOfStatic: creatTaskStaticData.initialData, header: creatTaskStaticData.LabelName)
//            guard creatTaskStaticData.type == .pickUpAddress, Singleton.sharedInstance.addressForNlevelFlow == "" else {
//                addressCell?.address.text = Singleton.sharedInstance.addressForNlevelFlow
//                return addressCell!
//            }
            
            
            return addressCell!
            
         case .phonenumber :
            
            let phoneCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.phoneCode, for: indexPath) as? PhoneNumberTableViewCell
            
            phoneCell?.setUpPhoneNumberCell(fortype: creatTaskStaticData.type, headerName: creatTaskStaticData.LabelName, isStatic: true, isPickUp: creatTaskStaticData.isPickUp, dataForNonStatic: nil, data: creatTaskStaticData.initialData)
            return phoneCell!
            
         case .startDateTime ,.endDateTime:
            
            let dateCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.dateCustomField, for: indexPath) as? DateCustomTableViewCell
            
            dateCell?.setUpForCustomField(type: creatTaskStaticData.type, textValueInCaseOfStatic: creatTaskStaticData.initialData, isForPickUp: creatTaskStaticData.isPickUp, isStatic: true, dataForNonStatic: nil, header: creatTaskStaticData.LabelName)
            
            return dateCell!
            
         default:
            print("")
         }
      }else if [headerType.pickUpCustomeDetails,.customDeliveryDetails].contains(creatTaskManager.headerArray[indexPath.section]){
         
         let data = creatTaskManager.headerArray[indexPath.section] == .pickUpCustomeDetails ?  creatTaskManager.customPickUpCells[indexPath.row] as! CustomFieldDetails : creatTaskManager.customDeliveryCells[indexPath.row] as! CustomFieldDetails
         
         switch data.dataType {
            
         case .Text,.email,.Number,.url:
            if data.app_side != .readOnly && data.app_side != .hidden {
               let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.customTextField, for: indexPath) as? CustomFTextFieldCell
               cell?.setUpForCustomField(type: data.dataType, textValueInCaseOfStatic: data.data!, isForPickUp: false, isStatic: false, dataForNonStatic: data, header: data.display_name)
               return cell!
            } else {
               let UneditCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.UnEdit, for: indexPath) as? UnEditingTableViewCell
               
               UneditCell?.setUpCellWithData(label: data.display_name, data: data.data!,type:data.dataType)
               
               return UneditCell!
            }
            
         case .date,.dateTime,.dateTimePast,.dateTimeFuture,.dateFuture,.datePast:
            
            let dateCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.dateCustomField, for: indexPath) as? DateCustomTableViewCell
            dateCell?.setUpForCustomField(type:data.dataType , textValueInCaseOfStatic: data.data!, isForPickUp: false, isStatic: false, dataForNonStatic: data, header: data.display_name)
            return dateCell!
            
         case .checkBox:
            
            let checkBoxCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.custCheckBox, for: indexPath) as? CheckBoxTableViewCell
            checkBoxCell?.setUpCheckBoxCell(data: data, header: data.display_name)
            return checkBoxCell!
            
         case .phonenumber:
            
            let phoneCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.phoneCode, for: indexPath) as? PhoneNumberTableViewCell
            phoneCell?.setUpPhoneNumberCell(fortype: .phonenumber, headerName: data.display_name, isStatic: false, isPickUp: true, dataForNonStatic: data, data:data.data!)
            return phoneCell!
            
         case .DropDown:
            
            let dropDownCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.DropDownCell, for: indexPath) as? DropDownTableViewCell
            dropDownCell?.setUpDropDown(data: data )
            return dropDownCell!
            
         case .image:
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.ImageCustom) as! ImagesCustomTableViewCell
            cell.imageCollectionView.tag = indexPath.row
            cell.selectionStyle = .none
            let customField = data
            let images = customField.data?.jsonObjectArray
            cell.setImageCollectionView(label: customField.display_name, imageArray: images!)
            
            cell.onAddbuttonTap = { [weak self] (action) in
               self?.selectImageFor(customField: customField, tableView: tableView)
            }
            
            cell.imageTappedAtIndex = { [weak self] (index) in
               self?.imageTappedIn(tableView: tableView, atIndex: index, forCustomField: customField)
            }
            cell.selfHeaderType = creatTaskManager.headerArray[indexPath.section]
            
            if customField.app_side == .readOnly  || customField.app_side == .hidden  {
               cell.isForEditing = false
            }else{
               cell.isForEditing = true
            }
            return cell
            
         default:
            print("")
            break
         }
      }else if creatTaskManager.headerArray[indexPath.section] == .Cart {
         
         
         guard !isSubtotalCellInCartSection(row: indexPath.row)  else {
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.checkoutSubtotalCell, for: indexPath) as! SubtotalTableViewCell
            cell.selectionStyle = .none
            let price = Singleton.sharedInstance.formDetailsInfo.currencyid + Singleton.sharedInstance.subTotal.roundTo(places: 2).description
            cell.setCellWith(title: TEXT.subtotal, price: price)
            return cell
         }
         
         let product = creatTaskManager.selectedProducts[indexPath.row]
         
         let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.checkoutCartCell, for: indexPath) as! CartCell
         cell.selectionStyle = .none
         cell.setCellWith(product: product)
         cell.delegate = self
         cell.tag = indexPath.generateTag()
         
         if isLastCartElement(row: indexPath.row) {
            cell.setLeftRightInsetsOfseperator(inset: 23)
            cell.seperatorView.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.3)
         } else {
            cell.setLeftRightInsetsOfseperator(inset: 60)
            cell.seperatorView.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.1)
         }
         
         return cell
      }
      
      
      return UITableViewCell()
      
   }
   
   
   
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
      switch  creatTaskManager.headerArray[indexPath.section] {
      case .AppointmentDetails,.DeliveryDetails,.PickUpDetails:
         let creatTaskStaticData = creatTaskManager.getStaticFieldData(headerType: creatTaskManager.headerArray[indexPath.section], index: indexPath.row)
         
         if !creatTaskStaticData.type.isDataTypeHandled() {
            return 0.001
         }
         
         return UITableViewAutomaticDimension
      case .Description:
         return 95
      case .customDeliveryDetails,.pickUpCustomeDetails:
         let data = creatTaskManager.headerArray[indexPath.section] == .pickUpCustomeDetails ?  creatTaskManager.customPickUpCells[indexPath.row] as! CustomFieldDetails : creatTaskManager.customDeliveryCells[indexPath.row] as! CustomFieldDetails
         
         if data.app_side == .hidden || data.label! == "Task_Details" || data.label! == "task_details" {
            return 0.001
         }else if data.app_side == .readOnly || data.dataType == .image{
            return UITableViewAutomaticDimension
         }else if !data.dataType.isDataTypeHandled() {
            return 0.001
         }
         return 57
      case .Cart:
         return UITableViewAutomaticDimension
         
      }
   }
   
   func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      switch creatTaskManager.headerArray[indexPath.section] {
      case .AppointmentDetails,.DeliveryDetails,.PickUpDetails:
         return 57
      case .Description:
         return 95
      case .Cart:
         return 85
      case .customDeliveryDetails,.pickUpCustomeDetails:
         return 57
      }
   }
   
   func reloadSection(type:headerType,section:Int){
      DispatchQueue.main.async {[weak self] in
         switch type {
         case .AppointmentDetails,.PickUpDetails,.DeliveryDetails:
            self?.tableView.reloadSections(IndexSet(integersIn: section...section+1), with: .automatic)
         default:
            self?.tableView.reloadSections(IndexSet.init(integer: section), with: .automatic)
         }
         
         if type == self?.creatTaskManager.headerArray[(self?.creatTaskManager.headerArray.count)! - 1] && (self?.creatTaskManager.collapsableArray.contains(type))! == true{
            
            let numberOfSections = self?.tableView.numberOfSections
            let numberOfRows = self?.tableView.numberOfRows(inSection: numberOfSections!-1)
            
            let indexPath = IndexPath(row: numberOfRows!-1 , section: numberOfSections!-1)
            self?.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
         }
      }
      
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
      let details = creatTaskManager.getHeading(creatTaskManager.headerArray[section])
      
      switch creatTaskManager.headerArray[section] {
         
      case .PickUpDetails,.AppointmentDetails,.Description,.DeliveryDetails,.Cart:
         
         
         let checkOutView = CheckoutHeaderView.getHeaderViewWith(headerType: creatTaskManager.headerArray[section] , headerImage: details.HeaderImage, HeaderLabelString: details.headerTitle, showLoader: false, isAdded: details.isAdded, leftButtonAction: { [weak self] (void) in
            self?.creatTaskManager.changeColapsableaState(forType: (self?.creatTaskManager.headerArray[section])!)
            self?.reloadSection(type: (self?.creatTaskManager.headerArray[section])!, section: section)
            }, rightButtonAction: { [weak self] (void) in
               self?.creatTaskManager.resetAndRemoveData(forType: (self?.creatTaskManager.headerArray[section])!, isRemove: true)
               self?.reloadSection(type: (self?.creatTaskManager.headerArray[section])!, section: section)
            }, AddAction: {  [weak self] (void) in
               self?.creatTaskManager.changeColapsableaState(forType: (self?.creatTaskManager.headerArray[section])!)
               self?.reloadSection(type: (self?.creatTaskManager.headerArray[section])!, section: section)
         })
         
         
         
         if creatTaskManager.headerArray[section] == .Cart {
            cartSectionHeader = checkOutView
            checkOutView.setCount(count: (creatTaskManager.cart?.getCartCount())!)
            checkOutView.rightButtonAction = {[weak self] (action) in
               //                  Singleton.sharedInstance.showAlertWithOption(owner: self!, title: "", message: TEXT.clearCartAlert, showRight: true, leftButtonAction: {
               //                     self?.creatTaskManager.cart?.clearCart()
               //                    self?.creatTaskManager.clearData()
               //                   let _ = self?.navigationController?.popViewController(animated: true)
               
               //                  }, rightButtonAction: nil, leftButtonTitle: TEXT.YES_ACTION, rightButtonTitle: TEXT.NO)
               
               self?.creatTaskManager.changeColapsableaState(forType: (self?.creatTaskManager.headerArray[section])!)
               self?.reloadSection(type: (self?.creatTaskManager.headerArray[section])!, section: section)
               
               
               
            }
            checkOutView.rightButton.setTitle( "\(Singleton.sharedInstance.formDetailsInfo.currencyid) \(Singleton.sharedInstance.subTotal)", for: UIControlState.normal)
            checkOutView.headerImage.tintColor = COLOR.SPLASH_TEXT_COLOR
            checkOutView.headerImage.image = checkOutView.headerImage.image?.renderWithAlwaysTemplateMode()
            checkOutView.rightButton.isHidden =  creatTaskManager.collapsableArray.contains(.Cart)
         }
         
         if [headerType.AppointmentDetails,.DeliveryDetails,.PickUpDetails].contains(creatTaskManager.headerArray[section] ) == true{
            checkOutView.rightButton.isHidden = creatTaskManager.toShowRightButton()
         }
         
         return checkOutView
         
         
      default:
         
         return UIView()
      }
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      switch creatTaskManager.headerArray[section] {
      case .AppointmentDetails,.DeliveryDetails,.PickUpDetails,.Description,.Cart:
         return 50
      default:
         return 0.001
      }
   }
   
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      return 0.001
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let cell = tableView.cellForRow(at: indexPath)
      if let _ = cell as? AddressCustomFieldTableViewCell {
         let vc = UIViewController.findIn(storyboard: .favLocation, withIdentifier: STORYBOARD_ID.addLocation) as? AddNewLocationViewController
         vc?.completionHandler = {(coordinate,address) in
            (cell as! AddressCustomFieldTableViewCell).assignAddress(address: address, coordinate: coordinate)
            tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
         }
         self.navigationController?.pushViewController(vc!, animated: true)
         
      } else if let _ = cell as? CheckBoxTableViewCell{
         (cell as! CheckBoxTableViewCell).updateStatus()
      } else if let _ = cell as? UnEditingTableViewCell{
         print((cell as! UnEditingTableViewCell).dataValue.text!)
         if (cell as! UnEditingTableViewCell).selfTypes == .url {
            self.open(url: (cell as! UnEditingTableViewCell).dataValue.text!)
         }
      }
   }
   
   func registerNib(){
      tableView.register(UINib(nibName: NIB_NAME.TaskDescriptionCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.taskDescription)
      tableView.register(UINib(nibName: NIB_NAME.CustomFTextField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.customTextField)
      tableView.register(UINib(nibName: NIB_NAME.AddressCustomeField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.AddressCustomeField)
      tableView.register(UINib(nibName: NIB_NAME.phoneNumberCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.phoneCode)
      tableView.register(UINib(nibName: NIB_NAME.DateCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.dateCustomField)
      tableView.register(UINib(nibName: NIB_NAME.CheckBoxCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.custCheckBox)
      tableView.register(UINib(nibName: NIB_NAME.DropDownCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.DropDownCell)
      tableView.register(UINib(nibName: NIB_NAME.TextFieldForUnEditing, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.UnEdit)
      tableView.registerCellWith(nibName: NIB_NAME.checkoutCartCell, reuseIdentifier: CELL_IDENTIFIER.checkoutCartCell)
      tableView.registerCellWith(nibName: NIB_NAME.checkoutSubtotalCell, reuseIdentifier: CELL_IDENTIFIER.checkoutSubtotalCell)
      self.tableView.register(UINib.init(nibName: NIB_NAME.ImgesCustomField, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.ImageCustom)
      
   }
   
   
   func setNavigationBar() {
      saveAndReviewbutton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
      
      if shouldShowSelectAgentScreen() {
         let buttonTitle = TEXT.select + " " + Singleton.sharedInstance.formDetailsInfo.callServiceProvidersAs
         saveAndReviewbutton.setTitle(buttonTitle, for: UIControlState.normal)
      }else if Singleton.sharedInstance.formDetailsInfo.toShowPayment == false  {
         saveAndReviewbutton.setTitle(TEXT.SUBMIT, for: UIControlState.normal)
      }else{
         saveAndReviewbutton.setTitle(TEXT.proceedToPay, for: UIControlState.normal)
      }
      
      saveAndReviewbutton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
      saveAndReviewbutton.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
      
      let screenTitle = Singleton.sharedInstance.formDetailsInfo.isNLevel ? TEXT.checkout : TEXT.DETAILS
      navigationBar = NavigationView.getNibFile(params:NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: screenTitle, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil) , leftButtonAction: {[weak self] in
         self?.creatTaskManager.clearData()
         _ = self?.navigationController?.popViewController(animated: true)
         
         }, rightButtonAction: nil)
      self.view.addSubview(navigationBar)
      
      
      if self.showBackButton == nil   {
         self.showBackButton = true
      }
      
      navigationBar.backButton.isHidden = !self.showBackButton!
      
   }
   
   
   @IBAction func saveButtonAction(_ sender: UIButton) {
      
      guard creatTaskManager.saveAndReviewButtonTapped() else {
         return
      }
      
      let formDetails = Singleton.sharedInstance.formDetailsInfo
      
      if formDetails.isNLevel && formDetails.showServiceProvider {
         
         let location = creatTaskManager.getSelectedLocation()
         let tags = formDetails.nLevelController!.getCommaSeperatedTagsOfSelectedProducts()
         
         SelectAgentViewController.pushIn(navVC: self.navigationController!, withTags: tags, formId: formDetails.form_id!, latitude: location.latitude, longitude: location.longitude) { [weak self] in
            self?.checkInternetAndHitCreateTaskAPI()
         }
         return
      }
      
      
      checkInternetAndHitCreateTaskAPI()
   }
   
   fileprivate func checkInternetAndHitCreateTaskAPI() {
      if IJReachability.isConnectedToNetwork() == true    {
         self.creatTaskManager.apiHitToCreatTask(owner: self)
      }else{
         ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isErrorMessage: true, removed: nil)
      }
   }
   
   fileprivate func shouldShowSelectAgentScreen() -> Bool {
      let formDetails = Singleton.sharedInstance.formDetailsInfo
      return formDetails.isNLevel && formDetails.showServiceProvider
   }
   
   fileprivate func isLastCartElement(row: Int) -> Bool {
      return row == (creatTaskManager.selectedProducts.count - 1)
   }
   
   fileprivate func reloadCartSection(animated: Bool) {
      let cartSection = creatTaskManager.headerArray.index(of: .Cart) ?? 0
      
      let indexSet = IndexSet.init(integer: cartSection)
      tableView.reloadSections(indexSet, with: animated ? .automatic : .none)
   }
   
   fileprivate func updatePricingInCartSection(row: Int) {
      let rowIndexPath = getIndexPathOfCartSection(row: row)
      
      let subtotalRow = getSubtotalRowIndex()
      let subtotalRowIndexPath = getIndexPathOfCartSection(row: subtotalRow)
      
      let rowsToReload = [subtotalRowIndexPath, rowIndexPath]
      
      tableView.reloadRows(at: rowsToReload, with: .none)
      updateCartCountInHeader()
   }
   
   fileprivate func getIndexPathOfCartSection(row: Int) -> IndexPath {
      let cartSection = getCartSection()
      return IndexPath(row: row, section: cartSection)
   }
   
   fileprivate func getSubtotalRowIndex() -> Int {
      return creatTaskManager.selectedProducts.count
   }
   
   fileprivate func getCartSection() -> Int {
      return creatTaskManager.headerArray.index(of: .Cart) ?? 0
   }
   
   fileprivate func updateCartCountInHeader() {
      let cartCount = creatTaskManager.cart?.getCartCount() ?? 0
      cartSectionHeader?.setCount(count: cartCount)
   }
   
   fileprivate func numberOfSelectedProductsChanged() {
      if creatTaskManager.selectedProducts.count == 0 {
         creatTaskManager.clearData()
         _ = self.navigationController?.popViewController(animated: true)
         
      }
   }
   
   fileprivate func isSubtotalCellInCartSection(row: Int) -> Bool {
      return row == creatTaskManager.selectedProducts.count
   }
   
   deinit {
      print("Checkout screen deintialized")
   }
   
   func open(url:String){
      
      if let _ = URL(string: url){
         
         if UIApplication.shared.canOpenURL(URL(string: url)!) {
            // UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            UIApplication.shared.openURL(URL(string: url)!)
            
         }else{
            ErrorView.showWith(message: "Invalid Url .", isErrorMessage: true, removed: nil)
         }
      }else{
         ErrorView.showWith(message: "Invalid Url .", isErrorMessage: true, removed: nil)
      }
      
   }
   var selectedImageSection : headerType?
}

// MARK: - Image Selection
extension CheckoutViewController {
   
    func addButtonTappedIn(tableView: UITableView, forCustomField customField: CustomFieldDetails) {
      if customField.checkIfImageIsBeingUploaded() == false{
         selectImageFor(customField: customField, tableView: tableView)
      }else{
         ErrorView.showWith(message: ERROR_MESSAGE.IMAGE_UPLOADING, isErrorMessage: true, removed: nil)
      }
   }
   
    func selectImageFor(customField: CustomFieldDetails, tableView: UITableView) {
      imagePickerManager = MediaSelector { result in
         guard result.isSuccessful else {
            ErrorView.showWith(message: result.error!.localizedDescription, removed: nil)
            return
         }
         
         customField.startImageUploadingProcessForImagePath(path: result.filePath!)
         tableView.reloadData()
         let indexAtWhichImageIsBeingUploaded = (customField.getImageArrayCount() - 1)
         CustomFieldDetails.uploadImageWith(path: result.filePath!) { (success, referencePath) in
            guard success else {
               customField.deleteImageAt(index: indexAtWhichImageIsBeingUploaded)
               tableView.reloadData()
               return
            }
            customField.imageUpadationSuccessfulAt(index: indexAtWhichImageIsBeingUploaded, withPath: referencePath!)
            tableView.reloadData()
         }
      }
      
      openImagePickerAccordingToAttribute(attribute: customField.attribute)
   }
   
    func openImagePickerAccordingToAttribute(attribute: ImageSource) {
      let imageFileName = Singleton.sharedInstance.convertDateToString()
      let vc = UIApplication.shared.keyWindow!.rootViewController!
      
      switch attribute {
      case .camera:
         imagePickerManager?.openCameraFor(fileName: imageFileName, fileType: .image, inViewController: vc)
      case .photoLibrary:
         imagePickerManager?.openPhotoLibraryFor(fileName: imageFileName, fileType: .image, inViewController: vc)
      case .cameraAndPhotoLibrary:
         imagePickerManager?.selectImage(viewController: vc, fileName: imageFileName, fileType: .image)
      }
   }
   
    func imageTappedIn(tableView: UITableView, atIndex index: Int, forCustomField customField: CustomFieldDetails) {
      
      guard !customField.checkIfImageIsBeingUploaded() else {
         ErrorView.showWith(message: ERROR_MESSAGE.IMAGE_UPLOADING, removed: nil)
         return
      }
      let arrayOfImagePath = (customField.data?.jsonObjectArray as? [String]) ?? [""]
      let imagePreview = ImagesPreview.loadForImagePaths(arrayOfPath: arrayOfImagePath, whilePreviewingImageAtIndex: index)
      imagePreview.deleteAtIndex = { (index) in
         customField.deleteImageAt(index: index)
         imagePreview.confirmationDoneForDeletion()
         tableView.reloadData()
      }
      imagePreview.viewWillBeRemoved = {}
   }
}

extension CheckoutViewController : locationManagerCustomDelegate {
   
   
   func didUpdateLocation(withCoordinates: CLLocationCoordinate2D) {
      locationManager.reverseGeocodeCoordinate(coordinate: withCoordinates) {[weak self] (address) in
         DispatchQueue.main.async {
            
            
            if self?.creatTaskManager.headerArray.contains(.AppointmentDetails) == true || (self?.creatTaskManager.headerArray.contains(.DeliveryDetails) == true && self?.creatTaskManager.headerArray.contains(.PickUpDetails) == false){
               
               if address != TEXT.goToPinText{
                  Singleton.sharedInstance.createTaskDetail.customerAddress = address.trimText
                  Singleton.sharedInstance.createTaskDetail.latitude = "\(withCoordinates.latitude)"
                  Singleton.sharedInstance.createTaskDetail.longitude = "\(withCoordinates.longitude)"
               }else{
                  Singleton.sharedInstance.createTaskDetail.customerAddress = TEXT.ADD_PICKUP_LOCATION
               }
               
            }else{
               
               if address != TEXT.goToPinText{
                  Singleton.sharedInstance.createTaskDetail.jobPickupAddress = address.trimText
                  Singleton.sharedInstance.createTaskDetail.jobPickupLatitude = "\(withCoordinates.latitude)"
                  Singleton.sharedInstance.createTaskDetail.jobPickupLongitude = "\(withCoordinates.longitude)"
               }else{
                  Singleton.sharedInstance.createTaskDetail.jobPickupAddress = TEXT.ADD_PICKUP_LOCATION
                  
               }
            }
            self?.tableView.reloadData()
         }
      }
      locationManager.stopUpdatingLocation()
   }
   
}

extension CheckoutViewController: CartCellDelegate {
   func addButtonPressedForCellWith(tag: Int) {
      let row = tag.getRowAndSection().row
      let element = creatTaskManager.selectedProducts[row]
      
      creatTaskManager.cart?.quantityAddedOf(element: element)
      updatePricingInCartSection(row: row)
   }
   
   func subtractButtonPressedForCellWith(tag: Int) {
      let row = tag.getRowAndSection().row
      let element = creatTaskManager.selectedProducts[row]
      
      creatTaskManager.cart?.quantitySubtractedOf(element: element)
      
      if isElementRemovedFromCart(element: element) {
         creatTaskManager.selectedProducts.remove(at: row)
         reloadCartSection(animated: false)
         
      } else {
         updatePricingInCartSection(row: row)
      }
      numberOfSelectedProductsChanged()
   }
   
   fileprivate func isElementRemovedFromCart(element: LevelElement) -> Bool {
      let quantity = creatTaskManager.cart?.getQuantityOfProductWith(id: element.id) ?? 0
      return quantity == 0
   }
   
   
}


extension UITableView {
   func scrollToBottom(animated: Bool) {
      let y = contentSize.height - frame.size.height
      setContentOffset(CGPoint(x: 0, y: (y<0) ? 0 : y), animated: animated)
   }
}

