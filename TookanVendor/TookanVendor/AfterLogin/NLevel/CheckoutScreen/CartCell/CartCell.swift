//
//  CartCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 03/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

protocol CartCellDelegate: class {
   func addButtonPressedForCellWith(tag: Int)
   func subtractButtonPressedForCellWith(tag: Int)
}

class CartCell: UITableViewCell {

   //MARK: - Properties
   weak var delegate: CartCellDelegate?
   
    //MARK: - IBOUTLETS
   @IBOutlet weak var shadowView: UIView!
   @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var subtractButton: UIButton!
    @IBOutlet weak var adddButton: UIButton!
    @IBOutlet weak var mulltiplyIcon: UILabel!
    @IBOutlet weak var oneQuantityCost: UILabel!
    @IBOutlet weak var totalCost: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
   @IBOutlet weak var seperatorView: UIView!
   @IBOutlet weak var leftDistanceOfSeperatorView: NSLayoutConstraint!
   @IBOutlet weak var rightDistanceOfSeperatorView: NSLayoutConstraint!
    
    var index = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        configureViews()
    }
   
   private func configureViews() {
      productName.font = UIFont(name: FONT.regular , size: FONT_SIZE.priceFontSize)
      productName.textColor = COLOR.SPLASH_TEXT_COLOR
      
      adddButton.tintColor = COLOR.THEME_FOREGROUND_COLOR
      adddButton.setImage(#imageLiteral(resourceName: "plus").renderWithAlwaysTemplateMode(), for: .normal)
      
      mulltiplyIcon.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
      mulltiplyIcon.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
      
      oneQuantityCost.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
      oneQuantityCost.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
      
      totalCost.font = UIFont(name: FONT.semiBold, size: FONT_SIZE.buttonTitle)
      totalCost.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
      
      seperatorView.backgroundColor = UIColor.black.withAlphaComponent(0.08)
      setShadowView()
      shadowView.layer.cornerRadius = 16
   }
   
   func setCellWith(product: Product) {
      productName.text = product.name
      oneQuantityCost.text = Singleton.sharedInstance.formDetailsInfo.currencyid + product.price.roundTo(places: 2).description
      quantityLabel.text = product.quantity.description
      
      totalCost.text = Singleton.sharedInstance.formDetailsInfo.currencyid + product.getPrice().roundTo(places: 2).description
      setViewFor(actionType: product.actionType)
   }
   
   func setLeftRightInsetsOfseperator(inset: CGFloat) {
      leftDistanceOfSeperatorView.constant = inset
      rightDistanceOfSeperatorView.constant = inset
   }
   
   private func setViewFor(actionType: ActionType) {
      switch actionType {
      case .singleAddition:
         adddButton.isHidden = true
      default:
         adddButton.isHidden = false

      }
   }
   
   // MARK: - IBAction
   @IBAction func addButtonPressed(_ sender: Any) {
      delegate?.addButtonPressedForCellWith(tag: tag)
   }
   
   @IBAction func subtractButtonPressed(_ sender: Any) {
      delegate?.subtractButtonPressedForCellWith(tag: tag)
   }
   
   func setShadowView() {
      shadowView.layer.shadowOffset = CGSize(width: 0, height: 5)
      shadowView.layer.shadowOpacity = 0.1
      shadowView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
      shadowView.layer.shadowRadius = 5
   }

}
