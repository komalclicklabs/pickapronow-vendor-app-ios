//
//  SubtotalTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 11/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class SubtotalTableViewCell: UITableViewCell {
   
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var priceLabel: UILabel!
   @IBOutlet weak var colonLabel: UILabel!
   

    override func awakeFromNib() {
        super.awakeFromNib()
      
   configureUI()
    }
   
   private func configureUI() {
      titleLabel.font = UIFont(name: FONT.regular, size: 17)
      titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
      
      colonLabel.font = UIFont(name: FONT.regular, size: 17)
      colonLabel.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
      
      priceLabel.font = UIFont(name: FONT.semiBold, size: 17)
      priceLabel.textColor = COLOR.THEME_FOREGROUND_COLOR
       
   }
   
   func setCellWith(title: String, price: String) {
      titleLabel.text = title
      priceLabel.text = price
   }


}
