//
//  UnEditingTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 10/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class UnEditingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var dataValue: UILabel!

    
    var selfTypes:CustomFieldTypes?
    
    override func awakeFromNib() {
        super.awakeFromNib()
      setUpUiAttributes()

        // Initialization code
    }

    
    func setUpUiAttributes(){
        dataValue.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        label.textColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    
    func setUpCellWithData(label:String,data:String,type:CustomFieldTypes){
        self.selfTypes = type
        self.label.text = label
        self.dataValue.text = data
        if type == .url {
            self.dataValue.textColor = UIColor.blue
        }else{
            self.dataValue.textColor = COLOR.SPLASH_TEXT_COLOR
        }
    }
   
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted == true{
            if selfTypes == .url{
            self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            }else{
                self.backgroundColor = UIColor.white
            }
        }else{
            self.backgroundColor = UIColor.white
        }
    }

}
