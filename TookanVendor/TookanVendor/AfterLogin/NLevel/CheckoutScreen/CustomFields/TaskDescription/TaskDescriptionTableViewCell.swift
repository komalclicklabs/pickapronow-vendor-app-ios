//
//  TaskDescriptionTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 06/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class TaskDescriptionTableViewCell: UITableViewCell,UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    
    var stringToBeStored = String()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.delegate = self
        // Initialization code
    }
    
    
    func setUpWith(stringToBeReturnes:inout String){
        setUpUiAttributes()
        stringToBeStored = stringToBeReturnes
        textView.text = stringToBeReturnes
        if textView.text == ""{
         textView.text = "Enter Description"
         textView.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
        }
    }
    
    func setUpUiAttributes(){
        textView.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
    }
   
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter Description"{
         textView.text = ""
         textView.textColor = COLOR.SPLASH_TEXT_COLOR
        }else{
            textView.textColor = COLOR.SPLASH_TEXT_COLOR
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = "Enter Description"
             textView.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
        }else{
             textView.textColor = COLOR.SPLASH_TEXT_COLOR
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var textToSend = String()
        if text != ""{
            textToSend = textView.text! + text
        }else{
            textToSend = textView.text!
            if textView.text != ""{
            textToSend.remove(at: textToSend.index(before:  textToSend.endIndex))
            }
        }
        Singleton.sharedInstance.createTaskDetail.jobDescription = textToSend
        
        return true
   }

}
