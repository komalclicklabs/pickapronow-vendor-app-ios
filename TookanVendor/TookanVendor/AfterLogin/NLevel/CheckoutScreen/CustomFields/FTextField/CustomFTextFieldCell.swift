//
//  CustomFTextFieldCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 06/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class CustomFTextFieldCell: UITableViewCell,UITextFieldDelegate {
   
   @IBOutlet weak var textField: UITextField!
   @IBOutlet weak var label: UILabel!
   
   var selfType: CustomFieldTypes?
   var isStatic: Bool?
   var isForPickUp: Bool?
   var customField: CustomFieldDetails?
   
   override func awakeFromNib() {
      super.awakeFromNib()
      textField.delegate = self
   }
   
   func setUiAttributes(placeHolderText:String)  {
      textField.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
      label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
      label.textColor = UIColor.black.withAlphaComponent(0.6)
   }
   
   func setUpForCustomField(type:CustomFieldTypes,textValueInCaseOfStatic:String = "",isForPickUp:Bool = false,isStatic:Bool = false,dataForNonStatic:CustomFieldDetails?,header:String) {
      
      setUiAttributes(placeHolderText: header)
      self.label.text = header
      self.textField.placeholder = "Enter " + header
      selfType = type
      self.isStatic = isStatic
      self.isForPickUp = isForPickUp
      setTextField(type: type)
      self.customField = dataForNonStatic
      isStatic == true ? (self.textField.text = textValueInCaseOfStatic) : (self.textField.text = dataForNonStatic?.data)
      
      self.textField.isUserInteractionEnabled = dataForNonStatic?.app_side.isEditable ?? true
   }
   
   
   
   func setTextField(type:CustomFieldTypes){
      switch type {
      case .email,.url:
         setForEmail()
         break
      case .name,.Text:
         setForName()
         break
      case .Number:
         setUpForNumber()
         break
      default:
         setForName()
      }
   }
   
   
   func setUpForNumber(){
      self.textField.keyboardType = .numberPad
      self.textField.autocapitalizationType = UITextAutocapitalizationType.none
   }
   
   func setForEmail(){
      self.textField.keyboardType = .emailAddress
      self.textField.autocapitalizationType = UITextAutocapitalizationType.none
   }
   
   func setForName(){
      self.textField.keyboardType = .alphabet
      self.textField.autocapitalizationType = UITextAutocapitalizationType.words
   }
   
   func updateForStatic(text:String){
      
      switch self.selfType! {
         
      case .email:
         
         isForPickUp! == true ? updateText(stringToStore: &Singleton.sharedInstance.createTaskDetail.jobPickupEmail, stringToBeStored: text) : updateText(stringToStore: &Singleton.sharedInstance.createTaskDetail.customerEmail, stringToBeStored: text)
         
      case .name:
         
         isForPickUp! == true ? updateText(stringToStore: &Singleton.sharedInstance.createTaskDetail.jobPickupName, stringToBeStored: text) : updateText(stringToStore: &Singleton.sharedInstance.createTaskDetail.customerUsername, stringToBeStored: text)
         
      default:
         print("")
      }
   }
   
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      if (self.selfType == CustomFieldTypes.email || self.selfType == CustomFieldTypes.Number || self.selfType == CustomFieldTypes.url)  && string == " "{
         return false
      }
      
      let oldString = textField.text! as NSString
      let textToSend = oldString.replacingCharacters(in: range, with: string)
      
      if isStatic == true{
         updateForStatic(text: textToSend)
      }else{
         self.customField?.data = textToSend
      }
      return true
   }
   
   
   func updateText(stringToStore: inout String,stringToBeStored:String){
      stringToStore = stringToBeStored
   }
   
}
