//
//  AddressCustomFieldTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 07/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation

class AddressCustomFieldTableViewCell: UITableViewCell {

    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var address: UILabel!
    
    var selfType : CustomFieldTypes?
    var completionHandler : (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpUiAttributes()
        // Initialization code
    }
    
    
    func setUpUiAttributes(){
        address.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        label.textColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    
    func setUpCell(type:CustomFieldTypes,textValueInCaseOfStatic:String,header:String){
         textValueInCaseOfStatic == "" ? (address.text = "Add Address") : (address.text = textValueInCaseOfStatic)
        label.text = header
        self.selfType = type
    }

    
    func assignAddress(address : String,coordinate:CLLocationCoordinate2D) {
        switch selfType! {
        case .pickUpAddress:
            Singleton.sharedInstance.createTaskDetail.jobPickupAddress = address
            Singleton.sharedInstance.createTaskDetail.jobPickupLatitude = "\(coordinate.latitude)"
            Singleton.sharedInstance.createTaskDetail.jobPickupLongitude = "\(coordinate.longitude)"
            
        default:
            Singleton.sharedInstance.createTaskDetail.customerAddress = address
            Singleton.sharedInstance.createTaskDetail.latitude = "\(coordinate.latitude)"
            Singleton.sharedInstance.createTaskDetail.longitude = "\(coordinate.longitude)"
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    

}
