//
//  DropDownTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 08/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class DropDownTableViewCell: UITableViewCell ,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    
    var optionsArray = [String]()
    var data : CustomFieldDetails?
    var picker = UIPickerView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUiAttributes()
        picker.delegate = self
        textField.delegate = self
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUiAttributes()  {
        textField.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        label.textColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    func setUpDropDown(data:CustomFieldDetails){
        optionsArray = (data.input?.components(separatedBy: ","))!
        self.label.text = data.display_name
        self.textField.placeholder = TEXT.select + " " + data.display_name
         self.data = data
        if (data.data?.contains(","))! == false{
       
            self.textField.text = data.data
        }else{
         self.textField.text = data.app_side == .readOnly ? "-" : ""
        }
        self.textField.inputView = picker
      
      self.textField.isUserInteractionEnabled = data.app_side == .readWrite
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.textField.text = optionsArray[row]
        self.data?.data = optionsArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return optionsArray[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return optionsArray.count
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == ""{
        textField.text = optionsArray[0]
         self.data?.data = optionsArray[0]
        }
    }
    
    
    
    
    

}
