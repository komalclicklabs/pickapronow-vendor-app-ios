//
//  Level.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

typealias Level = [LevelElement]

enum ActionType: Int {
   case next = 3
   case singleAddition = 1
   case multipleAddition = 2
   case nextButButtonHidden = 4
   
   init(rawLayout: [String: Any]) {
      guard let rawButtons = rawLayout["buttons"] as? [[String: Any]],
         rawButtons.count > 0,
         let rawType = rawButtons.first?["type"] as? Int,
         let type = ActionType.init(rawValue: rawType) else {
            self = ActionType.singleAddition
            return
      }
      self = type
   }
}



class LevelElement: NSObject, NSCoding {
   // MARK: - Properties
   private(set) var id = ""
   private(set) var parentId: String?
   
   private(set) var name = ""
   private(set) var lines = Lines(rawLayout: [:])
   private(set) var imageUrl: URL?
   
   private(set) var isDummy = false
   private(set) var parentIndexInPreviousLevel = 0
   private(set) var level = 0
   private(set) var childLayoutType = LevelLayout.list
   private(set) var layoutType = LevelLayout.list 
   
   var actionType = ActionType.nextButButtonHidden
   
   // MARK: - Intializer
   init?(json: [String: Any], level: Int) {
      super.init()
      
      guard let id = findIdIn(json: json), !id.isEmpty else {
         print("Catalogue/Product id not found")
         return nil
      }
      self.id = id
      
      self.name = (json["name"] as? String) ?? ""
      self.isDummy = (json["is_dummy"] as? Bool) ?? false
      self.parentIndexInPreviousLevel = (json["parent_index"] as? Int) ?? 0
      self.level = level
      
      if let tempParentId = json["parent_category_id"] {
         self.parentId = "\(tempParentId)"
      }
      
      if let rawLayout = json["layout_data"] as? [String: Any] {
         self.lines = Lines(rawLayout: rawLayout)
         self.actionType = ActionType(rawLayout: rawLayout)
         
         if let rawImagesArray = rawLayout["images"] as? [[String: Any]],
            rawImagesArray.count > 0,
            let imageUrlString = rawImagesArray.first?["data"] as? String
            {
            self.imageUrl = URL.get(from: imageUrlString)
         }
      }
      
      if let currentLayOutTypeRawValue = json["layout_type"] as? Int,
         let currentLayoutType = LevelLayout(rawValue: currentLayOutTypeRawValue) {
         self.layoutType = currentLayoutType
      }
      
      if let childLayoutTypeRawValue = json["child_layout_type"] as? Int, let childLayoutType =  LevelLayout(rawValue: childLayoutTypeRawValue) {
         self.childLayoutType = childLayoutType
      }
      
   }
   
   private func findIdIn(json: [String: Any]) -> String? {
      if let catlougeId = json["catalogue_id"] {
         return "\(catlougeId)"
      }
      
      if let productId = json["product_id"] {
         return "\(productId)"
      }
      
      return nil
   }
   
   // MARK: - NSCoding
   required init?(coder aDecoder: NSCoder) {
      guard let id = aDecoder.decodeObject(forKey: PropertyKey.id) as? String,
         let parentId = aDecoder.decodeObject(forKey: PropertyKey.parentId) as? String,
         let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            print("Unable to intialize LevelElement")
            return nil
      }
      
      self.id = id
      self.parentId = parentId
      self.name = name
      
      let actionTypeRawValue = aDecoder.decodeInteger(forKey: PropertyKey.actionType)
      let layoutTypeRawValue = aDecoder.decodeInteger(forKey: PropertyKey.layoutType)
      
      self.actionType = ActionType(rawValue: actionTypeRawValue)!
      self.layoutType = LevelLayout(rawValue: layoutTypeRawValue)!
      isDummy = aDecoder.decodeBool(forKey: PropertyKey.isDummy)
      parentIndexInPreviousLevel = aDecoder.decodeInteger(forKey: PropertyKey.parentIndexInPreviousLevel)
      self.level = aDecoder.decodeInteger(forKey: PropertyKey.level)
   }
   
   func encode(with aCoder: NSCoder) {
      aCoder.encode(id, forKey: PropertyKey.id)
      aCoder.encode(parentId, forKey: PropertyKey.parentId)
      aCoder.encode(name, forKey: PropertyKey.name)
      aCoder.encode(isDummy, forKey: PropertyKey.isDummy)
      aCoder.encode(parentIndexInPreviousLevel, forKey: PropertyKey.parentIndexInPreviousLevel)
      aCoder.encode(level, forKey: PropertyKey.level)
      aCoder.encode(actionType.rawValue, forKey: PropertyKey.actionType)
      aCoder.encode(layoutType.rawValue, forKey: PropertyKey.layoutType)
   }
   
   // MARK: - Types
//   extension LevelElement {
      struct PropertyKey {
         static let id = "id"
         static let parentId = "parentId"
         static let name = "name"
         static let isDummy = "isDummy"
         static let parentIndexInPreviousLevel = "parentIndexInPreviousLevel"
         static let level = "level"
         static let imageURL = "imageURL"
         static let actionType = "actionType"
         static let layoutType = "layoutType"
      }
//   }
   
   
   // MARK: - Type Methods
   static func getLevelFrom(json: [[String: Any]], levelIndex: Int) -> (level: Level, hashingDict: [Int: [Int]]) {
      var level = [LevelElement]()
      var hashingDict = [Int: [Int]]()
      
      for (index, rawLevel) in json.enumerated() {
         guard let element = LevelElement(json: rawLevel, level: levelIndex) else {
            continue
         }
         level.append(element)
         
         let parentIndex = element.parentIndexInPreviousLevel
            
            if var childIndexes = hashingDict[parentIndex] {
               childIndexes.append(index)
               hashingDict[parentIndex] = childIndexes
            } else {
               hashingDict[parentIndex] = [index]
            }
      }
      return (level, hashingDict)
   }

}


