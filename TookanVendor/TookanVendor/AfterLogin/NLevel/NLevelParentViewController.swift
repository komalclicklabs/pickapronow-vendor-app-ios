//
//  NLevelParentViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation

protocol NLevelFlowDelegate: class {
   func nextButtonPressed(forElement element: LevelElement, atIndexInChildArray index: Int)
   func addQuantityButtonPressed(forElement element: LevelElement)
   func subtractQuantityButtonPressed(forElement element: LevelElement)
    func popViewController()
    func checkoutButtonPressed()
   
   func getQuantityOfProductWith(id: String) -> Int
   func getChildrenOf(element: LevelElement, inCellWithTag tag: Int, completion: ((Bool, Level?) -> Void)?)
}

class NLevelParentViewController: UIViewController {

   // MARK: - Properties
   private var titleString = ""
   fileprivate var level: [LevelElement]!
   fileprivate var childrenLevelsOfLevel: [Level]? {
      didSet {
         cartButtonview.isHidden = !shouldCartBeVisible()
      }
   }
   var layoutType: LevelLayout!
   
   var cart: NLevelCart?
   var flowManager: NLevelNavigationHandler?
   
   var navBar: NavigationView?
   var childviewController: UIViewController?
   
   var isNavBarHidden = false
   var isBackButtonHidden = false
   private var neverShowCartButton = false
//   var address = ""
   var numberOfElementsInCart = 0 {
      didSet {
         numberOfElementsInCartChanged()
      }
   }
   var locationManager = CustomLocationManager.shared
    
   // MARK: - IBOutlets
   @IBOutlet weak var cartButton: UIButton!
   @IBOutlet weak var cartButtonview: UIView!
    @IBOutlet weak var cartPriceLabel: UILabel!
    @IBOutlet var addressOuterView: UIView!
//    @IBOutlet var addressTextView: UITextView!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var addressFieldHeight: NSLayoutConstraint!
    @IBOutlet var shadowView: UIView!
    @IBOutlet var locationImageView: UIButton!
    @IBOutlet var pencilImageView: UIButton!
    
   // MARK: - View Life Cycle
    override func viewDidLoad() {
      super.viewDidLoad()
      cartButtonview.backgroundColor = COLOR.THEME_FOREGROUND_COLOR

      cartButtonview.isHidden = !shouldCartBeVisible()
      cartButtonview.setShadow()
        if Singleton.sharedInstance.addressForNlevelFlow == "" {
            DispatchQueue.main.async {[weak self] in
                self?.locationManager.delegate = self
                self?.locationManager.startUpdatingLocation()
            }
        }
        
        self.setOuterAddressView()
      configureCartButton()
      configurePriceLabel()
      self.childviewController?.removeFromParentViewController()
      let vc = getViewControllerToSetAsAChild()
      setViewControllerAs(childViewController: vc, shouldSetFrame: layoutType.shouldSetFrame())
      
      view.bringSubview(toFront: cartButtonview)
      
      if !isNavBarHidden {
         setNavigationBar()
      }
    }
   
   func configureCartButton() {
      cartButton.configureNormalButtonWith(title: "")
      cartButton._cornerRadius = 0
      cartButton.setImage(#imageLiteral(resourceName: "cartHeading"), for: .normal)
      cartButton.tintColor = COLOR.SPLASH_BACKGROUND_COLOR
   }
   
    func setShadowView() {
        self.shadowView.setShadow()
        
        
        
    }
    
    func setAddressTextView() {
        self.addressLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.addressLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.addressLabel.text = Singleton.sharedInstance.addressForNlevelFlow
//        self.addressLabel.isUserInteractionEnabled = false
    }
    
    func setOuterAddressView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.addressTap))
        tap.numberOfTapsRequired = 1
        self.addressOuterView.addGestureRecognizer(tap)
        
        self.locationImageView.setImage( #imageLiteral(resourceName: "iconDropoff").renderWithAlwaysTemplateMode(), for: .normal)
        self.locationImageView.tintColor = UIColor.black
        self.locationImageView.isUserInteractionEnabled = false
        
        self.pencilImageView.setImage( #imageLiteral(resourceName: "iconEditRow").renderWithAlwaysTemplateMode(), for: .normal)
        self.pencilImageView.isUserInteractionEnabled = false
        self.setShadowView()
    }
    
    func setAddressPlaceholder() {
        self.addressLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.addressLabel.text = TEXT.GETTING_ADDRESS
        self.addressLabel.textColor = COLOR.PLACEHOLDER_COLOR
//        if self.addressLabel.text == "" {
//            self.addressLabel.isHidden = false
//        } else {
//            self.addressLabel.isHidden = true
//        }
    }
    
    func addressTap() {
//        self.addressTextView.resignFirstResponder()
        let vc: AddNewLocationViewController?
        vc = UIViewController.findIn(storyboard: .favLocation, withIdentifier: STORYBOARD_ID.addLocation) as? AddNewLocationViewController
        vc?.completionHandler = {(coordinate,address) in
            //            logEvent(label: "taxi_drop_location_added_payment")
            Singleton.sharedInstance.productLocation = coordinate
            Singleton.sharedInstance.addressForNlevelFlow = address
            if address == "" {
                self.addressLabel.textColor = COLOR.PLACEHOLDER_COLOR
                self.addressLabel.text = TEXT.GETTING_ADDRESS
            } else {
                self.addressLabel.textColor = COLOR.SPLASH_TEXT_COLOR
                if self.level.first is Product {
                    //                    self.productHit(location: coordinate)
                    self.appCatalogueHit(coordinate: coordinate, forProduct:true)
                } else {
                    self.appCatalogueHit(coordinate: coordinate, forProduct:false)
                }
                self.addressLabel.text = address
            }
            self.childviewController?.removeFromParentViewController()
            self.viewDidLayoutSubviews()
            
        }
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
   func configurePriceLabel() {
      cartPriceLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
      cartPriceLabel.textColor = COLOR.LOGIN_BUTTON_TITLE_COLOR
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
    
    numberOfElementsInCart = cart?.getCartCount() ?? 0
    UIApplication.shared.statusBarStyle = .default
    CustomLocationManager.shared.updateCoordinates(forced: false)
    if Singleton.sharedInstance.addressForNlevelFlow != "" {
        self.addressLabel.text = Singleton.sharedInstance.addressForNlevelFlow
        self.addressLabel.textColor = COLOR.SPLASH_TEXT_COLOR
    }
    
    if Singleton.sharedInstance.addressForNlevelFlow == "" {
        self.setAddressPlaceholder()
    } else {
        self.setAddressTextView()
    }
    
    guard level.first is Product else {
        if self.level.first?.level == 0 {
            self.getLevelElement()
        } else {
            productHit(location: Singleton.sharedInstance.productLocation)
        }
        
        return
    }
    
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      
      navBar?.setFrame()
      
      if layoutType.shouldSetFrame() && childviewController != nil {
         setFrameOf(viewController: childviewController!)
      }
   }
   
   // MARK: - IBAction
   @IBAction func checkoutButtonPressed() {
      cart?.checkoutButtonPressed()
   }
   
   // MARK: - Methods
   fileprivate func numberOfElementsInCartChanged() {
      changeCartButtonTitle()
      changePriceLabelText()

      cartButton.isHidden = numberOfElementsInCart == 0
      cartButtonview.isHidden = !shouldCartBeVisible()
   }
   
   fileprivate func changeCartButtonTitle() {
      let title =  " Checkout (\(numberOfElementsInCart.description) \(numberOfElementsInCart == 1 ? "Item" : "items"))"
      UIView.setAnimationsEnabled(false)
      cartButton.setTitle(title, for: .normal)
      cartButton.layoutIfNeeded()
      UIView.setAnimationsEnabled(true)
   }
   
   fileprivate func changePriceLabelText() {
      cartPriceLabel.text =  Singleton.sharedInstance.formDetailsInfo.currencyid + "\(Singleton.sharedInstance.subTotal)"
      cartPriceLabel.letterSpacing = 0.5
   }
   
   fileprivate func shouldCartBeVisible() -> Bool {
      if neverShowCartButton {
         return false
      }
      return numberOfElementsInCart > 0
   }
   
   private func setNavigationBar() {
      let navParams = NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: titleString, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil)
      
     navBar = NavigationView.getNibFile(params: navParams, leftButtonAction: { [weak self] in
        
         self?.flowManager?.fallBackToPreviousLevelWith(currentLevel: self!.level.first!.level)
      }, rightButtonAction: nil)
      navBar?.backButton.isHidden = isBackButtonHidden
      view.addSubview(navBar!)
   }
   
   func getViewControllerToSetAsAChild() -> UIViewController {
      var viewControllerToSetAsChild: UIViewController
      
      switch layoutType! {
      case .banner:
         viewControllerToSetAsChild = BannerTableViewController.getWith(level: level, delegate: self)
      
      case .menu:
         viewControllerToSetAsChild = MenuViewController.getWith(level: level, delegate: self)
      case .list:
         viewControllerToSetAsChild = ListTableViewController.getWith(level: level, delegate: self)
      case .map:
         neverShowCartButton = true
         viewControllerToSetAsChild = MapViewController.getWith(level: level, delegate: self)
      }
      viewControllerToSetAsChild.view.backgroundColor = UIColor.white
      childviewController = viewControllerToSetAsChild
      return viewControllerToSetAsChild
      
   }
   
   func setViewControllerAs(childViewController: UIViewController, shouldSetFrame: Bool = true) {
    
      addChildViewController(childViewController)
    
      if shouldSetFrame {
         setFrameOf(viewController: childViewController)
      }
      
      view.addSubview(childViewController.view)
      childViewController.didMove(toParentViewController: self)
   }
   
   func setFrameOf(viewController: UIViewController) {
      let bottomInset = self.view.safeAreaInsetsForAllOS.bottom
//    let heightOfTextView = (self.addressLabel.text?.heightWithConstrainedWidth(width: self.addressLabel.frame.size.width, font: self.addressLabel.font ?? UIFont(name: FONT.light, size: FONT_SIZE.large)!).size.height)! + self.addressFieldHeight.constant + 10
      let height = view.frame.height - HEIGHT.navigationHeight - heightOfCheckoutButton() - bottomInset - self.addressFieldHeight.constant
      let frameForChild = CGRect(x: 0, y: 0, width: view.frame.width, height: height)
      viewController.view.frame = frameForChild
      viewController.view.frame.origin.y = HEIGHT.navigationHeight + self.addressFieldHeight.constant
    self.view.bringSubview(toFront: self.addressOuterView)
   }
   
   private func heightOfCheckoutButton() -> CGFloat {
      return shouldCartBeVisible() ? 50 : 0
   }
   

   
   // MARK: - Navigation
   class func getWith(layoutType: LevelLayout, level: Level, title: String) -> NLevelParentViewController {
      let parentVC = findIn(storyboard: .nLevel, withIdentifier: "NLevelParentViewController") as! NLevelParentViewController
      parentVC.layoutType = layoutType
      parentVC.level = level
      parentVC.titleString = title
      return parentVC
   }
   
   deinit {
      print("NLevel parent deintialized")
    if let levelElement = level.first{
        if levelElement.level == 0 {
            Singleton.sharedInstance.addressForNlevelFlow = ""
            Singleton.sharedInstance.productLocation = LocationTracker.sharedInstance.myLocation.coordinate
        }
        
        
    }
   }
}

extension NLevelParentViewController : locationManagerCustomDelegate {
    
    
    func didUpdateLocation(withCoordinates: CLLocationCoordinate2D) {
        locationManager.reverseGeocodeCoordinate(coordinate: withCoordinates) {[weak self] (address) in
            DispatchQueue.main.async {
//                if Singleton.sharedInstance.addressForNlevelFlow == "" {
                    if address != "" {
                        self?.addressLabel.textColor = COLOR.SPLASH_TEXT_COLOR
                        self?.addressLabel.text = address
                        Singleton.sharedInstance.addressForNlevelFlow = address
                        Singleton.sharedInstance.productLocation = withCoordinates
                        
//                        self?.addressPlaceholder.isHidden = true
                        
                        self?.childviewController?.removeFromParentViewController()
                        self?.setOuterAddressView()
                        let vc = self?.getViewControllerToSetAsAChild()
                        self?.setViewControllerAs(childViewController: vc!, shouldSetFrame: (self?.layoutType.shouldSetFrame())!)
                        self?.locationManager.stopUpdatingLocation()
                    } else {
                        self?.addressLabel.textColor = COLOR.PLACEHOLDER_COLOR
                        self?.addressLabel.text = TEXT.GETTING_ADDRESS
                    }
//                }
                
            }
        }
        locationManager.stopUpdatingLocation()
    }
    
    func appCatalogueHit(coordinate: CLLocationCoordinate2D, forProduct:Bool) {
        
        
        let form = Singleton.sharedInstance.formDetailsInfo
        let flomngr = form.nLevelController
        
        flomngr?.updateCatalogueWith(location: coordinate, completion: { 
            
            self.productHit(location: coordinate)
 
        })
    }
    
    
    func getLevelElement() {
        let form = Singleton.sharedInstance.formDetailsInfo
        let flomngr = form.nLevelController
        let levelElement = level.first!
        
        
        flomngr?.getChildrenOfElementAt(index: levelElement.parentIndexInPreviousLevel, inLevel: levelElement.level-1, completion: { (success, level) in
            
            guard level == nil else {
                self.setController(levelElements: level!)
                return
            }
            self.popViewController()
            ErrorView.showWith(message: ERROR_MESSAGE.unableToFetchProducts, isErrorMessage: true, removed: nil)
        })
    }
    
    func productHit(location: CLLocationCoordinate2D) {
        let form = Singleton.sharedInstance.formDetailsInfo
        let flomngr = form.nLevelController
        let levelElement = level.first!
        
        if let (index, currentLevelElement) = flomngr?.catalogue.getParentLevelWith(parentId: levelElement.parentId, productLevel: levelElement.level-1) {
            
            
            guard index == nil, currentLevelElement == nil else {
                flowManager?.getNonDummyChildrenOf(element: currentLevelElement!, atChildIndex: 0, indexInLevel: index!, showActivityIndicator: false) { [weak self] (success, levelElements) in
//                    if self?.level.first is Product {
//                        self?.appCatalogueHit(coordinate: Singleton.sharedInstance.productLocation)
//                    }
                    guard levelElements == nil else {
                        self?.setController(levelElements: levelElements!)
                        return
                    }
                    self?.popViewController()
                    ErrorView.showWith(message: ERROR_MESSAGE.unableToFetchProducts, isErrorMessage: true, removed: nil)
                    //                if success {
                    ////                    self?.childrenLevelsOfLevel?[index] = levelElements!
                    //                }
                    ////                completion?(success, levelElements)
                }
                return
            }
            guard self.level.first is Product else {
                self.getLevelElement()
                return
            }
            self.popViewController()
            ErrorView.showWith(message: ERROR_MESSAGE.unableToFetchProducts, isErrorMessage: true, removed: nil)
//            flomngr?.getChildrenOfElementAt(index: levelElement.parentIndexInPreviousLevel, inLevel: levelElement.level-1, completion: { (success, level) in
//
//                guard level == nil else {
////                    self.childviewController?.removeFromParentViewController()
////                    self.level = level
////                    let vc = self.getViewControllerToSetAsAChild()
////                    self.setViewControllerAs(childViewController: vc, shouldSetFrame: self.layoutType.shouldSetFrame())
//                    self.setController(levelElements: level!)
//                    return
//                }
//                self.popViewController()
//            })

            
        }
        
        
        
        
        
    }
    
    
    func setController(levelElements: [LevelElement]) {
        self.childviewController?.removeFromParentViewController()
        self.level = levelElements
        let vc = self.getViewControllerToSetAsAChild()
        self.childviewController?.removeFromParentViewController()
        self.setViewControllerAs(childViewController: vc, shouldSetFrame: self.layoutType.shouldSetFrame())
    }
}

//extension NLevelParentViewController: UITextViewDelegate {
//
//    func textViewDidBeginEditing(_ textView: UITextView) {
//    }
//
//}

// MARK: - NLevelFlowDelegate
extension NLevelParentViewController: NLevelFlowDelegate {
    func popViewController() {
        
        flowManager?.fallBackToPreviousLevelWith(currentLevel: level.first!.level)
//        self.productHit(location: CLLocationCoordinate2D())
    }

   func getChildrenOf(element: LevelElement, inCellWithTag tag: Int, completion: ((Bool, Level?) -> Void)?) {
      
      guard let arrayOfChildLevels = childrenLevelsOfLevel else {
         childrenLevelsOfLevel = [Level].init(repeating: [], count: level.count)
         getChildrenOf(element: element, inCellWithTag: tag, completion: completion)
         return
      }
      
      let index = tag.getRowAndSection().row
      let childLevel = arrayOfChildLevels[index]
      
      guard childLevel.count > 0 else {
        flowManager?.getNonDummyChildrenOf(element: element, atChildIndex: index, indexInLevel: nil, showActivityIndicator: false) { [weak self] (success, levelElements) in
            if success {
               self?.childrenLevelsOfLevel?[index] = levelElements!
            }
            completion?(success, levelElements)
         }
         return
      }
      
      completion?(true, childLevel)
   }

   func getQuantityOfProductWith(id: String) -> Int {
      return cart!.getQuantityOfProductWith(id: id)
   }

   func addQuantityButtonPressed(forElement element: LevelElement) {
      cart?.quantityAddedOf(element: element)
      numberOfElementsInCart = cart?.getCartCount() ?? 0
   }
   
   func subtractQuantityButtonPressed(forElement element: LevelElement) {
      cart?.quantitySubtractedOf(element: element)
      numberOfElementsInCart = cart?.getCartCount() ?? 0
   }
   
   func nextButtonPressed(forElement element: LevelElement, atIndexInChildArray index: Int) {
      guard !(element is Product) else {
         print("ERROR -> Cannot go further after product level")
         return
      }
      flowManager?.pushNextControllerWithChildrenOf(element: element, atChildIndex: index)
   }
}
