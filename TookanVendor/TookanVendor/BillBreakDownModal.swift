//
//  BillBreakDownModal.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 16/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

class BillModal{
    
    var actualAmmount = String()
    var benifitType = String()
    var creditsToAdd = String()
    var discount = String()
    var discountedAmmount = String()
    var paybleAmount = String()
    var serviceTax = String()
    var tip = String()
    var vat = String()
    var currentCredits = String()
    var serviceTaxPercentage = String()
    var vatPercentage = String()
    var netPaybleAmount = String()
    var creditsUsed = String()
    
    
    init(json:[String:Any]) {
        
        print(json)
        
        if let value = json["ACTUAL_AMOUNT"] as? String{
            self.actualAmmount = value
        }else if let value = json["ACTUAL_AMOUNT"] as? NSNumber{
            self.actualAmmount = "\(value.doubleValue.roundTo(places: 2))"
        }
        
        if let value = json["BENEFIT_TYPE"] as? String{
            self.benifitType = value
        }else if let value = json["BENEFIT_TYPE"] as? NSNumber{
            self.benifitType = "\(value.intValue)"
        }
        
        if let value = json["CREDITS_TO_ADD"] as? String{
            self.creditsToAdd = value
        }else if let value = json["CREDITS_TO_ADD"] as? NSNumber{
            self.creditsToAdd = "\(value.doubleValue.roundTo(places: 2))"
        }
        
        
        if let value = json["DISCOUNT"] as? String{
            self.discount = value
        }else if let value = json["DISCOUNT"] as? NSNumber{
            self.discount = "\(value.doubleValue.roundTo(places: 2))"
        }
        
        
        if let value = json["DISCOUNTED_AMOUNT"] as? String{
            self.discountedAmmount = value
        }else if let value = json["DISCOUNTED_AMOUNT"] as? NSNumber{
            self.discountedAmmount = "\(value.doubleValue.roundTo(places: 2))"
        }
        
        
        if let value = json["PAYABLE_AMOUNT"] as? String{
            self.paybleAmount = value
        }else if let value = json["PAYABLE_AMOUNT"] as? NSNumber{
            self.paybleAmount = "\(value.doubleValue.roundTo(places: 2))"
        }
        
        
        if let value = json["SERVICE_TAX"] as? String{
            self.serviceTax = value
        }else if let value = json["SERVICE_TAX"] as? NSNumber{
            self.serviceTax = "\(value.doubleValue.roundTo(places: 2))"
        }
        
        
        if let value = json["TIP"] as? String{
            self.tip = value
        }else if let value = json["TIP"] as? NSNumber{
            self.tip = "\(value.doubleValue.roundTo(places: 2))"
        }
        
        
        if let value = json["VAT"] as? String {
            self.vat = value
        }else if let value = json["VAT"] as? NSNumber{
            self.vat = "\(value.doubleValue.roundTo(places: 2))"
        }
       
        if let value = json["CURRENT_CREDITS"] as? String{
            self.currentCredits = value
        }else if let value = json["CURRENT_CREDITS"] as? Int{
            self.currentCredits = "\(value)"
        }
        
        if let value = json["DEFAULT_VAT_PERCENT"] as? String{
            self.vatPercentage = value
        }else if let value = json["DEFAULT_VAT_PERCENT"] as? Double{
            self.vatPercentage = "\(value.roundTo(places: 1))"
        }
      
      if let serviceTaxPercent = json["DEFAULT_SERVICE_TAX_PERCENT"] as? Double {
        serviceTaxPercentage = "\(serviceTaxPercent)"
      }
        
        if let value = json["CREDITS_USED"] as? String{
            self.creditsUsed = value
        }else if let value = json["CREDITS_USED"] as? Double{
            self.creditsUsed = "\(value)"
        }
        
        if let value = json["NET_PAYABLE_AMOUNT"] as? String{
            self.netPaybleAmount = value
        }else if let value = json["NET_PAYABLE_AMOUNT"] as? Double{
            self.netPaybleAmount = "\(value)"
        }
        
        AppConfiguration.current.serviceTax = self.serviceTaxPercentage
        AppConfiguration.current.vat = self.vatPercentage
        
    }
    
    
    
    
    func  getDataArray() -> (nameArray:[String],valueArray:[String]) {
        var nameArray = [String]()
        var value = [String]()
        
        value.append(Singleton.sharedInstance.formDetailsInfo.currencyid + actualAmmount)
        nameArray.append(TEXT.subtotal)
        
        
        
//        if self.currentCredits != "0" && self.currentCredits != "0.0" && self.currentCredits != ""{
//            value.append(currentCredits)
//            nameArray.append(TEXT.Credits)
//        }
        
        let discountInNumber = Double(self.discount) ?? 0
        if discountInNumber != 0 {
            value.append("- " + Singleton.sharedInstance.formDetailsInfo.currencyid + discount)
            nameArray.append(TEXT.Discount)
        }
        
        let vatInNumber = Double(self.vat) ?? 0
        if vatInNumber != 0 {
         value.append(Singleton.sharedInstance.formDetailsInfo.currencyid + vat)
         nameArray.append(TEXT.VAT + " @ " + AppConfiguration.current.vat + "%")
        }
        
        
       
        
        let sertviceTaxInNumber = Double(self.serviceTax) ?? 0
        if sertviceTaxInNumber != 0 {
            value.append(Singleton.sharedInstance.formDetailsInfo.currencyid + serviceTax)
            nameArray.append(TEXT.serviceTax + " @ " + AppConfiguration.current.serviceTax + "%")
        }

      let creditsUsedInNumber = Double(self.creditsUsed) ?? 0
        if  creditsUsedInNumber != 0 {
            value.append("-" + " " + Singleton.sharedInstance.formDetailsInfo.currencyid + self.creditsUsed)
            nameArray.append(TEXT.Credits)
        }
      
      let tip = Double(self.tip) ?? 0
        if  tip != 0 && self.tip != ""{
            value.append( Singleton.sharedInstance.formDetailsInfo.currencyid + self.tip)
            nameArray.append(TEXT.tip)
        }
        
        value.append( Singleton.sharedInstance.formDetailsInfo.currencyid + netPaybleAmount)
        nameArray.append(TEXT.total)
        
        return (nameArray,value)
    }
    
    
    
   
    
    
    
    
    
}
extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
