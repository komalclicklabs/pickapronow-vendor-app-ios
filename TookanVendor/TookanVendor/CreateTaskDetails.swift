//
//  CreateTaskDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 30/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class CreateTaskDetails: NSObject {
    
    var autoAssignment:Int = 1
    var customerAddress:String = ""
    var customerEmail:String = ""
    var customerPhone:String = ""
    var customerUsername:String = ""
    var jobDeliveryDatetime:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var hasDelivery:Int = 0
    var hasPickup:Int = 1
    var jobDescription:String = ""
    var jobPickupAddress:String = ""
    var jobPickupDateTime:String = ""
    var jobPickupLatitude:String = ""
    var jobPickupLongitude:String = ""
    var jobPickupName:String = ""
    var jobPickupEmail:String = ""
    var jobPickupPhone:String = ""
    var layoutType:Int = 0
    var refImages:[String] = []
    var domainName:String = ""
    var vendorId:Int = 2
    var hidePickupDelivery:Bool = false
    var pickupMetadata = [Any]()
    var metaData = [Any]()
   var fleetId = ""
    
    
    init(json: [String:Any]) {
        
    }
   
}
