//
//  LocationManagerCustomeClass.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 05/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import UIKit

protocol locationManagerCustomDelegate: class {
    func didUpdateLocation(withCoordinates:CLLocationCoordinate2D)
}


class CustomLocationManager : NSObject,CLLocationManagerDelegate {
   static var shared = CustomLocationManager()
   private static var latestLocation: CLLocation?
   static var latestCoordinate: CLLocationCoordinate2D {
      return latestLocation != nil ? latestLocation!.coordinate : CLLocationCoordinate2D()
   }

   
    private var locationManager = CLLocationManager()
    weak var delegate : locationManagerCustomDelegate?
   private var coordinatesFetched: ((CLLocationCoordinate2D) -> Void)?
   
   // MARK: - Intializer
   private override init() {}
   
    func startUpdatingLocation() {
        setUpLocationManager()
        self.locationManager.startUpdatingLocation()
    }
    
    private func setUpLocationManager(){
        DispatchQueue.main.async { [weak self] in
            self?.locationManager.delegate = self
            self?.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
    }
    
    func stopUpdatingLocation(){
        self.locationManager.stopUpdatingLocation()
    }
   
   // MARK: - Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      let latestCoordinates = locations.last?.coordinate ?? CLLocationCoordinate2D()
      delegate?.didUpdateLocation(withCoordinates: locations.last!.coordinate)
      coordinatesFetched?(latestCoordinates)
      coordinatesFetched = nil
      CustomLocationManager.latestLocation = locations.last
    }
   
   func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      let fakeCoordinates = CLLocationCoordinate2D()
      coordinatesFetched?(fakeCoordinates)
      coordinatesFetched = nil
   }
   
   func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      guard status == .authorizedAlways || status == .authorizedWhenInUse else {
         return
      }
      
      updateCoordinates(forced: true)
   }
   
   // MARK: - 
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D , completionHandler:@escaping ((_ address:String)->Void)) {
        print(coordinate.latitude)
        print(coordinate.longitude)
        if IJReachability.isConnectedToNetwork()  == true   {
            let geocoder = GMSGeocoder()
               // self.jobPickUpLabel.text = TEXT.GettingAddress
            completionHandler(TEXT.GettingAddress)
             DispatchQueue.global(qos: .background).async {
            geocoder.reverseGeocodeCoordinate(coordinate) {[weak self] response, error in
                if  response != nil {
                    if (response?.results()?.count)! > 0{
                            if response?.results()?[0].lines?.count != 0 {
                              var address = ""
                              if let lines = response?.firstResult()?.lines {
                                 address = lines.joined(separator: " ")
                              }
                                DispatchQueue.main.async {
                                    completionHandler(address)
                                }
                                
                            }
                        
                    }else{
                         DispatchQueue.main.async {
                        completionHandler(TEXT.goToPinText)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                    completionHandler(TEXT.goToPinText)
                    }
                }
            }
            }
        }
    }

   /// if not forced it may send coordinates maximum of 7 minutes old
   func updateCoordinates(forced: Bool, completion:  ((CLLocationCoordinate2D) -> Void)? = nil) {
      
      guard CustomLocationManager.isLocationServicesEnabled() && (forced || isLocationUpdateNeeded()) else {
         completion?(CustomLocationManager.latestCoordinate)
         return
      }
   
      self.locationManager.delegate = self
      coordinatesFetched = completion
      self.locationManager.requestLocation()
   }
   
   func isLocationUpdateNeeded() -> Bool {
      guard CustomLocationManager.latestLocation != nil else {
         return true
      }
      
      let timeStampOfLatestLocation = CustomLocationManager.latestLocation!.timestamp
      let secondsSinceLocationUpdate = Date().timeIntervalSince(timeStampOfLatestLocation)
      let sevenMinutes: TimeInterval = 60 * 7
      
      return secondsSinceLocationUpdate > sevenMinutes
   }
   
   static func isLocationServicesEnabled() -> Bool {
      return CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .denied && CLLocationManager.authorizationStatus() != .notDetermined && CLLocationManager.authorizationStatus() != .restricted
   }
    
}
