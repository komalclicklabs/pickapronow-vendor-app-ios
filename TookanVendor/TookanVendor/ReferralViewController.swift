//
//  ReferralViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 10/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import Social

class ReferralViewController: UIViewController {

  let referalShareMessage: String = {
    let referralCode = Vendor.current!.referralCode
    let dataForSharing = AppConfiguration.current.referral.details
    let referalDescWithReferalCode = dataForSharing.replacingOccurrences(of: "X_Y_Z", with: referralCode)
    return referalDescWithReferalCode
  }()
    
    @IBOutlet weak var referImage: UIImageView!
    @IBOutlet weak var referText: UILabel!
    @IBOutlet weak var howItWorksButton: UIButton!
    @IBOutlet weak var refferalCode: UILabel!
    
    @IBOutlet weak var tapTheBoxLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twiterButton: UIButton!
    @IBOutlet weak var watsAppButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    var navigationBar: NavigationView?
    
    let colorArray = [UIColor(colorLiteralRed: 59/255, green: 89/255, blue: 152/255, alpha: 1),UIColor(colorLiteralRed: 29/255, green: 161/255, blue: 242/255, alpha: 1),UIColor(colorLiteralRed: 73/255, green: 200/255, blue: 88/255, alpha: 1),UIColor(colorLiteralRed: 34/255, green: 150/255, blue: 255/255, alpha: 1)]
    
    let imageArray = [#imageLiteral(resourceName: "iconFb"),#imageLiteral(resourceName: "twitterLogo"),#imageLiteral(resourceName: "whatsApp"),#imageLiteral(resourceName: "more")]
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle()
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        setUpLabelsAndButtons()
        // Do any additional setup after loading the view.
    }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      UIApplication.shared.statusBarStyle = .default
   }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
 
  @IBAction func buttonTappedOnReferalCode(_ sender: Any) {
    
    UIPasteboard.general.string = refferalCode.text!
    logEvent(label: "refer_copy_code")
    ErrorView.showWith(message: "Copied!", isErrorMessage: false, removed: nil)
  }
  
    @IBAction func faceBookAction(_ sender: UIButton) {
        if(SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook)){
            logEvent(label: "refer_share_fb")
             let socialController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
             socialController?.setInitialText(referalShareMessage)
             self.present(socialController!, animated: true, completion: nil)
        }else{
             Singleton.sharedInstance.showAlert("Your phone does not contain Facebook app")
        }
    }
    
    
    @IBAction func twitterAction(_ sender: UIButton) {
       
        if(SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter)){
            let socialController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            socialController?.setInitialText(referalShareMessage)
            logEvent(label: "refer_share_twitter")
            self.present(socialController!, animated: true, completion: nil)
        }else{
            Singleton.sharedInstance.showAlert("Your phone does not contain Twitter app")
        }
        
        
    }
    
    
  @IBAction func whatsAppAction(_ sender: UIButton) {
    
    let msg = referalShareMessage
    let urlWhats = "whatsapp://send?text=\(msg)"
    
    if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed),
      let whatsappURL = URL(string: urlString),
        
      UIApplication.shared.canOpenURL(whatsappURL as URL) {
      UIApplication.shared.openURL(whatsappURL as URL)
        logEvent(label: "refer_share_whatsapp")
    } else {
      Singleton.sharedInstance.showAlert(ERROR_MESSAGE.WHATSAPP_NOT_FOUND)
    }
    
    
  }
  
    @IBAction func moreAction(_ sender: UIButton) {
        let activityViewController = UIActivityViewController(activityItems: [referalShareMessage], applicationActivities: nil)
        logEvent(label: "refer_share_other")
        self.present(activityViewController, animated: true, completion: nil)
    }

    @IBAction func howItWorksButtonAction(_ sender: UIButton) {
    }
    
    
    
    func setUpLabelsAndButtons(){
      
        
        
        self.referText.font = UIFont(name: FONT.light, size: CGFloat(14).customizeSize())
        self.referText.textColor = COLOR.SPLASH_TEXT_COLOR
        //self.referText.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lobortis purus ex."
        self.referText.text = AppConfiguration.current.referral.description
        howItWorksButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        howItWorksButton.titleLabel?.font = UIFont(name: FONT.light, size: CGFloat(14).customizeSize())
        howItWorksButton.titleLabel?.textAlignment = .left
        howItWorksButton.setTitle(TEXT.howItWorks, for: UIControlState.normal)
        howItWorksButton.isHidden =  true   
        
        refferalCode.font = UIFont(name: FONT.regular, size: CGFloat(20).customizeSize())
        refferalCode.textColor = COLOR.SPLASH_TEXT_COLOR
        refferalCode.text = Vendor.current?.referralCode
        
        tapTheBoxLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.carTypeFontSize.customizeSize())
        tapTheBoxLabel.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
        tapTheBoxLabel.letterSpacing = 0.4
        tapTheBoxLabel.text = TEXT.tapTheBox
        
        shareLabel.font =  UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize.customizeSize())
        shareLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        shareLabel.letterSpacing = 1
        shareLabel.text = TEXT.shareOn
    
        let buttonArray = [facebookButton,twiterButton,watsAppButton,moreButton]
        
        for i in 0...buttonArray.count - 1{
            buttonArray[i]?.backgroundColor = colorArray[i]
            buttonArray[i]?.setImage(imageArray[i], for: UIControlState.normal)
            buttonArray[i]?.layer.shadowColor = colorArray[i].cgColor
            buttonArray[i]?.layer.shadowOpacity = 0.3
            buttonArray[i]?.layer.shadowOffset = CGSize(width: 0, height: 7)
            buttonArray[i]?.layer.shadowRadius = 6
            buttonArray[i]?.layer.cornerRadius = CGFloat(2).customizeSize()
        }
        
    
    }
    
    
    func setNavigationTitle(){
        
        self.navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "" , rightButtonTitle: "", title: TEXT.referrals, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil), leftButtonAction: {
            _ = self.navigationController?.popViewController(animated: true)
        }, rightButtonAction: nil)
        
        self.view.addSubview(navigationBar!)
    }
}


extension CGFloat{
    
    
    
    func customizeSize() -> CGFloat {
        return self
//         return (self/667 )*SCREEN_SIZE.height
    }
}


extension UILabel{
    @IBInspectable var letterSpacing:CGFloat{
        get{
            return 5
        }
        set{
            let attributedString = NSMutableAttributedString(string: text!)
            attributedString.addAttribute(NSKernAttributeName, value: newValue, range: NSRange(location: 0, length: text!.characters.count))
            attributedText = attributedString
        }
    }
}
