//
//  OrderHistoryCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/13/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
   
   // MARK: - IBOutlets
   @IBOutlet weak var upperOrderStatus: UILabel!
   @IBOutlet weak var upperDateTime: UILabel!
   @IBOutlet weak var lowerDateTime: UILabel!
   @IBOutlet weak var upperAddress: UILabel!
   @IBOutlet weak var lowerAddress: UILabel!
   
   @IBOutlet weak var backView: UIView!
   @IBOutlet weak var upperOrderTypeCircleLabel: UILabel!
   
   @IBOutlet weak var lowerOrderTypeCircleLabel: UILabel!
   @IBOutlet weak var dottedLineView: UIView!
   
   @IBOutlet weak var leftMarginOfUpperLabel: NSLayoutConstraint!
   @IBOutlet weak var diameterOfUpperCircleLabel: NSLayoutConstraint!
   @IBOutlet weak var diameterOfLowerCircleLabel: NSLayoutConstraint!
   
   //MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      
      configureViews()
   }
   
   func configureViews() {
      self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      dottedLineView.backgroundColor = UIColor(patternImage:#imageLiteral(resourceName: "iconPattern"))
      configureStatusLabel()
      configureUpperOrder()
      configureLowerOrder()
   }
   
   private func configureStatusLabel() {
      self.upperOrderStatus.font = UIFont(name: FONT.regular, size: 15.5)
      self.upperOrderStatus.textColor = COLOR.SPLASH_TEXT_COLOR
   }
   
   // MARK: - Configuring Upper Order
   private func configureUpperOrder() {
      configureUpperOrderAddress()
      configureUpperOrderDateLabel()
      configureUpperOrderCircleLabel()
   }
   
   private func configureUpperOrderDateLabel() {
      self.upperDateTime.font = UIFont(name: FONT.semiBold, size: 14)
      self.upperDateTime.textColor = COLOR.SPLASH_TEXT_COLOR
   }
   
   private func configureUpperOrderAddress() {
      self.upperAddress.font = UIFont(name: FONT.light, size: 14)
      self.upperAddress.textColor = COLOR.SPLASH_TEXT_COLOR
   }
   
   private func configureUpperOrderCircleLabel() {
      upperOrderTypeCircleLabel.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
   }
   
   // MARK: - Configuring Lower Order
   private func configureLowerOrder() {
      configureLowerOrderDateLabel()
      configureLowerOrderAddress()
      configureLowerOrderCircleLabel()
   }
   
   private func configureLowerOrderDateLabel() {
      self.lowerDateTime.font = UIFont(name: FONT.semiBold, size: 14)
      self.lowerDateTime.textColor = COLOR.SPLASH_TEXT_COLOR
   }
   
   private func configureLowerOrderAddress() {
      self.lowerAddress.font = UIFont(name: FONT.light, size: 14)
      self.lowerAddress.textColor = COLOR.SPLASH_TEXT_COLOR
   }
   
   private func configureLowerOrderCircleLabel() {
      upperOrderTypeCircleLabel.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
   }
   
   // MARK: - Methods
   
   func setCellWith(order: OrderHistory) {
      
      guard order.hasTasks() else {
         print("No Task found in Order")
         return
      }
      
      if order.hasSingleTask() {
         setCellForSingle(task: order.tasksOfOrder[0])
      } else {
         setCellForMultiple(tasks: order.tasksOfOrder)
      }
      
      setStatuslabelFor(order: order)

   }
   
   private func setCellForSingle(task: OrderDetails) {
      configureViewToShowOnlyUpperOrder(flag: true)
      
      upperDateTime.text = getPrefixedStringForDateLabelWith(task: task)
      upperAddress.text = task.getAddress()
   }
   
   private func configureViewToShowOnlyUpperOrder(flag: Bool) {
      lowerAddress.isHidden = flag
      lowerDateTime.isHidden = flag
      lowerOrderTypeCircleLabel.isHidden = flag
      dottedLineView.isHidden = flag
      upperOrderTypeCircleLabel.isHidden = flag
      leftMarginOfUpperLabel.constant = flag ? -6 : 11
   }
   
   private func setCellForMultiple(tasks: [OrderDetails]) {
      configureViewToShowOnlyUpperOrder(flag: false)

      let firstTask = tasks[1]
      let secondtask = tasks[0]
      
      upperAddress.text = firstTask.getAddress()
      upperDateTime.text = getPrefixedStringForDateLabelWith(task: firstTask)
      upperOrderTypeCircleLabel.backgroundColor = firstTask.job_status?.getTextAndColor(vertical: firstTask.vertical).color
      
      
      lowerAddress.text = secondtask.getAddress()
      lowerDateTime.text = getPrefixedStringForDateLabelWith(task: secondtask)
      lowerOrderTypeCircleLabel.backgroundColor = secondtask.job_status?.getTextAndColor(vertical: secondtask.vertical).color
      
      if firstTask.job_status!.processingOftaskStopped() {
         diameterOfUpperCircleLabel.constant = 6
         diameterOfLowerCircleLabel.constant = 8
      } else {
         diameterOfUpperCircleLabel.constant = 8
         diameterOfLowerCircleLabel.constant = 6
      }
      
   }
   
   private func setStatuslabelFor(order: OrderHistory) {
      
      if order.hasSingleTask() {
         let (statustext, statusColor) = getStatusTextAndColorFrom(order: order)
         
         upperOrderStatus.text = statustext
         upperOrderStatus.textColor = statusColor
      } else {
         let firstTask = order.tasksOfOrder[1]
         let secondTask = order.tasksOfOrder[0]
         
         if firstTask.job_status!.processingOftaskStopped() {
            let (text, color) = secondTask.job_status!.getTextAndColor(vertical: secondTask.vertical)
            upperOrderStatus.text = text
            upperOrderStatus.textColor = color
         } else {
            let (text, color) = firstTask.job_status!.getTextAndColor(vertical: firstTask.vertical)
            upperOrderStatus.text = text
            upperOrderStatus.textColor = color
         }
      }
      
      
   }
   
   private func getStatusTextAndColorFrom(order: OrderHistory) -> (text: String, color: UIColor) {
      let (status, vertical) = order.getLatestStatusAndVertical()
      return status.getTextAndColor(vertical: vertical)
   }
   
   private func getPrefixedStringForDateLabelWith(task: OrderDetails) -> String {
      var dateString = task.getDate()
      let prefix = task.getPrefixForJobType()
      
      if !prefix.isEmpty {
         dateString = prefix + " - " + dateString
      }
      
      return dateString
   }
   
}
