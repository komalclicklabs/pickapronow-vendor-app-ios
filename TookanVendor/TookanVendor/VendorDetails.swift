//
//  VendorDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 18/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation
import SDKDemo1

class Vendor: NSObject, NSCoding {
   
   static var current: Vendor?
   
   var accessToken: String?
   var address: String?
   var appAccessToken: String?
   var appVersionCode: String?
   var company: String?
   var email: String?
   var firstName: String?
   var lastName: String?
   var latitude: Double?
   var longitude: Double?
   var phoneNo: String?
   var userId: Int?
   var id: Int?
   var vendorImage: String?
   var is_phone_verified = "0"
   var pendingAmount = Double()
   var referralCode = String()
   var credits = String()
   var isFirstSignup = false
   var welcomePopUpMessage = ""
   var isSosEnabled = false
   
   var signupTemplate: SignupTemplate?
   var registrationStatus = RegistrationStatus.otpPending
   var reviewMessage = ""
   
   var fullName: String {
      return (firstName ?? "") + " " + (lastName ?? "")
   }

   
   required init(coder aDecoder: NSCoder) {
      accessToken = aDecoder.decodeObject(forKey: "accessToken") as? String
      address = aDecoder.decodeObject(forKey: "address") as? String
      appAccessToken = aDecoder.decodeObject(forKey: "appAccessToken") as? String
      appVersionCode = aDecoder.decodeObject(forKey: "appVersionCode") as? String
      company = aDecoder.decodeObject(forKey: "company") as? String
      email = aDecoder.decodeObject(forKey: "email") as? String
      firstName = aDecoder.decodeObject(forKey: "firstName") as? String
      lastName = aDecoder.decodeObject(forKey: "lastName") as? String
      latitude = aDecoder.decodeObject(forKey: "latitude") as? Double
      longitude = aDecoder.decodeObject(forKey: "longitude") as? Double
      phoneNo = aDecoder.decodeObject(forKey: "phoneNo") as? String
      vendorImage = aDecoder.decodeObject(forKey: "vendor_image") as? String
      userId = aDecoder.decodeObject(forKey: "userId") as? Int
      id = aDecoder.decodeObject(forKey: "vendorId") as? Int
   }
   
   func encode(with aCoder: NSCoder) {
      aCoder.encode(accessToken, forKey: "accessToken")
      aCoder.encode(address, forKey: "address")
      aCoder.encode(appAccessToken, forKey: "appAccessToken")
      aCoder.encode(appVersionCode, forKey: "appVersionCode")
      aCoder.encode(company, forKey: "company")
      aCoder.encode(email, forKey: "email")
      aCoder.encode(firstName, forKey: "firstName")
      aCoder.encode(lastName, forKey: "lastName")
      aCoder.encode(latitude, forKey: "latitude")
      aCoder.encode(longitude, forKey: "longitude")
      aCoder.encode(phoneNo, forKey: "phoneNo")
      aCoder.encode(userId, forKey: "userId")
      aCoder.encode(id, forKey: "vendorId")
      aCoder.encode(vendorImage, forKey: "vendor_image")
   }
   
   init(json: [String: Any], signupTemplate: SignupTemplate?, reviewMessage: String) {
      
      accessToken = (json["access_token"] as? String) ?? ""
      appAccessToken = (json["app_access_token"] as? String) ?? ""
      if let value = json["app_versioncode"] {
         appVersionCode = "\(value)"
      }
      
      if let value = json["vendor_id"] {
         id = Int("\(value)") ?? 0
      }
      if let value = json["user_id"] as? Int {
         self.userId = value
      } else if let value = json["user_id"] as? String {
         self.userId = Int(value)
      } else {
         self.userId = 0
      }
      
      firstName = (json["first_name"] as? String) ?? ""
      lastName = (json["last_name"] as? String) ?? ""
      vendorImage = (json["vendor_image"] as? String) ?? ""
      email = (json["email"] as? String) ?? ""
      phoneNo = (json["phone_no"] as? String) ?? ""
      address = (json["address"] as? String) ?? ""
      referralCode = (json["referral_code"] as? String) ?? ""
      company = (json["company"] as? String) ?? ""
      
      if let value = json["credits"] {
         self.credits = (Double("\(value)") ?? 0).description
      }
      
      if let value = json["latitude"] {
         self.latitude = Double("\(value)") ?? 0
      }
      
      if let value = json["longitude"] {
         self.longitude = Double("\(value)") ?? 0
      }
      
      if let value = json["is_phone_verified"] as? String{
         self.is_phone_verified = value
      }else if let value = json["is_phone_verified"] as? Int{
         self.is_phone_verified = "\(value)"
      }
      
      if let value = json["pending_amount"] as? Double{
         self.pendingAmount = value
      } else if let value = json["pending_amount"] as? String {
        if let nsValue = value as? NSNumber {
            if let doubleValue = nsValue as? Double {
                self.pendingAmount = doubleValue
            }
        }
        
      }
      
      if let rawRegistrationStatus = json["registration_status"] as? Int,
         let registrationStatus = RegistrationStatus(rawValue: rawRegistrationStatus) {
         self.registrationStatus = registrationStatus
      }
      
      if !AppConfiguration.current.isSignupTemplateEnabled {
         self.registrationStatus = .verified
      }
      
      self.signupTemplate = signupTemplate
      self.reviewMessage = reviewMessage
      
      UserDefaults.standard.set(appAccessToken, forKey: USER_DEFAULT.accessToken)
      UserDefaults.standard.set((firstName ?? "") + " " + (lastName ?? ""), forKey: USER_DEFAULT.vendorName)
      UserDefaults.standard.set(email, forKey: USER_DEFAULT.emailAddress)
      UserDefaults.standard.set(company, forKey: USER_DEFAULT.companyName)
      UserDefaults.standard.set(address, forKey: USER_DEFAULT.address)
      UserDefaults.standard.set(phoneNo, forKey: USER_DEFAULT.phoneNumber)
   }
   
   // MARK: - Methods
   func getCreditsDescription() -> String {
      let creditsInInt = Int(Double(credits) ?? 0)
      let creditString = creditsInInt == 0 ? TEXT.NO : creditsInInt.description
      
      let creditSuffix: String
      if creditsInInt == 0 || creditsInInt > 1 {
         creditSuffix = "Credits"
      } else {
         creditSuffix = "Credit"
      }
      
      return creditString + " " + creditSuffix
   }
   
   
   func sendReferralCodeToServer(referralCode: String, completion: Completion) {
      guard let params = getParamsForSendingreferalCode(referralCode: referralCode) else {
         completion?(false)
         return
      }
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_NAME.sendReferral) { (responseObject, _, _, statusCode) in
         
         if let response = responseObject as? [String: Any],
            let data = response["data"] as? [String: Any],
            let newCredits = data["new_credits"] {
            let creditsInInt = Int("\(newCredits)") ?? 0
            self.credits = creditsInInt.description
         }
         
         if statusCode == .some(STATUS_CODES.SHOW_DATA) {
            completion?(true)
         } else {
            completion?(false)
         }
      }
   }
   
   private func getParamsForSendingreferalCode(referralCode: String) -> [String: Any]? {
      guard let accessToken = self.accessToken,
         let appAccessToken = self.appAccessToken,
         let userId = self.userId else {
            print("UserId or AppAccessToken or accessToken not found")
            return nil
      }
      
      return [
         "user_id": userId,
         "access_token": accessToken,
         "app_access_token": appAccessToken,
         "device_token": APIManager.sharedInstance.getDeviceToken(),
         "app_version": APP_VERSION,
         "app_device_type": APP_DEVICE_TYPE,
         "referral_code": referralCode
      ]
   }
   
   
   // MARK: - Type Methods
   static func logInWith(data: [String: Any]) {
      
      if let details = data["vendor_details"] as? [String:Any] {
         let signupTemplate = SignupTemplate(data: data)
         let reviewMessage = (data["vendor_signup_info"] as? String) ?? ""
         let vendorDetails = Vendor(json: details, signupTemplate: signupTemplate, reviewMessage: reviewMessage)
         Vendor.current = vendorDetails
         Vendor.userHasLoggedIntoAppAtLeastOnce()
         
         CartPersistencyManager.shared = CartPersistencyManager()
         
         
         if let value = data["welcome_pop_up"] as? String {
            Vendor.current?.welcomePopUpMessage = value
         } else {
            Vendor.current?.welcomePopUpMessage = ""
         }
         
      }
      
      if AppConfiguration.current.isFuguEnabled {
         intializeFugu()
      }
      
      
      if let typeCategories = data["categories"] as? [Any] {
         Singleton.sharedInstance.typeCategories.removeAll()
         print(typeCategories)
         for i in typeCategories{
            if let data = i as? [String:Any]{
               Singleton.sharedInstance.typeCategories.append(CarTypeModal(json:data))
            }
         }
      }
      if let formDetails = data["formSettings"] as? [AnyObject] {
         if let details = formDetails[0] as? [String:Any] {
            Singleton.sharedInstance.formDetailsInfo = FormDetails(json: details)
         }
      }
      if let userOptions = data["userOptions"] as? [String:Any] {
         Singleton.sharedInstance.customFieldOptionsPickup = userOptions
      }
      if let userOptions = data["deliveryOptions"] as? [String:Any] {
         Singleton.sharedInstance.customFieldOptionsDelivery = userOptions
      }
      
      if let value = data["vendorPromos"] as? [String:Any] {
         Singleton.sharedInstance.getPromoCodeArray(data: value)
      }
      
      if let value = data["new_user_id"] as? Int{
         Vendor.current?.userId = value
      }
      
      if let newDeviceType = data["ios_device_type"] as? Int{
         logEvent(label: "user_sign_up_demo")
         UserDefaults.standard.set("\(newDeviceType)", forKey: "deviceType")
      }
      
      Singleton.sharedInstance.isSignedIn = true
   }
   
   private static func intializeFugu() {

    if APP_STORE.value == 0 {
        FuguConfig.shared.switchEnvironment(.dev)
    } else {
         FuguConfig.shared.switchEnvironment(.live)
    }
      FuguConfig.shared.setCredential(withToken: AppConfiguration.current.FuguToken, referenceId: Vendor.current!.userId!, appType: "2")
      
      let vendorId = "cust" + Vendor.current!.id!.description
      let fuguUserDetail = FuguUserDetail(fullName: Vendor.current!.fullName, email: Vendor.current!.email!, phoneNumber: Vendor.current!.phoneNo!, userUniqueKey: vendorId)
      
      FuguConfig.shared.updateUserDetail(userDetail: fuguUserDetail)
    
    
   }
   
   static func getNewVendorDetail(completion: @escaping (Vendor?) -> Void) {
      let params = getParamsToLoginViewAccessToken()
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: API_NAME.loginAccessToken) { (responseObject, _, _, statusCode) in
         
         guard let response = responseObject as? [String: Any],
            let data = response["data"] as? [String: Any],
            let details = data["vendor_details"] as? [String:Any] else {
               completion(nil)
               return
         }
         
         let signupTemplate = SignupTemplate(data: data)
         let reviewMessage = (data["vendor_signup_info"] as? String) ?? ""
         let vendor = Vendor(json: details, signupTemplate: signupTemplate, reviewMessage: reviewMessage)
         
         completion(vendor)
      }
   }
   
   private static func getParamsToLoginViewAccessToken() -> [String: Any] {
    let coordinate = LocationTracker.sharedInstance.myLocation.coordinate
      let params = [
         "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
         "device_token": APIManager.sharedInstance.getDeviceToken(),
         "latitude": "\(coordinate.latitude)",
         "longitude": "\(coordinate.longitude)",
         "app_version": APP_VERSION,
         "app_device_type": APP_DEVICE_TYPE
         ] as [String : Any]
      
      return params
   }
   
   static func checkUserWithEmailExists(email: String, completion: Completion) {
      let params = getParamsToCheckEmailExists(email: email)
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlertInDefaultCase: false, para: params, extendedUrl: API_NAME.checkEmailRegistered) { (responseObject, _, _, statusCode) in
         if statusCode == .some(STATUS_CODES.SHOW_DATA) {
            completion?(true)
         } else if statusCode == .some(STATUS_CODES.BAD_REQUEST) {
            completion?(false)
         }
      }
   }
   
   private static func getParamsToCheckEmailExists(email: String) -> [String: Any] {
      return [
         "device_token": APIManager.sharedInstance.getDeviceToken(),
         "app_version": APP_VERSION,
         "app_device_type": APP_DEVICE_TYPE,
         "email": email
      ]
   }
   
   static func hasSignedUpBefore() -> Bool {
      if let flag = UserDefaults.standard.value(forKey: "hasUserSignedUpBefore") as? Bool {
         return flag
      }
      
      return false
   }
   
   static func userHasLoggedIntoAppAtLeastOnce() {
      UserDefaults.standard.set(true, forKey: "hasUserSignedUpBefore")
   }
   
}
