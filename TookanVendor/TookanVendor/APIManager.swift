//
//  APIManager.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 17/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class APIManager: NSObject {
    static let sharedInstance = APIManager()
    
    
    func getDeviceToken() -> String {
        if let deviceToken = UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) {
            return deviceToken as! String
        }
        return "no device token"
    }
    
    func serverCall(baseURl:String = (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String),apiName:String,params: [String : AnyObject]?,httpMethod:String,receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        
        print(params ?? "")
        
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(baseUrl:baseURl,apiName, params: params! , httpMethod: httpMethod, isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async {
                    print(response)
                if(succeeded){
                    if let status = response["status"] as? Int {

                        switch(status) {
                        case STATUS_CODES.SHOW_DATA:
                            DispatchQueue.main.async(execute: { () -> Void in
                                receivedResponse(true, response)
                                logEvent(label: "api_success")
                            })
                            break
                            
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            Singleton.sharedInstance.logoutButtonAction()
                            Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                            receivedResponse(false, response)
                             logEvent(label: "api_failure")
                            break
                            
                        case STATUS_CODES.SHOW_MESSAGE:
                            receivedResponse(false, response)
                            logEvent(label: "api_failure")
                            break
                            
                        default:
                            receivedResponse(false, response)
                            logEvent(label: "api_failure")
                        }
                    } else {
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        logEvent(label: "api_failure")
                    }
                } else {
                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    logEvent(label: "api_failure")
                }
            }
            }
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
            logEvent(label: "api_failure")
        }
    }
    
    /*------------ Signup ---------------*/
   func signupRequest(_ firstName:String, contact:String, email:String, password:String,isFacebook:Bool = false,id:String = "", countryCode: String, continentCode: String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
   
      let ipConfig = [
         "country_code": countryCode,
         "continent_code": continentCode
      ]
   
        var params = [
            "first_name": firstName,
            "last_name": "",
            "phone_no": contact,
            "email": email,
            "company": "",
            "lat": "0.0",
            "lng": "0.0",
            "password": password,
            "domain_name": "",
            "device_token": self.getDeviceToken(),
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE,
            "ipConfig": ipConfig,
            "timezone":Singleton.sharedInstance.getTimeZone()
            ] as [String : Any]
        if isFacebook == true {
            params["social_token"] = id
        }
        
        
      
         HTTPClient.makeConcurrentConnectionWith(method: .POST, showActivityIndicator: false, para: params, extendedUrl: API_NAME.signup) { (responseObject, error, _, statusCode) in
            
            guard let response = responseObject as? [String: Any],
               statusCode == STATUS_CODES.SHOW_DATA else {
                  receivedResponse(false, ["message": (error?.localizedDescription ?? ERROR_MESSAGE.SERVER_NOT_RESPONDING)])
                  return
            }
            
            receivedResponse(true, response)
         }
         
         
   }
   
         
    
    /*------------ Login through email ---------------*/
    func loginRequest(_ email:String, password:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        let params = [
            "email": email,
            "latitude": "0.0",
            "longitude": "0.0",
            "password": password,
            "device_token": self.getDeviceToken(),
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE
            ] as [String : Any]
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showActivityIndicator: false, para: params, extendedUrl: API_NAME.loginEmail) { (responseObject, error, _, statusCode) in
         
         guard let response = responseObject as? [String: Any],
          statusCode == STATUS_CODES.SHOW_DATA else {
            receivedResponse(false, ["message": (error?.localizedDescription ?? ERROR_MESSAGE.SERVER_NOT_RESPONDING)])
            return
         }
         
         receivedResponse(true, response)
      }
      
    }
    
    /*------------ Login via AccessToken ---------------*/
    func loginRequestWithAccessToken(_ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        let coordinate = LocationTracker.sharedInstance.myLocation.coordinate
        let params = [
            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
            "device_token": self.getDeviceToken(),
            "latitude": "\(coordinate.latitude)",
            "longitude": "\(coordinate.longitude)",
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE
            ] as [String : Any]
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(API_NAME.loginAccessToken, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async {
                    if(succeeded){
                        print(response)
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                DispatchQueue.main.async(execute: { () -> Void in
                                    receivedResponse(true, response)
                                })
                                break
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                Singleton.sharedInstance.logoutButtonAction()
                                Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                               // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                                receivedResponse(false, response)
                            case STATUS_CODES.SHOW_MESSAGE:
                               // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                                receivedResponse(false, response)
                                break
                                
                            default:
                              //  Singleton.sharedInstance.showAlert(response["message"] as! String! ?? "")
                                receivedResponse(false, response)
                            }
                        } else {
                            //Singleton.sharedInstance.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
                            receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    } else {
                       // Singleton.sharedInstance.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            }
        } else {
            //Singleton.sharedInstance.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    /*------------ Create Task  ---------------*/
    func createTaskHit(url:String = API_NAME.createTask ,autoAssignment:Int, customerAddress:String, customerEmail:String, customerPhone:String, customerUsername:String, latitude:String, longitude:String, jobDeliveryDatetime:String, hasDelivery:String, hasPickup:String, jobDescription:String, jobPickupAddress:String, jobPickupEmail:String, jobPickupPhone:String, jobPickupDateTime:String, jobPickupLatitude:String, jobPickupLongitude:String, jobPickupName:String,domainName:String, layoutType:Int,vendorId:Int, metadata:[Any], pickupMetadata:[Any],pickupCustomFieldTemplate:String, customFieldTemplate:String,withPayment:Bool = false,ammount:String = "0",currency:String = "",payment_method:String = "",card_id:String = "",tags:String = "",tipAmount : String = "0",tipType:String = Singleton.sharedInstance.formDetailsInfo.tipDetails.type.rawValue, _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var params = [String:Any]()
        params = [
            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
            "app_access_token":Vendor.current!.appAccessToken!,
            "app_version":APP_VERSION,
            "device_token":self.getDeviceToken(),
            "auto_assignment":"\(autoAssignment)",
            "customer_address":customerAddress,
            "customer_email":customerEmail,
            "customer_phone":customerPhone,
            "customer_username":customerUsername,
            "job_delivery_datetime":jobDeliveryDatetime,
            "latitude":latitude,
            "longitude":longitude,
            "has_delivery":"\(hasDelivery)",
            "has_pickup":"\(hasPickup)",
            "job_description":jobDescription,
            "job_pickup_address":jobPickupAddress,
            "job_pickup_email":jobPickupEmail,
            "job_pickup_phone":jobPickupPhone,
            "job_pickup_datetime":jobPickupDateTime,
            "job_pickup_latitude":jobPickupLatitude,
            "job_pickup_longitude":jobPickupLongitude,
            "job_pickup_name":jobPickupName,
            "domain_name":domainName,
            "layout_type":"\(layoutType)",
            "app_device_type":APP_DEVICE_TYPE,
            "vendor_id":"\(vendorId)",
            "timezone":Singleton.sharedInstance.getTimeZone(),
            "meta_data":metadata,
            "pickup_meta_data":pickupMetadata,
            "pickup_custom_field_template":pickupCustomFieldTemplate,
            "custom_field_template":customFieldTemplate,
            "user_id" : Vendor.current!.userId!,
            "form_id" : Singleton.sharedInstance.formDetailsInfo.form_id!,
            "vertical" : Singleton.sharedInstance.formDetailsInfo.verticalType.rawValue,
            "tip_amount":tipAmount,
            "tip_type" : tipType,
            ]
      let fleetId = Singleton.sharedInstance.createTaskDetail.fleetId
      if !fleetId.isEmpty {
         params["fleet_id"] = fleetId
      }
        
        if withPayment == true{
            params["amount"] = ammount
            params["currency_id"] = currency
            params["payment_method"] = payment_method
            params["card_id"] = card_id
            params["access_id"] = Singleton.sharedInstance.promoAccessId
            params["promo_id"] = Singleton.sharedInstance.promoId
        }
      
       if Singleton.sharedInstance.formDetailsInfo.verticalType == .taxi {
         params["tags"] = tags
      }
      
        print(params)
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(url, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
               DispatchQueue.main.async {
                print(response)
                if(succeeded){
                    if let status = response["status"] as? Int {
                        switch(status) {
                        case STATUS_CODES.SHOW_DATA:
                            DispatchQueue.main.async(execute: { () -> Void in
                                receivedResponse(true, response)
                            })
                            break
                            
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            Singleton.sharedInstance.logoutButtonAction()
                            Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                            receivedResponse(false, response)
                            
                        case STATUS_CODES.SHOW_MESSAGE:
                            receivedResponse(false, response)
                            break
                            
                        default:
                            receivedResponse(false, response)
                        }
                    } else {
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                } else {
                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                }
            }
        }
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    
    func checkoutHit(metaData:[Any],pickUpMetaData:[Any],pickUpMetaDataName:String,metaDataTemplateName:String,hasPickup:String,hasDelivery:String, _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()){
        APIManager.sharedInstance.createTaskHit(autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                hasDelivery: hasDelivery,
                                                hasPickup: hasPickup,
                                                jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                                                jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                                                jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                                                jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                                                jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                                                jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                                                jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                                                jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                                                domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                                                layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                vendorId: Vendor.current!.id!,
                                                metadata:metaData,
                                                pickupMetadata:pickUpMetaData,
                                                pickupCustomFieldTemplate:pickUpMetaDataName, customFieldTemplate:metaDataTemplateName)
        { (isSuccess, response) in
           receivedResponse(isSuccess, response)
        }
    }
    
    
    
    
    func getPayment(metaData:[Any],pickUpMetaData:[Any],pickUpMetaDataName:String,metaDataTemplateName:String,hasPickup:String,hasDelivery:String, _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()){
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.createTaskHit(url:API_NAME.getPayment,autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                hasDelivery: "\(hasDelivery)",
                                                    hasPickup: "\(hasPickup)",
                                                jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                                                jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                                                jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                                                jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                                                jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                                                jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                                                jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                                                jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                                                domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                                                layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                vendorId: Vendor.current!.id!,
                                                metadata:metaData,
                                                pickupMetadata:pickUpMetaData,
                                                pickupCustomFieldTemplate:pickUpMetaDataName,
                                                customFieldTemplate:metaDataTemplateName,
                                                withPayment:true,
                                                payment_method:"\(Singleton.sharedInstance.formDetailsInfo.payementMethod)")
        { (isSuccess, response) in
            receivedResponse(isSuccess, response)
        }
    }
    
    
    
    
    func createTaxiTaskHit(url:String = API_NAME.createTask ,autoAssignment:Int, customerAddress:String, customerEmail:String, customerPhone:String, customerUsername:String, latitude:String, longitude:String, jobDeliveryDatetime:String, hasDelivery:Int, hasPickup:Int, jobDescription:String, jobPickupAddress:String, jobPickupEmail:String, jobPickupPhone:String, jobPickupDateTime:String, jobPickupLatitude:String, jobPickupLongitude:String, jobPickupName:String,domainName:String, layoutType:Int,vendorId:Int, metadata:[Any], pickupMetadata:[Any],pickupCustomFieldTemplate:String, customFieldTemplate:String,withPayment:Bool = false,ammount:String = "0",currency:String = "",payment_method:String = "",card_id:String = "",tags:String = "" ,tipAmount : String = "0",tipType:String = "0", _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var params = [String:Any]()
        params = [
            "access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
            "device_token":self.getDeviceToken(),
            "app_version":APP_VERSION,
            "app_access_token":Vendor.current!.appAccessToken!,
            "app_device_type":APP_DEVICE_TYPE,
            "user_id" : "\(Vendor.current!.userId!)",
            "vendor_id":"\(vendorId)",
            "has_delivery":"\(hasDelivery)",
            "has_pickup":"\(hasPickup)",
            "job_pickup_address":jobPickupAddress,
            "job_pickup_email":jobPickupEmail,
             "job_pickup_phone":jobPickupPhone,
            "job_pickup_datetime":jobPickupDateTime,
            "job_pickup_latitude":jobPickupLatitude,
            "job_pickup_longitude":jobPickupLongitude,
            "job_pickup_name":jobPickupName,
             "job_description":jobDescription,
            "auto_assignment":"\(autoAssignment)",
            "domain_name":domainName,
            "layout_type":"\(layoutType)",
            "timezone":Singleton.sharedInstance.getTimeZone(),
            "pickup_meta_data":pickupMetadata,
            "pickup_custom_field_template":pickupCustomFieldTemplate,
             "vertical" : Singleton.sharedInstance.formDetailsInfo.verticalType.rawValue,
            "tip_amount":tipAmount,
            "tip_type" : tipType
        ]
        
        if withPayment == true{
            params["amount"] = ammount
            params["currency_id"] = currency
            params["payment_method"] = payment_method
            params["card_id"] = card_id
            params["access_id"] = Singleton.sharedInstance.promoAccessId
            params["promo_id"] = Singleton.sharedInstance.promoId
        }

        if Singleton.sharedInstance.formDetailsInfo.verticalType == .taxi {
            params["tags"] = tags
            params["form_id"] = Singleton.sharedInstance.formDetailsInfo.form_id!
        }
        
        
        
//
//        addPickupCustomField(builder, userData.getData().getUserOptions());
//        
//        [3:48]
//        builder.add("pickup_custom_field_template", userData.getData().getUserOptions().getTemplate()).add("pickup_meta_data", getMetaData(userOptions));
        
        
        
        
        
        
        
        
        print(url)
        
        
        
        print(params)
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(url, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async {
                    print(response)
                    if(succeeded){
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                DispatchQueue.main.async(execute: { () -> Void in
                                    receivedResponse(true, response)
                                })
                                break
                                
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                Singleton.sharedInstance.logoutButtonAction()
                                Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                                receivedResponse(false, response)
                                
                            case STATUS_CODES.SHOW_MESSAGE:
                                receivedResponse(false, response)
                                break
                                
                            default:
                                receivedResponse(false, response)
                            }
                        } else {
                            receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    } else {
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            }
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    
    
    
    
    /*------------ Latitude and Longitude from Place Id --------------*/
    func getLatLongFromPlaceId(_ placeId:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])-> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            let session = URLSession.shared
            let urlString =  "https://maps.googleapis.com/maps/api/place/details/json?input=bar&placeid=\(placeId)"
            print(urlString)
            //&key=\(KEYS.GOOGLE_MAPS_API_KEY)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            session.dataTask(with: URL(string: urlString)!) {data, response, error in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                var encodedRoute: [String:Any]?
                do {
                    if let json = (try JSONSerialization.jsonObject(with: data!, options:[])) as? [String:Any] {
                        encodedRoute = json
                    }
                    DispatchQueue.main.async {
                        if(encodedRoute != nil) {
                            receivedResponse(true, encodedRoute!)
                        } else {
                            receivedResponse(false, [:])
                        }
                    }
                } catch {
                    receivedResponse(false, [:])
                }
                }.resume()
        } else {
            Singleton.sharedInstance.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
            receivedResponse(false, [:])
        }
    }
    
    /*------------ Get Address from Latitude, Longitude --------------*/
    func getAddressFromLatLong(_ latLong:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])-> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            let session = URLSession.shared
            let urlString =  "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latLong)"
            print(urlString)
            //&key=\(KEYS.GOOGLE_MAPS_API_KEY)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            session.dataTask(with: URL(string: urlString)!) {data, response, error in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                var encodedRoute:[String:Any]?
                do {
                    if let json = (try JSONSerialization.jsonObject(with: data!, options:[])) as? [String:Any] {
                        encodedRoute = json
                    }
                    DispatchQueue.main.async {
                        if(encodedRoute != nil) {
                            receivedResponse(true, encodedRoute!)
                        } else {
                            receivedResponse(false, [:])
                        }
                    }
                } catch {
                    receivedResponse(false, [:])
                }
                }.resume()
        } else {
            Singleton.sharedInstance.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
            receivedResponse(false, [:])
        }
    }
    
    /*----------------- Auto complete API ------------*/
    func autoCompleteSearch(_ searchKeyword:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])->()) {
      
      let formattedSearchKey = searchKeyword.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
      let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(formattedSearchKey)&radius=500&key=\(KEYS.GOOGLE_BROWSER_KEY)"
      
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
      
      HTTPClient.shared.makeSingletonConnectionWith(method: .GET, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: nil, baseUrl: urlString, extendedUrl: "") { (responseObject, _, _, statusCode) in
         UIApplication.shared.isNetworkActivityIndicatorVisible = false

         guard statusCode == .some(STATUS_CODES.SHOW_DATA),
            let response = responseObject as? [String: Any] else {
         receivedResponse(false, [:])
         return
         }
         receivedResponse(true, response)
      }
      
    }
    
    /*----------------- Edit Profile API ------------*/
    func editProfile(apiName:String, params: [String : AnyObject]?,httpMethod:String, isImage:Bool, vendorImage:UIImage!, imageKey:String, isNeedToShowActivityIndictor:Bool, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        print(params ?? "")
        print(isImage)
        if IJReachability.isConnectedToNetwork() == true {
            if isNeedToShowActivityIndictor == true {
                ActivityIndicator.sharedInstance.showActivityIndicator()
            }
            uploadingMultipleTask(apiName, params: params!, isImage: isImage, imageData: [vendorImage], imageKey: imageKey, receivedResponse: { (isSucceeded, response) in
                DispatchQueue.main.async {
                    if isNeedToShowActivityIndictor == true {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                    }
                    if(isSucceeded == true){
                        print(response)
                        if let status = response["status"] as? Int {
                            
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                DispatchQueue.main.async(execute: { () -> Void in
                                    receivedResponse(true, response)
                                })
                                break
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                Singleton.sharedInstance.logoutButtonAction()
                                Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                               // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                                receivedResponse(false, response)
                                break
                                
                            case STATUS_CODES.SHOW_MESSAGE:
                                //Singleton.sharedInstance.showAlert(response["message"] as! String! ?? "")
                                receivedResponse(false, response)
                                break
                            default:
                                //Singleton.sharedInstance.showAlert(response["message"] as! String! ?? "")
                                receivedResponse(false, response)
                            }
                        } else {
                           // Singleton.sharedInstance.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
                            receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    } else {
                        //Singleton.sharedInstance.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            })
        } else {
          //  Singleton.sharedInstance.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    /*---------- Upload Image to Server ------------*/
    func uploadImageToServer(_ refImage:UIImage, receivedResponse:@escaping (_ succeeded:Bool,_ response:[String:Any])-> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            uploadingMultipleTask("upload_reference_images", params: [:], isImage: true, imageData: [refImage], imageKey: "ref_image", receivedResponse: { (succeeded, response) -> () in
                print(response)
                DispatchQueue.main.async {
                    
                
                if(succeeded == true) {
                    switch(response["status"] as! Int) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: { () -> Void in
                            receivedResponse(true, response)
                        })
                        break
                    case STATUS_CODES.SHOW_MESSAGE:
                        //Singleton.sharedInstance.showAlert(response["message"] as! String!)
                        receivedResponse(false, ["message":response["message"] as! String!])
                        break
                        
                    default:
                      //  Singleton.sharedInstance.showAlert(response["message"] as! String!)
                        receivedResponse(false, ["message":response["message"] as! String!])
                    }
                } else {
                   // Singleton.sharedInstance.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                }
                }
            })
            
        } else {
           // Singleton.sharedInstance.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }

//FACEBOOK API HIT
    typealias faceBookCompletion = (_ firstName:String,_ lastName:String,_ email:String,_ id:String)->Void
    
    
    func CommonFunctionForFaceBookLogin(viewControler:UIViewController,fbDataOnSucess:@escaping faceBookCompletion,onfaLiure:@escaping (_ error:NSError) -> Void){
        var firstName = String()
        var lastName = String()
        var emailId = String()
        var id = String()
        if IJReachability.isConnectedToNetwork() == true{
            if FBSDKAccessToken.current() != nil{
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
            }
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.loginBehavior = FBSDKLoginBehavior.browser
            fbLoginManager.logIn(withReadPermissions: ["email"], from: viewControler) { (result, error) -> Void in
                switch error{
                case nil:
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    print(fbloginresult.isCancelled)
                    switch fbloginresult.isCancelled{
                    case false:
                        switch FBSDKAccessToken.current(){
                        case nil:
                            break
                        default:
//                            ViewControllerUtils.sharedInstance.showActivityIndicator(uiView: viewControler.view)
                            ActivityIndicator.sharedInstance.showActivityIndicator()
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                                ActivityIndicator.sharedInstance.hideActivityIndicator()
                                if (error == nil){
                                    if let newResult = result as? Dictionary<String,AnyObject>{
                                        self.genricParsing(value: newResult["first_name"], objectToStore: &firstName)
                                        self.genricParsing(value: newResult["id"], objectToStore: &id)
                                        self.genricParsing(value: newResult["last_name"], objectToStore: &lastName)
                                        self.genricParsing(value: newResult["email"], objectToStore: &emailId)
                                        fbDataOnSucess(firstName, lastName, emailId, id)
                                    }
//                                    ViewControllerUtils.sharedInstance.hideActivityIndicator(uiView: viewControler.view)
                                    return
                                }else{
//                                    ViewControllerUtils.sharedInstance.hideActivityIndicator(uiView: viewControler.view)
                                    onfaLiure(error as! NSError)
                                    return
                                }
                            })
                        }
                        break
                    case true:
                        break
                    }
                default:
                    print(error)
                    break
                }
            }
        }
    }
    
    private func genricParsing<T>(value:AnyObject?, objectToStore:inout T){
        print(value)
        if let value = value as? T{
            objectToStore = value
        }
    }
    
    func syncApiHit(_ url:String, params: [String:Any]) -> (Bool,[String:Any]?) {
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) + urlString!)!)
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        if IJReachability.isConnectedToNetwork() == true {
            if let json = URLSession.requestSynchronousJSON(request as URLRequest) {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                // print("jsonData" + "\(json)")
                return (true, json as! [String:Any])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return (false, ["message":"Invalid Access"])
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            return (false, ["message":"No Internet Connection"])
        }
    }
    
    
    
    //MARK : AgentRequest
    func AgentRequest(_ access_token: String,_ user_id:String,_ app_access_token:String,_ form_id:String,_ tags:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        let params = [
            //"email": email,
            "latitude": "0.0",
            "longitude": "0.0",
           // "password": password,
            "device_token": self.getDeviceToken(),
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE
            ] as [String : Any]
        print(params)
        //  Singleton.sharedInstance.showAlert("\(params)")
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(API_NAME.loginEmail, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async {
                    if(succeeded){
                        print(response)
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                DispatchQueue.main.async(execute: { () -> Void in
                                    receivedResponse(true, response)
                                })
                                break
                                
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                Singleton.sharedInstance.logoutButtonAction()
                                Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                                receivedResponse(false, response)
                                break
                                
                            case STATUS_CODES.SHOW_MESSAGE:
                                receivedResponse(false, response)
                                break
                                
                            default:
                                receivedResponse(false, response)
                            }
                        } else {
                            receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    } else {
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            }
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
}
