//
//  TermsAndConsitionViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 21/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class TermsAndConsitionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = Bundle.main.url(forResource: "terms", withExtension:"html")
        let request = URLRequest(url: url!)
        web.loadRequest(request)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
      
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var web: UIWebView!
}

