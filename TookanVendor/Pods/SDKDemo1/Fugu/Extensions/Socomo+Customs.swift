//
//  Socomo+Customs.swift
//  Fugu
//
//  Created by Gagandeep Arora on 08/09/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation
import UIKit

//@IBDesignable
class So_UIView: UIView {
    
}

//@IBDesignable
class So_CustomLabel: UILabel {
    
}

//@IBDesignable
class So_TableViewCell: UITableViewCell {
    
}

//@IBDesignable
class So_UIImageView: UIImageView {
    @IBInspectable
    var ImageColor: UIColor {
        set {
            self.image = self.image?.withRenderingMode(.alwaysTemplate)
            self.tintColor = newValue
            //self.font = UIFont.boldProximaNova(withSize: newValue)
        }
        get {
            return .clear
        }
    }
    
    func so_setImage(withURL url: NSURL, placeHolder: UIImage? = nil) {
        self.so_setImage(withURL: url, placeHolder: placeHolder, completion: nil)
    }
    
    func so_setImage(withURL url: NSURL, placeHolder: UIImage? = nil, completion: ((_ image: UIImage?, _ error: Error?) -> Void)?) {
        if let cachedImage = imageCacheFugu.object(forKey: url) as? UIImage {
            self.image = cachedImage
            if completion != nil {
                completion!(cachedImage, nil)
            }
        } else {
            if placeHolder != nil {
                self.image = placeHolder!
            }
            
            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                if error == nil {
                    DispatchQueue.main.async(execute: {
                        // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                        if let successData = data, let downloadedImage = UIImage(data: successData) {
                            imageCacheFugu.setObject(downloadedImage, forKey: url)
                            self.image = downloadedImage
                            if completion != nil {
                                completion!(downloadedImage, nil)
                            }
                        }
                    })
                } else {
                    if completion != nil {
                        completion!(nil, error)
                    }
                    
                }
            }).resume()
        }
    }    
}
