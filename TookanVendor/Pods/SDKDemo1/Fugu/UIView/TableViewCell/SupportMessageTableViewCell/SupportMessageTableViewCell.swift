//
//  SupportMessageTableViewCell.swift
//  Fugu
//
//  Created by Gagandeep Arora on 29/09/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit

class SupportMessageTableViewCell: UITableViewCell {
    @IBOutlet weak var shadowView: So_UIView!
    @IBOutlet var bgView: UIView!
    @IBOutlet weak var supportMessageTextView: UITextView!
    @IBOutlet var dateTimeLabel: UILabel!
    //@IBOutlet var supportImageView: UIImageView!
    //@IBOutlet weak var heightImageView: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        supportMessageTextView.backgroundColor = .clear
        supportMessageTextView.textContainer.lineFragmentPadding = 0
        supportMessageTextView.textContainerInset = .zero
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        adjustShadow()
    }
    
    func setupBoxBackground(messageType: Int) {
        bgView.backgroundColor = FuguConfig.shared.theme.incomingChatBoxColor
    }
    
    func adjustShadow() {
        // shadowView.layoutIfNeeded()
        bgView.layoutIfNeeded()
        
        // shadowView.layer.cornerRadius = self.bgView.layer.cornerRadius
        
        // shadowView.backgroundColor = //UIColor.makeColor(red: 234, green: 234, blue: 234, alpha: 0.3)
        //self.shadowView.roundCorner(cornerRect: .allCorners, cornerRadius: self.shadowView.layer.cornerRadius)
    }
    
    func resetPropertiesOfSupportCell() {
        selectionStyle = .none
        supportMessageTextView.text = ""
        supportMessageTextView.attributedText = nil
        supportMessageTextView.isEditable = false
        supportMessageTextView.dataDetectorTypes = UIDataDetectorTypes.all
        supportMessageTextView.backgroundColor = .clear
        
        dateTimeLabel.text = ""
        dateTimeLabel.font = FuguConfig.shared.theme.dateTimeFontSize
        dateTimeLabel.textAlignment = .left
        dateTimeLabel.textColor = FuguConfig.shared.theme.incomingMsgDateTextColor
        
        bgView.layer.cornerRadius = 10
        bgView.backgroundColor = FuguConfig.shared.theme.incomingChatBoxColor
        bgView.layer.borderWidth = FuguConfig.shared.theme.chatBoxBorderWidth
        bgView.layer.borderColor = FuguConfig.shared.theme.chatBoxBorderColor.cgColor
        
//        supportImageView.layer.cornerRadius = 4.0
//        supportImageView.image = nil
//        heightImageView.constant = 0
    }
    
    func configureCellOfSupportIncomingCell(resetProperties: Bool,
                                            attributedString: NSMutableAttributedString,
                                            channelId: Int,
                                            chatMessageObject: [String: Any]) -> SupportMessageTableViewCell {
        if resetProperties { resetPropertiesOfSupportCell() }
        
        let messageType = (chatMessageObject["message_type"] as? Int) ?? 1
        setupBoxBackground(messageType: messageType)

        if  channelId > 0,
            let dateTime = chatMessageObject["date_time"] as? String,
            dateTime.characters.count > 0 {
            let timeOfMessage = dateTime.changeDateFormat(toFormat: "h:mm a")
            dateTimeLabel.text = "\t" + "\(timeOfMessage)"
        }
        
        supportMessageTextView.attributedText = attributedString
        
        return self
    }
}

private extension String {
    func changeDateFormat(sourceFormat: String? = "yyyy-MM-dd HH:mm:ss",
                          toFormat: String) -> String {
        var dateString = self.replacingOccurrences(of: ".000Z", with: "")
        if dateString.contains(".") == true {
            dateString = (dateString.components(separatedBy: "."))[0]
        }
        dateString = dateString.replacingOccurrences(of: "T", with: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = toFormat
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        dateFormatter.timeZone = NSTimeZone.local
        if date != nil { return dateFormatter.string(from: date!) }
        return ""
    }
}
