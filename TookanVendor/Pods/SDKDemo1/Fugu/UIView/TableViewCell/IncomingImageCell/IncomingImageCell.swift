//
//  IncomingImageCell.swift
//  Fugu
//
//  Created by Gagandeep Arora on 31/07/17.
//  Copyright © 2017 Socomo Technologies Private Limited. All rights reserved.
//

import UIKit

class IncomingImageCell: So_TableViewCell {
    
    @IBOutlet weak var shadowView: So_UIView!
    @IBOutlet weak var mainContentView: So_UIView!
    @IBOutlet weak var thumbnailImageView: So_UIImageView!
    @IBOutlet weak var timeLabel: So_CustomLabel!
    @IBOutlet weak var customIndicator: So_UIImageView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    let rotationAnimationKey = String().randomString(OfLength: 36)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    deinit {
        stopIndicatorAnimation()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        adjustShadow()
    }
    
    func setupBoxBackground(messageType: Int) {
        mainContentView.backgroundColor = FuguConfig.shared.theme.incomingChatBoxColor
//        switch messageType {
//        case MessageType.privateNote.rawValue:
//            mainContentView.backgroundColor = UIColor.privateMessageBoxColor
//        default:
//            mainContentView.backgroundColor = UIColor.incomingMessageBoxColor
//        }
    }
    
    func adjustShadow() {
        shadowView.layoutIfNeeded()
        mainContentView.layoutIfNeeded()
        
        shadowView.layer.cornerRadius = mainContentView.layer.cornerRadius
        shadowView.clipsToBounds = true
        shadowView.backgroundColor = FuguConfig.shared.theme.chatBoxBorderColor//UIColor.makeColor(red: 234, green: 234, blue: 234, alpha: 0.3)
        //self.shadowView.roundCorner(cornerRect: .allCorners, cornerRadius: self.shadowView.layer.cornerRadius)
    }
}

//MARK: - Configure
extension IncomingImageCell {
    func resetPropertiesOfIncomingCell() {
        backgroundColor = .clear
        thumbnailImageView.image = nil
        selectionStyle = .none
        timeLabel.text = ""
        mainContentView.layer.cornerRadius = 5
        thumbnailImageView.layer.cornerRadius = mainContentView.layer.cornerRadius
        thumbnailImageView.clipsToBounds = true
    }
    
    func configureIncomingCell(resetProperties: Bool,
                               channelId: Int,
                               chatMessageObject: [String: Any]) -> IncomingImageCell {
        if resetProperties { resetPropertiesOfIncomingCell() }
        
        let messageType = (chatMessageObject["message_type"] as? Int) ?? 1
        setupBoxBackground(messageType: messageType)
        
        if channelId == -1 {}
        else if let dateTime = chatMessageObject["date_time"] as? String, dateTime.characters.count > 0 {
            let timeOfMessage = dateTime.changeDateFormat(toFormat: "h:mm a")
            timeLabel.text = "\t" + "\(timeOfMessage)"
        }
        
        if let thumbnailUrl = chatMessageObject["thumbnail_url"] as? String,
            thumbnailUrl.isEmpty == false,
            let url = NSURL(string: thumbnailUrl) {
            setupIndicatorView(true)
            let placeHolderImage = UIImage(named: "placeholderImg", in: bundle, compatibleWith: nil)
            
            thumbnailImageView.so_setImage(withURL: url,
                                           placeHolder: placeHolderImage,
                                           completion: { (image, error) in
                self.setupIndicatorView(false)
            })
        }
        
        return self
    }
}

//MARK: - HELPERS
extension IncomingImageCell {
    func setupIndicatorView(_ show: Bool) {
        customIndicator.image = UIImage(named: "app_loader_shape",
                                        in: bundle,
                                        compatibleWith: nil)
        
        if show {
            startIndicatorAnimation()
            customIndicator.isHidden = false
        } else {
            stopIndicatorAnimation()
            customIndicator.isHidden = true
        }
    }
    
    func startIndicatorAnimation() {
        startFuguRotation(ofView: customIndicator,
                          forKey: rotationAnimationKey)
    }
    
    func stopIndicatorAnimation() {
        stopFuguRotation(ofView: customIndicator,
                         forKey: rotationAnimationKey)
    }
}

private extension String {
    func randomString(OfLength length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    func changeDateFormat(sourceFormat: String? = "yyyy-MM-dd HH:mm:ss",
                          toFormat: String) -> String {
        var dateString = self.replacingOccurrences(of: ".000Z",
                                                   with: "")
        if dateString.contains(".") == true {
            dateString = (dateString.components(separatedBy: "."))[0]
        }
        dateString = dateString.replacingOccurrences(of: "T",
                                                     with: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateString)
        
        dateFormatter.dateFormat = toFormat
        dateFormatter.timeZone = NSTimeZone.local
        if date != nil { return dateFormatter.string(from: date!) }
        return ""
    }
    
}
