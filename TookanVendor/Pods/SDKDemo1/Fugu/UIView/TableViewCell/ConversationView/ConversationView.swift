//
//  ConversationView.swift
//  Fugu
//
//  Created by Gagandeep Arora on 25/09/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit

enum ChannelStatus: Int { case open = 1, close }

class ConversationView : UITableViewCell {
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var chatTextLabel: UILabel!
    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var placeHolderImageButton: UIButton!
    @IBOutlet var bgView: UIView!
    @IBOutlet weak var channelImageView: So_UIImageView!
    @IBOutlet var unreadCountLabel: UILabel!
    @IBOutlet var unreadCountWidthLabel: NSLayoutConstraint!
    @IBOutlet weak var selectionView: UIView!
    
    override func awakeFromNib() { super.awakeFromNib() }
    
    deinit {}
    
    override func setSelected(_ selected: Bool,
                              animated: Bool) {
        super.setSelected(selected,
                          animated: animated)
    }
}

extension ConversationView {
    func resetPropertiesOfConversationView() {
        selectionStyle = .none
    
        selectionView.backgroundColor = .clear
        
        headingLabel.text = ""
        chatTextLabel.text = ""
        timeLabel.text = ""
        
        channelImageView.image = nil
        channelImageView.layer.masksToBounds = true
        channelImageView.layer.cornerRadius = 15.0
        
        placeHolderImageButton.isHidden = true
        placeHolderImageButton.setImage(nil, for: .normal)
        placeHolderImageButton.layer.cornerRadius = 0.0
        placeHolderImageButton.backgroundColor = .white
        placeHolderImageButton.setTitle("", for: .normal)
        unreadCountLabel.layer.masksToBounds = true
        unreadCountLabel.text = ""
        unreadCountLabel.backgroundColor = .clear
        
        timeLabel.textColor = UIColor.black.withAlphaComponent(0.37)
    }
    
    func configureConversationCell(resetProperties: Bool,
                                   conersationObj: [String: Any]) -> ConversationView {
        if resetProperties == true {
         resetPropertiesOfConversationView()
         
      }
        
        var isOpened = true
        if let channelStatus = conersationObj["channel_status"] as? Int,
            channelStatus == ChannelStatus.close.rawValue,
            let labelId = conersationObj["label_id"] as? Int,
            labelId < 0 {
         isOpened = false
         
      }
        
        bgView.backgroundColor = UIColor.white.withAlphaComponent(isThisChatOpened(opened: isOpened))
        
        headingLabel.textColor = FuguConfig.shared.theme.conversationTitleColor.withAlphaComponent(isThisChatOpened(opened: isOpened))
        
        if let channelName = conersationObj["label"] as? String,
            channelName.characters.count > 0 { headingLabel.text = channelName }
        
        if let unreadCount = conersationObj["unread_count"] as? Int,
            unreadCount > 0 {
            unreadCountLabel.text = "\(unreadCount)"
            unreadCountLabel.backgroundColor = #colorLiteral(red: 0.8666666667, green: 0.09019607843, blue: 0.1176470588, alpha: 1).withAlphaComponent(isThisChatOpened(opened: isOpened))
            unreadCountLabel.layer.cornerRadius = 9.5
        }
        
        channelImageView.image = nil
        channelImageView.alpha = isThisChatOpened(opened: isOpened)
        
        if let channelImage = conersationObj["channel_image"] as? String,
            channelImage.isEmpty == false,
            let url = NSURL(string: channelImage) {
            channelImageView.so_setImage(withURL: url)
        } else if let channelName = conersationObj["label"] as? String,
            channelName.isEmpty == false {
            placeHolderImageButton.alpha = isThisChatOpened(opened: isOpened)
            placeHolderImageButton.isHidden = false
            placeHolderImageButton.setImage(nil,
                                            for: .normal)
            placeHolderImageButton.backgroundColor = .lightGray
            
            var channelNameInitials = channelName
            placeHolderImageButton.setTitle(String(channelNameInitials.remove(at: channelNameInitials.startIndex)).capitalized, for: .normal)
            placeHolderImageButton.layer.cornerRadius = 15.0
        }
        
        chatTextLabel.textColor = FuguConfig.shared.theme.conversationLastMsgColor.withAlphaComponent(isThisChatOpened(opened: isOpened))
        
        if let message = conersationObj["message"] as? String,
            message.isEmpty == false {
            chatTextLabel.text = message.removeNewLine()
        } else {
         chatTextLabel.text = "Attachment: Image"
      }
        
        timeLabel.textColor = FuguConfig.shared.theme.timeTextColor.withAlphaComponent(isThisChatOpened(opened: isOpened))
        
        if let channelId = conersationObj["channel_id"] as? Int,
            channelId == -1 {
            timeLabel.text = ""
        } else if let dateTime = conersationObj["date_time"] as? String {
            timeLabel.text = dateTime.toDate?.toString
        }
        
        return self
    }
    
    func isThisChatOpened(opened: Bool) -> CGFloat {
      return opened ? 1 : 0.5
   }
}
