//
//  Fugu+Enums.swift
//  Fugu
//
//  Created by Gagandeep Arora on 13/10/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation

public enum FuguEnvironment: Int { case live, dev }

enum FuguCredentialType: Int { case defaultType, reseller }

enum TypingMessage: Int { case messageRecieved = 0, startTyping, stopTyping }

enum MessageType: Int {
    case none = 0, normal = 1, assignAgent = 2, privateNote = 3, imageFile = 10
}

enum UserSubscriptionStatus: Int { case notSubscribed = 0, subscribed }

enum ReadUnReadStatus: Int { case none = 0, sent, delivered, read }

enum StatusCodeValue: Int {
    case Authorized_Min = 200
    case Authorized_Max = 300
    case Bad_Request = 400
    case Unauthorized = 401 //INCORRECT_PASSWORD
    case invalidToken = 403 //INVALID_TOKEN
}

enum NotificationType: Int {
    case assigned = 3
    case read_unread = 5
    case read_all = 6
}

enum FuguEndPoints: String {
    case API_PUT_USER_DETAILS = "api/users/putUserDetails"
    case API_GET_CONVERSATIONS = "api/conversation/getConversations"
    case API_GET_MESSAGES = "api/conversation/getMessages"
    case API_CREATE_CONVERSATION = "api/conversation/createConversation"
    case API_GET_MESSAGES_BASED_ON_LABEL = "api/conversation/getByLabelId"
    case API_CLEAR_USER_DATA_LOGOUT = "api/users/userlogout"
    case API_Reseller_Put_User = "api/reseller/putUserDetails"
    case API_UPLOAD_FILE = "api/conversation/uploadFile"
}
