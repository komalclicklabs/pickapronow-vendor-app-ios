//
//  FuguConfig.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/16/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation
import MZFayeClient
import UIKit

@objc public class FuguConfig : NSObject {
    public static var shared = FuguConfig()

    var deviceToken = ""
    var theme = FuguTheme.defaultTheme()
    var userDetail = FuguUserDetail()
    var appSecretKey: String {
        get {
            guard let appSecretKey = UserDefaults.standard.value(forKey: Fugu_AppSecret_Key) as? String
                else { return "" }
            return appSecretKey
        }
        set { UserDefaults.standard.set(newValue, forKey: Fugu_AppSecret_Key) }
    }
    var resellerToken = ""
    var referenceId = -1
    var appType: String?
    var credentialType = FuguCredentialType.defaultType
    var baseUrl = "https://api.fuguchat.com:3002/"//"https://beta-api.fuguchat.com:3001/"//
    var fayeBaseURLString: String { get { return baseUrl + "faye" } }

    var unreadCount: ((_ totalUnread: Int) -> ())?
    
    @available(*, deprecated, message: "To customize UI, use 'backgroundColor' in Class 'FuguTheme' instead")
    open var chatBackgroundColor: UIColor {
        get { return theme.backgroundColor }
        set { theme.backgroundColor = newValue }
    }
    
    let powererdByColor = #colorLiteral(red: 0.4980392157, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
    let FuguColor = #colorLiteral(red: 0.3843137255, green: 0.4901960784, blue: 0.8823529412, alpha: 1)
    let poweredByFont: UIFont = UIFont.systemFont(ofSize: 10.0)
    let FuguStringFont: UIFont = UIFont.systemFont(ofSize: 10.0)

    @available(*, deprecated, message: "To customize UI, use 'headerBackgroundColor' in Class 'FuguTheme' instead")
    open var navigationBackgroundColor: UIColor {
        get { return theme.headerBackgroundColor }
        set { theme.headerBackgroundColor = newValue }
    }

    @available(*, deprecated, message: "To customize UI, use 'headerTextColor' in Class 'FuguTheme' instead")
    open var navigationTitleColor:  UIColor {
        get { return theme.headerTextColor }
        set { theme.headerTextColor = newValue }
    }
    
    @available(*, deprecated, message: "To customize UI, use 'headerText' in Class 'FuguTheme' instead")
    open var navigationTitleText: String {
        get { return theme.headerText }
        set { theme.headerText = newValue }
    }
    
    open let navigationTitleTextAlignMent: NSTextAlignment? = .center
    
    @available(*, deprecated, message: "To customize UI, use 'headerTextFont' in Class 'FuguTheme' instead")
    open var navigationTitleFont: UIFont? {
        set { theme.headerTextFont = newValue }
        get { return theme.headerTextFont }
    }
    
    @available(*, deprecated, message: "To customize UI, use 'leftBarButtonImage' in Class 'FuguTheme' instead")
    open var backButtonImageIcon: UIImage? {
        set { theme.leftBarButtonImage = newValue }
        get { return theme.leftBarButtonImage }
    }
    
    @available(*, deprecated, message: "To customize UI, use 'leftBarButtonFont' in Class 'FuguTheme' instead")
    open var backButtonFont: UIFont? {
        set { theme.leftBarButtonFont = newValue }
        get { return theme.leftBarButtonFont }
    }
    
    @available(*, deprecated, message: "To customize UI, use 'leftBarButtonTextColor' in Class 'FuguTheme' instead")
    open var backButtonTextColor: UIColor {
        set { theme.leftBarButtonTextColor = newValue }
        get { return theme.leftBarButtonTextColor }
    }
    
    @available(*, deprecated, message: "To customize UI, use 'leftBarButtonText' in Class 'FuguTheme' instead")
    open var allConversationBackButtonText: String {
        set { theme.leftBarButtonText = newValue }
        get { return theme.leftBarButtonText }
    }
    
    @available(*, deprecated, message: "To customize UI, use 'leftBarButtonText' in Class 'FuguTheme' instead")
    open var chatScrenBackButtonText: String {
        set { theme.leftBarButtonText = newValue }
        get { return theme.leftBarButtonText }
    }
    
    //MARK: sendButton
    @available(*, deprecated, message: "To customize UI, use 'sendBtnIcon' in Class 'FuguTheme' instead")
    open var sendMessageButtonIcon: UIImage? {
        set { theme.sendBtnIcon = newValue }
        get { return theme.sendBtnIcon }
    }

    var pushInfo = [String: Any]()
    fileprivate var timerToCheckCachedMessages: Timer?
    
    override init() {
        super.init()
        fuguConnectionChangesStartNotifier()
        
        if timerToCheckCachedMessages == nil {
            timerToCheckCachedMessages = Timer(timeInterval: 15,
                                               target: self,
                                               selector: #selector(checkCachedMessages(timer:)),
                                               userInfo: nil,
                                               repeats: true)
        }
    }
    
    @objc fileprivate func checkCachedMessages(timer: Timer) { sendAllCachedMessages() }
    
    @available(*, deprecated, message: "Use method 'updateUserDetail(userDetail: FuguUserDetail)' instead")
    public convenience init(fullName: String? = nil,
                email: String? = nil,
                phoneNumber: String? = nil,
                userUniqueKey: String? = nil,
                deviceToken: String? = nil) {
        self.init()
        self.userDetail = FuguUserDetail(fullName: fullName ?? "",
                                         email: email ?? "",
                                         phoneNumber: phoneNumber ?? "",
                                         userUniqueKey: userUniqueKey ?? "")

        if let pushToken = deviceToken, pushToken.characters.count > 0  {
            self.deviceToken = pushToken
        }
    }
    
//    public convenience init(credentialType: FuguCredentialType) {
//        self.init()
//        self.credentialType = credentialType
//    }
    
    @available(*, deprecated, message: "Use method 'updateUserDetail(userDetail: FuguUserDetail)' instead")
    public func updateFuguUserDetails(fullName: String? = nil,
                                      email: String? = nil,
                                      phoneNumber: String? = nil,
                                      userUniqueKey: String? = nil,
                                      deviceToken: String? = nil) {
        let fuguUserDetail = FuguUserDetail(fullName: fullName ?? "",
                                            email: email ?? "",
                                            phoneNumber: phoneNumber ?? "",
                                            userUniqueKey: userUniqueKey ?? "")
        if let pushToken = deviceToken, pushToken.characters.count > 0  {
            self.deviceToken = pushToken
        }
        updateUserDetail(userDetail: fuguUserDetail)
    }
    
    public func updateUserDetail(userDetail: FuguUserDetail) {
        self.userDetail = userDetail
        getUserDetailsAndConversation()
    }
    
    public func setCustomisedFuguTheme(theme: FuguTheme) { self.theme = theme }
    
    public func setCredential(withAppSecretKey appSecretKey: String) {
        self.credentialType = FuguCredentialType.defaultType
        
        self.appSecretKey = appSecretKey
    }
    
    public func setCredential(withToken token: String,
                              referenceId: Int,
                              appType: String) {
        self.credentialType = FuguCredentialType.reseller
        
        self.resellerToken = token
        self.referenceId = referenceId
        self.appType = appType
    }
    
    public func registerDeviceToken(deviceToken: Data) { updateDeviceToken(deviceToken: deviceToken) }
    
    public func presentChatsViewController() {
        presentAllChatsViewController()
    }
    
    @available(*, deprecated, message: "Use method 'presentChatsViewController(_:)' instead")
    public func pushChatsViewController(_ navCtrl: UINavigationController) {
        presentAllChatsViewController()
    }
    
    @available(*, deprecated, message: "Use method 'openChatScreen(withChannelId _:)' instead")
    public func openSpecificChatViewController(_ viewCtrl: UIViewController,
                                               messageChannelId: Int) {
        openChatScreen(withLabelId: messageChannelId)
    }
    
    public func openChatScreen(withLabelId labelId: Int) {
        openChatViewController(labelId: labelId)
    }
    
    public func openChatScreen(withTransactionId transactionId: String,
                               tags: [String]? = nil,
                               channelName: String,
                               message: String = "") {
        showFuguChat(withTransationId: transactionId,
                     tags: tags,
                     channelName: channelName,
                     message: message)
    }
    
    public func clearFuguUserData() { logoutFromFugu() }
    
    public func switchEnvironment(_ envType: FuguEnvironment) {
        switch envType {
        case .dev:
         baseUrl = "https://beta-api.fuguchat.com:3001/"
        default:
         baseUrl = "https://api.fuguchat.com:3002/" /*Live*/
        }
        fayeConnection.localFaye = MZFayeClient(url: URL(string: FuguConfig.shared.fayeBaseURLString))
    }
    
    public func isFuguNotification(userInfo: [String: Any]) -> Bool {
        if validateFuguRemoteNotification(withUserInfo: userInfo) {
            return toShowInAppNotification(userInfo: userInfo)
        }
        return false
    }

    public func handleRemoteNotification(userInfo: [String: Any]) {
        if validateFuguCredential() == false { return }
        if validateFuguRemoteNotification(withUserInfo: userInfo) == false { return }
        
        if UIApplication.shared.applicationState == .active {
            var visibleController: UIViewController?
            if let rootViewController = UIApplication.shared.keyWindow?.rootViewController, let presentedViewController = rootViewController.presentedViewController {
                visibleController = presentedViewController
                if let presentedNavigationController = presentedViewController as? UINavigationController, let lastVisibleCtrl = presentedNavigationController.viewControllers.last {
                    visibleController = lastVisibleCtrl
                }
            }
            
            if let lastVisibleCtrl = visibleController as? ShowAllConersationsViewController {
                lastVisibleCtrl.updateChannelsWithrespectToPush(pushInfo: userInfo,
                                                                moveToChatController: true)
                return
            } else if let lastVisibleCtrl = visibleController as? ConversationsViewController {
                guard let channelId = userInfo["channel_id"] as? Int,
                    lastVisibleCtrl.channelId != channelId else {
                    return
                }
                
                DispatchQueue.main.async {
                    lastVisibleCtrl.isChatDeactivated = false
                    lastVisibleCtrl.getMessagesBasedOnChannel(channelId,
                                                              pageStart: 1)
                }
                return
            } else {
                isHandlingNotification = true
                pushInfo = userInfo
                presentAllChatsViewController()
            }
        } else {
            isHandlingNotification = true
            pushInfo = userInfo
            presentAllChatsViewController()
        }
    }
}
//"9ae38b6649c27f29719946f7d998fe6a"//"b0e4c0b1f4e0a41acf4272efcee4ccfe"//"dff8ab31b54372e9d183e16774604e34"//"kenwfdhwoiujGSDIUUHU82E8HD98"

