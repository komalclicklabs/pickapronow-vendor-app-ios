//
//  ShowAllConersationsViewController.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/10/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit

public class ShowAllConersationsViewController: UIViewController, NewChatSentDelegate {

    @IBOutlet var showConversationsTableView: UITableView!
    @IBOutlet var navigationBackgroundView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet weak var errorContentView: UIView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var errorLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet var newConversationButton: UIButton!
    @IBOutlet var newConversationButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet var poweredByFuguLabel: UILabel!
    @IBOutlet weak var heightOfBottomLabel: NSLayoutConstraint!
   @IBOutlet weak var heightofNavigationBar: NSLayoutConstraint!
   
    // MARK: PROPERTIES
    var isGetAllConversations = false
    var isPushed = false
    let refreshControl = UIRefreshControl()
    var returnImage:UIImage = UIImage()
    
    var tableViewDefaultText = "Loading ..."
    let urlForFuguChat = "https://fuguchat.com/"

    // MARK: LIFECYCLE
    override public func viewDidLoad() {
        super.viewDidLoad()
      
      let topInset = UIView.safeAreaInsetOfKeyWindow.top == 0 ? 20 : UIView.safeAreaInsetOfKeyWindow.top
      heightofNavigationBar.constant = 44 + topInset
      
        FuguConfig.shared.unreadCount = { (count) in
            print("total count of unread chat==>\(count)")
        }
        self.automaticallyAdjustsScrollViewInsets = false
        navigationSetUp()
        putUserDetailsAndGetConversations()
        uiSetup()
    }

    public override func viewWillAppear(_ animated: Bool) {
        getAllConversations()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func uiSetup() {
        
        newConversationButton.layer.cornerRadius = 30
        updateErrorLabelView(isHiding: true)
//        errorLabel.text = ""
//        errorLabelTopConstraint.constant = -20
        
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        showConversationsTableView.backgroundView = refreshControl
        
        poweredByFuguLabel.attributedText = attributedStringForLabelForTwoStrings("Powered by ", secondString: "Fugu", colorOfFirstString: FuguConfig.shared.powererdByColor, colorOfSecondString: FuguConfig.shared.FuguColor, fontOfFirstString: FuguConfig.shared.poweredByFont, fontOfSecondString: FuguConfig.shared.FuguStringFont, textAlighnment: .center, dateAlignment: .center)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openFuguChatWebLink(_:)))
        poweredByFuguLabel.addGestureRecognizer(tap)
        updateBottomLabel()
    }

    func navigationSetUp() {
        
        navigationBackgroundView.layer.shadowColor = UIColor.black.cgColor
        navigationBackgroundView.layer.shadowOpacity = 0.25
        navigationBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        navigationBackgroundView.layer.shadowRadius = 4
        
        //        if FuguConfig.shared.navigationHeaderViewBackgroundColor != nil {
        //            statusBarBackground.backgroundColor = FuguConfig.shared.navigationHeaderViewBackgroundColor
        //        }
        
        navigationBackgroundView.backgroundColor = FuguConfig.shared.theme.headerBackgroundColor
        
        navigationTitleLabel.textColor =  FuguConfig.shared.theme.headerTextColor
        
        if FuguConfig.shared.theme.leftBarButtonText.characters.count > 0 {
            backButton.setTitle((" " + FuguConfig.shared.theme.leftBarButtonText), for: .normal)
            
            if FuguConfig.shared.theme.leftBarButtonFont != nil {
                backButton.titleLabel?.font = FuguConfig.shared.theme.leftBarButtonFont
            }
            
           // if FuguConfig.shared.theme.leftBarButtonTextColor != nil {
            
            backButton.setTitleColor(FuguConfig.shared.theme.leftBarButtonTextColor, for: .normal)
            
//            } else {
//                backButton.setTitleColor(FuguConfig.shared.theme.headerTextColor, for: .normal)
//            }
        } else {
            if FuguConfig.shared.theme.leftBarButtonImage != nil {
                backButton.setImage(FuguConfig.shared.theme.leftBarButtonImage, for: .normal)
            }
        }
        
        navigationTitleLabel.text = FuguConfig.shared.theme.headerText
        
        if FuguConfig.shared.theme.headerTextFont  != nil {
            navigationTitleLabel.font = FuguConfig.shared.theme.headerTextFont
        }
        
        
        if FuguConfig.shared.navigationTitleTextAlignMent != nil {
            navigationTitleLabel.textAlignment = FuguConfig.shared.navigationTitleTextAlignMent!
        }
        
    }
    
    // MARK: UIView Actions
    
    func openFuguChatWebLink(_ sender: UITapGestureRecognizer) {
        guard
            let fuguURL = URL(string: urlForFuguChat),
            UIApplication.shared.canOpenURL(fuguURL)
            else { return }
        
        UIApplication.shared.openURL(fuguURL)
    }
    
    // MARK: UIButton Actions
    
    @IBAction func newConversationButtonAction(_ sender: UIButton) {
        
        //moveToChatViewController(chatObj: [:], isNew: true)
        
        let alert = UIAlertController(title: "Add label.", message: "Label?", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.keyboardType = .numberPad
            textField.text = ""
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if (textField?.text?.characters.count)! > 0 {
                let labelId = Int((textField?.text)!)
                FuguConfig.shared.openChatScreen(withLabelId: labelId!)
            }
        }))

        self.present(alert, animated: true, completion: nil)
        

        /*DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            openChatViewController(viewController: self, labelId: 21)
        }*/
        

    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        if !self.isPushed {
            self.dismiss(animated: true, completion: nil)
            return
        }
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func headerEmptyAction(_ sender: UITapGestureRecognizer) {
        
        tableViewDefaultText = "Loading ..."
        self.showConversationsTableView.reloadData()
        
        self.putUserDetailsAndGetConversations()
        
    }
    
    // MARK: UIRefreshControl
    
    func refresh(_ refreshControl: UIRefreshControl) {
        
        if let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]], chatCachedArray.isEmpty {
            self.putUserDetailsAndGetConversations()
            return
        }
        
        isGetAllConversations = true
        getAllConversations()
    }
    
    // MARK: SERVER HIT
    func putUserDetailsAndGetConversations() {
        if connectedToNetwork() == false {
            fuguDelay(1,
                      completion: {
                        self.tableViewDefaultText = "No Internet Connection\n Tap to retry"
                        self.showConversationsTableView.reloadData()
            })
            return
        }
        
        getUserDetailsAndConversation { (responseObject, error, tag, statusCode) in
            if error == nil {
                let response = (responseObject as? [String: Any]) ?? [:]
                switch (statusCode ?? -1) {
                case STATUS_CODE_SUCCESS:
                    if let tempChatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
                        tempChatCachedArray.isEmpty {
                        self.tableViewDefaultText = "Start a New Conversation."
                    }
                    
                    if self.showConversationsTableView != nil {
                        self.showConversationsTableView.reloadData()
                    }
                default:
                    if self.errorLabel != nil {
                        if connectedToNetwork() == false { return }
                        if let message = response["message"] as? String,
                            message.characters.count > 0 {
                            self.updateErrorLabelView(isHiding: false)
                            self.errorLabel.text = message
                            self.updateErrorLabelView(isHiding: true)
                        }
                    }
                }
                
                if let tempChatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
                    tempChatCachedArray.count < 2,
                    let conversationVC = setupChatBoxController(withChatArray: tempChatCachedArray) {
                    self.navigationController?.setViewControllers([conversationVC], animated: false)
                }
                self.updateBottomLabel()
            }
        }
    }
    
    func getAllConversations() {
        if connectedToNetwork() == false {
            guard
                let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
                chatCachedArray.isEmpty == false
                else {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        self.tableViewDefaultText = "No Internet Connection\n Tap to retry"
                        self.showConversationsTableView.reloadData()
                    }
                    return
            }
            
            self.refreshControl.endRefreshing()
            self.isGetAllConversations = false
            
            self.errorLabel.text = "No Internet Connection"
            self.updateErrorLabelView(isHiding: false)
            return
        }
        
        if FuguConfig.shared.appSecretKey.isEmpty { return }
        
        if !isGetAllConversations { return }
        
        var params = [String: Any]()
        params["app_secret_key"] = FuguConfig.shared.appSecretKey
        if let userId = UserDefaults.standard.value(forKey: FUGU_USER_ID) {
            params["user_id"] = userId
        } else if let userId = userDetailData["user_id"] as? Int {
            params["user_id"] = userId
        }

        HTTPClient.makeConcurrentConnectionWith(method: .POST,
                                                      showAlert: false,
                                                      showAlertInDefaultCase: false,
                                                      showActivityIndicator: false,
                                                      para: params,
                                                      extendedUrl: FuguEndPoints.API_GET_CONVERSATIONS.rawValue) { (responseObject, error, tag, statusCode) in
            
            let response = responseObject as? [String: Any]
            self.refreshControl.endRefreshing()
            self.isGetAllConversations = false
            
            if statusCode == STATUS_CODE_SUCCESS {
                if let data = response?["data"] as? [String: Any] {
                    print("data :==> \(data)")
                    if let conversationListArray = data["conversation_list"] as? [[String: Any]], conversationListArray.isEmpty == false {
                        FuguDefaults.set(value: conversationListArray, forKey: DefaultName.conversationData.rawValue)
                        _ = checkUnreadChatCount()
                        
                        if conversationListArray.count < 2,
                            let conversationVC = setupChatBoxController(withChatArray: conversationListArray) {
                            self.navigationController?.setViewControllers([conversationVC], animated: false)
                            return
                        }
                        
                        self.showConversationsTableView.reloadData()
                    }
                }
            } else {
                if connectedToNetwork() == false { return }
                if let message = response?["message"] as? String,
                    message.isEmpty == false {
                    self.updateErrorLabelView(isHiding: false)
                    self.errorLabel.text = message
                    self.updateErrorLabelView(isHiding: true)
                } else {
                    self.updateErrorLabelView(isHiding: false)
                    self.errorLabel.text = "No Internet Connection"
                    self.updateErrorLabelView(isHiding: true)
                }
                
            }
            
        }
        
    }
    
    // MARK: HELPER
    func updateErrorLabelView(isHiding: Bool) {
        if errorLabelTopConstraint == nil || errorLabel == nil { return }
        if isHiding {
            if self.errorLabelTopConstraint.constant == 0 {
                fuguDelay(3,
                          completion: {
                            self.errorLabelTopConstraint.constant = -20
                            self.errorLabel.text = ""
                            
                            UIView.animate(withDuration: 0.5,
                                           animations: {
                                            self.errorContentView.layoutIfNeeded()
                            })
                })
            }
            return
        }
        
        if errorLabelTopConstraint.constant != 0 {
            errorLabelTopConstraint.constant = 0
            UIView.animate(withDuration: 0.5,
                           animations: { self.errorContentView.layoutIfNeeded() })
        }
    }
    
    func updateBottomLabel() {
        if let isWhiteLabel = userDetailData["is_whitelabel"] as? Bool,
            isWhiteLabel == true { heightOfBottomLabel.constant = 0 }
        else { heightOfBottomLabel.constant = 20 }
        view.layoutIfNeeded()
    }
    
    // MARK: NewChatSentDelegate
    func newChatStartedDelgegate(isChatUpdated: Bool) {
      isGetAllConversations = isChatUpdated
   }
    
    // MARK: Navigation
    
    func moveToChatViewController(chatObj: [String: Any], isNew: Bool, selectedCell: Int = -1) {
        guard let conversationVC = self.storyboard?.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController else { return }

        conversationVC.delegate = self
        conversationVC.channelId = -1//""//"-1"
        
        if  chatObj.keys.count > 0,
            let channelId = chatObj["channel_id"] as? Int {
            conversationVC.channelId = channelId//"/\(channelId)"
            //conversationVC.client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "/\(channelId)")
            
            if let activationStatus = chatObj["status"] as? Int, activationStatus == 0 {
                conversationVC.isChatDeactivated = true
            }
            
            if let labelId = chatObj["label_id"] as? Int {
                conversationVC.labelId = labelId
            }
            
            if  selectedCell > -1,
                let unreadCount = chatObj["unread_count"] as? Int,
                unreadCount > 0 {
                
                if var chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]] {
                    var updatedChatObj = chatObj
                    updatedChatObj["unread_count"] = 0
                    chatCachedArray[selectedCell] = updatedChatObj
                    FuguDefaults.set(value: chatCachedArray, forKey: DefaultName.conversationData.rawValue)
                    _ = checkUnreadChatCount()
                }

                self.showConversationsTableView.reloadRows(at: [IndexPath(row: selectedCell, section: 0)], with: .none)
            }
            
            if let user_name = userDetailData["full_name"] as? String {
                conversationVC.userName = user_name
            }
            
            if isNew == false && channelId != -1 {
                //conversationVC.getMessagesBasedOnChannel(channelId, pageStart: 1)
            } else if channelId == -1 {
                //conversationVC.channelId = "\(channelId)"
                conversationVC.checkIfDateExistsElseUpdate([chatObj])
            }

            if let channelName = chatObj["label"] as? String {
                conversationVC.channelName = channelName
            }
        }
        self.navigationController?.pushViewController(conversationVC, animated: true)
    }
    
    // MARK: HANDLE PUSH NOTIFICATION
    
    func updateChannelsWithrespectToPush(pushInfo: [String: Any], moveToChatController: Bool) {
        guard
            let pushChannelId = pushInfo["channel_id"] as? Int,
            let pushMessage = pushInfo["new_message"] as? String,
            var chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]]
            else { return }
        
        for channel in 0..<chatCachedArray.count {
            let channelObj = chatCachedArray[channel]
            
            guard let channelId = channelObj["channel_id"] as? Int
                else { continue }
            
            if channelId == pushChannelId {
                var updatedChannelObj = channelObj
                updatedChannelObj["message"] = pushMessage
                
                if let unreadCount = pushInfo["unread_count"] as? Int,
                    unreadCount > 0 {
                    updatedChannelObj["unread_count"] = unreadCount
                } else if let unreadCount = updatedChannelObj["unread_count"] as? Int {
                    updatedChannelObj["unread_count"] = unreadCount + 1
                }
                
                if let dateTime = pushInfo["date_time"] as? String, dateTime.characters.count > 0 {
                    updatedChannelObj["date_time"] = dateTime
                }
                
                chatCachedArray[channel] = updatedChannelObj
                FuguDefaults.set(value: chatCachedArray, forKey: DefaultName.conversationData.rawValue)
                _ = checkUnreadChatCount()
                /*
                 showConversationsTableView.beginUpdates()
                 showConversationsTableView.moveRow(at: IndexPath(row: channel, section: 0), to: IndexPath(row: 0, section: 0))
                 
                 showConversationsTableView.endUpdates()
                 
                 conversationsArray.remove(at: channel)
                 conversationsArray.insert(updatedChannelObj, at: 0)
                 */
                if showConversationsTableView != nil {
                    showConversationsTableView.reloadRows(at: [IndexPath(row: channel, section: 0)], with: .none)
                }
                
                
                if moveToChatController {
                    moveToChatViewController(chatObj: updatedChannelObj, isNew: false, selectedCell: channel)
                }
                break
            }
        }
    }

}

extension ShowAllConersationsViewController: UITableViewDelegate, UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard
            let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]]
            else { return 0 }
        
        return chatCachedArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationView", for: indexPath) as? ConversationView,
            let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
            indexPath.row < chatCachedArray.count
        else {
         return UITableViewCell()
         
      }
        
        return cell.configureConversationCell(resetProperties: true, conersationObj: chatCachedArray[indexPath.row])
    }

    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ConversationView else { return }
        cell.selectionView.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.8156862745, blue: 0.8156862745, alpha: 1)
    }
    
    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ConversationView else { return }
        cell.selectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return UITableViewAutomaticDimension }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat { return 30 }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        tableView.isScrollEnabled = true
        guard
            let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
            chatCachedArray.isEmpty == false
            else {
                tableView.isScrollEnabled = false
                return tableView.frame.height
        }
        
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.frame = CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: tableView.frame.size.height)
        
        let footerLabel:UILabel = UILabel(frame: CGRect(x: 0, y: (tableView.frame.height / 2) - 90, width: tableView.frame.width, height: 90))
        footerLabel.textAlignment = NSTextAlignment.center
        footerLabel.textColor = #colorLiteral(red: 0.3490196078, green: 0.3490196078, blue: 0.4078431373, alpha: 1)
        footerLabel.numberOfLines = 0
        footerLabel.font = UIFont.systemFont(ofSize: 16.0)
        
        footerLabel.text = tableViewDefaultText

        footerView.addSubview(footerLabel)
        
        let emptyAction = UITapGestureRecognizer(target: self, action: #selector(headerEmptyAction(_:)))
        footerView.addGestureRecognizer(emptyAction)
        
        return footerView
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
      guard
         let chatCachedArray = FuguDefaults.object(forKey: DefaultName.conversationData.rawValue) as? [[String: Any]],
         indexPath.row < chatCachedArray.count
         else {
            return
            
      }
        
        let conversationObj = chatCachedArray[indexPath.row]
        moveToChatViewController(chatObj: conversationObj, isNew: false, selectedCell: indexPath.row)
        // updateChannelsWithrespectToPush(pushInfo: conversationObj, moveToChatController: true)
    }
}
