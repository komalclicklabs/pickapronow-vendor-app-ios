//
//  ConversationsViewController.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/9/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit
import MZFayeClient
import Photos

protocol NewChatSentDelegate: class {
   func newChatStartedDelgegate(isChatUpdated: Bool)
}

public class ConversationsViewController: UIViewController {
   // MARK: OUTLETS
   @IBOutlet var backgroundView: UIView!
   @IBOutlet var navigationBackgroundView: UIView!
   @IBOutlet var navigationTitleLabel: UILabel!
   @IBOutlet var backButton: UIButton!
   @IBOutlet var chatScreenTableView: UITableView!
   @IBOutlet var sendMessageButton: UIButton!
   @IBOutlet var messageTextView: UITextView!
   @IBOutlet var textViewBackGroundViewHeightConstraint: NSLayoutConstraint!
   @IBOutlet var textViewBottomConstraint: NSLayoutConstraint!
   @IBOutlet var isTypingLabel: UILabel!
   @IBOutlet weak var errorContentView: UIView!
   @IBOutlet var errorLabel: UILabel!
   @IBOutlet var errorLabelTopConstraint: NSLayoutConstraint!
   @IBOutlet var textViewBgView: UIView!
   @IBOutlet var placeHolderLabel: UILabel!
   @IBOutlet var addFileButtonAction: UIButton!
   @IBOutlet var loaderbutton: UIButton!
   @IBOutlet var seperatorView: UIView!
   @IBOutlet weak var addButton: UIButton!
   @IBOutlet weak var loaderView: So_UIImageView!
   @IBOutlet weak var hieghtOfNavigationBar: NSLayoutConstraint!
   
   // MARK: PROPERTIES
   let userType = 1 //Means it is a mobile user else a Web user.
   let textViewFixedHeight = 50
   let textViewPlaceHolder = "Send a message..."
   let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
   let accessoryView = UIView()
   let accessoryTextView = UITextView()
   
   fileprivate var isSendingCachedMessages = false
   
   weak var delegate: NewChatSentDelegate? = nil
   var keyboardHeight : CGFloat = 0
   var channelName = ""
   var returnImage:UIImage = UIImage()
   //   var _dateGroupedArray = [[[String: Any]]]()
   var dateGroupedArray: [[[String: Any]]] = [] {
      didSet {
         print(dateGroupedArray)
      }
   }
   var messagesArray = [[String : Any]]()
   var channelId = -1//""//"-1"
   var directChatDetail: DirectChat?
   var typingMessageValue = TypingMessage.messageRecieved.rawValue
   var isObserverAdded = false
   var userSubscribedValue = UserSubscriptionStatus.notSubscribed.rawValue
   var linkDetails = [String]()
   var textInTextField = ""
   var userName = "Anonymous"
   var willPaginationWork = false
   var timer = Timer()
   var isChatDeactivated = false
   var labelId = -1
   var sendMessageButtonInAccessoryView = UIButton()
   var imagePicker = UIImagePickerController()
   var isGettingResponse = false
   var messageArrayCount = 0
   var needToIgnoreTypingMessage = false
   
   var localFilePath: String {
      get {
         let existingImageCounter = FuguDefaults.totalImagesInImagesFlder() + 1
         guard
            let documentImageUrl = FuguDefaults.fuguImagesDirectory(),
            existingImageCounter > 0
            else { return "" }
         return documentImageUrl.appendingPathComponent("\(existingImageCounter).jpg").path
      }
   }
   
   var cachedUnsentMessagesData: [String: Any] {
      get {
         if let messagesInfo = FuguDefaults.object(forKey: DefaultName.unsentMessagesData.rawValue) as? [String: Any] {
            return messagesInfo
         } else {
            FuguDefaults.set(value: [String: Any](),
                             forKey: DefaultName.unsentMessagesData.rawValue)
            return [:]
         }
      }
      set { FuguDefaults.set(value: newValue,
                             forKey: DefaultName.unsentMessagesData.rawValue) }
   }
   
   var cachedSentMessagesInfo: [String: Any] {
      get {
         if let messagesInfo = FuguDefaults.object(forKey: DefaultName.sentMessagesData.rawValue) as? [String: Any] {
            return messagesInfo
         } else {
            FuguDefaults.set(value: [String: Any](),
                             forKey: DefaultName.sentMessagesData.rawValue)
            return [:]
         }
      }
      set { FuguDefaults.set(value: newValue,
                             forKey: DefaultName.sentMessagesData.rawValue) }
   }
   
   var getSavedUserId: Int {
      get {
         guard let savedUserId = UserDefaults.standard.value(forKey: FUGU_USER_ID) as? Int else {
            return -1
            
         }
         return savedUserId
      }
   }
   
   deinit {
      NotificationCenter.default.removeObserver(self)
      print("Conversation View Controller deintialized")
   }
   
   // MARK: LIFECYCLE
   override public func viewDidLoad() {
      super.viewDidLoad()
      
      let topInset = UIView.safeAreaInsetOfKeyWindow.top == 0 ? 20 : UIView.safeAreaInsetOfKeyWindow.top
      hieghtOfNavigationBar.constant = 44 + topInset

      
      guard !isDefaultChannel() else {
         channelId = -1
         configureChatScreen()
         openChatBasedOnSpecificLabel(labelId, pageStart: 1)
         return
      }
      configureChatScreen()
      if channelId > 0 {
         checkAndSendUnsentMessages()
      } else if directChatDetail != nil {
         startNewConversation(directChat: directChatDetail)
         directChatDetail = nil
      }
      
      //        if !isChatDeactivated { self.messageTextView.becomeFirstResponder() }
   }
   
   func isDefaultChannel() -> Bool {
      return labelId > -1
   }
   
   func checkAndSendUnsentMessages() {
      guard channelId > 0 else {
         return
      }
      
      if let sentMessagesArray = self.cachedSentMessagesInfo["\(self.channelId)"] as? [[String: Any]],
         sentMessagesArray.count > 0 {
         self.messagesArray = sentMessagesArray
      }
      
      if let cachedChannelData = self.cachedUnsentMessagesData["\(self.channelId)"] as? [[String: Any]],
         cachedChannelData.count > 0 {
         
         self.messagesArray.append(contentsOf: cachedChannelData)
         
         //if connectedToNetwork() { self.startLoaderAnimation() }
         self.sendCachedMessages(cachedMessagesArray: cachedChannelData)
      } else {
         self.setUpChatBoxFayeClient()
         self.getMessagesBasedOnChannel(self.channelId,
                                        pageStart: 1)
      }
      
      if self.messagesArray.count > 0 {
         self.checkIfDateExistsElseUpdate(self.messagesArray)
         self.messageArrayCount = self.messagesArray.count
         self.chatScreenTableView.beginUpdates()
         var arrayOfSections = [Int]()
         for i in 0..<self.dateGroupedArray.count {
            arrayOfSections.append(i)
            
         }
         let newSectionsOfTableView = IndexSet(arrayOfSections)
         self.chatScreenTableView.insertSections(newSectionsOfTableView,
                                                 with: .none)
         self.chatScreenTableView.endUpdates()
         // self.chatScreenTableView.reloadData()
         fuguDelay(0, completion: { [weak self] in
            self?.newScrollToBottom(animated: false)
         })
      }
   }
   
   
   override public func viewWillAppear(_ animated: Bool) {
      messageTextView.contentInset.top = 8
      
      NotificationCenter.default.removeObserver(self,
                                                name: NSNotification.Name.UIKeyboardWillShow,
                                                object: nil)
      NotificationCenter.default.removeObserver(self,
                                                name: NSNotification.Name.UIKeyboardWillHide,
                                                object: nil)
      NotificationCenter.default.removeObserver(self,
                                                name: NSNotification.Name.UIApplicationWillEnterForeground,
                                                object: nil)
      
      NotificationCenter.default.addObserver(self,
                                             selector: #selector(self.keyboardWillShow(_:)),
                                             name: NSNotification.Name.UIKeyboardWillShow,
                                             object: nil)
      NotificationCenter.default.addObserver(self,
                                             selector: #selector(self.keyboardWillHide(_:)),
                                             name: NSNotification.Name.UIKeyboardWillHide,
                                             object: nil)
      NotificationCenter.default.addObserver(self,
                                             selector: #selector(willEnterForeground(_:)),
                                             name: .UIApplicationWillEnterForeground,
                                             object: nil)
   }
   
   override public func viewWillDisappear(_ animated: Bool) {
      NotificationCenter.default.removeObserver(self,
                                                name: NSNotification.Name.UIKeyboardWillShow,
                                                object: nil)
      NotificationCenter.default.removeObserver(self,
                                                name: NSNotification.Name.UIKeyboardWillHide,
                                                object: nil)
      NotificationCenter.default.removeObserver(self,
                                                name: NSNotification.Name.UIApplicationWillEnterForeground,
                                                object: nil)
      
      //        FuguDefaults.set(value: cachedUnsentMessagesData,
      //                         forKey: DefaultName.unsentMessagesData.rawValue)
      //        FuguDefaults.set(value: cachedSentMessagesInfo,
      //                         forKey: DefaultName.sentMessagesData.rawValue)
   }
   
   override public func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   func willEnterForeground(_ notification: NSNotification!) {
      setUpChatBoxFayeClient()
   }
   
   func tableViewSetUp() {
      automaticallyAdjustsScrollViewInsets = false
      chatScreenTableView.contentInset.bottom = 3
      //      var contentInset = self.chatScreenTableView.contentInset
      //        contentInset.top = 5.0
      chatScreenTableView.backgroundColor = FuguConfig.shared.theme.backgroundColor
      // chatScreenTableView.contentInset = contentInset
      
      chatScreenTableView.register(UINib(nibName: "OutgoingImageCell",bundle: bundle),
                                   forCellReuseIdentifier: "OutgoingImageCell")
      chatScreenTableView.register(UINib(nibName: "IncomingImageCell", bundle: bundle),
                                   forCellReuseIdentifier: "IncomingImageCell")
   }
   
   func showTextViewBasedOnChatStatus() {
      if textViewBgView == nil { return }
      
      if isChatDeactivated {
         textViewBgView.isHidden = true
         textViewBackGroundViewHeightConstraint.constant = 0
      } else {
         textViewBgView.isHidden = false
         textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
      }
   }
   
   func navigationSetUp() {
      navigationBackgroundView.layer.shadowColor = UIColor.black.cgColor
      navigationBackgroundView.layer.shadowOpacity = 0.25
      navigationBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
      navigationBackgroundView.layer.shadowRadius = 4
      
      navigationBackgroundView.backgroundColor = FuguConfig.shared.theme.headerBackgroundColor
      
      navigationTitleLabel.textColor = FuguConfig.shared.theme.headerTextColor
      
      if FuguConfig.shared.theme.headerTextFont  != nil {
         navigationTitleLabel.font = FuguConfig.shared.theme.headerTextFont
      }
      
      if FuguConfig.shared.theme.sendBtnIcon != nil {
         sendMessageButton.setImage(FuguConfig.shared.theme.sendBtnIcon, for: .normal)
         
         if let tintColor = FuguConfig.shared.theme.sendBtnIconTintColor {
            sendMessageButton.tintColor = tintColor
         }
         
         sendMessageButton.setTitle("", for: .normal)
      } else { sendMessageButton.setTitle("SEND", for: .normal) }
      
      if FuguConfig.shared.theme.addButtonIcon != nil {
         addButton.setImage(FuguConfig.shared.theme.addButtonIcon, for: .normal)
         
         if let tintColor = FuguConfig.shared.theme.addBtnTintColor {
            addButton.tintColor = tintColor
         }
         
         addButton.setTitle("", for: .normal)
      } else { addButton.setTitle("ADD", for: .normal) }
      
      if FuguConfig.shared.theme.leftBarButtonText.characters.count > 0 {
         backButton.setTitle((" " + FuguConfig.shared.theme.leftBarButtonText), for: .normal)
         
         if FuguConfig.shared.theme.leftBarButtonFont != nil {
            backButton.titleLabel?.font = FuguConfig.shared.theme.leftBarButtonFont
         }
         
         // if FuguConfig.shared.theme.leftBarButtonTextColor != nil {
         
         backButton.setTitleColor(FuguConfig.shared.theme.leftBarButtonTextColor, for: .normal)
         
         //            } else {//if FuguConfig.shared.theme.headerTextColor != nil {
         //                backButton.setTitleColor(FuguConfig.shared.theme.headerTextColor, for: .normal)
         //            }
      } else {
         if FuguConfig.shared.theme.leftBarButtonImage != nil {
            backButton.setImage(FuguConfig.shared.theme.leftBarButtonImage, for: .normal)
         }
      }
      
      if FuguConfig.shared.theme.headerTextFont  != nil {
         navigationTitleLabel.font = FuguConfig.shared.theme.headerTextFont
      }
      
      if FuguConfig.shared.navigationTitleTextAlignMent != nil {
         navigationTitleLabel.textAlignment = FuguConfig.shared.navigationTitleTextAlignMent!
      }
      
      if channelName.characters.count > 0 {
         navigationTitleLabel.text = channelName
      } else {
         if let businessName = userDetailData["business_name"] as? String {
            navigationTitleLabel.text = businessName
         }
      }
      
      //messageTextView.text = textViewPlaceHolder
      //messageTextView.textColor = UIColor(red: 73.0/255.0, green: 73.0/255.0, blue: 73.0/255.0, alpha: 1.0)
      textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
   }
   
   // MARK: UIButton Actions
   @IBAction func addImagesButtonAction(_ sender: UIButton) {
      imagePicker.delegate = self
      let actionSheet = UIAlertController(title: nil,
                                          message: nil,
                                          preferredStyle: UIAlertControllerStyle.actionSheet)
      
      let cameraAction = UIAlertAction(title: "New Image via Camera",
                                       style: .default,
                                       handler: { (alert: UIAlertAction!) -> Void in
                                          self.view.endEditing(true)
                                          if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                                             self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                                             self.performActionBasedOnCameraPermission()
                                          }
      })
      
      let photoLibraryAction = UIAlertAction(title: "Photo Library",
                                             style: .default,
                                             handler: { (alert: UIAlertAction!) -> Void in
                                                self.view.endEditing(true)
                                                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                                                self.performActionBasedOnGalleryPermission()
      })
      
      let cancelAction = UIAlertAction(title: "Cancel",
                                       style: .cancel,
                                       handler: { (alert: UIAlertAction!) -> Void in })
      
      actionSheet.addAction(photoLibraryAction)
      actionSheet.addAction(cameraAction)
      actionSheet.addAction(cancelAction)
      
      self.present(actionSheet,
                   animated: true,
                   completion: nil)
   }
   
   func checkPhotoLibraryPermission() {
      let status = PHPhotoLibrary.authorizationStatus()
      switch status {
      case .authorized: break //handle authorized status
      case .denied, .restricted : break //handle denied status
      case .notDetermined: // ask for permissions
         PHPhotoLibrary.requestAuthorization() { status in
            switch status {
            case .authorized: break // as above
            case .denied, .restricted: break // as above
            case .notDetermined: break // won't happen but still
            }
         }
      }
   }
   
   func saveOffLineMessage(_ fayeMessageDict: [String: Any]) {
      if channelId < 0 {
         return
      }
      
      if let cachedChannelData = cachedUnsentMessagesData["\(channelId)"] as? [[String: Any]] {
         var messagesArray = cachedChannelData
         messagesArray.append(fayeMessageDict)
         
         cachedUnsentMessagesData["\(channelId)"] = messagesArray
      } else {
         cachedUnsentMessagesData["\(channelId)"] = [fayeMessageDict]
      }
   }
   
   func validateMessageAndSendToFaye() {
      if isMessageInvalid(messageText: messageTextView.text) {
         return
      }
      
      let trimmedMessage = messageTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
      var fayeMessageDict = self.messageInfo(ofMessage: trimmedMessage,
                                             isTyping: TypingMessage.messageRecieved.rawValue)
      fayeMessageDict["message_status"] = ReadUnReadStatus.none.rawValue
      fayeMessageDict["message_index"] = messageArrayCount
      
      if channelId > 0 {
         saveOffLineMessage(fayeMessageDict)
         
         messageTextView.text = ""
         textViewDidChange(messageTextView)
         textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
         
         updateMessagesArrayLocally(fayeMessageDict,
                                    needToScrollDown: true)
      }
      
      if connectedToNetwork() == false {
         self.errorLabel.text = "No Internet Connection"
         self.updateErrorLabelView(isHiding: false)
         return
      }
      
      if channelId < 0 {
         startNewConversation()
      } else {
         var when: Double = 0
         if fayeConnection.localFaye?.isConnected == false {
            when = 1
            setUpChatBoxFayeClient(onSuccess: { [weak self] (subscribed) in
               if subscribed {
                  fuguDelay(when,
                            completion: {
                              self?.publishMessageOnServer(messageInfo: fayeMessageDict)
                  })
               }
            })
            
            updateErrorLabelView(isHiding: false)
            errorLabel.text = "No Internet Connection. Please retry after sometime."
            updateErrorLabelView(isHiding: true)
         } else {
            publishMessageOnServer(messageInfo: fayeMessageDict)
         }
         //sendMessageButton.isEnabled = false
      }
   }
   
   @IBAction func sendMessageButtonAction(_ sender: UIButton) {
      validateMessageAndSendToFaye()
   }
   
   @IBAction func backButtonAction(_ sender: UIButton) {
      if channelId > 0 {
         var messageInfo: [String : Any] = self.messageInfo(ofMessage: "", isTyping: TypingMessage.stopTyping.rawValue)
         messageInfo["on_subscribe"] = UserSubscriptionStatus.notSubscribed.rawValue
         messageInfo["channel_id"] = channelId
         fayeConnection.localFaye?.sendMessage(messageInfo,
                                               toChannel: "/\(channelId)",
            success: {},
            failure: { (error) in })
         unsubscribeFayeClient()
      }
      
      _ = self.navigationController?.popViewController(animated: true)
      
      dismiss(animated: true, completion: nil)
   }
   
   func unsubscribeFayeClient() {
      fayeConnection.localFaye?.unsubscribe(fromChannel: "/\(channelId)", success: {
         
      }, failure: { (error) in
         
      })
   }
   func publishMessageOnServer(messageInfo: [String: Any]? = nil) {
      if channelId > 0 {
         typingMessageValue = TypingMessage.messageRecieved.rawValue
         
         if getSavedUserId == -1 {
            return
         }
         if messageInfo == nil {
            let trimmedMessage = self.messageTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let sendableMessagesArray = [self.messageInfo(ofMessage: trimmedMessage,
                                                          isTyping: typingMessageValue)]
            sendMessageToFaye(messageInfoArray: sendableMessagesArray)
         } else {
            sendMessageToFaye(messageInfoArray: [messageInfo!])
         }
         
         typingMessageValue = TypingMessage.startTyping.rawValue
         messageTextView.text = ""
         textViewDidChange(messageTextView)
         textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
      } else if channelId < 0 {
         if connectedToNetwork() == false {
            return
         }
         
         self.updateErrorLabelView(isHiding: false)
         self.errorLabel.text = "Please enter some text."
         self.updateErrorLabelView(isHiding: true)
      }
   }
   
   func changeShapeOfImageView(_ popupView: UIView,
                               isReverse: Bool) {
      let path = UIBezierPath()
      if isReverse {
         path.move(to: CGPoint(x: popupView.frame.size.width, y: 0))
         //  path.addLine(to: CGPoint(x: popupView.frame.size.width - 10, y: 12.5))
         path.addLine(to: CGPoint(x: popupView.frame.size.width , y: 12.5))
         //path.addLine(to: CGPoint(x: popupView.frame.size.width - 10, y: popupView.frame.size.height))
         path.addLine(to: CGPoint(x: popupView.frame.size.width, y: popupView.frame.size.height))
         path.addLine(to: CGPoint(x: 0, y: popupView.frame.size.height))
         path.addLine(to: CGPoint(x: 0, y: 0))
      } else {
         path.move(to: CGPoint(x: 10, y: 12.5))
         path.addLine(to: CGPoint(x: 10, y: popupView.frame.size.height))
         path.addLine(to: CGPoint(x: popupView.frame.size.width, y: popupView.frame.size.height))
         path.addLine(to: CGPoint(x: popupView.frame.size.width, y: 0))
         path.addLine(to: CGPoint(x: 0, y: 0))
         path.move(to: CGPoint(x: 0, y: 0))
      }
      
      UIColor.red.setFill()
      path.stroke()
      path.reversing()
      
      let shapeLayer = CAShapeLayer()
      shapeLayer.frame = popupView.bounds
      shapeLayer.path = path.cgPath
      shapeLayer.lineJoin = kCALineJoinRound
      shapeLayer.lineCap = kCALineCapRound
      shapeLayer.fillColor = UIColor.cyan.cgColor
      popupView.layer.mask = shapeLayer
      
      popupView.layer.masksToBounds = true
   }
   
   // MARK: SERVER HIT
   func getMessagesBasedOnChannel(_ channelId: Int,
                                  pageStart: Int) {
      if connectedToNetwork() == false {
         if self.dateGroupedArray.isEmpty {
            self.errorLabel.text = "No Internet Connection"
            self.updateErrorLabelView(isHiding: false)
         }
         return
      }
      
      if FuguConfig.shared.appSecretKey.isEmpty { return }
      if isGettingResponse { return }
      isGettingResponse = true
      
      let params: [String: Any] = ["app_secret_key" : FuguConfig.shared.appSecretKey,
                                   "channel_id" : channelId,
                                   "user_id" : getSavedUserId,
                                   "page_start": pageStart]
      
      if pageStart == 1, messagesArray.count == 0 { startLoaderAnimation() }
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST,
                                              showAlert: false,
                                              showAlertInDefaultCase: false,
                                              showActivityIndicator: false,
                                              para: params,
                                              extendedUrl: FuguEndPoints.API_GET_MESSAGES.rawValue) { (responseObject, error, tag, statusCode) in
                                                self.isGettingResponse = false
                                                self.showTextViewBasedOnChatStatus()
                                                self.userSubscribedValue = UserSubscriptionStatus.notSubscribed.rawValue
                                                self.stopLoaderAnimation()
                                                switch (statusCode ?? -1) {
                                                case STATUS_CODE_SUCCESS:
                                                   guard
                                                      let response = responseObject as? [String: Any],
                                                      let data = response["data"] as? [String: Any]
                                                      else { return }
                                                   print("data in back of conservation is \(data)")
                                                   
                                                   if let channelName = data["label"] as? String {
                                                      self.channelName = channelName
                                                      self.navigationTitleLabel.text = channelName
                                                   } else if let fullName = data["full_name"] as? String,
                                                      fullName.characters.count > 0 {
                                                      self.navigationTitleLabel.text = fullName
                                                   }
                                                   
                                                   if let onSubscribed = data["on_subscribe"] as? Int {
                                                      self.userSubscribedValue = onSubscribed
                                                   }
                                                   
                                                   if let messages = data["messages"] as? [[String : Any]],
                                                      messages.count > 0 {
                                                      self.willPaginationWork = false
                                                      if let pageSize = data["page_size"] as? Int {
                                                         if messages.count - pageSize == 0 {
                                                            self.willPaginationWork = true
                                                         }
                                                      }
                                                      
                                                      if pageStart == 1 {
                                                         self.messagesArray = []
                                                         self.messageArrayCount = 0
                                                      }
                                                      
                                                      self.dateGroupedArray = []
                                                      self.checkIfDateExistsElseUpdate(messages)//first, update new messages
                                                      let lastCell = self.getLastCellOfTableView()
                                                      if self.messagesArray.count > 0 {
                                                         self.checkIfDateExistsElseUpdate(self.messagesArray)//second, update existing messages
                                                      }
                                                      
                                                      self.messagesArray = messages + self.messagesArray
                                                      self.cachedSentMessagesInfo["\(channelId)"] = self.messagesArray
                                                      self.messageArrayCount = messages.count + self.messageArrayCount
                                                      
                                                      self.chatScreenTableView.reloadData()
                                                      
                                                      if pageStart == 1 {
                                                         self.newScrollToBottom(animated: false)
                                                         
                                                      }
                                                      else if lastCell != nil {
                                                         self.chatScreenTableView.scrollToRow(at: lastCell!,
                                                                                              at: .top,
                                                                                              animated: false)
                                                      }
                                                   }
                                                default: break
                                                }
      }
   }
   
   func newScrollToBottom(animated: Bool) {
      if chatScreenTableView.numberOfSections == 0 { return }
      
      if let lastCell = getLastCellOfTableView() {
         scroll(toIndexPath: lastCell,
                animated: animated)
      }
   }
   
   func getLastCellOfTableView() -> IndexPath? {
      if self.dateGroupedArray.count > 0 {
         let givenMessagesArray = self.dateGroupedArray[self.dateGroupedArray.count - 1]
         if givenMessagesArray.count > 0 {
            return IndexPath(row: givenMessagesArray.count - 1,
                             section: self.dateGroupedArray.count - 1)
         }
      }
      return nil
   }
   
   func scroll(toIndexPath indexPath: IndexPath, animated: Bool) {
      chatScreenTableView.scrollToRow(at: indexPath,
                                      at: .bottom,
                                      animated: animated)
   }
   
   func checkIfDateExistsElseUpdate(_ chatMessagesArray: [[String: Any]]) {
      
      for j in 0 ..< chatMessagesArray.count {
         let messageObj = chatMessagesArray[j]
         if let dateTimeString = messageObj["date_time"] as? String,
            dateTimeString.isEmpty == false {
            if self.dateGroupedArray.isEmpty {
               self.addNewDate(messageObj,
                               dateTimeString: dateTimeString)
            } else {
               var isDateFound = false
               if  var obj = dateGroupedArray.last,
                  obj.count > 0,
                  let dateString = obj[0]["date_time"] as? String,
                  dateString.isEmpty == false {
                  let comparisonResult = Calendar.current.compare(dateString.toDate!,
                                                                  to: dateTimeString.toDate!,
                                                                  toGranularity: .day)
                  
                  switch comparisonResult {
                  case .orderedSame:
                     isDateFound = true
                     obj.append(messageObj)
                     dateGroupedArray[dateGroupedArray.count - 1] = obj
                  default: break
                  }
               }
               if isDateFound == false {
                  addNewDate(messageObj,
                             dateTimeString: dateTimeString)
               }
            }
         }
      }
   }
   
   func addNewDate(_ dateObj: [String: Any],
                   dateTimeString: String) {
      self.dateGroupedArray.append([dateObj])
   }
   
   func openChatBasedOnSpecificLabel(_ labelId: Int,
                                     pageStart: Int,
                                     message: String = "") {
      if connectedToNetwork() == false {
         if self.dateGroupedArray.isEmpty {
            self.errorLabel.text = "No Internet Connection"
            self.updateErrorLabelView(isHiding: false)
         }
         return
      }
      
      if FuguConfig.shared.appSecretKey.isEmpty { return }
      self.labelId = labelId
      
      let params: [String: Any] = ["app_secret_key" : FuguConfig.shared.appSecretKey,
                                   "label_id" : labelId,
                                   "user_id" : getSavedUserId,
                                   "page_start": pageStart]
      
      if pageStart == 1 { startLoaderAnimation() }
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST,
                                              showAlert: false,
                                              showAlertInDefaultCase: false,
                                              showActivityIndicator: false,
                                              para: params,
                                              extendedUrl: FuguEndPoints.API_GET_MESSAGES_BASED_ON_LABEL.rawValue) { (responseObject, error, tag, statusCode) in
                                                self.stopLoaderAnimation()
                                                self.userSubscribedValue = UserSubscriptionStatus.notSubscribed.rawValue
                                                self.isChatDeactivated = false
                                                
                                                switch (statusCode ?? -1) {
                                                case StatusCodeValue.Authorized_Min.rawValue..<StatusCodeValue.Authorized_Max.rawValue:
                                                   let response = responseObject as? [String: Any]
                                                   if let data = response?["data"] as? [String: Any] {
                                                      if let channelName = data["label"] as? String {
                                                         self.channelName = channelName
                                                         self.navigationTitleLabel.text = channelName
                                                      } else if let fullName = data["full_name"] as? String, fullName.characters.count > 0 {
                                                         self.navigationTitleLabel.text = fullName
                                                      }
                                                      
                                                      if let onSubscribed = data["on_subscribe"] as? Int {
                                                         self.userSubscribedValue = onSubscribed
                                                      }
                                                      
                                                      if let chatActivationStatus = data["status"] as? Int, chatActivationStatus == 0 {
                                                         self.isChatDeactivated = true
                                                         self.showTextViewBasedOnChatStatus()
                                                      }
                                                      
                                                      if let labelId = data["label_id"] as? Int {
                                                         self.labelId = labelId
                                                      }
                                                      
                                                      if let channelId = data["channel_id"] as? Int,
                                                         channelId != -1 {
                                                         self.channelId = channelId//"/\(channelId)"
                                                         self.setUpChatBoxFayeClient()
                                                      }
                                                      
                                                      
                                                      
                                                      if let messages = data["messages"] as? [[String: Any]],
                                                         messages.count > 0 {
                                                         //                     self.dateGroupedArray = []
                                                         self.willPaginationWork = false
                                                         if let pageSize = data["page_size"] as? Int {
                                                            if messages.count - pageSize == 0 {
                                                               self.willPaginationWork = true
                                                            }
                                                         }
                                                         if pageStart == 1 {
                                                            self.messagesArray = []
                                                            self.messageArrayCount = 0
                                                         }
                                                         
                                                         
                                                         self.dateGroupedArray = []
                                                         self.checkIfDateExistsElseUpdate(messages)//first, update new messages
                                                         let lastCell = self.getLastCellOfTableView()
                                                         if self.messagesArray.count > 0 {
                                                            self.checkIfDateExistsElseUpdate(self.messagesArray)//second, update existing messages
                                                         }
                                                         
                                                         self.messagesArray = messages + self.messagesArray
                                                         self.cachedSentMessagesInfo["\(self.channelId)"] = self.messagesArray
                                                         self.messageArrayCount = messages.count + self.messageArrayCount
                                                         
                                                         self.chatScreenTableView.reloadData()
                                                         //self.startSendingCachedMessagesWhenInternetAvailable()
                                                         if pageStart == 1 {
                                                            self.newScrollToBottom(animated: false)
                                                            
                                                         }
                                                         else if lastCell != nil {
                                                            self.chatScreenTableView.scrollToRow(at: lastCell!,
                                                                                                 at: .top,
                                                                                                 animated: false)
                                                         }
                                                      }
                                                      
                                                      
                                                      if message.isEmpty == false {
                                                         self.messageTextView.text = message
                                                         self.validateMessageAndSendToFaye()
                                                      }
                                                   }
                                                   
                                                case 406:
                                                   guard let response = responseObject as? [String: Any],
                                                      let type = response["type"] as? Int,
                                                      type == 1,
                                                      let userUniqueKey = FuguConfig.shared.userDetail.userUniqueKey else {
                                                         return
                                                   }
                                                   
                                                   self.channelId = -1
                                                   self.labelId = -1
                                                   
                                                   let directChat = DirectChat(transactionId: "7865", userUniqueKey: userUniqueKey, channelName: "Fugu Default", preMessage: "")
                                                   
                                                   self.startNewConversation(directChat: directChat)
                                                default:
                                                   break
                                                }
      }
   }
   
   func startNewConversation(imageData: [String: Any]? = nil,
                             directChat: DirectChat? = nil) {
      if connectedToNetwork() == false {
         self.errorLabel.text = "No Internet Connection"
         self.updateErrorLabelView(isHiding: false)
         return
      }
      
      activityIndicator.stopAnimating()
      activityIndicator.frame = sendMessageButton.bounds
      activityIndicator.backgroundColor = UIColor.white
      sendMessageButton.addSubview(activityIndicator)
      activityIndicator.bringSubview(toFront: sendMessageButton)
      activityIndicator.startAnimating()
      
      if FuguConfig.shared.appSecretKey.isEmpty {
         return
      }
      
      var params = [String: Any]()
      if directChat != nil {
         params = directChat!.toJSONObj()
      }
      
      params["app_secret_key"] = FuguConfig.shared.appSecretKey
      params["user_id"] = getSavedUserId
      
      if channelId < 0 {
         params["label_id"] = labelId
         if self.dateGroupedArray.isEmpty == false {
            let messagesArray = self.dateGroupedArray[0]
            
            if messagesArray.count > 0 {
               let chatObj = messagesArray[0]
               
               if self.labelId != -1 {
                  params["label_id"] = self.labelId
               } else if let labelId = chatObj["label_id"] as? Int {
                  params["label_id"] = labelId
                  self.labelId = labelId
               }
            }
         }
      }
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST,
                                              showAlert: false,
                                              showAlertInDefaultCase: false,
                                              showActivityIndicator: false,
                                              para: params,
                                              extendedUrl: FuguEndPoints.API_CREATE_CONVERSATION.rawValue) { (responseObject, error, tag, statusCode) in
                                                
                                                let response = responseObject as? [String: Any]
                                                
                                                if statusCode == STATUS_CODE_SUCCESS {
                                                   if let chatObj = response?["data"] as? [String: Any] {
                                                      if let channelId = chatObj["channel_id"] as? Int {
                                                         self.channelId = channelId
                                                         self.setUpChatBoxFayeClient(onSuccess: { [weak self] (subscribed) in
                                                            
                                                            guard let weakSelf = self else {
                                                               return
                                                            }
                                                            
                                                            if subscribed {
                                                               if imageData != nil {
                                                                  if imageData!.keys.count > 0 {
                                                                     guard
                                                                        let imageUrl = imageData!["image_url"] as? String,
                                                                        let thumbnailUrl = imageData!["thumbnail_url"] as? String
                                                                        else {
                                                                           return
                                                                     }
                                                                     
                                                                     DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                                                                        let messageInfo: [String : Any] = ["message": "",
                                                                                                           "user_id": weakSelf.getSavedUserId,
                                                                                                           "full_name": weakSelf.userName,
                                                                                                           "date_time": convertCurrentDateTimeToUTC(),
                                                                                                           "is_typing": TypingMessage.messageRecieved.rawValue,
                                                                                                           "message_type" : MessageType.imageFile.rawValue,
                                                                                                           "user_type": weakSelf.userType,
                                                                                                           "image_url": imageUrl,
                                                                                                           "thumbnail_url": thumbnailUrl]
                                                                        
                                                                        fayeConnection.localFaye?.sendMessage(messageInfo,
                                                                                                              toChannel: "/\(weakSelf.channelId)",
                                                                           success: {},
                                                                           failure: { (error) in
                                                                              if error != nil {
                                                                                 print("failure: { (error)==> \(error!.localizedDescription)")
                                                                              }
                                                                        })
                                                                     }
                                                                  }
                                                               } else if weakSelf.messageTextView.text.isEmpty == false {
                                                                  weakSelf.validateMessageAndSendToFaye()
                                                               } else if directChat != nil,
                                                                  channelId > 0 {
                                                                  weakSelf.getMessagesBasedOnChannel(channelId, pageStart: 1)
                                                               }
                                                               fuguDelay(0.8,
                                                                         completion: {
                                                                           weakSelf.activityIndicator.stopAnimating()
                                                               })
                                                            }
                                                         })
                                                      }
                                                   }
                                                } else {
                                                   self.activityIndicator.stopAnimating()
                                                }
      }
   }
   
   // MARK: Notification Methods
   func keyboardWillShow(_ notification: Notification) {}
   
   func keyboardWillHide(_ notification: Notification) {
      if !isChatDeactivated {
         textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
         //  textViewBottomConstraint.constant = 0
      }
      
      if self.typingMessageValue == TypingMessage.startTyping.rawValue, channelId > 0 {
         typingMessageValue = TypingMessage.stopTyping.rawValue
         sendTypingStatusMessage(message: "", isTyping: typingMessageValue)
      }
      self.addRemoveShadowInTextView(toAdd: false)
   }
   
   func addRemoveShadowInTextView(toAdd: Bool) {
      self.seperatorView.isHidden = true
      self.seperatorView.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.8941176471, blue: 0.9294117647, alpha: 1)
      //self.textViewBgView.layer.masksToBounds = true
      
      if toAdd {
         self.seperatorView.isHidden = false
         /*self.textViewBgView.layer.masksToBounds = false
          self.textViewBgView.layer.shadowColor = UIColor.black.cgColor
          self.textViewBgView.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(-1.0))
          self.textViewBgView.layer.shadowOpacity = 0.3
          //self.textViewBgView.layer.shadowRadius = 4.0*/
      }
   }
   
   
}

// MARK: HELPERS
extension ConversationsViewController {
   func configureChatScreen() {
      //        if let messagesInfo = FuguDefaults.object(forKey: DefaultName.unsentMessagesData.rawValue) as? [String: Any] {
      //            cachedUnsentMessagesData = messagesInfo
      //        } else {
      //            FuguDefaults.set(value: [String: Any](),
      //                             forKey: DefaultName.unsentMessagesData.rawValue)
      //        }
      //
      //        if let messagesInfo = FuguDefaults.object(forKey: DefaultName.sentMessagesData.rawValue) as? [String: Any] {
      //            cachedSentMessagesInfo = messagesInfo
      //        } else {
      //            FuguDefaults.set(value: [String: Any](),
      //                             forKey: DefaultName.sentMessagesData.rawValue)
      //        }
      
      navigationSetUp()
      tableViewSetUp()
      configureFooterView()
      
      self.messageTextView.textAlignment = .left
      self.messageTextView.font = FuguConfig.shared.theme.typingTextFont
      self.messageTextView.textColor = FuguConfig.shared.theme.typingTextColor
      self.messageTextView.backgroundColor = .clear
      
      errorLabel.text = ""
      if errorLabelTopConstraint != nil {
         errorLabelTopConstraint.constant = -20
      }
      
      sendMessageButton.isEnabled = false
   }
   
   func configureAllFayeEvents() {
      fayeConnection.setupConnectionAndSubscribeAllFayeEvents { (messageReceived) in
         if let messageJSON = messageReceived as? [String: Any] {
            print(messageJSON)
            let notificationType = (messageJSON["notification_type"] as? Int) ?? -1
            switch notificationType {
            case NotificationType.read_unread.rawValue:
               var tempMessages = [[String: Any]]()
               _ = self.messagesArray.filter({
                  var editedObject = $0
                  editedObject["message_status"] = ReadUnReadStatus.read.rawValue
                  tempMessages.append($0)
                  return true
               })
               self.messagesArray = tempMessages
               self.checkIfDateExistsElseUpdate(self.messagesArray)
               self.chatScreenTableView.reloadData()
            case NotificationType.read_all.rawValue:
               var tempMessages = [[String: Any]]()
               _ = self.messagesArray.filter({
                  var editedObject = $0
                  editedObject["message_status"] = ReadUnReadStatus.read.rawValue
                  tempMessages.append($0)
                  return true
               })
               self.messagesArray = tempMessages
               self.checkIfDateExistsElseUpdate(self.messagesArray)
               self.chatScreenTableView.reloadData()
            default: break
            }
         }
      }
   }
   
   func configureFooterView() {
      if isObserverAdded == false {
         textViewBgView.layoutIfNeeded()
         let inputView = FrameObserverAccessaryView(frame: textViewBgView.bounds)
         inputView.isUserInteractionEnabled = false
         
         messageTextView.inputAccessoryView = inputView
         
         inputView.changeKeyboardFrame { [weak self] (keyboardVisible, keyboardFrame) in
            let value = UIScreen.main.bounds.height - keyboardFrame.minY
            self?.textViewBottomConstraint.constant = max(0, value)
            self?.view.layoutIfNeeded()
         }
         isObserverAdded = true
      }
   }
   
   func startLoaderAnimation() {
      startFuguRotation(ofView: loaderView, forKey: "chat_loading_first")
      loaderView.isHidden = false
   }
   
   func stopLoaderAnimation() {
      loaderView.isHidden = true
      stopFuguRotation(ofView: loaderView, forKey: "chat_loading_first")
   }
   
   func updateTopBottomSpace(cell: UITableViewCell, indexPath: IndexPath) {
      let minDistance: CGFloat = 3
      let maxDistance: CGFloat = 5
      var topConstraint: CGFloat = maxDistance
      var bottomConstraint: CGFloat = maxDistance
      let totalRowsofCurrentSection = self.tableView(chatScreenTableView, numberOfRowsInSection: indexPath.section)
      
      if totalRowsofCurrentSection == 1 {
         topConstraint = maxDistance
         bottomConstraint = 0//maxDistance
      } else {
         if indexPath.row == 0 {
            topConstraint = maxDistance
            bottomConstraint = 0
         } else {
            let previousRow = indexPath.row - 1
            let previousSection = indexPath.section
            if previousSection < dateGroupedArray.count,
               indexPath.section < dateGroupedArray.count {//}, previousRow < messagesArray[previousSection].count {
               let previousMessagesArray = dateGroupedArray[previousSection]
               let givenMessagesArray = dateGroupedArray[indexPath.section]
               
               if  previousMessagesArray.count > previousRow,
                  indexPath.row < givenMessagesArray.count  {
                  let previousMessageObject = previousMessagesArray[previousRow]
                  let givenMessageObject = givenMessagesArray[indexPath.row]
                  let previousUserId = (previousMessageObject["user_id"] as? Int) ?? -1
                  let givenUserId = (givenMessageObject["user_id"] as? Int) ?? -1
                  
                  if previousUserId == givenUserId {
                     topConstraint = minDistance
                  } else { topConstraint = maxDistance }
               }
            }
            
            if indexPath.row == (totalRowsofCurrentSection - 1) {//last row
               bottomConstraint = 0//maxDistance
            } else { bottomConstraint = 0 }
         }
      }
      
      if let editedCell = cell as? SelfMessageTableViewCell {
         editedCell.topConstraint.constant = topConstraint
         editedCell.bottomConstraint.constant = bottomConstraint
      } else if let editedCell = cell as? SupportMessageTableViewCell {
         editedCell.topConstraint.constant = topConstraint
         editedCell.bottomConstraint.constant = bottomConstraint
      } else if let editedCell = cell as? IncomingImageCell {
         editedCell.topConstraint.constant = topConstraint
         editedCell.bottomConstraint.constant = bottomConstraint
      } else if let editedCell = cell as? OutgoingImageCell {
         editedCell.topConstraint.constant = topConstraint
         editedCell.bottomConstraint.constant = bottomConstraint
      }
   }
   
   func imageTapped(_ sender: UITapGestureRecognizer) {
      let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowImageViewController") as! ShowImageViewController
      let imageView = sender.view as! UIImageView
      self.modalPresentationStyle = .overCurrentContext
      destinationVC.imageToShow = imageView.image!
      self.present(destinationVC, animated: true, completion: nil)
   }
   
   func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
      sender.view?.removeFromSuperview()
   }
   
   func watcherOnTextView() {
      if textInTextField == messageTextView.text,
         typingMessageValue == TypingMessage.stopTyping.rawValue,
         channelId > 0 {
         sendTypingStatusMessage(message: "", isTyping: typingMessageValue)
         self.typingMessageValue = TypingMessage.startTyping.rawValue
      } else { textInTextField = messageTextView.text }
   }
   
   func updateErrorLabelView(isHiding: Bool) {
      if isHiding {
         if self.errorLabelTopConstraint.constant == 0 {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
               self.errorLabelTopConstraint.constant = -20
               UIView.animate(withDuration: 0.5,
                              animations: {
                                 self.errorLabel.text = ""
                                 self.errorContentView.layoutIfNeeded()
               })
            }
         }
         return
      }
      
      if errorLabelTopConstraint != nil && errorLabelTopConstraint.constant != 0 {
         self.errorLabelTopConstraint.constant = 0
         UIView.animate(withDuration: 0.5,
                        animations: { self.errorContentView.layoutIfNeeded() })
      }
   }
   
   func messageInfo(ofMessage message: String = "",
                    isTyping: Int = TypingMessage.messageRecieved.rawValue,
                    messageType: Int = MessageType.normal.rawValue) -> [String : Any] {
      return ["message": message,
              "user_id": getSavedUserId,
              "full_name": userName,
              "date_time": convertCurrentDateTimeToUTC(),
              "is_typing": isTyping,
              "message_type": messageType,
              "user_type": userType]
   }
   
   func sendTypingStatusMessage(message: String = "", isTyping: Int) {
      fayeConnection.localFaye?.sendMessage(messageInfo(ofMessage: message, isTyping: isTyping), toChannel: "/\(channelId)", success: {}, failure: { (error) in
         if error != nil {
            print("failure: { (error)==> \(error!.localizedDescription)")
         }
      })
   }
   
   func sendMessageToFaye(messageInfoArray: [[String: Any]],
                          onSuccess:((_ unSentMessagesArray: [[String: Any]]) -> Void)? = nil) {
      var chatArray = messageInfoArray
      
      guard messageInfoArray.count > 0 else {
         if onSuccess != nil {
            onSuccess!(chatArray)
         }
         return
      }
      
      var messageDict = chatArray[0]
      
      if let imageFile = messageDict["image_file"] as? String,
         imageFile.isEmpty == false,
         let fuguImagePath = FuguDefaults.fuguImagesDirectory()?.path {
         
         let imagePath = fuguImagePath + "/" + imageFile
         
         fayeConnection.getImageURL(fromImageInfo: imagePath, fayeMessageInfo: messageDict, onCompletion: { [weak self] (imageURLInfo, success) in
            guard let weakSelf = self else {
               return
            }
            
            var when: Double = 0
            if success {
               when = 5
               messageDict.removeValue(forKey: "image_file")
               if imageURLInfo != nil {
                  for (key, value) in imageURLInfo! {
                     messageDict[key] = value
                  }
               }
               chatArray[0] = messageDict
            }
            fuguDelay(when,
                      completion: {
                        weakSelf.sendMessageToFaye(messageInfoArray: chatArray, onSuccess: onSuccess)
            })
         })
      } else {
         
         fayeConnection.localFaye?.sendMessage(messageDict, toChannel: "/\(channelId)", success: { [weak self] in
            chatArray.remove(at: 0)
            self?.sendMessageToFaye(messageInfoArray: chatArray, onSuccess: onSuccess)
         }, failure: { [weak self] (error) in
            if error != nil {
               print("failure: { (error)==> \(error!.localizedDescription)")
            }
            fuguDelay(0.1, completion: {
               self?.sendMessageToFaye(messageInfoArray: chatArray, onSuccess: onSuccess)
            })
         })
      }
      
      
   }
   
   func scrollTableViewToBottom(animated: Bool = false) {
      //        DispatchQueue.main.async {
      //             self.chatScreenTableView.scrollRectToVisible(CGRect(x: 0, y: self.chatScreenTableView.contentSize.height - 100, width: self.chatScreenTableView.contentSize.width, height: self.chatScreenTableView.contentSize.height), animated: animated)
      ////            let calculationForY = self.chatScreenTableView.contentSize.height - self.chatScreenTableView.contentOffset.y
      ////            let offsetY = (calculationForY > 0) ? calculationForY : 0
      ////            self.chatScreenTableView.setContentOffset(CGPoint(x: 0, y: offsetY), animated: animated)
      //            // self.chatScreenTableView.reloadData()
      //        }
      
      DispatchQueue.main.async {
         if self.dateGroupedArray.count > 0 {
            let givenMessagesArray = self.dateGroupedArray[self.dateGroupedArray.count - 1]
            if givenMessagesArray.count > 0 {
               let indexPath = IndexPath(row: givenMessagesArray.count - 1, section: self.dateGroupedArray.count - 1)
               self.chatScreenTableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
         }
      }
   }
   
   func isMessageInvalid(messageText: String) -> Bool {
      if messageText.replacingOccurrences(of: " ", with: "").characters.count == 0 ||
         messageText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count == 0 {
         
         if connectedToNetwork() == false {
            return true
         }
         
         self.updateErrorLabelView(isHiding: false)
         self.errorLabel.text = "Please enter some text."
         self.updateErrorLabelView(isHiding: true)
         return true
      }
      return false
   }
   
   func updateMessageInfoInCachedData(withMessageIndex messageIndex: Int, messageDict: [String: Any]) {
      if let cachedChannelData = cachedUnsentMessagesData["\(self.channelId)"] as? [[String: Any]] {
         let messagesArray = cachedChannelData.filter({ (cachedInfo) -> Bool in
            if let cachedIndex = cachedInfo["message_index"] as? Int,
               messageIndex == cachedIndex { return false }
            return true
         }) //this filter is being used to remove <Dictionary> similar to key<messageIndex> value and update the cached data
         
         if messagesArray.count == 0 {
            cachedUnsentMessagesData.removeValue(forKey: "\(channelId)")
         } else {
            if cachedChannelData.count != messagesArray.count {
               cachedUnsentMessagesData["\(channelId)"] = messagesArray
            }
         }
      }
      
      updateReceivedMessageInfo(withMessageIndex: messageIndex, messageDict: messageDict)
   }
   
   func updateReceivedMessageInfo(withMessageIndex messageIndex: Int, messageDict: [String: Any]) {
      var isAnyUpdate = false
      var tempGroupArray = [[[String: Any]]]()
      _ = self.dateGroupedArray.filter({
         var tempMessageDictArray = [[String: Any]]()
         _ = $0.filter({
            var editedObject = $0
            if let cachedIndex = editedObject["message_index"] as? Int,
               cachedIndex == messageIndex {
               isAnyUpdate = true
               
               if let imageFile = editedObject["image_file"] as? String,
                  imageFile.isEmpty == false,
                  let fuguImagePath = FuguDefaults.fuguImagesDirectory()?.path {
                  let imagePath = fuguImagePath + "/" + imageFile
                  fuguDelay(2, completion: {
                     FuguDefaults.deleteFile(atFilePath: imagePath)
                  })
                  editedObject.removeValue(forKey: "image_file")
               }
               
               editedObject = messageDict
               //                    if let messageStatus = editedObject["message_status"] as? Int,
               //                        messageStatus == ReadUnReadStatus.none.rawValue {
               editedObject["message_status"] = ReadUnReadStatus.sent.rawValue
               //}
            }
            
            tempMessageDictArray.append(editedObject)
            return false
         })
         tempGroupArray.append(tempMessageDictArray)
         return false
      })
      if isAnyUpdate == true {
         self.dateGroupedArray = tempGroupArray
         self.chatScreenTableView.reloadData()
      }
   }
   
   func isThereAnyNeedToUpdateMessageData(messageDict: [String: Any]) -> Bool {
      if let message = messageDict["message"] as? String,
         message.characters.count > 0 { return true }
      else if let thumbnailUrl = messageDict["thumbnail_url"] as? String,
         thumbnailUrl.characters.count > 0 { return true }
      else if let image_url = messageDict["image_url"] as? String,
         image_url.characters.count > 0 { return true }
      return false
   }
   
   func userId(ofMessageDict messageDict: [String: Any]) -> Int {
      if let user_id = messageDict["user_id"] as? Int { return user_id }
      else if let user_id = messageDict["user_id"] as? String,
         let intUserId = Int(user_id) { return intUserId }
      return -1
   }
   
   func updateAllMessageStatus(toStatus messageStatus: Int) {
      var tempMessages = [[String: Any]]()
      _ = messagesArray.filter({
         var editedObject = $0
         editedObject["message_status"] = messageStatus
         tempMessages.append(editedObject)
         return false
      })
      messagesArray = tempMessages
      
      var tempGroupArray = [[[String: Any]]]()
      _ = dateGroupedArray.filter({
         var tempMessageDictArray = [[String: Any]]()
         _ = $0.filter({
            var editedObject = $0
            editedObject["message_status"] = messageStatus
            tempMessageDictArray.append(editedObject)
            return false
         })
         tempGroupArray.append(tempMessageDictArray)
         return false
      })
      dateGroupedArray = tempGroupArray
      chatScreenTableView.reloadData()
   }
   
   func startSendingCachedMessagesWhenInternetAvailable() {
      guard channelId > 0 else { return }
      if let cachedChannelData = cachedUnsentMessagesData["\(channelId)"] as? [[String: Any]],
         cachedChannelData.count > 0 {
         sendCachedMessages(cachedMessagesArray: cachedChannelData)
      }
   }
   
   func getIncomingAttributedString(chatMessageObject: [String: Any]) -> NSMutableAttributedString {
      var messageString = ""
      if let message_string = chatMessageObject["message"] as? String, message_string.characters.count > 0 {
         messageString = message_string
      }
      
      var userNameString = "--"
      if let full_name = chatMessageObject["full_name"] as? String,
         full_name.characters.count > 0 {
         userNameString = full_name
      } else if let businessName = userDetailData["business_name"] as? String {
         userNameString = businessName
      } else if self.channelName.characters.count > 0 {
         userNameString = self.channelName
      }
      
      return attributedStringForLabel(userNameString, secondString: "\n" + messageString, thirdString: "", colorOfFirstString: FuguConfig.shared.theme.senderNameColor, colorOfSecondString: FuguConfig.shared.theme.incomingMsgColor, colorOfThirdString: UIColor.black.withAlphaComponent(0.5), fontOfFirstString: FuguConfig.shared.theme.senderNameFont, fontOfSecondString:  FuguConfig.shared.theme.incomingMsgFont, fontOfThirdString: UIFont.systemFont(ofSize: 11.0), textAlighnment: .left, dateAlignment: .right)
   }
   
   func expectedHeight(OfMessageObject chatMessageObject: [String: Any]) -> CGFloat {
      let availableWidthSpace = FUGU_SCREEN_WIDTH - CGFloat(60 + 10) - CGFloat(10 + 5)
      let availableBoxSize = CGSize(width: availableWidthSpace,
                                    height: CGFloat.greatestFiniteMagnitude)
      var isOutgoingMsg = false
      if let user_id = chatMessageObject["user_id"] as? Int,
         getSavedUserId == user_id { isOutgoingMsg = true }
      var cellTotalHeight: CGFloat = 5 + 5 + 2.5 + 13 + 5 + 5
      if isOutgoingMsg == true {
         var attributes: [String : Any]?
         if let applicableFont = FuguConfig.shared.theme.inOutChatTextFont {
            attributes = [NSFontAttributeName: applicableFont]
         }
         if let messageString = chatMessageObject["message"] as? String, messageString.isEmpty == false {
            cellTotalHeight += messageString.boundingRect(with: availableBoxSize,
                                                          options: .usesLineFragmentOrigin,
                                                          attributes: attributes,
                                                          context: nil).size.height// + 5
            //print("row height \(cellTotalHeight) of \(messageString)")
         }
         
      } else {
         let incomingAttributedString = getIncomingAttributedString(chatMessageObject: chatMessageObject)
         cellTotalHeight += incomingAttributedString.boundingRect(with: availableBoxSize,
                                                                  options: .usesLineFragmentOrigin,
                                                                  context: nil).size.height// + 5
         //print("row height \(cellTotalHeight) of \(incomingAttributedString.string)")
         
      }
      
      return cellTotalHeight
   }
}

// MARK: UIScrollViewDelegate
extension ConversationsViewController: UIScrollViewDelegate {
   public func scrollViewDidScroll(_ scrollView: UIScrollView) {
      if self.chatScreenTableView.contentOffset.y < -5.0 &&
         self.willPaginationWork {
         self.getMessagesBasedOnChannel(channelId,
                                        pageStart: messageArrayCount + 1)
      }
   }
}

// MARK: UITableView Delegates
extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource {
   public func numberOfSections(in tableView: UITableView) -> Int {
      if !isTypingLabel.isHidden { return self.dateGroupedArray.count + 1 }
      return self.dateGroupedArray.count
   }
   
   public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if section < self.dateGroupedArray.count {
         return dateGroupedArray[section].count
      } else { if !isTypingLabel.isHidden { return 1 } }
      return 0
   }
   
   public func tableView(_ tableView: UITableView,
                         cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      case let typingSection where typingSection == self.dateGroupedArray.count && !isTypingLabel.isHidden:
         
         let cell = tableView.dequeueReusableCell(withIdentifier: "TypingViewTableViewCell", for: indexPath) as! TypingViewTableViewCell
         
         cell.backgroundColor = .clear
         cell.selectionStyle = .none
         cell.bgView.isHidden = false
         cell.gifImageView.image = nil
         cell.bgView.backgroundColor = .clear
         cell.gifImageView.layer.cornerRadius = 15.0
         
         let imageBundle = bundle ?? Bundle.main
         if let getImagePath = imageBundle.path(forResource: "typingImage", ofType: ".gif") {
            cell.gifImageView.image = UIImage.animatedImageWithData(try! Data(contentsOf: URL(fileURLWithPath: getImagePath)))!
         }
         
         return cell
      case let chatSection where chatSection < self.dateGroupedArray.count:
         var messagesArray = dateGroupedArray[chatSection]
         
         if messagesArray.count > indexPath.row {
            let chatMessageObject = messagesArray[indexPath.row]
            let messageType = (chatMessageObject["message_type"] as? Int) ?? MessageType.normal.rawValue
            var isOutgoingMsg = false
            
            if let user_id = chatMessageObject["user_id"] as? Int,
               getSavedUserId == user_id {
               isOutgoingMsg = true
            }
            switch messageType {
            case MessageType.imageFile.rawValue:
               if isOutgoingMsg == true {
                  guard
                     let cell = tableView.dequeueReusableCell(withIdentifier: "OutgoingImageCell", for: indexPath) as? OutgoingImageCell
                     else {
                        let cell = UITableViewCell()
                        cell.backgroundColor = .clear
                        return cell
                  }
                  //updateTopBottomSpace(cell: cell, indexPath: indexPath)
                  return cell.configureCellOfOutGoingImageCell(resetProperties: true, userSubscribedValue: userSubscribedValue, chatMessageObject: chatMessageObject)
               } else {
                  guard let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingImageCell", for: indexPath) as? IncomingImageCell
                     else {
                        let cell = UITableViewCell()
                        cell.backgroundColor = .clear
                        return cell
                  }
                  // updateTopBottomSpace(cell: cell, indexPath: indexPath)
                  return cell.configureIncomingCell(resetProperties: true, channelId: channelId, chatMessageObject: chatMessageObject)
               }
            default:
               if isOutgoingMsg == false {
                  guard let cell = tableView.dequeueReusableCell(withIdentifier: "SupportMessageTableViewCell", for: indexPath) as? SupportMessageTableViewCell
                     else {
                        let cell = UITableViewCell()
                        cell.backgroundColor = .clear
                        return cell
                  }
                  
                  updateTopBottomSpace(cell: cell, indexPath: indexPath)
                  
                  let incomingAttributedString = getIncomingAttributedString(chatMessageObject: chatMessageObject)
                  return cell.configureCellOfSupportIncomingCell(resetProperties: true,
                                                                 attributedString: incomingAttributedString,
                                                                 channelId: channelId,
                                                                 chatMessageObject: chatMessageObject)
               } else {
                  guard
                     let cell = tableView.dequeueReusableCell(withIdentifier: "SelfMessageTableViewCell", for: indexPath) as? SelfMessageTableViewCell
                     else {
                        let cell = UITableViewCell()
                        cell.backgroundColor = .clear
                        return cell
                  }
                  
                  updateTopBottomSpace(cell: cell, indexPath: indexPath)
                  
                  return cell.configureIncomingMessageCell(resetProperties: true, userSubscribedValue: userSubscribedValue, chatMessageObject: chatMessageObject)
               }
            }
         }
      default:
         let cell = UITableViewCell()
         cell.backgroundColor = .clear
         return cell
      }
      
      let cell = UITableViewCell()
      cell.backgroundColor = .clear
      return cell
   }
   
   public func tableView(_ tableView: UITableView,
                         didSelectRowAt indexPath: IndexPath) {
      switch indexPath.section {
      case let typingSection where typingSection == self.dateGroupedArray.count && !isTypingLabel.isHidden: break
      case let chatSection where chatSection < self.dateGroupedArray.count:
         var messagesArray = self.dateGroupedArray[chatSection]
         if messagesArray.count > indexPath.row {
            let chatMessageObject = messagesArray[indexPath.row] //{
            
            guard
               let messageType = chatMessageObject["message_type"] as? Int
               else { return }
            switch messageType {
            case MessageType.imageFile.rawValue:
               if messageTextView.isFirstResponder {
                  messageTextView.resignFirstResponder()
               }
               guard
                  let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowImageViewController") as? ShowImageViewController,
                  let thumbnailUrl = chatMessageObject["thumbnail_url"] as? String,
                  thumbnailUrl.characters.count > 0
                  else { return }
               destinationVC.imageURLString = thumbnailUrl
               self.modalPresentationStyle = .overCurrentContext
               self.present(destinationVC, animated: true, completion: nil)
            default: break
            }
         }
      default: break
      }
   }
   
   public func tableView(_ tableView: UITableView,
                         heightForRowAt indexPath: IndexPath) -> CGFloat {
      switch indexPath.section {
      case let typingSection where typingSection == self.dateGroupedArray.count && !isTypingLabel.isHidden: return 34
      case let chatSection where chatSection < self.dateGroupedArray.count:
         var messagesArray = self.dateGroupedArray[chatSection]
         if messagesArray.count > indexPath.row {
            let chatMessageObject = messagesArray[indexPath.row]
            let messageType = (chatMessageObject["message_type"] as? Int) ?? MessageType.normal.rawValue
            switch messageType {
            case MessageType.imageFile.rawValue: return 288
            case MessageType.normal.rawValue:
               let rowHeight = expectedHeight(OfMessageObject: chatMessageObject)
               
               return rowHeight
            default: break
            }
         }
      default: break
      }
      return UITableViewAutomaticDimension
   }
   
   //    public func tableView(_ tableView: UITableView,
   //                          estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
   //        return 240
   //    }
   
   public func tableView(_ tableView: UITableView,
                         heightForHeaderInSection section: Int) -> CGFloat {
      if section < self.dateGroupedArray.count {
         if section == 0 &&
            channelId < 0 {
            return 0
         }
         return 28
      }
      return 0
   }
   
   public func tableView(_ tableView: UITableView,
                         viewForHeaderInSection section: Int) -> UIView? {
      let labelBgView = UIView()
      
      labelBgView.frame = CGRect(x: 0.0,
                                 y: 0.0,
                                 width: UIScreen.main.bounds.size.width,
                                 height: 28)
      labelBgView.backgroundColor = .clear
      
      let dateLabel = UILabel()
      dateLabel.layer.masksToBounds = true
      
      dateLabel.text = ""
      dateLabel.layer.cornerRadius = 10
      dateLabel.textColor = #colorLiteral(red: 0.3490196078, green: 0.3490196078, blue: 0.4078431373, alpha: 1)
      dateLabel.textAlignment = .center
      dateLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
      dateLabel.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
      dateLabel.layer.borderColor = #colorLiteral(red: 0.862745098, green: 0.8784313725, blue: 0.9019607843, alpha: 1).cgColor
      dateLabel.layer.borderWidth = 0.5
      if section < self.dateGroupedArray.count {
         let localMessagesArray = self.dateGroupedArray[section]
         if  localMessagesArray.count > 0,
            let dateTime = localMessagesArray[0]["date_time"] as? String,
            dateTime.characters.count > 0 {
            dateLabel.text = changeDateToParticularFormat((dateTime.toDate)!,
                                                          dateFormat: "MMM d, yyyy",
                                                          showInFormat: false).capitalized
         }
      }
      let widthIs: CGFloat = CGFloat(dateLabel.text!.boundingRect(with: dateLabel.frame.size,
                                                                  options: .usesLineFragmentOrigin,
                                                                  attributes: [NSFontAttributeName: dateLabel.font],
                                                                  context: nil).size.width) + 10
      let dateLabelHeight = CGFloat(24)
      dateLabel.frame = CGRect(x: (UIScreen.main.bounds.size.width / 2) - (widthIs/2), y: (labelBgView.frame.height - dateLabelHeight)/2, width: widthIs + 10, height: dateLabelHeight)
      labelBgView.addSubview(dateLabel)
      
      return labelBgView
   }
}

// MARK: FayeClientDelegate
extension ConversationsViewController: MZFayeClientDelegate {
   func updateMessagesArrayLocally(_ messageDict: [String: Any],
                                   needToScrollDown: Bool) {
      if delegate != nil {
         delegate?.newChatStartedDelgegate(isChatUpdated: true)
      }
      //if needToScrollDown == true  {
      //DispatchQueue.main.async {
      let previousTotalChatCount = self.dateGroupedArray.count
      var previousLastSectionRows = 0
      if previousTotalChatCount > 0 {
         self.chatScreenTableView.beginUpdates()
         previousLastSectionRows = self.dateGroupedArray[self.dateGroupedArray.count - 1].count
      }
      
      self.checkIfDateExistsElseUpdate([messageDict])
      self.messagesArray.append(messageDict)
      self.messageArrayCount += 1
      
      if self.dateGroupedArray.count == 0 {
         return
         
      }
      if previousTotalChatCount == self.dateGroupedArray.count {
         //                if self.isTypingLabel.isHidden == false {
         //                    self.isTypingLabel.isHidden = true
         //                    self.chatScreenTableView.deleteSections(IndexSet([previousTotalSections]), with: .fade)
         //                    self.needToIgnoreTypingMessage = true
         //                    fuguDelay(4,
         //                              completion: {
         //                        self.needToIgnoreTypingMessage = false
         //                    })
         //                }
         let currentLastSectionRows = self.dateGroupedArray[self.dateGroupedArray.count - 1].count
         if previousLastSectionRows != currentLastSectionRows {
            let lastIndexPath = IndexPath(row: currentLastSectionRows - 1, section: self.dateGroupedArray.count - 1)
            self.chatScreenTableView.insertRows(at: [lastIndexPath], with: .none)
         }
         
         if self.isTypingLabel.isHidden == true,
            self.dateGroupedArray.count < chatScreenTableView.numberOfSections {
            self.chatScreenTableView.deleteSections(IndexSet([chatScreenTableView.numberOfSections - 1]), with: .none)
         }
      } else {
         var newSectionsOfTableView = IndexSet([self.dateGroupedArray.count - 1])
         if self.isTypingLabel.isHidden == false, previousTotalChatCount < chatScreenTableView.numberOfSections {
            self.chatScreenTableView.deleteSections(IndexSet([chatScreenTableView.numberOfSections - 1]), with: .none)
            newSectionsOfTableView = IndexSet([self.dateGroupedArray.count - 1, self.dateGroupedArray.count])
         }
         self.chatScreenTableView.insertSections(newSectionsOfTableView, with: .none)
      }
      
      self.chatScreenTableView.endUpdates()
      if needToScrollDown == true {
         self.newScrollToBottom(animated: true)
      }
      // }
      //        } else {
      //            self.checkIfDateExistsElseUpdate([messageDict])
      //            self.messagesArray.append(messageDict)
      //            self.messageArrayCount += 1
      //        }
   }
}

//MARK: - MZFayeClient 
extension ConversationsViewController {
   func setUpChatBoxFayeClient(onSuccess: ((_ subscribed: Bool) -> Void)? = nil) {
      if fayeConnection.localFaye?.isConnected == true {
         subscribeFayeClient(onSuccess: onSuccess)
      } else {
         fayeConnection.setupFayeClient( {
            self.subscribeFayeClient(onSuccess: onSuccess)
         }, failure: { (error) in
         })
      }
   }
   
   func subscribeFayeClient(onSuccess: ((_ connected: Bool) -> Void)? = nil) {
      if connectedToNetwork() == true {
         if fayeConnection.localFaye?.isConnected == true {
            guard channelId > 0  else {
               return
            }
            guard fayeConnection.isChannelSubscribed(channelID: "\(channelId)") == false  else {
               if onSuccess != nil {
                  onSuccess!(true)
               }
               return
            }
            fayeConnection.localFaye?.subscribe(toChannel: "/\(channelId)",
               success: { [weak self] in
                  guard let weakSelf = self else {
                     return
                  }
                  
                  var messageInfo = weakSelf.messageInfo(isTyping: weakSelf.typingMessageValue)
                  messageInfo["on_subscribe"] = UserSubscriptionStatus.subscribed.rawValue
                  messageInfo["channel_id"] = weakSelf.channelId
                  
                  fayeConnection.localFaye?.sendMessage(messageInfo,
                                                        toChannel: "/\(weakSelf.channelId)",
                     success: {},
                     failure: { (error) in
                        if error != nil {
                           print("failure: { (error)==> \(error!.localizedDescription)")
                        }})
                  if onSuccess != nil { onSuccess!(true) }
            },failure: { [weak self] (error) in
               if error != nil { print("errorakdsnm==>\(error!.localizedDescription)") }
               fuguDelay(1.5,
                         completion: {
                           self?.subscribeFayeClient(onSuccess: onSuccess)
               })},
              receivedMessage: { [weak self] (messageInfo) in
               
               guard let weakSelf = self else {
                  return
               }
               
               if let messageDict = messageInfo as? [String: Any] {
                  print("messageDictmessageDictmessageDict=>> \(messageDict)")
                  //self.isTypingLabel.isHidden = true
                  let senderUserId = weakSelf.userId(ofMessageDict: messageDict)
                  
                  if let givenNotificationType = messageDict["notification_type"] as? Int,
                     self?.getSavedUserId != senderUserId {
                     switch givenNotificationType {
                     case NotificationType.read_all.rawValue:
                        self?.updateAllMessageStatus(toStatus: ReadUnReadStatus.read.rawValue)
                     default: break
                     }
                  }
                  
                  if let messageType = messageDict["message_type"] as? Int {
                     var needToChangeScroll = false
                     if messageType == MessageType.normal.rawValue ||
                        messageType == MessageType.imageFile.rawValue {
                        if weakSelf.getSavedUserId == senderUserId {
                           if let messageIndex = messageDict["message_index"] as? Int {
                              self?.updateMessageInfoInCachedData(withMessageIndex: messageIndex,
                                                                 messageDict: messageDict)
                           }
                        } else {
                           needToChangeScroll = weakSelf.isThereAnyNeedToUpdateMessageData(messageDict: messageDict)
                           
                           if let onSubscribed = messageDict["on_subscribe"] as? Int {
                              self?.userSubscribedValue = onSubscribed
                           } else {
                              self?.userSubscribedValue = UserSubscriptionStatus.subscribed.rawValue
                           }
                           
                           if let isTyping = messageDict["is_typing"] as? Int,
                              self?.needToIgnoreTypingMessage == false {
                              print("messageDict==>[is_typing]=>\(isTyping)")
                              if isTyping == TypingMessage.startTyping.rawValue {
                                 self?.isTypingLabel.isHidden = false
                              } else if isTyping == TypingMessage.stopTyping.rawValue {
                                 self?.isTypingLabel.isHidden = true
                                 self?.chatScreenTableView.reloadData()
                              }
                           }
                        }
                        
                        if needToChangeScroll == true {
                           print("new Info REceived to update message==>\(messageDict)")
                           //self.isTypingLabel.isHidden = true
                           weakSelf.updateMessagesArrayLocally(messageDict, needToScrollDown: true)
                           weakSelf.sendNotificaionAfterReceivingMsg(senderUserId: senderUserId)
                           self?.isTypingLabel.isHidden = true
                           self?.chatScreenTableView.beginUpdates()
                           if weakSelf.isTypingLabel.isHidden == true,                        weakSelf.dateGroupedArray.count < weakSelf.chatScreenTableView.numberOfSections {                        weakSelf.chatScreenTableView.deleteSections(IndexSet([weakSelf.chatScreenTableView.numberOfSections - 1]), with: .none)
                           }
                           weakSelf.chatScreenTableView.endUpdates()
                        }
                        
                        if weakSelf.isTypingLabel.isHidden == false {
                           if weakSelf.chatScreenTableView.numberOfSections != (weakSelf.dateGroupedArray.count + 1) {
                              weakSelf.chatScreenTableView.reloadData()
                              
                              let indexPathOfGifCell = IndexPath(row: 0, section: weakSelf.dateGroupedArray.count)
                              weakSelf.chatScreenTableView.scrollToRow(at: indexPathOfGifCell, at: .none, animated: false)
                           }
                           
                        } else {
                           if needToChangeScroll == true {
                              weakSelf.chatScreenTableView.reloadData()
                              weakSelf.newScrollToBottom(animated: false)
                           }
                        }
                     }
                  }
               }
            })
         } else {
            fayeConnection.setupFayeClient({ if onSuccess != nil { onSuccess!(false) } },
                                           failure: { (error) in })
         }
      } else {
         isSendingCachedMessages = false
      }
   }
   
   
   func sendCachedMessages(cachedMessagesArray: [[String: Any]]) {
      if cachedMessagesArray.count > 0 {
         //if isSendingCachedMessages { return }
         
         isSendingCachedMessages = true
         
         var chatMessagesArray = cachedMessagesArray
         subscribeFayeClient { (connected) in
            if connected {
               self.sendMessageToFaye(messageInfoArray: chatMessagesArray,
                                      onSuccess: { [weak self] (unSentMessagesArray) in
                                       if unSentMessagesArray.count == 0 {
                                          chatMessagesArray.removeAll()
                                          self?.sendCachedMessages(cachedMessagesArray: unSentMessagesArray)
                                       }
               })
            } else {
               fuguDelay(0.1, completion: { [weak self] in
                  self?.sendCachedMessages(cachedMessagesArray: chatMessagesArray)
               })
            }
         }
      } else {
         isSendingCachedMessages = false
         cachedUnsentMessagesData.removeValue(forKey: "\(channelId)")
         getMessagesBasedOnChannel(channelId,
                                   pageStart: 1)
      }
   }
   
   func sendNotificaionAfterReceivingMsg(senderUserId: Int) {
      if senderUserId != getSavedUserId {
         let params = ["user_type": userType,
                       "notification_type": NotificationType.read_all.rawValue,
                       "channel_id": channelId,
                       "user_id": getSavedUserId]
         
         fayeConnection.localFaye?.sendMessage(params,
                                               toChannel: "/\(channelId)",
            success: {},
            failure: { (error) in
               if error != nil {
                  print("failure: { (error)==> \(error!.localizedDescription)")
               }})
      }
   }
}

// MARK: UITextViewDelegates
extension ConversationsViewController: UITextViewDelegate {
   public func textViewDidChangeSelection(_ textView: UITextView) {
      placeHolderLabel.isHidden = textView.hasText
   }
   
   public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
      self.addRemoveShadowInTextView(toAdd: true)
      
      placeHolderLabel.textColor = #colorLiteral(red: 0.2862745098, green: 0.2862745098, blue: 0.2862745098, alpha: 0.8)
      textInTextField = textView.text
      textViewBgView.backgroundColor = .white
      timer = Timer.scheduledTimer(timeInterval: 3.0,
                                   target: self,
                                   selector: #selector(self.watcherOnTextView),
                                   userInfo: nil,
                                   repeats: true)
      
      return true
   }
   
   public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
      textViewBgView.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9098039216, blue: 0.9137254902, alpha: 1)
      placeHolderLabel.textColor = #colorLiteral(red: 0.2862745098, green: 0.2862745098, blue: 0.2862745098, alpha: 0.5)
      
      timer.invalidate()
      return true
   }
   
   public func textViewDidBeginEditing(_ textView: UITextView) {
      typingMessageValue = TypingMessage.startTyping.rawValue
   }
   
   public func textViewDidChange(_ textView: UITextView) {
      if  messageTextView.contentSize.height >= CGFloat(textViewFixedHeight) &&
         messageTextView.contentSize.height <= 130 {
         textViewBackGroundViewHeightConstraint.constant = messageTextView.contentSize.height
      } else if messageTextView.contentSize.height > 130 {
         self.textViewBackGroundViewHeightConstraint.constant = 130
      } else {
         self.textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
      }
   }
   
   public func textViewDidEndEditing(_ textView: UITextView) {
      if messageTextView.text.characters.count == 0 {
         textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
         //messageTextView.text = textViewPlaceHolder
         //messageTextView.textColor = UIColor(red: 73.0/255.0, green: 73.0/255.0, blue: 73.0/255.0, alpha: 1.0)
      } else {
         //messageTextView.textColor = UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0)
      }
      
      if  messageTextView.contentSize.height >= CGFloat(textViewFixedHeight) &&
         messageTextView.contentSize.height <= 180 {
         textViewBackGroundViewHeightConstraint.constant = messageTextView.contentSize.height - 10
      }
   }
   
   public func textView(_ textView: UITextView,
                        shouldChangeTextIn range: NSRange,
                        replacementText text: String) -> Bool {
      
      let newText = ((textView.text as NSString?)?.replacingCharacters(in: range,
                                                                       with: text))!
      if newText.trimmingCharacters(in: .whitespacesAndNewlines).characters.count == 0 {
         self.sendMessageButton.isEnabled = false
         
         if text == "\n" { textView.resignFirstResponder() }
         if channelId > 0 {
            self.typingMessageValue = TypingMessage.stopTyping.rawValue
            sendTypingStatusMessage(isTyping: typingMessageValue)
            self.typingMessageValue = TypingMessage.startTyping.rawValue
         }
         if text == " " { return false }
      } else {
         self.sendMessageButton.isEnabled = true
         if typingMessageValue == TypingMessage.startTyping.rawValue, channelId > 0 {
            sendTypingStatusMessage(isTyping: typingMessageValue)
            self.typingMessageValue = TypingMessage.stopTyping.rawValue
         }
      }
      return true
   }
}

// MARK: UIImagePicker Delegates
extension ConversationsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   public func imagePickerController(_ picker: UIImagePickerController,
                                     didFinishPickingMediaWithInfo info: [String : Any]) {
      guard let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
         else {
            picker.dismiss(animated: true, completion: nil)
            return
      }
      
      if picker.sourceType == .camera {
         sendConfirmedImage(image: pickedImage)
         picker.dismiss(animated: true, completion: nil)
      } else if let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectImageViewController") as? SelectImageViewController {
         destinationVC.pickedImage = pickedImage
         destinationVC.delegate = self
         picker.modalPresentationStyle = .overCurrentContext
         self.imagePicker.present(destinationVC, animated: true, completion: nil)
      }
   }
   
   func sendConfirmedImage(image confirmedImage: UIImage) {
      let imageFilePath = localFilePath
      if UIImageJPEGRepresentation(confirmedImage, 1.0)!.count > 3*1024 {
         try? UIImageJPEGRepresentation(confirmedImage, 0.1)?.write(to: Foundation.URL(fileURLWithPath: imageFilePath), options: [.atomic])
      } else if UIImageJPEGRepresentation(confirmedImage, 1.0)!.count > 2*1024 {
         try? UIImageJPEGRepresentation(confirmedImage, 0.5)?.write(to: Foundation.URL(fileURLWithPath: imageFilePath), options: [.atomic])
      } else {
         try? UIImageJPEGRepresentation(confirmedImage, 0.75)?.write(to: Foundation.URL(fileURLWithPath: imageFilePath), options: [.atomic])
      }
      print("File Path:+++++++++", imageFilePath)
      
      if imageFilePath.isEmpty == false {
         sendImageDataToServer(filePath: imageFilePath)
      }
   }
   
   public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
      picker.dismiss(animated: true, completion: nil)
   }
   
   func performActionBasedOnGalleryPermission() {
      let status = PHPhotoLibrary.authorizationStatus()
      if status == .authorized {
         DispatchQueue.main.async {
            self.present(self.imagePicker, animated: true, completion: nil)
         }
      } else if status == .denied {
         if connectedToNetwork() == false { return }
         
         self.updateErrorLabelView(isHiding: false)
         self.errorLabel.text = "Access to photo library is denied. Please enable from setings."
         self.updateErrorLabelView(isHiding: true)
      } else if status == .notDetermined {
         PHPhotoLibrary.requestAuthorization({ [weak self] (newStatus) in
            guard let weakSelf = self else {
               return
            }
            if (newStatus == .authorized) {
               DispatchQueue.main.async {
                  weakSelf.present(weakSelf.imagePicker, animated: true, completion: nil)
               }
            } else {
               weakSelf.performActionBasedOnGalleryPermission()
            }
         })
      }
   }
   
   func performActionBasedOnCameraPermission() {
      let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
      if status == .authorized {
         DispatchQueue.main.async {
            self.present(self.imagePicker, animated: true, completion: nil)
         }
      } else if status == .denied {
         if connectedToNetwork() == false { return }
         
         self.updateErrorLabelView(isHiding: false)
         self.errorLabel.text = "Access to Camera is denied. Please enable from setings."
         self.updateErrorLabelView(isHiding: true)
      } else if status == .notDetermined {
         AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [weak self] (newStatus) in
            
            guard let weakSelf = self else {
               return
            }
            if newStatus {
               DispatchQueue.main.async {
                  self?.present(weakSelf.imagePicker, animated: true, completion: nil)
               }
            } else {
               self?.performActionBasedOnCameraPermission()
            }
         })
      }
   }
   
   func sendImageDataToServer(filePath: String, saveLocal: Bool = true) {
      if FuguConfig.shared.appSecretKey.isEmpty { return }
      
      let params = ["app_secret_key": FuguConfig.shared.appSecretKey,
                    "file_type": "image"]
      
      let imageList = ["file": filePath]
      
      var fayeMessageDict = self.messageInfo(messageType: MessageType.imageFile.rawValue)
      fayeMessageDict["message_status"] = ReadUnReadStatus.none.rawValue
      fayeMessageDict["message_index"] = messageArrayCount
      
      if channelId > 0,
         saveLocal == true {
         fayeMessageDict["image_file"] = filePath.components(separatedBy: "/").last ?? ""
         fayeMessageDict["image_param"] = params
         
         if let cachedChannelData = cachedUnsentMessagesData["\(channelId)"] as? [[String: Any]] {
            var messagesArray = cachedChannelData
            messagesArray.append(fayeMessageDict)
            
            cachedUnsentMessagesData["\(channelId)"] = messagesArray
         } else { cachedUnsentMessagesData["\(channelId)"] = [fayeMessageDict] }
         
         messageTextView.text = ""
         textViewDidChange(messageTextView)
         textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
         
         updateMessagesArrayLocally(fayeMessageDict, needToScrollDown: true)
      }
      
      if connectedToNetwork() == false {
         self.errorLabel.text = "No Internet Connection"
         self.updateErrorLabelView(isHiding: false)
         return
      }
      
      activityIndicator.stopAnimating()
      activityIndicator.frame = sendMessageButton.bounds
      activityIndicator.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9098039216, blue: 0.9137254902, alpha: 1)
      
      sendMessageButton.addSubview(activityIndicator)
      activityIndicator.bringSubview(toFront: loaderbutton)
      activityIndicator.startAnimating()
      
      HTTPClient.makeMultiPartRequestWith(method: .POST,
                                          showAlert: false,
                                          showAlertInDefaultCase: false,
                                          showActivityIndicator: false,
                                          para: params,
                                          baseUrl: FuguConfig.shared.baseUrl,
                                          extendedUrl: FuguEndPoints.API_UPLOAD_FILE.rawValue,
                                          imageList: imageList) { (responseObject, error, tag, statusCode) in
                                             switch (statusCode ?? -1) {
                                             case StatusCodeValue.Authorized_Min.rawValue..<StatusCodeValue.Authorized_Max.rawValue:
                                                let response = responseObject as? [String: Any]
                                                
                                                if let data = response?["data"] as? [String: Any],
                                                   let imageUrl = data["image_url"] as? String,
                                                   let thumbnailUrl = data["thumbnail_url"] as? String {
                                                   print("API_UPLOAD_FILE.ra==> \(data)")
                                                   
                                                   if let cachedImage = UIImage(contentsOfFile: filePath),
                                                      let url = NSURL(string: thumbnailUrl) {
                                                      imageCacheFugu.setObject(cachedImage, forKey: url)
                                                   }
                                                   
                                                   if self.channelId < 0 {
                                                      self.startNewConversation(imageData: data)
                                                      return
                                                   } else { fayeMessageDict.removeValue(forKey: "image_file") }
                                                   fayeMessageDict["image_url"] = imageUrl
                                                   fayeMessageDict["thumbnail_url"] = thumbnailUrl
                                                   fuguDelay(2, completion: { [weak self] in
                                                      self?.sendMessageToFaye(messageInfoArray: [fayeMessageDict])
                                                   })
                                                }
                                                self.activityIndicator.stopAnimating()
                                             default:
                                                if connectedToNetwork() == false { return }
                                                
                                                self.activityIndicator.stopAnimating()
                                                self.updateErrorLabelView(isHiding: false)
                                                self.errorLabel.text = "Some error occured. Please try again"
                                                self.updateErrorLabelView(isHiding: true)
                                             }
      }
   }
}

// MARK: SelectImageViewControllerDelegate Delegates
extension ConversationsViewController: SelectImageViewControllerDelegate {
   func selectImageVC(_ selectedImageVC: SelectImageViewController, selectedImage: UIImage) {
      selectedImageVC.dismiss(animated: false) {
         self.imagePicker.dismiss(animated: false) {
            self.sendConfirmedImage(image: selectedImage)
         }
      }
   }
   
   func goToConversationViewController() {}
}

private extension String {
   func changeDateFormat(sourceFormat: String? = "yyyy-MM-dd HH:mm:ss",
                         toFormat: String) -> String {
      var dateString = self.replacingOccurrences(of: ".000Z", with: "")
      if dateString.contains(".") == true {
         dateString = (dateString.components(separatedBy: "."))[0]
      }
      dateString = dateString.replacingOccurrences(of: "T",
                                                   with: " ")
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = sourceFormat
      dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
      let date = dateFormatter.date(from: dateString)
      
      dateFormatter.dateFormat = toFormat
      dateFormatter.amSymbol = "am"
      dateFormatter.pmSymbol = "pm"
      dateFormatter.timeZone = NSTimeZone.local
      if date != nil { return dateFormatter.string(from: date!) }
      
      return ""
   }
}
