//
//  FuguUserDetail.swift
//  Fugu
//
//  Created by Gagandeep Arora on 31/08/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit

@objc public class FuguUserDetail: NSObject {
    var fullName: String?
    var email: String?
    var phoneNumber: String?
    var userUniqueKey: String?// = "f593c696ca8103"//"f593c79fc08201"
    var addressAttribute: FuguAttributes?
    var customAttributes: [String: Any]?
    
    override init() {}
    
    public init(fullName: String,
                email: String,
                phoneNumber: String,
                userUniqueKey: String,
                addressAttribute: FuguAttributes? = nil,
                customAttributes: [String: Any]? = nil) {
        super.init()
        
        self.fullName = fullName
        self.email = email
        self.phoneNumber = phoneNumber
        self.userUniqueKey = userUniqueKey
        self.addressAttribute = addressAttribute ?? FuguAttributes()
        self.customAttributes = customAttributes
    }
    
    func toJson() -> [String: Any] {
        var params: [String: Any] = ["device_id": UIDevice.current.identifierForVendor?.uuidString ?? 0,
                                     "device_type" : Device_Type_iOS]
        
        switch FuguConfig.shared.credentialType {
        case FuguCredentialType.reseller:
            if FuguConfig.shared.resellerToken.isEmpty == false &&
                FuguConfig.shared.referenceId > 0 {
                params["reseller_token"] = FuguConfig.shared.resellerToken
                params["reference_id"] = FuguConfig.shared.referenceId
            }
            if let applicationType = FuguConfig.shared.appType,
                applicationType.isEmpty == false {
                params["app_type"] = applicationType
            }
        case FuguCredentialType.defaultType:
            if FuguConfig.shared.appSecretKey.isEmpty == false {
                params["app_secret_key"] = FuguConfig.shared.appSecretKey
            }
        }

        let userName = self.fullName ?? ""
        if userName.isEmpty == false { params["full_name"] = userName }
        
        let userEmail = self.email ?? ""
        if userEmail.isEmpty == false { params["email"] = userEmail }
        
        let userPhoneNumber = self.phoneNumber ?? ""
        if userPhoneNumber.isEmpty == false { params["phone_number"] = userPhoneNumber }
        
        let userUniqueKey = self.userUniqueKey ?? ""
        if userUniqueKey.isEmpty == false { params["user_unique_key"] = userUniqueKey }
        
        if let addressInfo = addressAttribute?.toJSON() { params["attributes"] = addressInfo }
        
        if customAttributes != nil { params["custom_attributes"] = customAttributes! }
        
        if FuguConfig.shared.deviceToken.isEmpty == false { params["device_token"] = FuguConfig.shared.deviceToken }
        
        var deviceDetails = [String: Any]()
        deviceDetails["ios_operating_system"] = "IOS"
        deviceDetails["ios_model"] = UIDevice.current.model
        deviceDetails["ios_manufacturer"] = "APPLE"
        deviceDetails["ios_os_version"] = UIDevice.current.systemVersion
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: deviceDetails, options: .prettyPrinted)
            
            var service = (NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! ) as String
            service = service.replacingOccurrences(of: "\n", with: "")
            service = service.replacingOccurrences(of: "\\", with: "")
            
            params["device_details"] = service
        } catch let error as NSError {
            print(error.description)
        }
        return params
    }
}
